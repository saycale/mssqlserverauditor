﻿using System.Linq;
using Caliburn.Micro;

namespace MSSQLServerAuditor.Presentation.Dialogs
{
	public class DialogViewModel : Screen
	{
		private Dialog           _dialog;
		private BindableResponse _response;

		public DialogViewModel(Dialog dialog)
		{
			Responses = new BindableCollection<BindableResponse>();
			Dialog    = dialog;
		}

		public BindableResponse Response
		{
			get { return _response; }
			set
			{
				if (value != null)
				{
					this._response = value;
					Dialog.Answer  = this._response.Answer;
				}
			}
		}

		public IObservableCollection<BindableResponse> Responses { get; }

		public Dialog Dialog
		{
			get { return this._dialog; }
			set
			{
				this._dialog = value;

				CreateResponses();
			}
		}

		public void SetResponse(BindableResponse response)
		{
			Response = response;
			TryClose();
		}

		private void CreateResponses()
		{
			Responses.Clear();

			if (Dialog != null)
			{
				int count = Dialog.PossibleAnswers.Count;

				if (count > 0)
				{
					Responses.AddRange(Dialog.PossibleAnswers
						.Select(r => new BindableResponse {Answer = r})
					);

					Responses.First().IsDefault = true;
					if (count > 1)
					{
						Responses.Last().IsCancel = true;
					}
				}
			}
		}

		protected override void OnDeactivate(bool close)
		{
			base.OnDeactivate(close);
			if (close)
			{
				EnsureResponseOnClose();
			}
		}

		private void EnsureResponseOnClose()
		{
			if (Dialog.IsAnswerGiven)
			{
				return;
			}

			Response = Response
				?? Responses.FirstOrDefault(x => x.IsCancel)
				?? Responses.FirstOrDefault(x => x.IsDefault)
				?? Responses.First();
		}
	}
}