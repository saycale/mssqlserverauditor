﻿using MSSQLServerAuditor.Presentation.Localization;

namespace MSSQLServerAuditor.Presentation.Dialogs
{
	public enum Answer
	{
		[Localized("Yes", typeof (DialogResources))]
		Yes,

		[Localized("No", typeof (DialogResources))]
		No,

		[Localized("Ok", typeof (DialogResources))]
		Ok,

		[Localized("Cancel", typeof (DialogResources))]
		Cancel,

		[Localized("Abort", typeof (DialogResources))]
		Abort,

		[Localized("Retry", typeof (DialogResources))]
		Retry,

		[Localized("Ignore", typeof (DialogResources))]
		Ignore
	}
}