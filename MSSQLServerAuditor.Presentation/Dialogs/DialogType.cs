﻿using MSSQLServerAuditor.Presentation.Localization;

namespace MSSQLServerAuditor.Presentation.Dialogs
{
	public enum DialogType
	{
		None,

		[Localized("Question", typeof (DialogResources))]
		Question,

		[Localized("Warning", typeof (DialogResources))]
		Warning,

		[Localized("Information", typeof (DialogResources))]
		Information,

		[Localized("Error", typeof (DialogResources))]
		Error
	}
}