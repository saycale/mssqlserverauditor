﻿namespace MSSQLServerAuditor.Presentation.Dialogs
{
	public class WarningDialog : Dialog
	{
		public WarningDialog(string message)
			: base(DialogType.Warning, message, Answer.Ok)
		{
		}

		public WarningDialog(string message, params Answer[] possibleResponens)
			: base(DialogType.Warning, message, possibleResponens)
		{
		}

		public WarningDialog(string subject, string message, params Answer[] possibleResponens)
			: base(DialogType.Warning, subject, message, possibleResponens)
		{
		}
	}
}