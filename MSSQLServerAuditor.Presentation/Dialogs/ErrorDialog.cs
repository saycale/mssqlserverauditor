﻿namespace MSSQLServerAuditor.Presentation.Dialogs
{
	public class ErrorDialog : Dialog
	{
		public ErrorDialog(string message)
			: base(DialogType.Error, message, Answer.Ok)
		{
		}

		public ErrorDialog(string message, params Answer[] possibleResponens)
			: base(DialogType.Error, message, possibleResponens)
		{
		}

		public ErrorDialog(string subject, string message, params Answer[] possibleResponens)
			: base(DialogType.Error, subject, message, possibleResponens)
		{
		}
	}
}