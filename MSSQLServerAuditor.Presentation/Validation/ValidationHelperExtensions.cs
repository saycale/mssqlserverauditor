﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Presentation.Validation.Internals;

namespace MSSQLServerAuditor.Presentation.Validation
{
	/// <summary>
	/// Contains extensions methods for <see cref="ValidationHelper"/>.
	/// </summary>
	public static class ValidationHelperExtensions
	{
		/// <summary>
		/// Adds a rule that checks that the property represented by <paramref name="propertyExpression"/> is not
		/// null or empty string. 
		/// </summary>
		/// <param name="validator">An instance of <see cref="ValidationHelper"/> that is used for validation.</param>
		/// <param name="propertyExpression">Expression that specifies the property to validate. Example: Validate(() => MyProperty).</param>
		/// <param name="errorMessage">Error message in case if the property is null or empty.</param>
		/// <returns>An instance of <see cref="IValidationRule"/> that represents the newly created validation rule.</returns>
		public static IValidationRule AddRequiredRule(
			this ValidationHelper    validator,
			Expression<Func<object>> propertyExpression, 
			string                   errorMessage
		)
		{
			Check.NotNull(validator,           nameof(validator));
			Check.NotNull(propertyExpression,  nameof(propertyExpression));
			Check.NotNullOrEmpty(errorMessage, nameof(errorMessage));

			Func<object> propertyGetter = propertyExpression.Compile();

			return validator.AddRule(PropertyName.For(propertyExpression, false), () =>
			{
				object propertyValue = propertyGetter();

				string stringPropertyValue = propertyValue as string;

				if (propertyValue == null || (stringPropertyValue != null && string.IsNullOrEmpty(stringPropertyValue)))
				{
					return RuleResult.Invalid(errorMessage);
				}

				return RuleResult.Valid();
			});
		}

		/// <summary>
		/// Creates a validation rule that validates the specified child <see cref="IValidatable"/> object and adds errors
		/// to this object if invalid.
		/// </summary>
		/// <param name="validator">An instance of <see cref="ValidationHelper"/> that is used for validation.</param>
		/// <param name="childValidatableGetter">Expression for getting the <see cref="IValidatable"/> object to add as child.</param>
		/// <returns>An instance of <see cref="IValidationRule"/> that represents the newly created validation rule.</returns>
		public static IAsyncValidationRule AddChildValidatable(
			this ValidationHelper          validator,
			Expression<Func<IValidatable>> childValidatableGetter
		)
		{
			Check.NotNull(validator,              nameof(validator));
			Check.NotNull(childValidatableGetter, nameof(childValidatableGetter));

			Func<IValidatable> getter = childValidatableGetter.Compile();

			return validator.AddAsyncRule(PropertyName.For(childValidatableGetter), () =>
			{
				IValidatable validatable = getter();

				if (validatable != null)
				{
					return validatable.Validate().ContinueWith(r =>
					{
						ValidationResult result = r.Result;

						RuleResult ruleResult = new RuleResult();

						foreach (ValidationError error in result.ErrorList)
						{
							ruleResult.AddError(error.ErrorText);
						}

						return ruleResult;
					});
				}

				return Task.FromResult(RuleResult.Valid());
			});
		}

		/// <summary>
		/// Creates a validation rule that validates all the <see cref="IValidatable"/> items in the collection specified in <paramref name="validatableCollectionGetter"/>
		/// and adds error to this object from all the validatable items in invalid.
		/// </summary>
		/// <param name="validator">An instance of <see cref="ValidationHelper"/> that is used for validation.</param>
		/// <param name="validatableCollectionGetter">Expression for getting the collection of <see cref="IValidatable"/> objects to add as child items.</param>
		/// <returns>An instance of <see cref="IValidationRule"/> that represents the newly created validation rule.</returns>
		public static IAsyncValidationRule AddChildValidatableCollection(
			this ValidationHelper                       validator,
			Expression<Func<IEnumerable<IValidatable>>> validatableCollectionGetter
		)
		{
			Check.NotNull(validator,                   nameof(validator));
			Check.NotNull(validatableCollectionGetter, nameof(validatableCollectionGetter));

			Func<IEnumerable<IValidatable>> getter = validatableCollectionGetter.Compile();

			return validator.AddAsyncRule(PropertyName.For(validatableCollectionGetter), () =>
			{
				IEnumerable<IValidatable> items = getter();

				if (items == null)
				{
					return Task.FromResult(RuleResult.Valid());
				}

				items = items as IValidatable[] ?? items.ToArray();

				if (!items.Any())
				{
					return Task.FromResult(RuleResult.Valid());
				}

				RuleResult result = new RuleResult();

				// Execute validation on all items at the same time, wait for all
				// to finish and combine the results.

				List<ValidationResult> results = new List<ValidationResult>();

				List<Task<ValidationResult>> tasks = new List<Task<ValidationResult>>();

				foreach (IValidatable item in items)
				{
					Task<ValidationResult> task = item.Validate().ContinueWith(tr =>
					{
						ValidationResult r = tr.Result;
						AggregateException ex = tr.Exception;

						lock (results)
						{
							if (ex == null && r != null)
							{
								results.Add(r);
							}
						}

						return tr.Result;
					});

					tasks.Add(task);
				}

				Task<RuleResult> resultTask = Task.WhenAll(tasks).ContinueWith(tr =>
				{
					if (tr.Exception == null)
					{
						// Add errors from all validation results
						foreach (ValidationResult itemResult in results)
						{
							foreach (ValidationError error in itemResult.ErrorList)
							{
								result.AddError(error.ErrorText);
							}
						}

						return result;
					}

					throw new AggregateException(tr.Exception);
				});

				return resultTask;
			});
		}
	}
}