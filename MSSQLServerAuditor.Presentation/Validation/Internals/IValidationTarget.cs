﻿using System.Collections.Generic;

namespace MSSQLServerAuditor.Presentation.Validation.Internals
{
	internal interface IValidationTarget
	{
		IEnumerable<object> UnwrapTargets();

		bool IsMatch(object target);
	}
}