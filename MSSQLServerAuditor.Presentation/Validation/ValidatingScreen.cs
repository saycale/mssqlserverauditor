﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace MSSQLServerAuditor.Presentation.Validation
{
	public class ValidatingScreen : Screen, IValidatable, INotifyDataErrorInfo
	{
		protected ValidationHelper Validator { get; }

		private NotifyDataErrorInfoAdapter NotifyDataErrorInfoAdapter { get; }

		protected ValidatingScreen()
		{
			Validator = new ValidationHelper();

			NotifyDataErrorInfoAdapter = new NotifyDataErrorInfoAdapter(Validator);
			NotifyDataErrorInfoAdapter.ErrorsChanged += OnErrorsChanged;
		}

		private void OnErrorsChanged(object sender, DataErrorsChangedEventArgs e)
		{
			// Notify the UI that the property has changed so that the validation error gets displayed (or removed).
			NotifyOfPropertyChange(e.PropertyName);
		}

		public Task<ValidationResult> Validate()
		{
			return Validator.ValidateAllAsync();
		}

		public ValidationResult Validate(string propertyName)
		{
			return Validator.Validate(propertyName);
		}

		#region Implementation of INotifyDataErrorInfo

		public IEnumerable GetErrors(string propertyName)
		{
			if (!string.IsNullOrWhiteSpace(propertyName))
			{
				return NotifyDataErrorInfoAdapter.GetErrors(propertyName);
			}

			return Enumerable.Empty<string>();
		}

		public bool HasErrors => NotifyDataErrorInfoAdapter.HasErrors;

		public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged
		{
			add { NotifyDataErrorInfoAdapter.ErrorsChanged += value; }
			remove { NotifyDataErrorInfoAdapter.ErrorsChanged -= value; }
		}

		#endregion
	}
}
