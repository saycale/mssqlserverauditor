using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Presentation.Validation.Internals;

namespace MSSQLServerAuditor.Presentation.Validation
{
	/// <summary>
	/// Main helper class that contains the functionality of managing validation rules, 
	/// executing validation using those rules and keeping validation results.
	/// </summary>
	public class ValidationHelper
	{
		#region Fields

		private readonly IDictionary<object, IDictionary<ValidationRule, RuleResult>> _ruleValidationResultMap =
			new Dictionary<object, IDictionary<ValidationRule, RuleResult>>();

		private readonly object _syncRoot = new object();

		#endregion

		#region Construction

		/// <summary>
		/// Initializes a new instance of the <see cref="ValidationHelper"/> class.
		/// </summary>
		public ValidationHelper()
			: this(new ValidationSettings())
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ValidationHelper"/> class.
		/// </summary>
		/// <param name="settings">An instance of <see cref="ValidationSettings"/> that control the behavior of this instance of <see cref="ValidationHelper"/>.</param>
		public ValidationHelper(ValidationSettings settings)
		{
			Check.NotNull(settings, nameof(settings));

			ValidationRules = new ValidationRuleCollection();
			Settings = settings;
		}

		#endregion

		#region Properties

		private ValidationRuleCollection ValidationRules { get; }
		private ValidationSettings       Settings        { get; }

		/// <summary>
		/// Indicates whether the validation is currently suspended using the <see cref="SuppressValidation"/> method.
		/// </summary>
		public bool IsValidationSuspended { get; private set; }

		#endregion

		#region Rules Construction

		/// <summary>
		/// Adds a validation rule that validates the <paramref name="target"/> object.
		/// </summary>
		/// <param name="target">The validation target (object that is being validated by <paramref name="validateDelegate"/>).</param>
		/// <param name="validateDelegate">
		/// The validation delegate - a function that returns an instance 
		/// of <see cref="RuleResult"/> that indicated whether the rule has passed and 
		/// a collection of errors (in not passed).
		/// </param>
		/// <returns>An instance of <see cref="IValidationRule"/> that represents the newly created validation rule.</returns>
		public IValidationRule AddRule(object target, Func<RuleResult> validateDelegate)
		{
			Check.NotNull(target, nameof(target));
			Check.NotNull(validateDelegate, nameof(validateDelegate));

			IAsyncValidationRule rule = AddRuleCore(
				new GenericValidationTarget(target),
				validateDelegate,
				null
			);

			return rule;
		}

		/// <summary>
		/// Adds a simple validation rule.
		/// </summary>
		/// <param name="validateDelegate">
		/// The validation delegate - a function that returns an instance 
		/// of <see cref="RuleResult"/> that indicated whether the rule has passed and 
		/// a collection of errors (in not passed).
		/// </param>
		/// <returns>An instance of <see cref="IValidationRule"/> that represents the newly created validation rule.</returns>
		public IValidationRule AddRule(Func<RuleResult> validateDelegate)
		{
			Check.NotNull(validateDelegate, nameof(validateDelegate));

			IAsyncValidationRule rule = AddRuleCore(
				new UndefinedValidationTarget(),
				validateDelegate,
				null
			);

			return rule;
		}

		/// <summary>
		/// Adds a validation rule that validates a property of an object. The target property is specified in the <paramref name="targetName"/> parameter.
		/// </summary>
		/// <param name="targetName">The target property name. Example: AddRule(nameof(MyProperty), ...).</param>
		/// <param name="validateDelegate">
		/// The validation delegate - a function that returns an instance 
		/// of <see cref="RuleResult"/> that indicated whether the rule has passed and 
		/// a collection of errors (in not passed).
		/// </param>
		/// <example>
		/// <code>
		/// AddRule(() => Foo, , () => RuleResult.Assert(Foo > 10, "Foo must be greater than 10"))
		/// </code>
		/// </example>
		/// <returns>An instance of <see cref="IValidationRule"/> that represents the newly created validation rule.</returns>
		public IValidationRule AddRule(string targetName, Func<RuleResult> validateDelegate)
		{
			Check.NotNull(targetName,       nameof(targetName));
			Check.NotNull(validateDelegate, nameof(validateDelegate));

			IValidationRule rule = AddRule(
				new[] { targetName },
				validateDelegate
			);

			return rule;
		}

		/// <summary>
		/// Adds a validation rule that validates two dependent properties.
		/// </summary>
		/// <param name="property1Name">The first target property name. Example: AddRule(nameof(MyProperty), ...).</param>
		/// <param name="property2Name">The second target property name. Example: AddRule(..., nameof(MyProperty), ...).</param>
		/// <param name="validateDelegate">
		/// The validation delegate - a function that returns an instance 
		/// of <see cref="RuleResult"/> that indicated whether the rule has passed and 
		/// a collection of errors (in not passed).
		/// </param>
		/// <example>
		/// <code>
		/// AddRule(() => Foo, () => Bar, () => RuleResult.Assert(Foo > Bar, "Foo must be greater than bar"))
		/// </code>
		/// </example>
		/// <returns>An instance of <see cref="IValidationRule"/> that represents the newly created validation rule.</returns>
		public IValidationRule AddRule(
			string           property1Name,
			string           property2Name,
			Func<RuleResult> validateDelegate
		)
		{
			Check.NotNull(property1Name,    nameof(property1Name));
			Check.NotNull(property2Name,    nameof(property2Name));
			Check.NotNull(validateDelegate, nameof(validateDelegate));

			IValidationRule rule = AddRule(new[] { property1Name, property2Name }, validateDelegate);

			return rule;
		}

		/// <summary>
		/// Adds a validation rule that validates a collection of dependent properties.
		/// </summary>
		/// <param name="properties">The collection of target property expressions. Example: AddRule(new [] { () => MyProperty1, () => MyProperty2, () => MyProperty3 }, ...).</param>
		/// <param name="validateDelegate">
		/// The validation delegate - a function that returns an instance 
		/// of <see cref="RuleResult"/> that indicated whether the rule has passed and 
		/// a collection of errors (in not passed).
		/// </param>
		/// <returns>An instance of <see cref="IValidationRule"/> that represents the newly created validation rule.</returns>
		public IValidationRule AddRule(
			IEnumerable<string> properties,
			Func<RuleResult>    validateDelegate
		)
		{
			Check.NotNull(properties,       nameof(properties));
			Check.Assert(properties.Any(),  "properties.Any()");
			Check.NotNull(validateDelegate, nameof(validateDelegate));

			IValidationTarget target = CreatePropertyValidationTarget(properties);

			IAsyncValidationRule rule = AddRuleCore(target, validateDelegate, null);

			return rule;
		}

		#region Async Rules

		/// <summary>
		/// Adds an asynchronious validation rule that validates the <paramref name="target"/> object.
		/// </summary>
		/// <param name="target">The validation target (object that is being validated by <paramref name="validateAction"/>).</param>
		/// <param name="validateAction">The validation delegate - a function that performs asyncrhonious validation.</param>
		/// <returns>An instance of <see cref="IAsyncValidationRule"/> that represents the newly created validation rule.</returns>
		public IAsyncValidationRule AddAsyncRule(
			object                 target,
			Func<Task<RuleResult>> validateAction
		)
		{
			Check.NotNull(target, nameof(target));
			Check.NotNull(validateAction, nameof(validateAction));

			IAsyncValidationRule rule = AddRuleCore(
				new GenericValidationTarget(target),
				null,
				validateAction
			);

			return rule;
		}

		/// <summary>
		/// Adds an asynchronious validation rule.
		/// </summary>
		/// <param name="validateAction">The validation delegate - a function that performs asyncrhonious validation.</param>
		/// <returns>An instance of <see cref="IAsyncValidationRule"/> that represents the newly created validation rule.</returns>
		public IAsyncValidationRule AddAsyncRule(Func<Task<RuleResult>> validateAction)
		{
			Check.NotNull(validateAction, nameof(validateAction));

			IAsyncValidationRule rule = AddRuleCore(
				new UndefinedValidationTarget(),
				null,
				validateAction
			);

			return rule;
		}

		/// <summary>
		/// Adds an asynchronious validation rule that validates a property of an object. The target property is specified in the <paramref name="propertyName"/> parameter.
		/// </summary>
		/// <param name="propertyName">The target property name. Example: AddAsyncRule(nameof(MyProperty), ...).</param>
		/// <param name="validateAction">The validation delegate - a function that performs asyncrhonious validation.</param>
		/// <example>
		/// <code>
		/// AddRule(() => Foo, 
		///         () => 
		///         {
		///             return ValidationServiceFacade.ValidateFooAsync(Foo)
		///                 .ContinueWith(t => return RuleResult.Assert(t.Result.IsValid, "Foo must be greater than 10"));
		///         })
		/// </code>
		/// </example>
		/// <returns>An instance of <see cref="IAsyncValidationRule"/> that represents the newly created validation rule.</returns>
		public IAsyncValidationRule AddAsyncRule(
			string                 propertyName,
			Func<Task<RuleResult>> validateAction
		)
		{
			Check.NotNull(propertyName,   nameof(propertyName));
			Check.NotNull(validateAction, nameof(validateAction));

			IAsyncValidationRule rule = AddAsyncRule(
				new[] { propertyName }.Select(c => c),
				validateAction
			);

			return rule;
		}

		/// <summary>
		/// Adds an asynchronious validation rule that validates two dependent properties.
		/// </summary>
		/// <param name="property1Name">The first target property name. Example: AddRule(nameof(MyProperty), ...).</param>
		/// <param name="property2Name">The second target property name. Example: AddRule(..., nameof(MyProperty), ...).</param>
		/// <param name="validateAction">The validation delegate - a function that performs asyncrhonious validation.</param>
		/// <example>
		/// <code>
		/// AddRule(() => Foo, () => Bar
		///            () => 
		///         {
		///                return ValidationServiceFacade.ValidateFooAndBar(Foo, Bar)
		///                       .ContinueWith(t => RuleResult.Assert(t.Result.IsValid, "Foo must be greater than 10"));
		///            })
		/// </code>
		/// </example>
		/// <returns>An instance of <see cref="IAsyncValidationRule"/> that represents the newly created validation rule.</returns>
		public IAsyncValidationRule AddAsyncRule(
			string                 property1Name,
			string                 property2Name,
			Func<Task<RuleResult>> validateAction
		)
		{
			Check.NotNull(property1Name,  nameof(property1Name));
			Check.NotNull(property2Name,  nameof(property2Name));
			Check.NotNull(validateAction, nameof(validateAction));

			IAsyncValidationRule rule = AddAsyncRule(
				new[] { property1Name, property2Name },
				validateAction
			);

			return rule;
		}

		/// <summary>
		/// Adds an asynchronious validation rule that validates a collection of dependent properties.
		/// </summary>
		/// <param name="properties">The collection of target property names. Example: AddAsyncRule(new [] { nameof(MyProperty1), nameof(MyProperty2), nameof(MyProperty3) }, ...).</param>
		/// <param name="validateAction">The validation delegate - a function that performs asyncrhonious validation.</param>
		/// <returns>An instance of <see cref="IAsyncValidationRule"/> that represents the newly created validation rule.</returns>
		public IAsyncValidationRule AddAsyncRule(
			IEnumerable<string>    properties,
			Func<Task<RuleResult>> validateAction
		)
		{
			Check.NotNull(properties,      nameof(properties));
			Check.Assert(properties.Any(), "properties.Any()");
			Check.NotNull(validateAction,  nameof(validateAction));

			IValidationTarget target = CreatePropertyValidationTarget(properties);

			IAsyncValidationRule rule = AddRuleCore(target, null, validateAction);

			return rule;
		}

		#endregion

		/// <summary>
		/// Removes the specified <paramref name="rule"/>.
		/// </summary>
		/// <param name="rule">Validation rule instance.</param>
		public void RemoveRule(IValidationRule rule)
		{
			Check.NotNull(rule, nameof(rule));

			ValidationRule typedRule = rule as ValidationRule;

			Check.Assert(
				typedRule != null,
				string.Format(CultureInfo.InvariantCulture, "Rule must be of type \"{0}\".", typeof(ValidationRule).FullName)
			);

			lock (this._syncRoot)
			{
				UnregisterValidationRule(typedRule);

				lock (this._ruleValidationResultMap)
				{
					// Clear the results if any
					foreach (KeyValuePair<object, IDictionary<ValidationRule, RuleResult>> ruleResultsPair in this._ruleValidationResultMap)
					{
						bool removed = ruleResultsPair.Value.Remove(typedRule);

						if (removed)
						{
							// Notify that validation result for the target has changed
							NotifyResultChanged(ruleResultsPair.Key, GetResult(ruleResultsPair.Key), null, false);
						}
					}
				}
			}
		}

		/// <summary>
		/// Removes all validation rules.
		/// </summary>
		public void RemoveAllRules()
		{
			lock (this._syncRoot)
			{
				UnregisterAllValidationRules();

				object[] targets;

				lock (this._ruleValidationResultMap)
				{
					targets = this._ruleValidationResultMap.Keys.ToArray();

					// Clear the results
					this._ruleValidationResultMap.Clear();
				}

				// Notify that validation result has changed
				foreach (object target in targets)
				{
					NotifyResultChanged(target, ValidationResult.Valid, null, false);
				}
			}
		}

		private IAsyncValidationRule AddRuleCore(
			IValidationTarget      target,
			Func<RuleResult>       validateDelegate,
			Func<Task<RuleResult>> asyncValidateAction
		)
		{
			ValidationRule rule = new ValidationRule(target, validateDelegate, asyncValidateAction);

			RegisterValidationRule(rule);

			return rule;
		}

		private static IValidationTarget CreatePropertyValidationTarget(IEnumerable<string> properties)
		{
			IValidationTarget target;

			// Getting rid of thw "Possible multiple enumerations of IEnumerable" warning
			properties = properties.ToArray();

			if (properties.Count() == 1)
			{
				target = new PropertyValidationTarget(properties.First());
			}
			else
			{
				target = new PropertyCollectionValidationTarget(properties);
			}

			return target;
		}

		private void RegisterValidationRule(ValidationRule rule)
		{
			lock (this._syncRoot)
			{
				ValidationRules.Add(rule);
			}
		}

		private void UnregisterValidationRule(ValidationRule rule)
		{
			lock (this._syncRoot)
			{
				ValidationRules.Remove(rule);
			}
		}

		private void UnregisterAllValidationRules()
		{
			lock (this._syncRoot)
			{
				ValidationRules.Clear();
			}
		}

		#endregion

		#region Getting Validation Results

		/// <summary>
		/// Returns the current validation state (all errors tracked by this instance of <see cref="ValidationHelper"/>).
		/// </summary>
		/// <returns>An instance of <see cref="ValidationResult"/> that contains an indication whether the object is valid and a collection of errors if not.</returns>
		public ValidationResult GetResult()
		{
			return GetResultInternal();
		}

		/// <summary>
		/// Returns the current validation state for the given <paramref name="target"/> (all errors tracked by this instance of <see cref="ValidationHelper"/>).
		/// </summary>
		/// <param name="target">The validation target for which to retrieve the validation state.</param>
		/// <returns>An instance of <see cref="ValidationResult"/> that contains an indication whether the object is valid and a collection of errors if not.</returns>
		public ValidationResult GetResult(object target)
		{
			Check.NotNull(target, nameof(target));

			bool returnAllResults = string.IsNullOrEmpty(target as string);

			ValidationResult result = returnAllResults ? GetResultInternal() : GetResultInternal(target);

			return result;
		}

		/// <summary>
		/// Returns the current validation state for a property represented by <paramref name="targetName"/> (all errors tracked by this instance of <see cref="ValidationHelper"/>).
		/// </summary>
		/// <param name="targetName">The property for which to retrieve the validation state. Example: GetResult(() => MyProperty)</param>
		/// <returns>An instance of <see cref="ValidationResult"/> that contains an indication whether the object is valid and a collection of errors if not.</returns>
		public ValidationResult GetResult(string targetName)
		{
			Check.NotNull(targetName, nameof(targetName));

			return GetResult((object)targetName);
		}

		private ValidationResult GetResultInternal(object target)
		{
			ValidationResult result = ValidationResult.Valid;

			lock (this._ruleValidationResultMap)
			{
				IDictionary<ValidationRule, RuleResult> ruleResultMap;

				if (this._ruleValidationResultMap.TryGetValue(target, out ruleResultMap))
				{
					foreach (RuleResult ruleValidationResult in ruleResultMap.Values)
					{
						result = result.Combine(new ValidationResult(target, ruleValidationResult.Errors));
					}
				}
			}

			return result;
		}

		private ValidationResult GetResultInternal()
		{
			ValidationResult result = ValidationResult.Valid;

			lock (this._ruleValidationResultMap)
			{
				foreach (KeyValuePair<object, IDictionary<ValidationRule, RuleResult>> ruleResultsMapPair in this._ruleValidationResultMap)
				{
					object ruleTarget = ruleResultsMapPair.Key;

					IDictionary<ValidationRule, RuleResult> ruleResultsMap = ruleResultsMapPair.Value;

					foreach (RuleResult validationResult in ruleResultsMap.Values)
					{
						result = result.Combine(
							new ValidationResult(ruleTarget, validationResult.Errors)
						);
					}
				}
			}

			return result;
		}

		#endregion

		#region Validation Execution

		/// <summary>
		/// Validates (executes validation rules) the property specified in the <paramref name="targetName"/> parameter.
		/// </summary>
		/// <param name="targetName">Name of the property to validate. Example: Validate(nameof(MyProperty)).</param>
		/// <returns>Result that indicates whether the given property is valid and a collection of errors, if not valid.</returns>
		public ValidationResult Validate(string targetName)
		{
			Check.NotNull(targetName, nameof(targetName));

			return ValidateInternal(targetName);
		}

		/// <summary>
		/// Validates (executes validation rules) the specified target object.
		/// </summary>
		/// <param name="target">The target object to validate.</param>
		/// <returns>Result that indicates whether the given target object is valid and a collection of errors, if not valid.</returns>
		public ValidationResult Validate(object target)
		{
			Check.NotNull(target, nameof(target));

			return ValidateInternal(target);
		}

		/// <summary>
		/// Validates (executes validation rules) the calling property.
		/// </summary>
		/// <param name="callerName">Name of the property to validate (provided by the c# compiler and should not be specified exlicitly).</param>
		/// <returns>Result that indicates whether the given property is valid and a collection of errors, if not valid.</returns>
		public ValidationResult ValidateCaller([CallerMemberName] string callerName = null)
		{
			Check.NotNullOrEmpty(callerName, nameof(callerName));

			return Validate(callerName);
		}

		/// <summary>
		/// Executes validation using all validation rules. 
		/// </summary>
		/// <returns>Result that indicates whether the validation was succesfull and a collection of errors, if it wasn't.</returns>
		public ValidationResult ValidateAll()
		{
			return ValidateInternal(null);
		}

		private ValidationResult ValidateInternal(object target)
		{
			ReadOnlyCollection<ValidationRule> rulesToExecute;

			lock (this._syncRoot)
			{
				if (IsValidationSuspended)
				{
					return ValidationResult.Valid;
				}

				rulesToExecute = GetRulesForTarget(target);

				if (rulesToExecute.Any(r => !r.SupportsSyncValidation))
				{
					throw new InvalidOperationException(
						"There are asynchronous rules that cannot be executed synchronously. Please use ValidateAsync method to execute validation instead."
					);
				}
			}

			try
			{
				ValidationResult validationResult = ExecuteValidationRulesAsync(rulesToExecute).Result;
				return validationResult;
			}
			catch (Exception ex)
			{
				throw new ValidationException(
					"An exception occurred during validation. See inner exception for details.",
					ExceptionUtils.UnwrapException(ex)
				);
			}
		}

		private async Task<ValidationResult> ExecuteValidationRulesAsync(
			IEnumerable<ValidationRule> rulesToExecute,
			SynchronizationContext      syncContext = null
		)
		{
			ValidationResult result        = new ValidationResult();
			HashSet<object>  failedTargets = new HashSet<object>();

			foreach (ValidationRule rule in rulesToExecute.ToArray())
			{
				bool isValid = await ExecuteRuleAsync(rule, failedTargets, result, syncContext)
					.ConfigureAwait(false);

				if (!isValid)
				{
					break;
				}
			}

			return result;
		}

		private async Task<bool> ExecuteRuleAsync(
			ValidationRule         rule,
			ISet<object>           failedTargets,
			ValidationResult       validationResultAccumulator,
			SynchronizationContext syncContext
		)
		{
			// Skip rule if the target is already invalid and the rule is not configured to execute anyway
			if (failedTargets.Contains(rule.Target) && !ShouldExecuteOnAlreadyInvalidTarget(rule))
			{
				// Assume that the rule is valid at this point because we are not interested in this error until
				// previous rule is fixed.
				SaveRuleValidationResultAndNotifyIfNeeded(rule, RuleResult.Valid(), syncContext);

				return true;
			}

			RuleResult ruleResult = !rule.SupportsSyncValidation
				? await rule.EvaluateAsync().ConfigureAwait(false)
				: rule.Evaluate();

			SaveRuleValidationResultAndNotifyIfNeeded(rule, ruleResult, syncContext);

			AddErrorsFromRuleResult(validationResultAccumulator, rule, ruleResult);

			if (!ruleResult.IsValid)
			{
				failedTargets.Add(rule.Target);
			}

			return true;
		}

		private bool ShouldExecuteOnAlreadyInvalidTarget(ValidationRule rule)
		{
			bool defaultValue = Settings.DefaultRuleSettings?.ExecuteOnAlreadyInvalidTarget ?? false;

			return rule.Settings.ExecuteOnAlreadyInvalidTarget.GetValueOrDefault(defaultValue);
		}

		private ReadOnlyCollection<ValidationRule> GetRulesForTarget(object target)
		{
			lock (this._syncRoot)
			{
				Func<ValidationRule, bool> ruleFilter = CreateRuleFilterFor(target);

				ReadOnlyCollection<ValidationRule> result = new ReadOnlyCollection<ValidationRule>(
					ValidationRules.Where(ruleFilter).ToList()
				);

				return result;
			}
		}

		private static void AddErrorsFromRuleResult(
			ValidationResult resultToAddTo,
			ValidationRule   validationRule,
			RuleResult       ruleResult
		)
		{
			if (!ruleResult.IsValid)
			{
				IEnumerable<object> errorTargets = validationRule.Target.UnwrapTargets();

				foreach (object errorTarget in errorTargets)
				{
					foreach (string ruleError in ruleResult.Errors)
					{
						resultToAddTo.AddError(errorTarget, ruleError);
					}
				}
			}
		}

		private static Func<ValidationRule, bool> CreateRuleFilterFor(object target)
		{
			Func<ValidationRule, bool> ruleFilter = r => true;

			if (target != null)
			{
				ruleFilter = r => r.Target.IsMatch(target);
			}

			return ruleFilter;
		}

		private void SaveRuleValidationResultAndNotifyIfNeeded(
			ValidationRule         rule, 
			RuleResult             ruleResult,
			SynchronizationContext syncContext
		)
		{
			lock (this._syncRoot)
			{
				IEnumerable<object> ruleTargets = rule.Target.UnwrapTargets();

				foreach (object ruleTarget in ruleTargets)
				{
					IDictionary<ValidationRule, RuleResult> targetRuleMap = GetRuleMapForTarget(ruleTarget);

					RuleResult currentRuleResult = GetCurrentValidationResultForRule(targetRuleMap, rule);

					if (!Equals(currentRuleResult, ruleResult))
					{
						lock (this._ruleValidationResultMap)
						{
							targetRuleMap[rule] = ruleResult;
						}

						// Notify that validation result for the target has changed
						NotifyResultChanged(ruleTarget, GetResult(ruleTarget), syncContext);
					}
				}
			}
		}

		private static RuleResult GetCurrentValidationResultForRule(
			IDictionary<ValidationRule,RuleResult> ruleMap,
			ValidationRule                         rule
		)
		{
			lock (ruleMap)
			{
				if (!ruleMap.ContainsKey(rule))
				{
					ruleMap.Add(rule, RuleResult.Valid());
				}

				return ruleMap[rule];
			}
		}

		private IDictionary<ValidationRule, RuleResult> GetRuleMapForTarget(object target)
		{
			lock (this._ruleValidationResultMap)
			{
				if (!this._ruleValidationResultMap.ContainsKey(target))
				{
					this._ruleValidationResultMap.Add(target, new Dictionary<ValidationRule, RuleResult>());
				}

				IDictionary<ValidationRule, RuleResult> ruleMap = _ruleValidationResultMap[target];

				return ruleMap;
			}
		}

		/// <summary>
		/// Executes validation for the given property asynchronously. 
		/// Executes all (normal and async) validation rules for the property specified in the <paramref name="targetName"/>.
		/// </summary>
		/// <param name="targetName">Expression for the property to validate. Example: ValidateAsync(() => MyProperty, ...).</param>
		/// <returns>Task that represents the validation operation.</returns>
		public Task<ValidationResult> ValidateAsync(string targetName)
		{
			Check.NotNull(targetName, nameof(targetName));

			return ValidateInternalAsync(targetName);
		}

		/// <summary>
		/// Executes validation for the calling property asynchronously.
		/// </summary>
		/// <param name="callerName">Name of the property to validate (provided by the c# compiler and should not be specified exlicitly).</param>
		/// <returns>Result that indicates whether the given property is valid and a collection of errors, if not valid.</returns>
		public Task<ValidationResult> ValidateCallerAsync([CallerMemberName] string callerName = null)
		{
			Check.NotNullOrEmpty(callerName, nameof(callerName));

			return ValidateAsync(callerName);
		}

		/// <summary>
		/// Executes validation for the given target asynchronously. 
		/// Executes all (normal and async) validation rules for the target object specified in the <paramref name="target"/>.
		/// </summary>
		/// <param name="target">The target object to validate.</param>
		/// <returns>Task that represents the validation operation.</returns>
		public Task<ValidationResult> ValidateAsync(object target)
		{
			Check.NotNull(target, nameof(target));

			return ValidateInternalAsync(target);
		}

		/// <summary>
		/// Executes validation using all validation rules asynchronously.
		/// </summary>
		/// <returns>Task that represents the validation operation.</returns>
		public Task<ValidationResult> ValidateAllAsync()
		{
			return ValidateInternalAsync(null);
		}

		private Task<ValidationResult> ValidateInternalAsync(object target)
		{
			if (IsValidationSuspended)
			{
				return Task.FromResult(ValidationResult.Valid);
			}

			ReadOnlyCollection<ValidationRule> rulesToExecute = GetRulesForTarget(target);

			SynchronizationContext syncContext = SynchronizationContext.Current;

			return Task.Factory.StartNew(
				() => ExecuteValidationRulesAsync(rulesToExecute, syncContext),
				CancellationToken.None, TaskCreationOptions.None, TaskScheduler.Default
			)
			.Unwrap()
			.ContinueWith(t =>
			{
				if (t.Exception != null)
				{
					throw new ValidationException(
						"An exception occurred during validation. See inner exception for details.",
						ExceptionUtils.UnwrapException(t.Exception));
				}

				return t.Result;
			});
		}

		#endregion

		#region ResultChanged

		/// <summary>
		/// Occurs when the validation result have changed for a property or for the entire entity (the result that is returned by the <see cref="GetResult()"/> method).
		/// </summary>
		public event EventHandler<ValidationResultChangedEventArgs> ResultChanged;

		private void NotifyResultChanged(
			object                 target,
			ValidationResult       newResult,
			SynchronizationContext syncContext,
			bool                   useSyncContext = true
		)
		{
			if (useSyncContext && syncContext != null)
			{
				syncContext.Post(_ => NotifyResultChanged(target, newResult, syncContext, useSyncContext: false), null);
				return;
			}

			ResultChanged?.Invoke(this, new ValidationResultChangedEventArgs(target, newResult));
		}

		#endregion

		#region Misc

		/// <summary>
		/// Suppresses all the calls to the Validate* methods until the returned <see cref="IDisposable"/> is disposed
		/// by calling <see cref="IDisposable.Dispose"/>. 
		/// </summary>
		/// <remarks>
		/// This method is convenient to use when you want to suppress validation when setting initial value to a property. In this case you would
		/// wrap the code that sets the property into a <c>using</c> block. Like this:
		/// <code>
		/// using (Validation.SuppressValidation()) 
		/// {
		///     MyProperty = "Initial Value";
		/// }
		/// </code>
		/// </remarks>
		/// <returns>An instance of <see cref="IDisposable"/> that serves as a handle that you can call <see cref="IDisposable.Dispose"/> on to resume validation. The value can also be used in a <c>using</c> block.</returns>
		public IDisposable SuppressValidation()
		{
			IsValidationSuspended = true;

			return new DelegateDisposable(() => { IsValidationSuspended = false; });
		}

		/// <summary>
		/// Resets the validation state. If there were any broken rules 
		/// then the targets for those rules will become valid again and the <see cref="ResultChanged"/> event will be rised.
		/// </summary>
		public void Reset()
		{
			lock (this._syncRoot)
			{
				lock (this._ruleValidationResultMap)
				{
					IEnumerable<object> targets = this._ruleValidationResultMap.Keys.ToArray();

					foreach (object target in targets)
					{
						ValidationResult resultForTarget = GetResultInternal(target);

						this._ruleValidationResultMap.Remove(target);

						if (!resultForTarget.IsValid)
						{
							NotifyResultChanged(target, ValidationResult.Valid, null, false);
						}
					}
				}
			}
		}

		#endregion
	}
}