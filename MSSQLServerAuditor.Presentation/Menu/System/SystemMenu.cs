﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Interop;
using MSSQLServerAuditor.Presentation.Interop;

namespace MSSQLServerAuditor.Presentation.Menu.System
{
	/// <summary>
	/// Represents a Window System Menu
	/// </summary>
	public class SystemMenu : IDisposable
	{
		private const uint WM_SYSCOMMAND = 0x0112;

		private const uint MF_STRING    = 0x0;
		private const uint MF_SEPARATOR = 0x0800;
		
		private IntPtr _sysMenuHandle;
		private bool   _disposed;

		private readonly List<SystemMenuItem> _menuItems;

		/// <summary>
		/// Creates a new instance of SystemMenu class
		/// </summary>
		public SystemMenu(Window window, bool reset = true)
		{
			IntPtr windowHandle = new WindowInteropHelper(window).Handle;

			if (windowHandle == IntPtr.Zero || !UnsafeNativeMethods.IsWindow(windowHandle))
			{
				throw new ArgumentException("Failed to get window handle");
			}

			if (reset)
			{
				// reset menu to default state
				UnsafeNativeMethods.GetSystemMenu(windowHandle, 1);
			}

			IntPtr sysMenuHandle = UnsafeNativeMethods.GetSystemMenu(windowHandle, 0);

			if (sysMenuHandle == IntPtr.Zero)
			{
				throw new ArgumentException("Failed to get system menu handle");
			}

			this._sysMenuHandle = sysMenuHandle;
			this._menuItems     = new List<SystemMenuItem>();

			HwndSource hwndSource = HwndSource.FromHwnd(windowHandle);
			hwndSource?.AddHook(new HwndSourceHook(WndProc));
		}

		public IntPtr Handle => this._sysMenuHandle;

		public void Apply()
		{
			foreach (SystemMenuItem menuItem in this._menuItems)
			{
				if (menuItem.IsSeparator)
				{
					UnsafeNativeMethods.AppendMenu(
						Handle,
						MF_SEPARATOR,
						0,
						string.Empty
					);
				}
				else
				{
					UnsafeNativeMethods.AppendMenu(
						Handle,
						MF_STRING,
						menuItem.Id,
						menuItem.Text
					);
				}
			}
		}

		public void AppendItem(SystemMenuItem menuItem)
		{
			this._menuItems.Add(menuItem);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!_disposed)
			{
				if (disposing)
				{
					// Release managed resources.
				}

				// Free the unmanaged resource
				this._sysMenuHandle = IntPtr.Zero;

				this._disposed = true;
			}
		}

		~SystemMenu()
		{
			Dispose(false);
		}

		private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
		{
			if (msg == WM_SYSCOMMAND)
			{
				uint menuId = (uint) (wParam.ToInt32() & 0xffff);

				SystemMenuItem menuItem = this._menuItems
					.FirstOrDefault(m => m.Id == menuId);

				if (menuItem != null)
				{
					menuItem.Action();
					handled = true;
				}
			}

			return IntPtr.Zero;
		}
	}
}
