﻿using System;
using System.Globalization;

namespace MSSQLServerAuditor.Presentation.Converters
{
	public class NullableDateTimeToStringConverter : ConverterBase
	{
		private static string DefaultString = "----/--/--";
		private static string DateFormat    = "yyyy'/'MM'/'dd HH:mm:ss";

		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			DateTime? dateTime = value as DateTime?;
			if (dateTime != null)
			{				
				if (dateTime == DateTime.MinValue)
				{
					return DefaultString;
				}

				return dateTime.Value.ToString(DateFormat);
			}

			return DefaultString;
		}

		public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null)
			{
				return DateTime.MinValue;
			}

			if (value.ToString() != DefaultString)
			{
				DateTime val;
				if (DateTime.TryParseExact(value.ToString(), DateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out val))
				{
					return val;
				}
			}

			return DateTime.MinValue;
		}
	}
}
