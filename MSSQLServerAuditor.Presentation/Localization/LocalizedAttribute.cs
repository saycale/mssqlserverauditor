﻿using System;
using System.Globalization;
using System.Resources;

namespace MSSQLServerAuditor.Presentation.Localization
{
	public class LocalizedAttribute : Attribute
	{
		private readonly string          _resourceKey;
		private readonly ResourceManager _resource;

		public LocalizedAttribute(string resourceKey, Type resourceType)
		{
			this._resource    = new ResourceManager(resourceType);
			this._resourceKey = resourceKey;
		}

		public string GetLocalizedText(CultureInfo culture)
		{
			string displayName = this._resource.GetString(
				this._resourceKey,
				culture
			);

			return string.IsNullOrEmpty(displayName)
				? $"[[{this._resourceKey}]]"
				: displayName;
		}

		public string UiText => GetLocalizedText(CultureInfo.CurrentUICulture);
	}
}
