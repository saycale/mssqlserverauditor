﻿using System.Globalization;
using System.Threading;
using WPFLocalizeExtension.Engine;

namespace MSSQLServerAuditor.Presentation.Localization
{
	public static class AppLocalizer
	{
		public static void ChangeCulture(string cultureName)
		{
			CultureInfo newCulture = new CultureInfo(cultureName);

			ChangeCulture(newCulture);
		}

		public static void ChangeCulture(CultureInfo culture)
		{
			LocalizeDictionary.Instance.SetCurrentThreadCulture = true;

			LocalizeDictionary.Instance.Culture   = culture;
			Thread.CurrentThread.CurrentUICulture = culture;
			Thread.CurrentThread.CurrentCulture   = culture;
		}
	}
}
