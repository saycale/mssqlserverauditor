﻿using System;
using System.Runtime.InteropServices;
using System.Security;

namespace MSSQLServerAuditor.Presentation.Interop
{
	// ReSharper disable InconsistentNaming
	internal static class ExternDll
	{
		public const string User32 = "user32.dll";
	}

	[SuppressUnmanagedCodeSecurity]
	public static class UnsafeNativeMethods
	{
		[DllImport(ExternDll.User32, CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern IntPtr GetSystemMenu(IntPtr hWnd, int bRevert);

		[DllImport(ExternDll.User32, CharSet = CharSet.Unicode, SetLastError = true)]
		public static extern bool AppendMenu(IntPtr hMenu, uint uFlags, uint uIDNewItem, string lpNewItem);

		[DllImport(ExternDll.User32, CharSet = CharSet.Unicode, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool IsWindow(IntPtr hwnd);
	}
}
