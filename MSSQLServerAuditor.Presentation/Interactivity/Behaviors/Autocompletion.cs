﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Threading;
using MSSQLServerAuditor.Common.Collections;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Presentation.Infrastructure;
using MSSQLServerAuditor.Presentation.Interactivity.Autocomplete;

namespace MSSQLServerAuditor.Presentation.Interactivity.Behaviors
{
	public class Autocompletion : Behavior<TextBox>
	{
		public static readonly DependencyProperty AutocompleteItemsProperty = DependencyProperty.Register(
			nameof(AutocompleteItems),
			typeof(IEnumerable<AutocompleteItem>),
			typeof (Autocompletion),
			new PropertyMetadata(new List<AutocompleteItem>(),
				(sender, e) => ((Autocompletion) sender).OnAutocompleteItemsChanged())
		);

		public static readonly DependencyProperty SearchPatternProperty = DependencyProperty.Register(
			nameof(SearchPattern),
			typeof(string),
			typeof(Autocompletion),
			new PropertyMetadata(@"[\w\.]")
		);

		public static readonly DependencyProperty AppearIntervalProperty = DependencyProperty.Register(
			nameof(AppearInterval),
			typeof(int),
			typeof(Autocompletion),
			new PropertyMetadata(200)
		);

		public static readonly DependencyProperty MinFragmentLengthProperty = DependencyProperty.Register(
			nameof(MinFragmentLength),
			typeof(int),
			typeof(Autocompletion),
			new PropertyMetadata(2)
		);

		public static readonly DependencyProperty AutoPopupProperty = DependencyProperty.Register(
			nameof(AutoPopup),
			typeof(bool),
			typeof(Autocompletion),
			new PropertyMetadata(true)
		);

		public static readonly DependencyProperty AcceptsTabProperty = DependencyProperty.Register(
			nameof(AcceptsTab),
			typeof(bool),
			typeof(Autocompletion),
			new PropertyMetadata(true)
		);

		/// <summary>
		/// Occurs when popup menu is about to be opened
		/// </summary>
		[Description("Occurs when popup menu is about to be opened.")]
		public event EventHandler<CancelEventArgs> PopupOpening;

		/// <summary>
		/// User selects item
		/// </summary>
		[Description("Occurs when user selects item.")]
		public event EventHandler<SelectingItemEventArgs> ItemSelecting;

		/// <summary>
		/// It fires after item was inserting
		/// </summary>
		[Description("Occurs after user selected item.")]
		public event EventHandler<SelectedItemEventArgs> ItemSelected;

		private readonly DispatcherTimer _timer;

		private Window   _window;
		private Popup    _popup;
		private ListView _listView;

		private bool  _forcedOpened = false;
		private Range _fragment;

		private readonly ObservableCollectionEx<AutocompleteItem> _visibleItems;

		public Autocompletion()
		{
			this._timer        = new DispatcherTimer();
			this._visibleItems = new ObservableCollectionEx<AutocompleteItem>();

			this._timer.Tick += timer_Tick;
		}

		public IEnumerable<AutocompleteItem> AutocompleteItems
		{
			get { return (IEnumerable<AutocompleteItem>)GetValue(AutocompleteItemsProperty); }
			set { SetValue(AutocompleteItemsProperty, value); }
		}

		public string SearchPattern
		{
			get { return (string) GetValue(SearchPatternProperty); }
			set { SetValue(SearchPatternProperty, value); }
		}

		public int AppearInterval
		{
			get { return (int) GetValue(AppearIntervalProperty); }
			set { SetValue(AppearIntervalProperty, value);}
		}

		public int MinFragmentLength
		{
			get { return (int) GetValue(MinFragmentLengthProperty); }
			set { SetValue(MinFragmentLengthProperty, value); }
		}

		public bool AutoPopup
		{
			get { return (bool) GetValue(AutoPopupProperty); }
			set { SetValue(AutoPopupProperty, value); }
		}

		public bool AcceptsTab
		{
			get { return (bool) GetValue(AcceptsTabProperty); }
			set { SetValue(AcceptsTabProperty, value); }
		}

		protected override void OnAttached()
		{
			base.OnAttached();

			this._window = Window.GetWindow(AssociatedObject);
			if (this._window != null)
			{
				this._window.Deactivated      += Window_Deactivated;
				this._window.LocationChanged  += Window_LocationChanged;
				this._window.PreviewMouseDown += Window_PreviewMouseDown;
				this._window.StateChanged     += Window_StateChanged;
			}

			this._popup = new Popup
			{
				IsOpen              = false,
				PlacementTarget     = AssociatedObject,
				Placement           = PlacementMode.Relative,
				SnapsToDevicePixels = true
			};

			this._listView = new ListView
			{
				Focusable = false,
				MaxWidth  = 200,
				MaxHeight = 200,
			};

			// Make listview items unfocusable
			Style style = new Style
			{
				TargetType = typeof(ListViewItem)
			};

			style.Setters.Add(new Setter(UIElement.FocusableProperty, false));
			this._listView.ItemContainerStyle = style;

			this._listView.ItemsSource = this._visibleItems;

			this._popup.Child = this._listView;

			AssociatedObject.LostFocus      += TextBox_LostFocus;
			AssociatedObject.PreviewKeyDown += TextBox_PreviewKeyDown;
			
			this._listView.PreviewMouseLeftButtonDown += ListBox_PreviewMouseLeftButtonDown;
			this._listView.PreviewMouseDoubleClick    += ListBox_PreviewMouseDoubleClick;
		}

		protected override void OnDetaching()
		{
			if (this._window != null)
			{
				this._window.Deactivated      -= Window_Deactivated;
				this._window.LocationChanged  -= Window_LocationChanged;
				this._window.PreviewMouseDown -= Window_PreviewMouseDown;
				this._window.StateChanged     -= Window_StateChanged;
			}

			AssociatedObject.LostFocus      -= TextBox_LostFocus;
			AssociatedObject.PreviewKeyDown -= TextBox_PreviewKeyDown;

			this._listView.PreviewMouseLeftButtonDown -= ListBox_PreviewMouseLeftButtonDown;
			this._listView.PreviewMouseDoubleClick    -= ListBox_PreviewMouseDoubleClick;

			this._window   = null;
			this._popup    = null;
			this._listView = null;

			base.OnDetaching();
		}

		private void timer_Tick(object sender, EventArgs e)
		{
			this._timer.Stop();

			ShowAutocomplete(false);
		}

		private void Window_Deactivated(object sender, EventArgs eventArgs)
		{
			ClosePopup();
		}

		private void Window_LocationChanged(object sender, EventArgs eventArgs)
		{
			ClosePopup();
		}

		private void Window_PreviewMouseDown(object sender, MouseButtonEventArgs eventArgs)
		{
			if (eventArgs.Source != AssociatedObject)
			{
				ClosePopup();
			}
		}

		private void Window_StateChanged(object sender, EventArgs eventArgs)
		{
			ClosePopup();
		}

		private void TextBox_LostFocus(object sender, RoutedEventArgs eventArgs)
		{
			ClosePopup();
		}

		private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
		{
			bool backspaceORdel = e.Key == Key.Back || e.Key == Key.Delete;

			if (this._popup.IsOpen)
			{
				if (ProcessKey((char)e.Key, Keyboard.Modifiers))
				{
					e.Handled = true;
				}
				else
				{
					if (!backspaceORdel)
					{
						ResetTimer(1);
					}
					else
					{
						ResetTimer();

						if (!string.IsNullOrEmpty(AssociatedObject.SelectedText))
						{
							ClosePopup();
						}
					}
				}

				return;
			}

			if (!this._popup.IsOpen)
			{
				switch (e.Key)
				{
					case Key.Tab:
						if (!AssociatedObject.AcceptsTab)
						{
							this._timer.Stop();
							return;
						}
						break;
					case Key.Up:
					case Key.Down:
					case Key.PageUp:
					case Key.PageDown:
					case Key.Left:
					case Key.Right:
					case Key.End:
					case Key.Home:
					case Key.LeftCtrl:
					case Key.RightCtrl:
						{
							this._timer.Stop();
							return;
						}
				}

				if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.Space)
				{
					ShowAutocomplete(true);
					e.Handled = true;
					return;
				}
			}

			ResetTimer();
		}

		private void ListBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs eventArgs)
		{
			ListViewItem item = GetListViewItem(eventArgs.OriginalSource as DependencyObject);
			if (item != null)
			{
				item.IsSelected = true;
			}
		}

		private void ListBox_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			OnItemSelecting();

			e.Handled = true;

			AssociatedObject.Focus();
		}

		private void ResetTimer(int interval = -1)
		{
			int validInterval = interval <= 0 ? AppearInterval : interval;

			this._timer.Interval = TimeSpan.FromMilliseconds(validInterval);
			this._timer.Stop();
			this._timer.Start();
		}

		private ScrollViewer GetScrollViewer()
		{
			return GetVisualDescendants(this._listView)
				.OfType<ScrollViewer>()
				.FirstOrDefault();
		}

		private bool ProcessKey(char c, ModifierKeys modifiers)
		{
			int          pageHeight   = 1;
			ScrollViewer scrollViewer = GetScrollViewer();

			if (scrollViewer != null)
			{
				pageHeight = Math.Max(1, (int) scrollViewer.ViewportHeight);
			}

			if (modifiers == ModifierKeys.None)
			{
				switch ((Key)c)
				{
					case Key.Down:
						SelectNext(+1);
						return true;
					case Key.PageDown:
						SelectNext(+pageHeight);
						return true;
					case Key.Up:
						SelectNext(-1);
						return true;
					case Key.PageUp:
						SelectNext(-pageHeight);
						return true;
					case Key.Enter:
						OnItemSelecting();
						return true;
					case Key.Tab:
						if (!AcceptsTab)
						{
							break;
						}
						OnItemSelecting();
						return true;
					case Key.Left:
					case Key.Right:
						ClosePopup();
						return false;
					case Key.Escape:
						ClosePopup();
						return true;
				}
			}

			return false;
		}

		private void ClosePopup()
		{
			this._popup.IsOpen = false;
			this._forcedOpened = false;
		}

		private void SelectNext(int shift)
		{
			this._listView.SelectedIndex = Math.Max(
				0, 
				Math.Min(this._listView.SelectedIndex + shift, this._visibleItems.Count - 1)
			);

			this._listView.ScrollIntoView(this._listView.SelectedItem);
		}

		private void ShowAutocomplete(bool forced)
		{
			if (forced)
			{
				this._forcedOpened = true;
			}

			if (AssociatedObject.IsReadOnly)
			{
				ClosePopup();
				return;
			}

			if (!AssociatedObject.IsEnabled)
			{
				ClosePopup();
				return;
			}

			if (!this._forcedOpened && !AutoPopup)
			{
				ClosePopup();
				return;
			}

			//build list
			BuildAutocompleteList(this._forcedOpened);

			//show popup menu
			if (this._visibleItems.Count > 0)
			{
				if (forced && this._visibleItems.Count == 1 && this._listView.SelectedIndex == 0)
				{
					//do autocomplete if menu contains only one line and user press CTRL-SPACE
					OnItemSelecting();
					ClosePopup();
				}
				else
				{
					ShowMenu();
				}
			}
			else
			{
				ClosePopup();
			}
		}

		private void BuildAutocompleteList(bool forced)
		{
			List<AutocompleteItem> visibleItems = new List<AutocompleteItem>();

			bool foundSelected = false;
			int  selectedIndex = -1;

			//get fragment around the caret
			Range  fragment = GetFragment(SearchPattern);
			string text     = fragment.Text;

			if (AutocompleteItems != null)
			{
				if (forced || text.Length >= MinFragmentLength)
				{
					this._fragment = fragment;
					//build popup menu
					foreach (AutocompleteItem item in AutocompleteItems)
					{
						CompareResult res = item.Compare(fragment);
						if (res != CompareResult.Hidden)
						{
							visibleItems.Add(item);
						}
						if (res == CompareResult.VisibleAndSelected && !foundSelected)
						{
							foundSelected = true;
							selectedIndex = visibleItems.Count - 1;
						}
					}
				}
			}

			this._visibleItems.ReplaceRange(visibleItems);

			this._listView.SelectedIndex = foundSelected ? selectedIndex : 0;
		}

		private Range GetFragment(string searchPattern)
		{
			if (AssociatedObject.SelectionLength > 0)
			{
				return new Range(AssociatedObject);
			}

			string text   = AssociatedObject.Text;
			Regex  regex  = new Regex(searchPattern);
			Range  result = new Range(AssociatedObject);

			int startPos = AssociatedObject.SelectionStart;
			//go forward
			int i = startPos;

			while (i >= 0 && i < text.Length)
			{
				if (!regex.IsMatch(text[i].ToString()))
				{
					break;
				}

				i++;
			}

			result.End = i;

			//go backward
			i = startPos;

			while (i > 0 && (i - 1) < text.Length)
			{
				if (!regex.IsMatch(text[i - 1].ToString()))
				{
					break;
				}

				i--;
			}

			result.Start = i;

			return result;
		}

		private void ShowMenu()
		{
			if (!this._popup.IsOpen)
			{
				CancelEventArgs args = new CancelEventArgs();
				OnPopupOpening(args);
				if (!args.Cancel)
				{
					Point p = AssociatedObject.GetRectFromCharacterIndex(this._fragment.Start).BottomLeft;

					this._popup.HorizontalOffset = p.X;
					this._popup.VerticalOffset   = p.Y;

					this._popup.IsOpen = true;
					this._popup.Focus();
				}
			}
			else
			{
				this._popup.InvalidateVisual();
			}
		}

		private void OnItemSelecting()
		{
			int selectedIndex = this._listView.SelectedIndex;
			if (selectedIndex < 0 || selectedIndex >= this._visibleItems.Count)
			{
				return;
			}

			AutocompleteItem item = this._visibleItems[selectedIndex];
			SelectingItemEventArgs selectingItemArgs = new SelectingItemEventArgs
			{
				Item          = item,
				SelectedIndex = selectedIndex
			};

			OnItemSelecting(selectingItemArgs);

			if (selectingItemArgs.Cancel)
			{
				this._listView.SelectedIndex = selectingItemArgs.SelectedIndex;
				return;
			}

			if (!selectingItemArgs.Handled)
			{
				Range fragment = this._fragment;

				ApplyAutocomplete(item, fragment);
			}

			ClosePopup();

			SelectedItemEventArgs selectedItemArgs = new SelectedItemEventArgs
			{
				Item    = item,
				TextBox = AssociatedObject
			};

			item.OnSelected(selectedItemArgs);
			OnItemSelected(selectedItemArgs);
		}

		private void ApplyAutocomplete(AutocompleteItem item, Range fragment)
		{
			string newText = item.GetTextForReplace();

			//replace text of fragment
			fragment.Text = newText;
			fragment.TextBox.CaretIndex = fragment.Start + newText.Length;
			fragment.TextBox.Focus();
		}

		private void OnItemSelecting(SelectingItemEventArgs args)
		{
			ItemSelecting?.Invoke(this, args);
		}

		private void OnItemSelected(SelectedItemEventArgs args)
		{
			ItemSelected?.Invoke(this, args);
		}

		private void OnPopupOpening(CancelEventArgs args)
		{
			PopupOpening?.Invoke(this, args);
		}

		private void OnAutocompleteItemsChanged()
		{
			ClosePopup();
		}

		private static ListViewItem GetListViewItem(DependencyObject element)
		{
			if (element == null)
			{
				return null;
			}

			return GetSelfAndVisualAncestors(element)
				.OfType<ListViewItem>()
				.FirstOrDefault();
		}

		private static IEnumerable<DependencyObject> GetVisualDescendants(DependencyObject dependencyObject)
		{
			Check.NotNull(dependencyObject, nameof(dependencyObject));

			return TreeHelper.GetDescendants(dependencyObject, GetVisualChildren, true);
		}

		private static IEnumerable<DependencyObject> GetSelfAndVisualAncestors(DependencyObject dependencyObject)
		{
			Check.NotNull(dependencyObject, nameof(dependencyObject));

			return TreeHelper.GetSelfAndAncestors(
				dependencyObject,
				VisualTreeHelper.GetParent
			);
		}

		public static IEnumerable<DependencyObject> GetVisualChildren(DependencyObject dependencyObject)
		{
			Check.NotNull(dependencyObject, nameof(dependencyObject));

			return GetVisualChildrenImpl(dependencyObject);
		}

		private static IEnumerable<DependencyObject> GetVisualChildrenImpl(DependencyObject dependencyObject)
		{
			FrameworkElement frameworkElement = dependencyObject as FrameworkElement;
			frameworkElement?.ApplyTemplate();

			int count = VisualTreeHelper.GetChildrenCount(dependencyObject);
			for (int i = 0; i < count; ++i)
			{
				yield return VisualTreeHelper.GetChild(dependencyObject, i);
			}
		}
	}
}
