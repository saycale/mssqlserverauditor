﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Controls.TreeView;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Notification;
using MSSQLServerAuditor.Core.Reporting;
using MSSQLServerAuditor.Core.Reporting.Preprocessors;
using MSSQLServerAuditor.Core.Schedule;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Core.Xml;
using MSSQLServerAuditor.Tasks;
using MSSQLServerAuditor.Tree;
using MSSQLServerAuditor.ViewModels;

namespace MSSQLServerAuditor.Schedule
{
	public class GuiJobScheduler : AbstractJobScheduler
	{
		private readonly AuditForestViewModel _auditForest;
		private readonly IAppSettings         _appSettings;
		private readonly IPreprocessorLoader  _proprocessorLoader;

		private readonly NotificationService  _notificationService;

		public GuiJobScheduler(
			AuditForestViewModel auditForest,
			IAppSettings         appSettings,
			IPreprocessorLoader  proprocessorLoader
		)
		{
			Check.NotNull(auditForest,        nameof(auditForest));
			Check.NotNull(appSettings,        nameof(appSettings));
			Check.NotNull(proprocessorLoader, nameof(proprocessorLoader));

			this._auditForest          = auditForest;
			this._appSettings          = appSettings;
			this._proprocessorLoader   = proprocessorLoader;

			this._notificationService = CreateNotificationService();
		}

		public void ScheduleJobs(TemplateTreeRoot treeRoot)
		{
			AuditContext context = treeRoot.AuditTree.Context;
			
			ScheduleTree  scheduleTree = new ScheduleTree(treeRoot);
			List<JobInfo> jobs         = scheduleTree.GetScheduleJobs();

			foreach (JobInfo jobInfo in jobs)
			{
				ScheduleJob(context, jobInfo);
			}
		}

		protected override void ExecuteJob(AuditContext context, JobInfo jobInfo)
		{
			AuditTreeNode treeNode = FindTreeNode(context, jobInfo);
			if (treeNode == null)
			{
				treeNode = CreateTreeNode(context, jobInfo);
			}

			if (treeNode != null)
			{
				AuditTree tree = treeNode.AuditTree;

				TreeTaskInfo taskInfo = new TreeTaskInfo(treeNode)
				{
					FetchMode            = FetchMode.Remote,
					UpdateHierarchically = jobInfo.ScheduleDetails.UpdateHierarchically,
					UpdateDeep           = jobInfo.ScheduleDetails.UpdateDeep
				};

				if (jobInfo.ScheduleDetails?.SendMessage == true)
				{
					taskInfo.PostUpdateAction = token =>
						SendNotification(
							tree,
							jobInfo,
							token
						);
				}

				// request node update on the main application thread
				Dispatcher dispatcher = Application.Current?.Dispatcher;
				dispatcher?.Invoke(() =>
					tree.RequestUpdate(taskInfo)
				);
			}
		}

		private AuditTreeNode CreateTreeNode(AuditContext context, JobInfo jobInfo)
		{
			Dispatcher dispatcher = Application.Current?.Dispatcher;
			return dispatcher?.Invoke(() => 
				CreateStaticNode(context, jobInfo.TemplateNodeInfo)
			);
		}

		private AuditTreeNode CreateStaticNode(AuditContext context, TemplateNodeInfo tnInfo)
		{
			AuditTree tree = null;
			foreach (AdvTreeNode child in this._auditForest.Origin.Children)
			{
				TemplateTreeRoot root = child as TemplateTreeRoot;

				if (root == null)
				{
					continue;
				}

				if (root.AuditTree.Context == context)
				{
					tree = root.AuditTree;
				}
			}

			if (tree == null)
			{
				return null;
			}

			TreeNodeFactory factory = new TreeNodeFactory(context, tree);

			AuditTreeNode treeNode = new TemplateTreeNode(
				tnInfo,
				factory,
				tree
			);

			return treeNode;
		}

		private AuditTreeNode FindTreeNode(AuditContext context, JobInfo jobInfo)
		{
			TemplateNodeInfo tnInstance = jobInfo.TemplateNodeInfo;

			foreach (AdvTreeNode child in this._auditForest.Origin.Children)
			{
				TemplateTreeRoot root = child as TemplateTreeRoot;

				if (root == null)
				{
					continue;
				}

				if (root.AuditTree.Context != context)
				{
					continue;
				}

				List<TemplateTreeNode> templateChildren = root.Descendants()
					.OfType<TemplateTreeNode>()
					.ToList();

				foreach (TemplateTreeNode templateChild in templateChildren)
				{
					long childId = templateChild.TemplateNodeInfo.NodeInstanceId;
					if (childId == tnInstance.NodeInstanceId)
					{
						return templateChild;
					}
				}
			}

			return null;
		}

		protected override bool ShouldScheduleJob(JobInfo jobInfo)
		{
			ScheduleDetails settings = jobInfo.ScheduleDetails;

			if (settings.IsEnabled == true)
			{
				return !jobInfo.ScheduleDetails.ServiceOnly;
			}

			return false;
		}

		private NotificationService CreateNotificationService()
		{
			IPreprocessManager processorManager = new PreprocessManager();

			processorManager.AllPreprocessors.Add(new HtmlPreprocessor());

			NotificationService notificationService = new NotificationService(
				this._appSettings,
				this._proprocessorLoader,
				processorManager
			);

			return notificationService;
		}

		private void SendNotification(
			AuditTree         tree,
			JobInfo           jobInfo,
			CancellationToken token
		)
		{
			ScheduleDetails  settings = jobInfo.ScheduleDetails;
			TemplateNodeInfo tnInfo   = jobInfo.TemplateNodeInfo;

			Log.Debug($"Sending email notification to '{settings.MessageRecipients}' " +
			          $"(node id '{tnInfo.NodeInstanceId}', schedule id '{settings.Id}')...");
			
			ReportData reportData = tree.LoadResults(
				jobInfo.TemplateNodeInfo,
				token,
				settings.MessageLanguage
			);

			try
			{
				this._notificationService.SendNotification(
					jobInfo.ScheduleDetails.MessageRecipients,
					reportData
				);

				Log.Debug($"Email notification to '{jobInfo.ScheduleDetails.MessageRecipients}' " +
				          $"(node id '{tnInfo.NodeInstanceId}', schedule id '{settings.Id}') has been sent.");
			}
			catch (Exception exc)
			{
				Log.Error(exc, "Failed to send email notificaion.");
			}
		}
	}
}
