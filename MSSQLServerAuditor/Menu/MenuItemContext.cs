﻿using Caliburn.Micro;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Tree;
using MSSQLServerAuditor.ViewModels;

namespace MSSQLServerAuditor.Menu
{
	public class MenuItemContext
	{
		private readonly IShell _shell;

		public MenuItemContext(
			IShell        shell,
			AuditTreeNode node
		)
		{
			this._shell = shell;
			AuditNode   = node;
		}

		public AuditTreeNode AuditNode { get; }

		public IWindowManager  WindowManager => this._shell.WindowManager;
		public IAppSettings    AppSettings   => this._shell.AppSettings;
		public IWorkspace      Workspace     => this._shell.Workspace;

		public IStorageManager Storage       => this.AuditNode.AuditTree.StorageManager;
	}
}
