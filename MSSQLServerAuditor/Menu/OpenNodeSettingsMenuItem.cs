﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MSSQLServerAuditor.Common.Async;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Images;
using MSSQLServerAuditor.Resources;
using MSSQLServerAuditor.Tree;
using MSSQLServerAuditor.ViewModels;

namespace MSSQLServerAuditor.Menu
{
	public class OpenNodeSettingsMenuItem : ContextMenuItem
	{
		public override BitmapImage GetIcon(MenuItemContext context)
		{
			return StaticImages.Settings;
		}

		public override string GetHeader(MenuItemContext context)
		{
			return Strings.NodeSettings;
		}

		public override async Task ExecuteAsync(MenuItemContext context)
		{
			TemplateTreeNode treeNode = context.AuditNode as TemplateTreeNode;

			if (treeNode == null)
			{
				throw new ArgumentException(
					"Node settings menu is not implemented for tree node type: " + context.AuditNode.GetType()
				);
			}

			TemplateNodeInfo tnInfo = treeNode.TemplateNodeInfo;

			bool wasDisabled = tnInfo.IsDisabled;

			NodeSettingsViewModel dialog = new NodeSettingsViewModel(context.AppSettings, treeNode);

			bool ok = context.WindowManager.ShowDialog(dialog) ?? false;

			if (ok)
			{
				string    name       = dialog.NodeName;
				NamedIcon icon       = dialog.SelectedIcon;
				Color?    color      = dialog.SelectedColor;
				bool      deactivate = dialog.IsDeactivated;

				tnInfo.FontColor = color.HasValue 
					? new ColorConverter().ConvertToString(color.Value)
					: null;

				tnInfo.UIcon      = icon.Name;
				tnInfo.UName      = name;
				tnInfo.IsDisabled = deactivate;

				treeNode.IsActivated = !deactivate;
				treeNode.Refresh();

				treeNode.AuditTree.SaveNodeWithParams(tnInfo);

				if (wasDisabled != tnInfo.IsDisabled)
				{
					if (tnInfo.Children.Count > 0)
					{
						bool disableChild = deactivate;

						UpdateChildNodes(treeNode, disableChild);
					}
				}
			}

			await TaskConstants.Completed;
		}

		private void UpdateChildNodes(TemplateTreeNode treeNode, bool disable)
		{
			foreach (TemplateTreeNode childNode in treeNode.Children.OfType<TemplateTreeNode>())
			{
				TemplateNodeInfo tnChildInfo = childNode.TemplateNodeInfo;
				if (!tnChildInfo.IsDisabled)
				{
					childNode.IsActivated = !disable;
					childNode.Refresh();

					UpdateChildNodes(childNode, disable);
				}
			}
		}
	}
}
