﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using MSSQLServerAuditor.Controls.TreeView;
using MSSQLServerAuditor.Images;
using MSSQLServerAuditor.Resources;
using MSSQLServerAuditor.Tree;

namespace MSSQLServerAuditor.Menu
{
	public class CollapseSubtreeMenuItem : ContextMenuItem
	{
		public override async Task ExecuteAsync(MenuItemContext context)
		{
			AuditTreeNode auditNode = context.AuditNode;
			await Collapse(auditNode);
		}

		public override bool IsEnabled(MenuItemContext context)
		{
			AuditTreeNode auditNode = context.AuditNode;
			return auditNode.CanCollapse;
		}

		public override BitmapImage GetIcon(MenuItemContext context)
		{
			return StaticImages.Collapse;
		}

		public override string GetHeader(MenuItemContext context)
		{
			return Strings.CollapseHierarchically;
		}

		private async Task Collapse(AdvTreeNode node)
		{
			if (node.Children.Any())
			{
				IEnumerable<Task> collapseChildren = node.Children
					.Select(Collapse);

				await Task.WhenAll(collapseChildren);
			}

			await node.Collapse();
		}
	}
}
