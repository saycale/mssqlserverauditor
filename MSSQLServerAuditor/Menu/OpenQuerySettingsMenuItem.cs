﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using MSSQLServerAuditor.Common.Async;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Images;
using MSSQLServerAuditor.Resources;
using MSSQLServerAuditor.Tasks;
using MSSQLServerAuditor.Tree;
using MSSQLServerAuditor.ViewModels;

namespace MSSQLServerAuditor.Menu
{
	public class OpenQuerySettingsMenuItem : ContextMenuItem
	{
		public override BitmapImage GetIcon(MenuItemContext context)
		{
			return StaticImages.Filter;
		}

		public override string GetHeader(MenuItemContext context)
		{
			return Strings.QuerySettings;
		}

		public override async Task ExecuteAsync(MenuItemContext context)
		{
			TemplateTreeNode treeNode = context.AuditNode as TemplateTreeNode;

			if (treeNode == null)
			{
				throw new ArgumentException(
					"Node settings menu is not implemented for tree node type: " + context.AuditNode.GetType()
				);
			}

			AuditTree              auditTree = context.AuditNode.AuditTree;
			TemplateNodeInfo       tnInfo    = treeNode.TemplateNodeInfo;
			QuerySettingsViewModel dialog    = new QuerySettingsViewModel(tnInfo);

			dialog.ApplyClicked += (sender, args) =>
			{
				QuerySettingsViewModel dlg = sender as QuerySettingsViewModel;

				if (dlg != null)
				{
					CopyAndSaveParameters(auditTree, dialog.TemplateNodeInfo, tnInfo);

					RequestUpdate(context);
				}
				
			};

			bool ok = context.WindowManager.ShowDialog(dialog) ?? false;

			if (ok)
			{
				CopyAndSaveParameters(auditTree, dialog.TemplateNodeInfo, tnInfo);
			}

			await TaskConstants.Completed;
		}

		private void CopyAndSaveParameters(
			AuditTree        tree,
			TemplateNodeInfo src,
			TemplateNodeInfo dst
		)
		{
			CopyQueryParameters(src, dst);
			tree.SaveNodeWithParams(dst);
		}

		private void RequestUpdate(MenuItemContext context)
		{
			AuditTreeNode node = context.AuditNode;
			AuditTree     tree = node.AuditTree;

			TreeTaskInfo taskInfo = new TreeTaskInfo(node)
			{
				FetchMode            = FetchMode.Remote,
				UpdateHierarchically = false
			};

			tree.RequestUpdate(taskInfo);
		}

		private void CopyQueryParameters(
			TemplateNodeInfo source,
			TemplateNodeInfo dest)
		{
			foreach (TemplateNodeQueryInfo srcQuery in source.Queries)
			{
				TemplateNodeQueryInfo dstQuery = dest.Queries.FirstOrDefault(tnq => tnq.Id == srcQuery.Id);
				if (dstQuery != null)
				{
					CopyQueryParameters(srcQuery.ParameterValues, dstQuery.ParameterValues);
				}
			}

			foreach (TemplateNodeQueryInfo srcQuery in source.ConnectionQueries)
			{
				TemplateNodeQueryInfo dstQuery = dest.ConnectionQueries.FirstOrDefault(tnq => tnq.Id == srcQuery.Id);
				if (dstQuery != null)
				{
					CopyQueryParameters(srcQuery.ParameterValues, dstQuery.ParameterValues);
				}
			}

			foreach (TemplateNodeQueryInfo srcQuery in source.GroupQueries)
			{
				TemplateNodeQueryInfo dstQuery = dest.GroupQueries.FirstOrDefault(tnq => tnq.Id == srcQuery.Id);
				if (dstQuery != null)
				{
					CopyQueryParameters(srcQuery.ParameterValues, dstQuery.ParameterValues);
				}
			}
		}

		private void CopyQueryParameters(List<ParameterValue> srcList, List<ParameterValue> dstList)
		{
			foreach (ParameterValue srcParam in srcList)
			{
				ParameterValue dstParam = dstList.FirstOrDefault(p => p.Name == srcParam.Name);
				if (dstParam != null)
				{
					dstParam.UserValue = srcParam.UserValue;
				}
			}
		}
	}
}
