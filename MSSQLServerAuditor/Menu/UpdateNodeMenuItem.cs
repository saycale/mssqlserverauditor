﻿using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using MSSQLServerAuditor.Common.Async;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Images;
using MSSQLServerAuditor.Resources;
using MSSQLServerAuditor.Tasks;
using MSSQLServerAuditor.Tree;

namespace MSSQLServerAuditor.Menu
{
	public class UpdateNodeMenuItem : ContextMenuItem
	{
		public override BitmapImage GetIcon(MenuItemContext context)
		{
			return StaticImages.Refresh;
		}

		public override string GetHeader(MenuItemContext context)
		{
			return Strings.UpdateNode;
		}

		public override bool IsEnabled(MenuItemContext context)
		{
			TemplateTreeNode treeNode = context.AuditNode as TemplateTreeNode;
			if (treeNode != null)
			{
				return treeNode.IsActivated;
			}

			return base.IsEnabled(context);
		}

		public override async Task ExecuteAsync(MenuItemContext context)
		{
			AuditTreeNode node = context.AuditNode;
			AuditTree     tree = node.AuditTree;

			TreeTaskInfo taskInfo = new TreeTaskInfo(node)
			{
				FetchMode            = FetchMode.Remote,
				UpdateHierarchically = false
			};

			tree.RequestUpdate(taskInfo);

			await TaskConstants.Completed;
		}
	}
}
