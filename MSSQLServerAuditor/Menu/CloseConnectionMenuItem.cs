﻿using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using MSSQLServerAuditor.Common.Async;
using MSSQLServerAuditor.Images;
using MSSQLServerAuditor.Resources;
using MSSQLServerAuditor.Tree;
using MSSQLServerAuditor.ViewModels;
using NLog;

namespace MSSQLServerAuditor.Menu
{
	public class CloseConnectionMenuItem : ContextMenuItem
	{
		public static readonly Logger Log = LogManager.GetCurrentClassLogger();

		public override async Task ExecuteAsync(MenuItemContext context)
		{
			TemplateTreeRoot rootNode = context.AuditNode as TemplateTreeRoot;

			if (rootNode != null)
			{
				AuditTree    tree   = rootNode.AuditTree;
				IAuditForest forest = tree.AuditForest;

				if (forest != null)
				{
					await forest.CloseTreeAsync(rootNode);
				}
			}

			await TaskConstants.Completed;
		}

		public override bool IsEnabled(MenuItemContext context)
		{
			return context.AuditNode is TemplateTreeRoot;
		}

		public override BitmapImage GetIcon(MenuItemContext context)
		{
			return StaticImages.Close;
		}

		public override string GetHeader(MenuItemContext context)
		{
			return Strings.CloseConnection;
		}
	}
}
