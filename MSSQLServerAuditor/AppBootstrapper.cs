﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Threading;
using Caliburn.Micro;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Persistence.Providers;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Querying;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Core.Xml;
using MSSQLServerAuditor.Presentation;
using MSSQLServerAuditor.Presentation.Localization;
using MSSQLServerAuditor.Properties;
using MSSQLServerAuditor.Resources;
using MSSQLServerAuditor.ViewModels;
using MSSQLServerAuditor.Web;
using NLog;
using LogManager = NLog.LogManager;

namespace MSSQLServerAuditor
{
	public class AppBootstrapper : PresentationBootstrapper
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private SimpleContainer _container;

		private bool      _configured;
		private Exception _configurationFailure;

		private readonly SplashScreen _splashScreen = new SplashScreen(@"Resources\splashscreen.png");

		public AppBootstrapper()
		{
			Initialize();
		}

		protected override void Configure()
		{
			this._splashScreen.Show(false);
			Log.Info("Configuring application...");

			this._container = new SimpleContainer();

			try
			{
				IAppSettings          appSettings     = AppSettings.CreateDefault();
				StorageConnectionInfo connectionInfo  = StorageInfoProvider.GetConnectionInfo(appSettings);
				XmlLoader             xmlLoader       = XmlLoader.Create(appSettings, connectionInfo.StorageType);
				IStorageProvider      storageProvider = StorageProviders.CreateProvider(connectionInfo, xmlLoader);
				IStorageManager       storageManager  = storageProvider.CreateStorages();
				QueryExecutor         queryExecutor   = new QueryExecutor(appSettings, storageManager.StorageConnectionFactory, storageManager.QueryFormatter);
				WebServer             webServer       = WebServerStarter.StartServer(storageManager);

				this._container.RegisterInstance(typeof(IAppSettings),          null, appSettings);
				this._container.RegisterInstance(typeof(StorageConnectionInfo), null, connectionInfo);
				this._container.RegisterInstance(typeof(XmlLoader),             null, xmlLoader);
				this._container.RegisterInstance(typeof(IStorageManager),       null, storageManager);
				this._container.RegisterInstance(typeof(QueryExecutor),         null, queryExecutor);
				this._container.RegisterInstance(typeof(WebServer),             null, webServer);

				this._container.Singleton<IWindowManager,   WindowManager>();
				this._container.Singleton<IEventAggregator, EventAggregator>();

				this._container.PerRequest<IShell, ShellViewModel>();

				this._configured = true;
			}
			catch (Exception exc)
			{
				this._configurationFailure = exc;
				this._configured           = false;
			}
		}

		protected override object GetInstance(Type serviceType, string key)
		{
			return this._container.GetInstance(serviceType, key);
		}

		protected override IEnumerable<object> GetAllInstances(Type serviceType)
		{
			return this._container.GetAllInstances(serviceType);
		}

		protected override void BuildUp(object instance)
		{
			this._container.BuildUp(instance);
		}

		protected override void OnStartup(object sender, StartupEventArgs e)
		{
			if (!this._configured)
			{
				this._splashScreen.Close(TimeSpan.Zero);
				throw this._configurationFailure;
			}

			IAppSettings appSettings = this._container.GetInstance<IAppSettings>();

			CultureInfo culture = new CultureInfo(appSettings.User.UiLanguage);
			AppLocalizer.ChangeCulture(culture);
			Strings.Culture = culture;

			// apply current UI language as default language for views
			// see: http://www.amaravadee.com/how-to-set-and-change-the-culture-in-wpf/
			FrameworkElement.LanguageProperty.OverrideMetadata(
				typeof(FrameworkElement),
				new FrameworkPropertyMetadata(
					XmlLanguage.Empty,
					null,
					CoerceCurrentLang
				)
			);

			DisplayRootViewFor<IShell>();
			this._splashScreen.Close(TimeSpan.Zero);
		}

		private object CoerceCurrentLang(DependencyObject dependencyObject, object baseValue)
		{
			XmlLanguage lang    = baseValue as XmlLanguage;
			CultureInfo culture = CultureInfo.CurrentUICulture;

			return lang != null && lang.IetfLanguageTag.EqualsIgnoreCase(culture.Name)
				? lang
				: XmlLanguage.GetLanguage(culture.Name);
		}

		protected override void OnExit(object sender, EventArgs e)
		{
			try
			{
				Settings.Default.Save();
			}
			catch (Exception exc)
			{
				Log.Error(exc, "Failed to save settings");
			}
		}

		protected override void OnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
		{
			Exception exception = e.Exception;
			if (exception is TargetInvocationException)
			{
				exception = exception.InnerException;
			}

			HandleException(exception, false);
			e.Handled = true;
		}

		private void HandleException(Exception exc, bool isTerminating)
		{
			if (exc != null)
			{
				Log.Error(exc);

				if (!isTerminating)
				{
					MessageBox.Show(
						exc.Message,
						Strings.ApplicationError,
						MessageBoxButton.OK,
						MessageBoxImage.Error
					);

					if (!this._configured)
					{
						Application.Shutdown();
					}
				}
			}
		}
	}
}
