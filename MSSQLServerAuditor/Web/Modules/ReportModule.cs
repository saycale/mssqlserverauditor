﻿using System.IO;
using MSSQLServerAuditor.Core.Persistence.Storages;
using Nancy;
using NLog;

namespace MSSQLServerAuditor.Web.Modules
{
	public class ReportModule : NancyModule
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly IStorageManager _storageManager;

		public ReportModule(IStorageManager storageManager) : base("/result")
		{
			this._storageManager = storageManager;

			Get["{url*}"] = parameters =>
			{
				string url       = parameters.url;
				string extension = Path.GetExtension(url);

				byte[] reportBytes = this._storageManager.FsStorage.ReadReport(
					url.Replace('/', Path.DirectorySeparatorChar)
				);
				
				return Response.FromStream(
					new MemoryStream(reportBytes),
					MimeTypes.GetMimeType(extension)
				);
			};
		}
	}
}
