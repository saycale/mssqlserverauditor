﻿using Nancy;

namespace MSSQLServerAuditor.Web.Modules
{
	public class ResourceModule : NancyModule
	{
		public ResourceModule() : base("/file")
		{
			Get["{file*}"] = parameters =>
			{
				string path = string.Format("js/{0}", parameters.file);
				return Response.AsFile(path);
			};
		}
	}
}
