﻿using System;
using MSSQLServerAuditor.Core.Persistence.Storages;
using Nancy.Bootstrapper;
using Nancy.Hosting.Self;
using NLog;

namespace MSSQLServerAuditor.Web
{
	public class WebServer : IDisposable
	{
		private const string HostAddressPattern = "http://localhost:{0}";

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly NancyHost _host;

		public WebServer(IStorageManager storageManager, int port)
		{
			string hostAddress = string.Format(HostAddressPattern, port);

			HostPort = port;
			HostUri  = new Uri(hostAddress);

			INancyBootstrapper bootstrapper = new WebServerBootstrapper(storageManager);
			HostConfiguration  hostConfig   = new HostConfiguration
			{
				RewriteLocalhost = false
			};

			this._host = new NancyHost(bootstrapper, hostConfig, HostUri);
		}

		public void Start()
		{
			this._host.Start();
		}

		public void Stop()
		{
			this._host.Stop();
		}

		public void Dispose()
		{
			this._host.Dispose();
		}

		public int HostPort { get; private set; }
		public Uri HostUri  { get; private set; }
	}
}
