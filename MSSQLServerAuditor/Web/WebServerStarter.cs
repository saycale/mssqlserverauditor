﻿using System;
using MSSQLServerAuditor.Core.Persistence.Storages;
using NLog;

namespace MSSQLServerAuditor.Web
{
	public static class WebServerStarter
	{
		private const int DefaultWebServerPort = 2568;

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		public static WebServer StartServer(IStorageManager storageManager)
		{
			const int maxAttempts = 10;

			Random random   = new Random();
			int    hostPort = DefaultWebServerPort;
			
			for (int i = 0; i < maxAttempts; i++)
			{
				try
				{
					WebServer webServer = new WebServer(storageManager, hostPort);
					webServer.Start();

					Log.Info($"Web-server has started at port: {hostPort}");

					return webServer;
				}
				catch (Exception ex)
				{
					Log.Error(ex, $"Unable to start web-server at port: {hostPort}");
				}

				hostPort = 10000 + random.Next(50000);
			}

			throw new ApplicationException($"Failed to start web-server for '{maxAttempts}' times");
		}
	}
}
