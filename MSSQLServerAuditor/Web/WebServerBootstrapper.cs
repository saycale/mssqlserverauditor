﻿using MSSQLServerAuditor.Core.Persistence.Storages;
using Nancy;
using Nancy.Conventions;
using Nancy.TinyIoc;

namespace MSSQLServerAuditor.Web
{
	public class WebServerBootstrapper : DefaultNancyBootstrapper
	{
		private readonly IStorageManager _storageManager;

		public WebServerBootstrapper(IStorageManager storageManager)
		{
			this._storageManager = storageManager;
		}

		protected override void ConfigureRequestContainer(TinyIoCContainer container, NancyContext context)
		{
			container.Register((x, options) => this._storageManager);
		}

		protected override void ConfigureConventions(NancyConventions nancyConventions)
		{
			nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("js", @"js"));
			base.ConfigureConventions(nancyConventions);
		}
	}
}
