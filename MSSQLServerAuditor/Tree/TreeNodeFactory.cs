﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Schedule;
using MSSQLServerAuditor.Core.Session;
using NLog;

namespace MSSQLServerAuditor.Tree
{
	public class TreeNodeFactory
	{
		public static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly AuditContext _auditContext;
		private readonly AuditTree    _auditTree;

		public TreeNodeFactory(
			AuditContext auditContext,
			AuditTree    auditTree)
		{
			Check.NotNull(auditContext, nameof(auditContext));
			Check.NotNull(auditTree,    nameof(auditTree));

			this._auditContext = auditContext;
			this._auditTree    = auditTree;
		}

		public List<AuditTreeNode> FetchChildren(
			TemplateNodeInfo  tnInfo,
			FetchMode         fetchMode,
			CancellationToken cancelToken
		)
		{
			List<AuditTreeNode> children = new List<AuditTreeNode>();

			if (tnInfo.IsDisabled)
			{
				fetchMode = FetchMode.Internal;
			}

			bool updated = false;
			if (fetchMode == FetchMode.Internal || fetchMode == FetchMode.Combined)
			{
				updated = this._auditTree.UpdateChildren(tnInfo);
			}

			if (updated || fetchMode == FetchMode.Internal)
			{
				foreach (TemplateNodeInfo childInfo in tnInfo.Children)
				{
					CheckAndRunTemplateSchedules(childInfo);
					this._auditTree.LoadNodeSettings(childInfo);

					AuditTreeNode staticChild = CreateStaticNode(childInfo);
					children.Add(staticChild);
				}
			}
			else
			{
				tnInfo.Children.Clear();
				tnInfo.Children.AddRange(
					 tnInfo.Template.Children
						.Where(templateNode => string.IsNullOrWhiteSpace(templateNode.GroupSelectId))
						.Select(templateNode =>
							templateNode.Instantiate(null, tnInfo)
						)
					);

				if (!tnInfo.IsSaved)
				{
					this._auditTree.SaveNode(tnInfo);
				}

				this._auditTree.DisableChildren(tnInfo);
				children.AddRange(InitializeStaticNodes(tnInfo));

				if (tnInfo.HasDynamicChildren())
				{
					children.AddRange(
						CreateDynamicNodes(tnInfo, cancelToken)
					);
				}

				this._auditTree.SetChildrenProcessed(tnInfo);
			}

			return children;
		}

		public IEnumerable<AuditTreeNode> InitializeStaticNodes(TemplateNodeInfo tnInfo)
		{
			foreach (TemplateNodeInfo childInfo in tnInfo.Children)
			{
				if (!childInfo.IsSaved)
				{
					this._auditTree.SaveNode(childInfo);
				}

				CheckAndRunTemplateSchedules(childInfo);

				this._auditTree.LoadNodeSettings(childInfo);

				yield return CreateStaticNode(childInfo);
			}
		}

		private void CheckAndRunTemplateSchedules(TemplateNodeInfo childInfo)
		{
			List<JobInfo> jobs = GetTemplateSchedules(childInfo);
			if (jobs.Any())
			{
				foreach (JobInfo jobInfo in jobs)
				{
					this._auditTree.AuditForest.JobScheduler.ScheduleJob(
						this._auditContext,
						jobInfo
					);
				}
			}
		}

		public IEnumerable<AuditTreeNode> CreateDynamicNodes(TemplateNodeInfo parentNodeInfo, CancellationToken token)
		{
			List<AuditTreeNode> dynamicNodes = new List<AuditTreeNode>();

			foreach (TemplateNodeQueryInfo queryInfo in parentNodeInfo.GroupQueries)
			{
				List<TemplateNodeInfo> replicaTemplates = parentNodeInfo.GetGroupQueryNodes(queryInfo);
				List<AuditSession>     sessions         = this._auditContext.CreateSessions();

				lock (parentNodeInfo.Children)
				{
					parentNodeInfo.Children.RemoveAll(tn => tn.ReplicaQuery == queryInfo);
				}

				foreach (AuditSession auditSession in sessions)
				{
					List<NodeAttributes> dynamicNodesAttributes = this._auditTree
						.QueryDynamicNodes(auditSession, queryInfo, token);

					foreach (NodeAttributes dynamicNodeAttributes in dynamicNodesAttributes)
					{
						foreach (TemplateNodeInfo replicaTemplate in replicaTemplates)
						{
							AuditTreeNode dynamicNode = ReplicateNode(
								parentNodeInfo,
								replicaTemplate,
								dynamicNodeAttributes,
								queryInfo
							);

							dynamicNodes.Add(dynamicNode);
						}
					}
				}
			}

			return dynamicNodes;
		}

		private AuditTreeNode CreateStaticNode(TemplateNodeInfo tnInfo)
		{
			AuditTreeNode treeNode = new TemplateTreeNode(
				tnInfo,
				this,
				this._auditTree
			);

			return treeNode;
		}

		private List<JobInfo> GetTemplateSchedules(TemplateNodeInfo tnInfo)
		{
			List<ScheduleSettings> scheduleSettings = this._auditTree.StorageManager
				.MainStorage.Settings.GetScheduleSettings(tnInfo);

			List<JobInfo> jobs = new List<JobInfo>();

			List<ScheduleSettings> templateSchedules = tnInfo.RefreshSchedules;
			if (!templateSchedules.IsNullOrEmpty())
			{
				foreach (ScheduleSettings scheduleOrig in templateSchedules.Where(ts => ts != null))
				{
					try
					{
						// check if the schedule is already saved
						ScheduleSettings storedSchedule = scheduleSettings.FirstOrDefault(s => s.UId == scheduleOrig.UId);

						if (storedSchedule == null)
						{
							// save template schedule to database and dispatch it to the scheduler
							ScheduleSettings schedule = scheduleOrig.Clone();

							schedule.NodeInstanceId = tnInfo.NodeInstanceId;

							this._auditTree.StorageManager.MainStorage.Settings
								.SaveScheduleSettings(tnInfo, schedule);

							ScheduleDetails details = ScheduleDetails.Create(
								tnInfo,
								this._auditContext,
								schedule
							);

							JobInfo job = this._auditTree.StorageManager.MainStorage.Settings
								.CreateJob(tnInfo, details);

							jobs.Add(job);
						}
					}
					catch (Exception exc)
					{
						Log.Error(exc, "Error creating schedule from xml template");
					}
				}
			}

			return jobs;
		}

		private AuditTreeNode ReplicateNode(
			TemplateNodeInfo      parentNode,
			TemplateNodeInfo      replicaTemplate,
			NodeAttributes        nodeAttributes,
			TemplateNodeQueryInfo replicaQuery)
		{
			TemplateNodeInfo replicaNode = replicaTemplate.Replicate(
				nodeAttributes,
				replicaQuery,
				parentNode
			);

			lock (parentNode.Children)
			{
				parentNode.Children.Add(replicaNode);
			}

			if (!replicaNode.IsSaved)
			{
				this._auditTree.SaveNode(replicaNode);
			}

			this._auditTree.LoadNodeSettings(replicaNode);

			AuditTreeNode dynamicNode = new TemplateTreeNode(
				replicaNode,
				this,
				this._auditTree
			);

			return dynamicNode;
		}
	}
}
