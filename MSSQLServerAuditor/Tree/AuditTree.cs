﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using MSSQLServerAuditor.Common;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Persistence;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Querying;
using MSSQLServerAuditor.Core.Querying.Data;
using MSSQLServerAuditor.Core.Reporting;
using MSSQLServerAuditor.Core.Reporting.Preprocessors;
using MSSQLServerAuditor.Core.Schedule;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Core.Tasks;
using MSSQLServerAuditor.Core.Xml;
using MSSQLServerAuditor.Images;
using MSSQLServerAuditor.Reporting;
using MSSQLServerAuditor.Schedule;
using MSSQLServerAuditor.Tasks;
using MSSQLServerAuditor.Utils;
using MSSQLServerAuditor.ViewModels;
using NLog;

namespace MSSQLServerAuditor.Tree
{
	public class AuditTree
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private const string LangDefault = "en";

		private readonly IAppSettings        _settings;
		private readonly AuditContext        _auditContext;
		private readonly QueryExecutor       _queryExecutor;
		private readonly XmlLoader           _xmlLoader;
		private readonly IStorageManager     _storageManager;
		private readonly IQueueTaskManager   _taskManager;
		private readonly Uri                 _hostUri;
		private readonly TemplateInfo        _templateInfo;
		private readonly IPreprocessManager  _preprocessManager;
		private readonly AuditCoordinator    _auditCoordinator;

		public AuditTree(
			IAppSettings      settings,
			AuditContext      auditContext,
			QueryExecutor     queryExecutor,
			XmlLoader         xmlLoader,
			IStorageManager   storageManager,
			IQueueTaskManager taskManager,
			IAuditForest      forest,
			Uri               hostUri,
			TemplateInfo      templateInfo)
		{
			Check.NotNull(settings,       nameof(settings));
			Check.NotNull(auditContext,   nameof(auditContext));
			Check.NotNull(queryExecutor,  nameof(queryExecutor));
			Check.NotNull(xmlLoader,      nameof(xmlLoader));
			Check.NotNull(storageManager, nameof(storageManager));
			Check.NotNull(taskManager,    nameof(taskManager));
			Check.NotNull(forest,         nameof(forest));
			Check.NotNull(templateInfo,   nameof(templateInfo));

			this._settings       = settings;
			this._auditContext   = auditContext;
			this._queryExecutor  = queryExecutor;
			this._xmlLoader      = xmlLoader;
			this._storageManager = storageManager;
			this._taskManager    = taskManager;
			this._hostUri        = hostUri;
			this._templateInfo   = templateInfo;
			this.AuditForest     = forest;

			this._preprocessManager = CreatePreprocessManager();

			this._auditCoordinator = new AuditCoordinator(
				storageManager,
				queryExecutor,
				xmlLoader
			);

			this.Root = CreateRoot();
		}

		public IAuditForest     AuditForest   { get; private set; }
		public TemplateTreeRoot Root          { get; private set; }
		
		public IStorageManager StorageManager => this._storageManager;
		public MainStorage     MainStorage    => this._storageManager.MainStorage;
		public AuditContext    Context        => this._auditContext;
		public IAppSettings    Settings       => this._settings;
		public XmlLoader       XmlLoader      => this._xmlLoader;

		public async Task<XmlDocument> ExecuteQuery(TemplateNodeInfo tnInfo, CancellationToken token)
		{
			// query remote source
			XmlDocument xml = await Task.Run(() =>
			{
				// execute node's queries and save results
				QueryResultSet results = this._auditCoordinator.ProcessQueries(
					this._auditContext,
					tnInfo,
					token
				);

				return Transform(results);
			}, token);

			return xml;
		}

		public bool ShowXmlTab => this._settings.User.ShowXml;

		public string Title => FormatTitle(this._templateInfo.WindowTitle);

		public string FormatTitle(List<Translation> translations)
		{
			string uiLang = this._settings.User.UiLanguage ?? LangDefault;

			Translation translation = translations.FirstOrDefault(t => t.Language == uiLang);

			if (translation != null)
			{
				string pattern = translation.Text.TrimmedOrEmpty();

				TitleFormatter formatter = new TitleFormatter(
					Context,
					this._templateInfo
				);

				return formatter.Format(pattern, uiLang).TrimmedOrEmpty();
			}

			return null;
		}

		public void RequestUpdate(TreeTaskInfo taskInfo)
		{
			TreeTask task = new TreeTask(taskInfo);

			this._taskManager.Enqueue(task);
		}

		public async Task<ReportData> LoadResultsAsync(TemplateNodeInfo tnInfo, CancellationToken cancelToken)
		{
			// query remote source
			ReportData reportData = await Task.Run(
				() => LoadResults(tnInfo, cancelToken),
				cancelToken
			);

			return reportData;
		}

		public ReportData LoadResults(TemplateNodeInfo tnInfo, CancellationToken cancelToken, string reportLang = null)
		{
			QueryResultReader resultReader = new QueryResultReader(
				this._storageManager,
				this._auditCoordinator,
				this._xmlLoader
			);

			QueryResultSet results = resultReader
				.LoadResults(this._auditContext, tnInfo);

			cancelToken.ThrowIfCancellationRequested();

			XmlDocument xmlDoc    = Transform(results, reportLang);
			XmlReport   xmlReport = new XmlReport(results, xmlDoc);

			ReportData data = new ReportData
			{
				XmlReport         = xmlReport,
				DateTime          = xmlReport.SourceData.UpdateTime,
				Duration          = xmlReport.SourceData.UpdateDuration,
				TemplateNodeInfo  = tnInfo,
				ConnectionGroupId = this._auditContext.ConnectionGroup.Id
			};

			cancelToken.ThrowIfCancellationRequested();

			PrepareReport(data);

			return data;
		}

		public List<NodeAttributes> QueryDynamicNodes(
			AuditSession          session,
			TemplateNodeQueryInfo tnQueryInfo,
			CancellationToken     token)
		{
			return this._auditCoordinator.QueryDynamicNodes(
				session,
				tnQueryInfo,
				token
			);
		}

		public NamedIcon GetNamedIcon(TemplateNodeInfo tnInfo)
		{
			return GetNamedIcon(tnInfo.Attributes.Icon) ?? GetNamedIcon(tnInfo.DefaultIcon);
		}

		public NamedIcon GetNamedIcon(string iconName)
		{
			return Icons.GetIcon(iconName);
		}

		public string GetUiText(List<Translation> translations)
		{
			string      uiLang      = this._settings.User.UiLanguage ?? LangDefault;
			Translation translation = translations.FirstOrDefault(t => t.Language == uiLang);

			if (translation != null)
			{
				return translation.Text.TrimmedOrEmpty();
			}

			return string.Empty;
		}

		public async Task<DateTime?> GetNextUpdateTimeAsync(
			TemplateNodeInfo  tnInfo,
			CancellationToken cancelToken)
		{
			List<JobInfo> updateJobs = await Task.Run(
				() => MainStorage.Settings.GetJobs(tnInfo),
				cancelToken
			);

			return GetNextUpdateTime(updateJobs);
		}

		public DateTime? GetNextUpdateTime(List<JobInfo> jobs)
		{
			DateTime? nextUpdate = null;
			
			GuiJobScheduler scheduler = AuditForest.JobScheduler;
			foreach (JobInfo updateJob in jobs)
			{
				DateTime nextDate = scheduler.GetNextFireTime(Context, updateJob);

				if (nextDate != DefaultValues.Date.Minimum)
				{
					if (nextUpdate != null)
					{
						if (nextDate.CompareTo(nextUpdate) < 0)
						{
							nextUpdate = nextDate;
						}
					}
					else
					{
						nextUpdate = nextDate;
					}
				}
			}

			return nextUpdate;
		}

		public void PrepareReport(ReportData reportData)
		{
			TemplateNodeInfo nodeInfo = reportData.TemplateNodeInfo;

			string procFile = nodeInfo.ProcessorFile;
			if (procFile.IsNullOrEmpty())
			{
				return;
			}

			string procPath = Path.Combine(
				this._settings.System.TemplateDirectory,
				"Templates",
				procFile
			);

			List<PreprocessorArea> preprocessorAreas =
				this._xmlLoader.LoadPreprocessors(procPath);

			foreach (PreprocessorArea area in preprocessorAreas)
			{
				IPreprocessor handler = this._preprocessManager
					.ResolvePreprocessor(area.Preprocessor);

				if (handler != null && handler.IsEnabled(this._settings))
				{
					foreach (PreprocessorConfig preprocessor in area.PreprocessorsConfigs)
					{
						handler.Prepare(reportData, preprocessor);
					}
				}
			}
		}

		public List<TabViewModel> CreateReportTabs(ReportData reportData)
		{
			List<TabViewModel> tabs = new List<TabViewModel>();

			TemplateNodeInfo nodeInfo = reportData.TemplateNodeInfo;

			string procFile = nodeInfo.ProcessorFile;
			if (procFile.IsNullOrEmpty())
			{
				return tabs;
			}

			string procPath = Path.Combine(
				this._settings.System.TemplateDirectory,
				"Templates",
				procFile
			);

			List<PreprocessorArea> preprocessorAreas =
				this._xmlLoader.LoadPreprocessors(procPath);

			foreach (PreprocessorArea area in preprocessorAreas)
			{
				IPreprocessor handler = this._preprocessManager
					.ResolvePreprocessor(area.Preprocessor);

				if (handler != null && handler.IsEnabled(this._settings))
				{
					TabViewModel tab = new TabViewModel
					{
						VisualReport = new CompositeGridReport(area, handler, reportData),
						Header       = handler.GetTitle(reportData, area)
					};

					tabs.Add(tab);
				}
			}

			return tabs;
		}

		public bool UpdateChildren(TemplateNodeInfo tnInfo)
		{
			if (!tnInfo.IsSaved)
			{
				MainStorage.Nodes.SaveNode(
					Context,
					tnInfo
				);
			}

			bool? childrenNotProcessed = MainStorage.Nodes
				.AreChildrenNotProcessed(tnInfo.NodeInstanceId);

			if (childrenNotProcessed ?? true)
			{
				return false;
			}

			MainStorage.Nodes.LoadChildren(tnInfo);

			Dictionary<string, TemplateNodeInfo> absent = new Dictionary<string, TemplateNodeInfo>();

			foreach (TemplateNodeInfo child in tnInfo.Template.Children.Where(n => string.IsNullOrWhiteSpace(n.GroupSelectId)))
			{
				absent[child.Id] = child;
			}

			foreach (TemplateNodeInfo child in tnInfo.Children)
			{
				absent.Remove(child.Template.Id);
			}

			if (absent.Any())
			{
				Dictionary<TemplateNodeInfo, int> templateIndexes = new Dictionary<TemplateNodeInfo, int>();

				for (int i = 0; i < tnInfo.Template.Children.Count; i++)
				{
					templateIndexes[tnInfo.Template.Children[i]] = i;
				}

				List<TemplateNodeInfo> newStatics = absent.Values
					.Select(t => t.Instantiate(null, tnInfo))
					.ToList();

				List<TemplateNodeInfo> oldChildren = tnInfo.Children.ToList();

				IOrderedEnumerable<TemplateNodeInfo> newList = oldChildren
					.Union(newStatics)
					.OrderBy(n => templateIndexes[n.Template]);

				tnInfo.Children.Clear();
				tnInfo.Children.AddRange(newList);

				// update old static children
				SaveChildren(tnInfo, oldChildren);

				// save new static nodes
				MainStorage.Nodes.SaveNodes(Context, newStatics);
			}

			return true;
		}

		public void SaveChildren(
			TemplateNodeInfo       parent,
			List<TemplateNodeInfo> childrenToSave
		)
		{
			if (childrenToSave.Count == 0)
			{
				return;
			}

			MainStorage.Nodes.DisableMissingChildren(
				parent.NodeInstanceId,
				childrenToSave
			);

			MainStorage.Nodes.SaveNodes(Context, childrenToSave);

			MainStorage.Nodes.SetChildrenNotProcessed(
				parent.NodeInstanceId,
				false
			);
		}

		public void SaveNode(TemplateNodeInfo tnInfo)
		{
			MainStorage.Nodes.SaveNode(
				Context,
				tnInfo
			);
		}

		public void DisableChildren(TemplateNodeInfo tnInfo)
		{
			MainStorage.Nodes.DisableChildren(tnInfo.NodeInstanceId);

			MainStorage.Nodes.SetChildrenNotProcessed(
				tnInfo.NodeInstanceId,
				false
			);
		}

		public void SetChildrenProcessed(TemplateNodeInfo tnInfo)
		{
			MainStorage.Nodes.SetChildrenNotProcessed(
				tnInfo.NodeInstanceId,
				false
			);
		}

		public void SaveNodeWithParams(TemplateNodeInfo tnInfo)
		{
			MainStorage.Nodes.SaveNodeWithParams(Context, tnInfo, this._settings.User.UiLanguage);
		}

		public List<ScheduleSettings> LoadSchedules(TemplateNodeInfo tnInfo)
		{
			return MainStorage.Settings.GetScheduleSettings(tnInfo);
		}

		public async Task SaveSchedulesAsync(TemplateNodeInfo tnInfo, List<ScheduleSettings> updatedSettings)
		{
			await Task.Run(() =>
				MainStorage.Settings.SaveScheduleSettings(tnInfo, updatedSettings)
			);
		}

		public bool HasSchedules(TemplateNodeInfo tnInfo)
		{
			return MainStorage.Settings.HasSchedules(tnInfo);
		}

		public async Task<bool> HasSchedulesAsync(TemplateNodeInfo tnInfo)
		{
			return await Task.Run(() =>
				MainStorage.Settings.HasSchedules(tnInfo)
			);
		}

		public void LoadNodeSettings(TemplateNodeInfo tnInfo)
		{
			MainStorage.Nodes.LoadNodeSettings(
				Context,
				tnInfo,
				this._settings.User.UiLanguage
			);

			tnInfo.HasActiveSchedules = HasActiveSchedules(tnInfo);
		}

		public bool HasActiveSchedules(TemplateNodeInfo tnInfo)
		{
			return MainStorage.Settings.HasActiveSchedules(tnInfo);
		}

		public async Task<bool> HasActiveSchedulesAsync(TemplateNodeInfo tnInfo)
		{
			return await Task.Run(() =>
				MainStorage.Settings.HasActiveSchedules(tnInfo)
			);
		}

		private IPreprocessManager CreatePreprocessManager()
		{
			IPreprocessManager formPreprocessManager = new PreprocessManager();

			formPreprocessManager.AllPreprocessors.Add(new WebPreprocessor(this._hostUri.AbsoluteUri, this._storageManager));
			formPreprocessManager.AllPreprocessors.Add(new HtmlPreprocessor());

			return formPreprocessManager;
		}

		
		private TemplateTreeRoot CreateRoot()
		{
			TreeNodeFactory factory = new TreeNodeFactory(
				Context,
				this
			);

			TemplateTreeRoot treeRoot = new TemplateTreeRoot(
				this._templateInfo,
				factory,
				this
			);

			return treeRoot;
		}

		private XmlDocument Transform(QueryResultSet resultSet, string lang = null)
		{
			if (lang == null)
			{
				lang = this._settings.User.ReportLanguage ?? LangDefault;
			}

			XmlTransformer transformer = new XmlTransformer(lang);

			return transformer.Transform(resultSet);
		}
	}
}
