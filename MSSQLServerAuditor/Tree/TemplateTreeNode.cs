﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;
using MSSQLServerAuditor.Common.Async;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Common.Utils;
using MSSQLServerAuditor.Core.Cryptography;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Reporting;
using MSSQLServerAuditor.Images;
using MSSQLServerAuditor.Infrastructure;
using MSSQLServerAuditor.Menu;
using MSSQLServerAuditor.Processing;
using MSSQLServerAuditor.Reporting;
using MSSQLServerAuditor.Resources;
using MSSQLServerAuditor.Tasks;
using MSSQLServerAuditor.ViewModels;
using NLog;

namespace MSSQLServerAuditor.Tree
{
	public class TemplateTreeNode : AuditTreeNode
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly TemplateNodeInfo _tnInfo;
		private readonly TreeNodeFactory  _factory;
		private readonly ThreadingSupport _threading;

		private bool _isActivated;

		public TemplateTreeNode(
			TemplateNodeInfo tnInfo,
			TreeNodeFactory  factory,
			AuditTree        auditTree
		) : base(auditTree)
		{
			Check.NotNull(tnInfo,  nameof(tnInfo));
			Check.NotNull(factory, nameof(factory));
			
			this._tnInfo  = tnInfo;
			this._factory = factory;
			
			this.LazyLoading  = true;
			this._threading   = new ThreadingSupport(AuditTree);
			this._isActivated = !tnInfo.IsDisabled;
		}

		protected override async Task LoadChildrenAsync(FetchMode fetchMode, CancellationToken token)
		{
			if (HasChildren)
			{
				await this._threading.LoadChildrenAsync(this, fetchMode, token, FetchChildren);
			}
		}

		protected override async Task LoadChildrenOnExpandAsync(CancellationToken token)
		{
			if (!token.IsCancellationRequested)
			{
				TreeTaskInfo taskInfo = new TreeTaskInfo(this)
				{
					FetchMode            = FetchMode.Combined,
					UpdateHierarchically = false,
					Update               = false,
					Expand               = true
				};

				AuditTree.RequestUpdate(taskInfo);
			}

			await TaskConstants.Completed;
		}

		public override bool CanExpand    => !IsExpanded && HasChildren;
		public override bool CanCollapse  => IsExpanded;
		public override bool ShowExpander => !LazyLoading ? Children.Any() : HasChildren;

		public bool IsActivated
		{
			get { return this._isActivated; }
			set
			{
				if (this._isActivated != value)
				{
					this._isActivated = value;
					NotifyOfPropertyChange(() => IsActivated);
				}
			}
		}

		public override Brush Foreground
		{
			get
			{
				if (!IsActivated)
				{
					return Brushes.Gray;
				}

				string colorHex = this._tnInfo.FontColor;
				if (string.IsNullOrWhiteSpace(colorHex))
				{
					return Brushes.Black;
				}

				try
				{
					return ColorUtils.GetBrush(colorHex);
				}
				catch (Exception exc)
				{
					Log.Error(exc, $"Error parsing font color '{colorHex}' of the node '{Text}'");
					return Brushes.Black;
				}
			}
		}

		public TemplateNodeInfo TemplateNodeInfo => this._tnInfo;

		private IEnumerable<AuditTreeNode> FetchChildren(FetchMode fetchMode, CancellationToken cancelToken)
		{
			IEnumerable<AuditTreeNode> children = this._factory.FetchChildren(
				this._tnInfo,
				fetchMode,
				cancelToken
			);

			if (!IsActivated)
			{
				foreach (TemplateTreeNode childNode in children.OfType<TemplateTreeNode>())
				{
					childNode.IsActivated = false;
				}
			}

			return children;
		}

		public override object Text =>
			this._tnInfo.Attributes.UName ?? StringUtils.FirstNonEmpty(
				AuditTree.GetUiText(this._tnInfo.Title),
				this._tnInfo.Name
			);

		public override object AuxText =>
			this._tnInfo.Counter != null ? $"({this._tnInfo.Counter})" : string.Empty;

		public override Brush AuxForeground => Brushes.DodgerBlue;

		public override object Icon => AuditTree.GetNamedIcon(this._tnInfo)?.Image;

		public override object OverlayIcon => this._tnInfo.HasActiveSchedules 
			? StaticImages.Clock 
			: null;

		public override List<ContextMenuItem> CreateMenuItems()
		{
			List<ContextMenuItem> menuItems = new List<ContextMenuItem>
			{
				new UpdateNodeMenuItem(),
				new UpdateSubtreeMenuItem(),

				SeparatorMenuItem.Instance,

				new ExpandSubtreeMenuItem(),
				new CollapseSubtreeMenuItem(),

				SeparatorMenuItem.Instance,

				new OpenNodeSettingsMenuItem(),
				new OpenQuerySettingsMenuItem()
			};

			if (!IsDynamic)
			{
				// enable schedule settings menu for static nodes only
				menuItems.Add(new OpenScheduleSettingsMenuItem());
			}

			return menuItems;
		}

		public override async Task ExecuteUpdateAsync(CancellationToken token)
		{
			try
			{
				await AuditTree.ExecuteQuery(this._tnInfo, token);
			}
			catch (Exception exc)
			{
				Log.Error(exc, "Failed to update tree node.");
				throw;
			}
			finally
			{
				Refresh();
			}
		}

		public override async Task<ContentViewModel> CreateContentAsync(CancellationToken cancelToken)
		{
			try
			{
				ReportData report     = await AuditTree.LoadResultsAsync(this._tnInfo, cancelToken);
				DateTime?  nextUpdate = await AuditTree.GetNextUpdateTimeAsync(TemplateNodeInfo, cancelToken);

				ReportViewModel reportvm = new ReportViewModel(this._tnInfo.NodeInstanceId)
				{
					XmlResult      = report.XmlReport.XmlString,
					UpdateDate     = report.DateTime,
					UpdateDuration = report.Duration,
					NextUpdateDate = nextUpdate
				};

				List<TabViewModel> tabs = new List<TabViewModel>();

				if (AuditTree.ShowXmlTab)
				{
					TabViewModel xmlTab = new TabViewModel
					{
						Header = "XML",
						VisualReport = new XmlVisualReport(report.XmlReport)
					};

					tabs.Add(xmlTab);
				}

				cancelToken.ThrowIfCancellationRequested();

				tabs.AddRange(AuditTree.CreateReportTabs(report));
				foreach (TabViewModel tab in tabs)
				{
					reportvm.Tabs.Add(tab);
				}

				if (reportvm.Id == ReportHistory.LastReportId)
				{
					// report was refreshed, preserve selected tab
					reportvm.SelectedTabIndex = ReportHistory.LastTabIndex;
				}
				else
				{
					// another node is selected
					if (AuditTree.ShowXmlTab)
					{
						if (ReportHistory.LastTabIndex <= 0)
						{
							reportvm.SelectedTabIndex = tabs.Count > 1 ? 1 : 0;
						}
						else
						{
							reportvm.SelectedTabIndex = Math.Min(ReportHistory.LastTabIndex, tabs.Count - 1);
						}
					}
					else
					{
						if (ReportHistory.LastTabIndex >= 0)
						{
							reportvm.SelectedTabIndex = Math.Min(ReportHistory.LastTabIndex, tabs.Count - 1);
						}
					}

					ReportHistory.LastReportId = reportvm.Id;
				}

				return reportvm;
			}
			catch (OperationCanceledException)
			{
				throw;
			}
			catch (QueryVerificationFailedException exc)
			{
				StringBuilder sb = new StringBuilder();
				sb.AppendLine(Strings.QueriesHaveWrongSignatures + ":");

				foreach (QueryItemInfo queryItem in exc.VerificationResult.InvalidQueryItems)
				{
					QueryInfo queryInfo =  queryItem.Parent;
					sb.AppendLine(string.Join(", ", new[] { queryInfo.Name, queryInfo.Id }.Where(s => !string.IsNullOrEmpty(s))));
				}

				return new TextContentViewModel { Text = sb.ToString(), IsError = true };
			}
			catch (Exception exc)
			{
				return new TextContentViewModel { Text = exc.Message, IsError = true };
			}
		}

		private bool IsDynamic =>
			!string.IsNullOrWhiteSpace(this._tnInfo.GroupSelectId);

		private bool HasChildren =>
			this._tnInfo.Template.Children.Any() || this._tnInfo.HasDynamicChildren();

		public void RefreshSettings()
		{
			try
			{
				AuditTree.LoadNodeSettings(this._tnInfo);
			}
			catch (Exception exc)
			{
				Log.Error(exc,
					$"Failed to load settings of the node '{this._tnInfo.Name}', id: {this._tnInfo.Id}"
				);
			}
		}
	}
}
