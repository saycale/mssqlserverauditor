﻿using System.Collections.Generic;
using System.Linq;
using MSSQLServerAuditor.Common.Collections;
using MSSQLServerAuditor.Core.Domain;

namespace MSSQLServerAuditor.Infrastructure
{
	public class ConnectionStringParameters
	{
		private static readonly List<string> _mssql = new List<string>
		{
			"Address",
			"Application Name",
			"Connection Timeout",
			"Integrated Security",
			"Data Source",
			"Database",
			"Initial Catalog",
			"Encrypt",
			"Server",
			"Trusted_Connection",
			"Max Pool Size",
			"Min Pool Size",
			"Network Library",
			"Net",
			"Password",
			"Pwd",
			"Persist Security Info",
			"Pooling",
			"User ID",
			"User Instance",
			"Workstation ID",
			"Type System Version",
			"TrustServerCertificate",
			"Transaction Binding",
			"Packet Size"
		};

		private static readonly List<string> _odbc = new List<string>
		{
			"Address",
			"Database",
			"Driver",
			"DSN",
			"Encrypt",
			"PWD",
			"Server",
			"UID",
			"WSID"
		};

		private static readonly List<string> _teradata = new List<string>
		{
			"Account String",
			"Authentication Mechanism",
			"Authentication String",
			"Command Timeout",
			"Connection Pooling",
			"Connection Pooling Timeout",
			"Connection Timeout",
			"Connect Max Retry Count",
			"Database",
			"Data Encryption",
			"Data Integrity",
			"Data Source",
			"Integated Security",
			"Max Pool Size",
			"Min Pool Size",
			"Password",
			"Port Number",
			"Profile Name",
			"Response Buffer Size",
			"Session Mode",
			"User ID"
		};

		public static List<string> GetOfType(bool isOdbc, ConnectionType type)
		{
			if (isOdbc)
			{
				return Obdc;
			}

			switch (type)
			{
				case ConnectionType.MSSQL:
					return Mssql;
				case ConnectionType.TDSQL:
					return Teradata;
				default:
					return Lists.Empty<string>();
			}
		}

		public static List<string> Mssql
		{
			get { return _mssql.OrderBy(p => p).ToList(); }
		}

		public static List<string> Obdc
		{
			get { return _odbc.OrderBy(p => p).ToList(); }
		}

		public static List<string> Teradata
		{
			get { return _teradata.OrderBy(p => p).ToList(); }
		}
	}
}
