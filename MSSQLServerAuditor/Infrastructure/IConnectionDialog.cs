﻿using MSSQLServerAuditor.Core.Domain.Models;

namespace MSSQLServerAuditor.Infrastructure
{
	public interface IConnectionDialog
	{
		ServerInstance Instance { get; set; }

		bool IsKeyModificationEnabled { get; set; }
	}

	public interface IConnectionDialog<T> : IConnectionDialog
		where T : ServerInstance
	{
		new T Instance { get; set; }

		new bool IsKeyModificationEnabled { get; set; }
	}

	public abstract class ConnectionDialog<T> : IConnectionDialog<T>
		where T : ServerInstance
	{
		ServerInstance IConnectionDialog.Instance
		{
			get { return Instance; }
			set { Instance = (T) value; }
		}

		public abstract bool IsKeyModificationEnabled { get; set; }
		public abstract T    Instance                 { get; set; }
	}
}
