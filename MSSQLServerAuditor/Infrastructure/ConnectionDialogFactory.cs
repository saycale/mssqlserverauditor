﻿using System;
using System.Collections.Generic;
using Caliburn.Micro;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.ViewModels;

namespace MSSQLServerAuditor.Infrastructure
{
	public class ConnectionDialogFactory
	{
		private readonly IStorageManager _storageManager;
		private readonly IWindowManager  _windowManager;
		private readonly IAppSettings    _appSettings;

		public ConnectionDialogFactory(
			IStorageManager storageManager,
			IWindowManager  windowManager,
			IAppSettings    appSettings)
		{
			this._storageManager = storageManager;
			this._windowManager  = windowManager;
			this._appSettings    = appSettings;
		}

		public IConnectionDialog CreateDialog(
			ConnectionType     connectionType,
			List<TemplateFile> templates)
		{
			switch (connectionType)
			{
				case ConnectionType.Internal:
					return new InternalConnectionViewModel(
						this._storageManager,
						this._appSettings,
						templates
					);
				
				case ConnectionType.NetworkInformation:
					return new NetworkConnectionViewModel();

				case ConnectionType.MSSQL:
				case ConnectionType.TDSQL:
					return new DatabaseConnectionViewModel(
						this._storageManager,
						this._windowManager,
						connectionType
					);
				
				case ConnectionType.WMI:
					return new WmiConnectionViewModel(
						this._storageManager,
						this._windowManager
					);

				case ConnectionType.SQLite:
					return new SqliteConnectionViewModel();

				case ConnectionType.ActiveDirectory:
				case ConnectionType.EventLog:
					return new CommonConnectionViewModel(
						connectionType
					);

				default:
					throw new ArgumentOutOfRangeException($"No dialog defined for connection '{connectionType}'");
			}
		}
	}
}
