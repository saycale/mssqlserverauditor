﻿using System.Windows;

namespace MSSQLServerAuditor
{
	public class ThemeProperties : DependencyObject
	{
		public static readonly DependencyProperty ErrorIconMarginProperty = DependencyProperty.RegisterAttached(
			"ErrorIconMargin",
			typeof(Thickness),
			typeof(ThemeProperties),
			new FrameworkPropertyMetadata(default(Thickness))
		);

		public static void SetErrorIconMargin(DependencyObject element, Thickness value)
		{
			element.SetValue(ErrorIconMarginProperty, value);
		}

		public static Thickness GetErrorIconMargin(DependencyObject element)
		{
			return (Thickness) element.GetValue(ErrorIconMarginProperty);
		}
	}
}
