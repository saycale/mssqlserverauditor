﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Media.Imaging;
using MSSQLServerAuditor.Common.Extensions;
using NLog;

namespace MSSQLServerAuditor.Images
{
	public static class Icons
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private static readonly List<string> Extensions = 
			new List<string> { ".png", ".bmp" };

		private static readonly List<NamedIcon> AllIcons;

		static Icons()
		{
			AllIcons = Load();
		}

		public static List<NamedIcon> All => new List<NamedIcon>(AllIcons);

		public static NamedIcon GetIcon(string iconName)
		{
			return AllIcons.FirstOrDefault(ic =>
				ic.Name.EqualsIgnoreCase(iconName)
			);
		}

		private static List<NamedIcon> Load()
		{
			string directory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images");
			List<NamedIcon> result = new List<NamedIcon>();

			foreach (FileInfo file in new DirectoryInfo(directory).GetFiles())
			{
				if (!Extensions.Contains(Path.GetExtension(file.Name)))
				{
					continue;
				}

				try
				{
					BitmapImage bitmap = LoadBitmap(file.FullName);
					string      name   = Path.GetFileNameWithoutExtension(file.Name);
					NamedIcon   icon   = new NamedIcon(bitmap, name);

					result.Add(icon);
				}
				catch (Exception ex)
				{
					Log.Error(ex, $"Failed to load image: {file.FullName}");
				}
			}

			return result;
		}

		private static BitmapImage LoadBitmap(string path)
		{
			BitmapImage image = new BitmapImage(new Uri(path));
			image.Freeze();

			return image;
		}
	}
}
