﻿using System;
using System.Windows.Media.Imaging;

namespace MSSQLServerAuditor.Images
{
	public static class StaticImages
	{
		static BitmapImage LoadBitmap(string name)
		{
			BitmapImage image = new BitmapImage(new Uri("pack://application:,,,/Images/" + name + ".png"));
			image.Freeze();
			return image;
		}

		public static readonly BitmapImage Refresh        = LoadBitmap("Refresh");
		public static readonly BitmapImage RefreshSubtree = LoadBitmap("RefreshSubtree");
		public static readonly BitmapImage Expand         = LoadBitmap("Expand");
		public static readonly BitmapImage Collapse       = LoadBitmap("Collapse");
		public static readonly BitmapImage Stop           = LoadBitmap("Stop");
		public static readonly BitmapImage Waiting        = LoadBitmap("Waiting");
		public static readonly BitmapImage Default        = LoadBitmap("Default");
		public static readonly BitmapImage Settings       = LoadBitmap("Settings");
		public static readonly BitmapImage Settings2      = LoadBitmap("Settings2");
		public static readonly BitmapImage Filter         = LoadBitmap("Filter");
		public static readonly BitmapImage Params         = LoadBitmap("Params");
		public static readonly BitmapImage Schedule       = LoadBitmap("Schedule");
		public static readonly BitmapImage Clock          = LoadBitmap("Clock");
		public static readonly BitmapImage Close          = LoadBitmap("Close");
		public static readonly BitmapImage Close32        = LoadBitmap("Close32");
		public static readonly BitmapImage Delete         = LoadBitmap("Delete");
		public static readonly BitmapImage Remove         = LoadBitmap("Remove");
		public static readonly BitmapImage Properties     = LoadBitmap("Properties");
		public static readonly BitmapImage Network        = LoadBitmap("Network");
		public static readonly BitmapImage NextFireTime   = LoadBitmap("NextFireTime");
		public static readonly BitmapImage RebuildTree    = LoadBitmap("RebuildTree");
		public static readonly BitmapImage Exit           = LoadBitmap("Exit");
		public static readonly BitmapImage Exit32         = LoadBitmap("Exit32");
	}
}
