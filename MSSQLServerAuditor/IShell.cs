﻿using System.Threading.Tasks;
using Caliburn.Micro;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Core.Xml;
using MSSQLServerAuditor.Tree;
using MSSQLServerAuditor.ViewModels;

namespace MSSQLServerAuditor
{
	public interface IShell
	{
		IWorkspace     Workspace     { get; }
		IStatusBar     StatusBar     { get; }
		IAuditForest   AuditForest   { get; }
		IAppSettings   AppSettings   { get; }
		IWindowManager WindowManager { get; }
		XmlLoader      XmlLoader     { get; }

		Task<AuditTree> OpenConnectionAsync(AuditContext context);
	}
}
