﻿using System;
using System.Globalization;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Presentation.Converters;

namespace MSSQLServerAuditor.Converters
{
	public class TemplateNodeQueryInfoConverter : ConverterBase
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			string lang = culture.TwoLetterISOLanguageName;
			TemplateNodeQueryInfo query = value as TemplateNodeQueryInfo;
			if (query != null)
			{
				foreach (Translation title in query.Title)
				{
					if (lang.EqualsIgnoreCase(title.Language))
					{
						return title.Text.TrimmedOrEmpty();
					}
				}

				return $"{query.QueryName} [{query.Id}]";
			}

			return string.Empty;
		}
	}
}
