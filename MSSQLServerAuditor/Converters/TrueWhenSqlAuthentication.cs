﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;
using MSSQLServerAuditor.ViewModels;

namespace MSSQLServerAuditor.Converters
{
	public class TrueWhenSqlAuthentication : MarkupExtension, IValueConverter
	{
		public static readonly TrueWhenSqlAuthentication Instance = new TrueWhenSqlAuthentication();

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return Instance;
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			AuthenticationMode? authMode = value as AuthenticationMode?;
			return authMode == AuthenticationMode.SqlServer;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
