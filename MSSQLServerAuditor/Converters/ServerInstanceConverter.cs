﻿using System;
using System.Globalization;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Presentation.Converters;

namespace MSSQLServerAuditor.Converters
{
	public class ServerInstanceConverter : ConverterBase
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			ServerInstance serverInstance = value as ServerInstance;
			if (serverInstance != null)
			{
				return serverInstance.ToString();
			}

			return string.Empty;
		}
	}
}
