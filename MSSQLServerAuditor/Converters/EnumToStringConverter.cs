﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;
using MSSQLServerAuditor.Resources;

namespace MSSQLServerAuditor.Converters
{
	public sealed class EnumToStringConverter : MarkupExtension, IValueConverter
	{
		public string Prefix { set; private get; }

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null)
			{
				return null;
			}

			string resourceName = Prefix == null ? value.ToString() : Prefix + value;
			return Strings.ResourceManager.GetString(resourceName);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			string stringValue = (string) value;

			foreach (object enumValue in Enum.GetValues(targetType))
			{
				string enumString = enumValue.ToString();

				string resourceName = Prefix == null ? enumString : Prefix + enumString;
				if (stringValue == Strings.ResourceManager.GetString(resourceName))
				{
					return enumValue;
				}
			}

			throw new ArgumentException(null, nameof(value));
		}

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return this;
		}
	}
}
