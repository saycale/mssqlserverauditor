﻿using System;
using System.Globalization;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Presentation.Converters;

namespace MSSQLServerAuditor.Converters
{
	public class ConnectionGroupConverter : ConverterBase
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			ConnectionGroup group = value as ConnectionGroup;
			if (group != null)
			{
				return group.Name;
			}

			return string.Empty;
		}
	}
}
