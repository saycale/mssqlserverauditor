﻿using System;
using System.Globalization;
using System.Reflection;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using MSSQLServerAuditor.Presentation.Localization;

namespace MSSQLServerAuditor.Converters
{
	public class LocalizedEnumConverter : MarkupExtension, IValueConverter
	{
		public static readonly LocalizedEnumConverter Instance = new LocalizedEnumConverter();

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return Instance;
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null)
			{
				return string.Empty;
			}

			return GetLocalizedText(value, culture);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return DependencyProperty.UnsetValue;
		}

		private static string GetLocalizedText(object value, CultureInfo culture)
		{
			FieldInfo fi = value.GetType().GetField(value.ToString());

			LocalizedAttribute[] attributes =
				(LocalizedAttribute[]) fi.GetCustomAttributes(typeof(LocalizedAttribute), false);

			if (attributes.Length > 0)
			{
				return attributes[0].GetLocalizedText(culture);
			}

			return value.ToString();
		}
	}
}
