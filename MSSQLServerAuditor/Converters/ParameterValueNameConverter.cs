﻿using System;
using System.Globalization;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Presentation.Converters;

namespace MSSQLServerAuditor.Converters
{
	public class ParameterValueNameConverter : ConverterBase
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			string lang = culture.TwoLetterISOLanguageName;
			ParameterValue param = value as ParameterValue;
			if (param != null)
			{
				foreach (Translation title in param.Title)
				{
					if (lang.EqualsIgnoreCase(title.Language))
					{
						return title.Text.TrimmedOrEmpty();
					}
				}

				return param.Name;
			}

			return string.Empty;
		}
	}
}
