﻿using System;
using System.ComponentModel;
using System.Windows;
using MSSQLServerAuditor.Extensions;
using MSSQLServerAuditor.Properties;

namespace MSSQLServerAuditor.Views
{
	/// <summary>
	/// Interaction logic for ShellView.xaml
	/// </summary>
	public partial class ShellView : Window
	{
		public ShellView()
		{
			InitializeComponent();
		}

		protected override void OnSourceInitialized(EventArgs e)
		{
			base.OnSourceInitialized(e);

			this.SetPlacement(Settings.Default.ShellViewPlacement);
		}

		private void Window_Closing(object sender, CancelEventArgs e)
		{
			Settings.Default.ShellViewPlacement = this.GetPlacement();
		}
	}
}
