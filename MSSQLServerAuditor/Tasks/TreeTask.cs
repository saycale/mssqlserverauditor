﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Tasks;
using MSSQLServerAuditor.Tree;

namespace MSSQLServerAuditor.Tasks
{
	public class TreeTask : QueueTask
	{
		private readonly TreeTaskInfo _taskInfo;

		public TreeTask(TreeTaskInfo taskInfo)
		{
			this._taskInfo = taskInfo;
		}

		public TreeTaskInfo Info => this._taskInfo;
		public override long Id  => this._taskInfo.Id;

		public override bool UpdateHierarchically => this._taskInfo.UpdateHierarchically;
		public override int  UpdateDeep           => this._taskInfo.UpdateDeep;

		public override string Title
		{
			get
			{
				AuditTreeNode treeNode = this._taskInfo.TreeNode;

				return treeNode.Parent == null
					? treeNode.Text.ToString()
					: treeNode.Parent.Text + "\\" + treeNode.Text;
			}
		}

		protected override async Task ExecuteAsync(CancellationToken cancelToken)
		{
			AuditTreeNode treeNode = this._taskInfo.TreeNode;
			await treeNode.EnsureLazyChildren(
				Info.FetchMode,
				cancelToken,
				Info.FetchMode == FetchMode.Remote
			);

			if (Info.Update)
			{
				await treeNode.ExecuteUpdateAsync(cancelToken);
				
				if (Info.PostUpdateAction != null)
				{
					await Task.Run(() => Info.PostUpdateAction(cancelToken), cancelToken);
				}
			}

			if (!CancellationSource.IsCancellationRequested)
			{
				if (treeNode.CanExpand && Info.Expand)
				{
					await treeNode.Expand(cancelToken);
				}
			}
		}

		public override IEnumerable<QueueTask> GetChildTasks()
		{
			AuditTreeNode treeNode = this._taskInfo.TreeNode;

			foreach (AuditTreeNode childNode in treeNode.Children.OfType<AuditTreeNode>())
			{
				TreeTaskInfo childTaskInfo = new TreeTaskInfo(childNode)
				{
					FetchMode  = Info.FetchMode,
					ParentTask = Info,
					Update     = Info.Update,
					Expand     = Info.Expand
				};

				if (Info.UpdateDeep < 0) // infinite update deep
				{
					childTaskInfo.UpdateHierarchically = true;
					childTaskInfo.UpdateDeep           = -1;
				}
				else
				{
					childTaskInfo.UpdateHierarchically = true;
					childTaskInfo.UpdateDeep           = Info.UpdateDeep - 1;
				}

				yield return new TreeTask(childTaskInfo);
			}
		}
	}
}
