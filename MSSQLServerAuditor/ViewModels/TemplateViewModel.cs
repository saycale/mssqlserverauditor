using System.Globalization;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;

namespace MSSQLServerAuditor.ViewModels
{
	public class TemplateViewModel
	{
		public TemplateViewModel(
			Template     template,
			TemplateInfo templateInfo)
		{
			Template = template;
			Info     = templateInfo;
		}

		public Template     Template { get; set; }
		public TemplateInfo Info     { get; set; }

		public string Display
		{
			get
			{
				if (Info == null)
				{
					return Template.Name;
				}

				string lang = CultureInfo.CurrentUICulture.TwoLetterISOLanguageName;

				foreach (Translation title in Info.TemplateTitle)
				{
					if (lang.EqualsIgnoreCase(title.Language))
					{
						return title.Text.TrimmedOrEmpty();
					}
				}

				return Info.Id.TrimmedOrEmpty();
			}
		}
	}
}