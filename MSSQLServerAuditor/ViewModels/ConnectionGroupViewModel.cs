﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Data;
using Caliburn.Micro;
using MSSQLServerAuditor.Common.Async;
using MSSQLServerAuditor.Common.Collections;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Persistence.Services;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Core.Xml;
using MSSQLServerAuditor.Infrastructure;
using MSSQLServerAuditor.Presentation.Dialogs;
using MSSQLServerAuditor.Presentation.Validation;
using MSSQLServerAuditor.Resources;
using NLog;
using LogManager = NLog.LogManager;

namespace MSSQLServerAuditor.ViewModels
{
	public class ConnectionGroupViewModel : ValidatingScreen
	{
		public class ConnectionViewModel
		{
			public ConnectionViewModel(ServerInstance serverInstance)
			{
				this.ServerInstance = serverInstance;
			}

			/// <summary>
			/// The server instance
			/// </summary>
			public ServerInstance ServerInstance { get; set; }

			/// <summary>
			/// Determines whether the server instance is selected (highlighted) by user
			/// </summary>
			public bool IsSelected { get; set; }

			/// <summary>
			/// Determines whether the server instance is chosen for current connection group
			/// </summary>
			public bool IsChosen { get; set; }
		}

		private enum ConnectionsFocus { None, AvailableView, ChosenView }

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly IStorageManager _storageManager;
		private readonly IWindowManager  _windowManager;
		private readonly IAppSettings    _appSettings;
		private readonly ITemplateLoader _templateLoader;
		private readonly IDialogService  _dialogService;

		private readonly ConnectionDialogFactory _connectionDialogFactory;

		private ConnectionTypeInfo _selectedConnectionType;
		private ModuleTypeInfo     _selectedModuleType;
		private TemplateFile       _selectedTemplate;
		private ConnectionGroup    _selectedGroup;

		private List<TemplateFile>   _allTemplates;
		private List<LastConnection> _lastConnections;
		private string               _groupName;
		private bool                 _isExternalTemplate;
		private string               _externalTemplateFile;
		private LastConnection       _lastConnection;

		private bool _isEditMode;

		private CollectionViewSource _availableConnections;
		private CollectionViewSource _chosenConnections;

		// focus tracking
		private ConnectionsFocus _connectionsFocus = ConnectionsFocus.None;

		public ConnectionGroupViewModel(
			IStorageManager storageManager,
			IWindowManager  windowManager,
			IAppSettings    appSettings,
			ITemplateLoader templateLoader
		)
		{
			Check.NotNull(storageManager, nameof(storageManager));
			Check.NotNull(windowManager,  nameof(windowManager));
			Check.NotNull(appSettings,    nameof(appSettings));
			Check.NotNull(templateLoader, nameof(templateLoader));

			this._storageManager = storageManager;
			this._windowManager  = windowManager;
			this._appSettings    = appSettings;
			this._templateLoader = templateLoader;

			this._dialogService = new DialogService(windowManager);

			this._connectionDialogFactory = new ConnectionDialogFactory(
				storageManager,
				windowManager,
				appSettings
			);

			this._lastConnections = new List<LastConnection>();
			Initialize();
		}

		public CollectionViewSource ChosenConnections     => this._chosenConnections;
		public ICollectionView      ChosenConnectionsView => this._chosenConnections.View;
		public CollectionViewSource AvailableConnections  => this._availableConnections;

		public AuditContext SelectedAuditContext
		{
			get
			{
				ConnectionType type = SelectedConnectionType.ConnectionType;
				
				ConnectionGroup group = new ConnectionGroup
				{
					ConnectionType = type,
					Name           = GroupName
				};

				List<ServerInstance> instances = Connections
					.Where(c => c.IsChosen)
					.Select(c => c.ServerInstance)
					.ToList();

				group.ServerInstances = instances;

				Template template = new Template();
				if (IsExternalTemplate)
				{
					TemplateInfo templateInfo = this._templateLoader.LoadTemplate(ExternalTemplateFile);

					template.Directory  = Path.GetDirectoryName(ExternalTemplateFile);
					template.Name       = Path.GetFileName(ExternalTemplateFile);
					template.UId        = templateInfo.Id;
					template.IsExternal = true;
				}
				else
				{
					template.Name       = SelectedTemplate.FileName;
					template.UId        = SelectedTemplate.Content.Id;
					template.Directory  = this._appSettings.System.TemplateDirectory;
					template.IsExternal = false;
				}

				return new AuditContext(group, template);
			}
		}

		public async Task SetAuditContextAsync(AuditContext context)
		{
			ConnectionGroup group = context.ConnectionGroup;

			await SetConnectionTypeAsync(
				ConnectionTypes.FirstOrDefault(cti => cti.ConnectionType == group.ConnectionType)
			);

			GroupName = group.Name;

			foreach (ServerInstance groupConnection in group.ServerInstances)
			{
				string connectionName = groupConnection.ConnectionName;
				ConnectionViewModel connection = Connections.FirstOrDefault(vm =>
					string.Equals(
						vm.ServerInstance.ConnectionName,
						connectionName,
						StringComparison.OrdinalIgnoreCase
					)
				);

				if (connection != null)
				{
					connection.IsChosen = true;
				}
			}

			RefreshConnectionViews();

			Template template = context.Template;
			if (template.IsExternal)
			{
				IsExternalTemplate = true;
				string path = Path.Combine(
					template.Directory,
					template.Name
				);

				ExternalTemplateFile = path;
			}
			else
			{
				IsExternalTemplate = false;
				SelectedTemplate = this._allTemplates.FirstOrDefault(tf => tf.Content.Id == template.UId);

				if (SelectedTemplate != null && SelectedConnectionType != null)
				{
					SelectedModuleType = ModuleTypes.FirstOrDefault(mt =>
						mt.Id == SelectedTemplate.Content.ModuleType
					);
				}
			}
		}

		public ObservableCollectionEx<ConnectionTypeInfo> ConnectionTypes { get; set; }
		public ObservableCollectionEx<ModuleTypeInfo>     ModuleTypes     { get; set; }
		public ObservableCollectionEx<TemplateFile>       Templates       { get; set; }
		public ObservableCollectionEx<ConnectionGroup>    Groups          { get; set; }
		public BindableCollection<ConnectionViewModel>    Connections     { get; set; }

		public bool IsEditMode
		{
			get { return this._isEditMode; }
			set
			{
				if (this._isEditMode != value)
				{
					this._isEditMode = value;
					NotifyOfPropertyChange(() => IsEditMode);
				}
			}
		}

		public ConnectionTypeInfo SelectedConnectionType
		{
			get { return this._selectedConnectionType; }
			set
			{
				if (this._selectedConnectionType != value)
				{
					NotifyTaskCompletion.Create(SetConnectionTypeAsync(value));
				}
			}
		}

		public async Task SetConnectionTypeAsync(ConnectionTypeInfo connectionType)
		{
			this._selectedConnectionType = connectionType;

			this._lastConnection = GetLastConnection(
				this._selectedConnectionType?.ConnectionType
			);

			RefreshModuleTypes();

			await UpdateConnectionGroupsAsync();
			await UpdateConnectionsAsync();

			NotifyOfPropertyChange(() => SelectedConnectionType);
			NotifyOfPropertyChange(() => IsSystemConnection);
		}

		public bool IsSystemConnection => 
			SelectedConnectionType != null && SelectedConnectionType.ConnectionType == ConnectionType.System;

		public ModuleTypeInfo SelectedModuleType
		{
			get { return this._selectedModuleType; }
			set
			{
				if (this._selectedModuleType != value)
				{
					this._selectedModuleType = value;
					RefreshTemplates();

					NotifyOfPropertyChange(() => SelectedModuleType);
				}
			}
		}

		public TemplateFile SelectedTemplate
		{
			get { return this._selectedTemplate; }
			set
			{
				if (this._selectedTemplate != value)
				{
					this._selectedTemplate = value;

					NotifyOfPropertyChange(() => SelectedTemplate);
				}
			}
		}

		public ConnectionGroup SelectedGroup
		{
			get { return this._selectedGroup; }
			set
			{
				if (this._selectedGroup != value)
				{
					this._selectedGroup = value;
					RefreshGroupConnections();

					NotifyOfPropertyChange(() => SelectedGroup);
				}
			}
		}

		public string GroupName
		{
			get { return this._groupName; }
			set
			{
				if (this._groupName != value)
				{
					this._groupName = value;
					NotifyOfPropertyChange(() => GroupName);
				}
			}
		}

		public bool IsExternalTemplate
		{
			get { return this._isExternalTemplate; }
			set
			{
				if (this._isExternalTemplate != value)
				{
					this._isExternalTemplate = value;
					NotifyOfPropertyChange(() => IsExternalTemplate);
				}
			}
		}

		public string ExternalTemplateFile
		{
			get { return this._externalTemplateFile; }
			set
			{
				if (this._externalTemplateFile != value)
				{
					this._externalTemplateFile = value;
					NotifyOfPropertyChange(() => ExternalTemplateFile);
				}
			}
		}

		public bool CanAppendSelectedConnections      => Connections.Any(m => !m.IsChosen && m.IsSelected);
		public bool CanAppendAllConnections           => Connections.Any(m => !m.IsChosen);
		public bool CanRemoveSelectedGroupConnections => Connections.Any(m => m.IsChosen && m.IsSelected);
		public bool CanRemoveAllGroupConnections      => Connections.Any(m => m.IsChosen);

		public bool CanEditSelectedConnection => this._connectionsFocus != ConnectionsFocus.None;

		public void Accept()
		{
			ValidationResult results = Validator.ValidateAll();
			if (results.IsValid)
			{
				TryClose(true);
			}
		}

		public void Cancel()
		{
			TryClose(false);
		}

		public void ChosenConnectionsSelectionChanged()
		{
			NotifyOfPropertyChange(() => CanRemoveSelectedGroupConnections);
			NotifyOfPropertyChange(() => CanRemoveAllGroupConnections);

			this._connectionsFocus = ConnectionsFocus.None;
			if (Connections.Count(c => c.IsChosen && c.IsSelected) == 1)
			{
				this._connectionsFocus = ConnectionsFocus.ChosenView;
			}

			NotifyOfPropertyChange(() => CanEditSelectedConnection);
		}

		public void AvailableConnectionsSelectionChanged()
		{
			NotifyOfPropertyChange(() => CanAppendSelectedConnections);

			this._connectionsFocus = ConnectionsFocus.None;
			if (Connections.Count(c => !c.IsChosen && c.IsSelected) == 1)
			{
				this._connectionsFocus = ConnectionsFocus.AvailableView;
			}

			NotifyOfPropertyChange(() => CanEditSelectedConnection);
		}

		public void AppendConnection(object obj)
		{
			ConnectionViewModel connection = obj as ConnectionViewModel;
			if (connection != null)
			{
				connection.IsChosen   = true;
				connection.IsSelected = false;
			}

			RefreshConnectionViews();
		}

		public void RemoveConnection(object obj)
		{
			ConnectionViewModel connection = obj as ConnectionViewModel;
			if (connection != null)
			{
				connection.IsChosen   = false;
				connection.IsSelected = false;
			}

			RefreshConnectionViews();
		}

		public void AppendSelectedConnections()
		{
			Connections.Where(vm => !vm.IsChosen && vm.IsSelected)
				.Apply(vm =>
				{
					vm.IsChosen   = true;
					vm.IsSelected = false;
				});

			RefreshConnectionViews();
		}

		public void AppendAllConnections()
		{
			 Connections.Where(vm => !vm.IsChosen)
				.Apply(vm =>
				{
					vm.IsChosen   = true;
					vm.IsSelected = false;
				});

			RefreshConnectionViews();
		}

		public void RemoveSelectedGroupConnections()
		{
			Connections.Where(vm => vm.IsChosen && vm.IsSelected)
				.Apply(vm =>
				{
					vm.IsChosen   = false;
					vm.IsSelected = false;
				});

			RefreshConnectionViews();
		}

		public void RemoveAllGroupConnections()
		{
			Connections.Where(vm => vm.IsChosen)
				.Apply(vm =>
				{
					vm.IsChosen   = false;
					vm.IsSelected = false;
				});

			RefreshConnectionViews();
		}

		private void RefreshConnectionViews()
		{
			AvailableConnections.View.Refresh();
			ChosenConnections.View.Refresh();

			NotifyOfPropertyChange(() => CanAppendSelectedConnections);
			NotifyOfPropertyChange(() => CanAppendAllConnections);
			NotifyOfPropertyChange(() => CanRemoveSelectedGroupConnections);
			NotifyOfPropertyChange(() => CanRemoveAllGroupConnections);
			NotifyOfPropertyChange(() => ChosenConnectionsView);
			NotifyOfPropertyChange(() => CanEditSelectedConnection);
		}

		public void EditSelectedConnection()
		{
			if (this._connectionsFocus == ConnectionsFocus.AvailableView)
			{
				try
				{
					ConnectionViewModel connection = Connections.Single(m => !m.IsChosen && m.IsSelected);
					EditConnection(connection);
				}
				catch (InvalidOperationException)
				{
					/* The collection does not contain exactly one element. */
				}
			}
			else if (this._connectionsFocus == ConnectionsFocus.ChosenView)
			{
				try
				{
					ConnectionViewModel connection = Connections.Single(m => m.IsChosen && m.IsSelected);
					EditConnection(connection);
				}
				catch (InvalidOperationException)
				{
					/* The collection does not contain exactly one element. */
				}
			}
		}

		public void EditConnection(ConnectionViewModel connection)
		{
			if (connection == null)
			{
				return;
			}

			ServerInstance serverInstance = connection.ServerInstance;

			IConnectionDialog dialog = this._connectionDialogFactory
				.CreateDialog(SelectedConnectionType.ConnectionType, this._allTemplates);

			dialog.IsKeyModificationEnabled = false;
			dialog.Instance = serverInstance;

			bool ok = this._windowManager.ShowDialog(dialog) ?? false;
			if (ok)
			{
				ServerInstance      modifiedInstance   = dialog.Instance;
				ConnectionViewModel modifiedConnection = new ConnectionViewModel(modifiedInstance)
				{
					IsChosen   = connection.IsChosen,
					IsSelected = connection.IsSelected
				};

				UpdateInstance(connection, modifiedConnection);
			}
		}

		public async Task DeleteSelectedConnections()
		{
			List<ConnectionViewModel> selectedConnections = Connections
				.Where(vm => !vm.IsChosen && vm.IsSelected)
				.ToList();

			if (selectedConnections.Any())
			{
				foreach (ConnectionViewModel connection in selectedConnections)
				{
					ServerInstance serverInstance = connection.ServerInstance;
					try
					{
						// disable connection
						this._storageManager.MainStorage.Connections.DisableServerInstance(serverInstance);

						// update UI
						// update list of available connections
						Connections.Remove(connection);

						// update all group's connections
						foreach (ConnectionGroup connectionGroup in Groups)
						{
							ServerInstance deleteInstance = connectionGroup.ServerInstances.SingleOrDefault(
								s => s.ConnectionName.EqualsIgnoreCase(serverInstance.ConnectionName)
							);

							if (deleteInstance != null)
							{
								connectionGroup.ServerInstances.Remove(deleteInstance);
							}
						}

						this._storageManager.MainStorage.Connections
							.DeleteConnectionFromGroups(serverInstance.Id);
					}
					catch (Exception exc)
					{
						Log.Error(exc, $"Error while deleting new instance: {connection}");

						string header = string.Format(Strings.ErrorDeleteInstance, connection);
						Dialog dialog = new ErrorDialog($"{header}:\n" + exc.Message, Answer.Ok);
						this._dialogService.ShowDialog(dialog);
					}
				}

				// reload connections from the database
				await UpdateConnectionsAsync();
				RefreshGroupConnections();
			}
		}

		public async Task CreateConnection()
		{
			IConnectionDialog dialog = this._connectionDialogFactory
				.CreateDialog(SelectedConnectionType.ConnectionType, this._allTemplates);

			bool ok = this._windowManager.ShowDialog(dialog) ?? false;
			if (ok)
			{
				ServerInstance instance = dialog.Instance;
				await SaveNewInstanceAsync(instance);
			}
		}

		protected override void OnInitialize()
		{
			base.OnInitialize();

			DisplayName = Strings.Connection;
		}

		protected override void OnActivate()
		{
			base.OnActivate();

			Connections.CollectionChanged += ConnectionsChanged;
		}

		protected override void OnDeactivate(bool close)
		{
			base.OnDeactivate(close);

			Connections.CollectionChanged -= ConnectionsChanged;
		}

		private void ConnectionsChanged(object sender, EventArgs args)
		{
			NotifyConnectionsChanged();
		}

		private void NotifyConnectionsChanged()
		{
			NotifyOfPropertyChange(() => CanRemoveAllGroupConnections);
			NotifyOfPropertyChange(() => CanRemoveSelectedGroupConnections);
			NotifyOfPropertyChange(() => CanAppendSelectedConnections);
			NotifyOfPropertyChange(() => CanEditSelectedConnection);
		}

		private void RefreshModuleTypes()
		{
			if (SelectedConnectionType == null)
			{
				ModuleTypes.Clear();
				SelectedModuleType = null;
				return;
			}

			ModuleTypes.ReplaceRange(SelectedConnectionType.ModuleTypes);

			ModuleTypeInfo lastModule = null;
			if (this._lastConnection != null)
			{
				lastModule = ModuleTypes.FirstOrDefault(
					mt => mt.Id == this._lastConnection.ModuleType
				);
			}
			
			SelectedModuleType = lastModule ?? ModuleTypes.FirstOrDefault();
		}

		private void RefreshTemplates()
		{
			if (SelectedModuleType == null)
			{
				Templates.Clear();
				SelectedTemplate = null;
				return;
			}

			List<TemplateFile> filesOfType = this._allTemplates
				.Where(t => t.Content.ModuleType == SelectedModuleType.Id)
				.ToList();

			Templates.ReplaceRange(filesOfType);

			TemplateFile lastTemplate = null;
			if (this._lastConnection != null)
			{
				string templateDir = this._lastConnection.Template.Directory;

				if (!string.IsNullOrEmpty(templateDir) && Path.IsPathRooted(templateDir))
				{
					IsExternalTemplate = true;
					ExternalTemplateFile = Path.Combine(templateDir, this._lastConnection.Template.Name);
				}
				else
				{
					foreach (TemplateFile templateFile in Templates)
					{
						TemplateInfo content = templateFile.Content;
						if (string.Equals(content.Id, this._lastConnection.Template.UId, StringComparison.OrdinalIgnoreCase))
						{
							lastTemplate = templateFile;
							break;
						}
					}
				}
			}

			SelectedTemplate = lastTemplate ?? Templates.FirstOrDefault();
		}

		private List<LastConnection> LoadLastConnections()
		{
			return this._storageManager.MainStorage.Connections
				.GetLastConnections(Environment.MachineName);
		}

		private LastConnection GetLastConnection(ConnectionType? connectionType = null)
		{
			if (connectionType == null)
			{
				// get the most recent connection
				return this._lastConnections.FirstOrDefault();
			}

			// get last connection for a particular connection type
			return this._lastConnections
				.FirstOrDefault(lc => lc.ConnectionType == connectionType);
		}

		private async Task UpdateConnectionGroupsAsync()
		{
			GroupName = null;

			if (SelectedConnectionType == null)
			{
				Groups.Clear();
				return;
			}

			if (IsSystemConnection)
			{
				Groups.Clear();
				GroupName = ConnectionType.System.ToString();

				SetSystemConnectionForCurrentGroup();

				return;
			}

			ConnectionService service = this._storageManager
				.MainStorage.Connections;

			List<ConnectionGroup> groups = await Task.Run(() => service.GetConnectionGroups(
				SelectedConnectionType.ConnectionType
			));

			Groups.ReplaceRange(groups);

			await TaskConstants.Completed;
		}

		private async Task UpdateConnectionsAsync()
		{
			if (SelectedConnectionType == null)
			{
				Connections.Clear();
				return;
			}

			if (IsSystemConnection)
			{
				return;
			}

			ConnectionService service = this._storageManager
				.MainStorage.Connections;

			List<ServerInstance> instances = await Task.Run(() => service.GetServerInstances(
				SelectedConnectionType.ConnectionType
			));

			Connections.Clear();
			Connections.AddRange(
				instances.Select(instance => new ConnectionViewModel(instance))
			);

			NotifyOfPropertyChange(() => CanAppendAllConnections);

			await TaskConstants.Completed;
		}

		private void RefreshGroupConnections()
		{
			if (SelectedGroup == null)
			{
				Connections.Apply(c =>
				{
					if (c.IsChosen)
					{
						c.IsChosen   = false;
						c.IsSelected = false;
					}
				});

				RefreshConnectionViews();
				return;
			}

			if (IsSystemConnection)
			{
				SetSystemConnectionForCurrentGroup();
			}
			else
			{
				Connections.Apply(c =>
					{
						c.IsChosen   = false;
						c.IsSelected = false;
					}
				);

				List<ServerInstance> groupConnections = SelectedGroup.ServerInstances;

				int count = groupConnections.Count;
				foreach (ServerInstance groupConnection in groupConnections)
				{
					string connectionName = groupConnection.ConnectionName;
					ConnectionViewModel connection = Connections.FirstOrDefault(vm =>
						string.Equals(
							vm.ServerInstance.ConnectionName,
							connectionName,
							StringComparison.OrdinalIgnoreCase
						)
					);

					if (connection != null)
					{
						connection.IsChosen = true;
						if (count == 1)
						{
							connection.IsSelected = true;
						}
					}
				}

				RefreshConnectionViews();
			}
		}

		private void SetSystemConnectionForCurrentGroup()
		{
			string system = ConnectionType.System.ToString();

			ServerInstance systemConnection = new SystemConnectionInstance
			{
				IsActive         = true,
				ConnectionName   = system,
				Login            = Login.CreateEmpty(),
				ConnectionType   = ConnectionType.System
			};

			ConnectionViewModel connection = new ConnectionViewModel(systemConnection)
			{
				IsChosen = true
			};

			Connections.Clear();
			Connections.Add(connection);

			RefreshConnectionViews();
		}

		private void Initialize()
		{
			ConfigureValidationRules();

			ConnectionTypes = new ObservableCollectionEx<ConnectionTypeInfo>(
				this._appSettings.System.ConnectionTypes
			);

			ModuleTypes = new ObservableCollectionEx<ModuleTypeInfo>();
			Templates   = new ObservableCollectionEx<TemplateFile>();
			Groups      = new ObservableCollectionEx<ConnectionGroup>();
			Connections = new BindableCollection<ConnectionViewModel>();

			this._availableConnections = new CollectionViewSource { Source = Connections };
			this._availableConnections.Filter += (s, e) => e.Accepted = !((ConnectionViewModel) e.Item).IsChosen;

			this._chosenConnections = new CollectionViewSource { Source = Connections };
			this._chosenConnections.Filter += (s, e) => e.Accepted = ((ConnectionViewModel) e.Item).IsChosen;

			this._allTemplates = this._templateLoader.LoadTemplates();

			this._lastConnections = LoadLastConnections();
			this._lastConnection  = GetLastConnection();

			if (this._lastConnection != null)
			{
				SelectedConnectionType = ConnectionTypes.FirstOrDefault(ct => 
					ct.ConnectionType == this._lastConnection.ConnectionType
				);
			}
			else
			{
				SelectedConnectionType = ConnectionTypes.FirstOrDefault();
			}
		}

		private void ConfigureValidationRules()
		{
			Validator.AddRequiredRule(() => GroupName, Strings.ConnectionGroupNameIsRequired);

			Validator.AddRule(
				nameof(ChosenConnectionsView),
				() => 
				{
					if (!Connections.Any(vm => vm.IsChosen))
					{
						return RuleResult.Invalid(Strings.ConnectionListMustNotBeEmpty);
					}

					return RuleResult.Valid();
				}
			);

			Validator.AddRule(
				nameof(SelectedTemplate),
				() =>
				{
					if (!IsExternalTemplate)
					{
						TemplateFile templateFile = SelectedTemplate;

						if (string.IsNullOrWhiteSpace(templateFile?.Content?.Id))
						{
							return RuleResult.Invalid(Strings.TemplateIsRequired);
						}
					}

					return RuleResult.Valid();
				}
			);

			Validator.AddRule(
				nameof(ExternalTemplateFile),
				() =>
				{
					if (IsExternalTemplate)
					{
						string externalFile = ExternalTemplateFile;

						if (string.IsNullOrWhiteSpace(externalFile))
						{
							return RuleResult.Invalid(Strings.PathToTemplateFileIsRequired);
						}

						if (!File.Exists(externalFile))
						{
							return RuleResult.Invalid(Strings.TemplateFileNotExist);
						}
					}

					return RuleResult.Valid();
				}
			);
		}

		private async Task SaveNewInstanceAsync(ServerInstance newInstance)
		{
			try
			{
				ConnectionService service = this._storageManager
					.MainStorage.Connections;

				// save new connection
				service.SaveServerInstance(newInstance);

				// reload connections from the database
				await UpdateConnectionsAsync();
			}
			catch (Exception exc)
			{
				Log.Error(
					exc,
					$"Error while saving new instance: {newInstance.ConnectionName}"
				);

				string header = string.Format(Strings.ErrorSaveInstance, newInstance.ConnectionName);
				Dialog dialog = new ErrorDialog($"{header}:\n" + exc.Message, Answer.Ok);
				this._dialogService.ShowDialog(dialog);
			}
		}

		private void UpdateInstance(ConnectionViewModel originalConnection, ConnectionViewModel modifiedConnection)
		{
			ServerInstance modifiedInstance = modifiedConnection.ServerInstance;

			try
			{
				// save modified connection
				this._storageManager.MainStorage.Connections
					.SaveServerInstance(modifiedInstance);

				// update UI
				// update list of connections
				int editInstanceIndex = Connections.IndexOf(originalConnection);

				if (editInstanceIndex >= 0)
				{
					Connections[editInstanceIndex] = modifiedConnection;
				}
			}
			catch (Exception exc)
			{
				Log.Error(
					exc,
					$"Error while saving modified instance: {modifiedInstance}"
				);

				Dialog dialog = new ErrorDialog($"{Strings.ErrorSaveInstance}:\n" + exc.Message, Answer.Ok);
				this._dialogService.ShowDialog(dialog);
			}
		}
	}
}
