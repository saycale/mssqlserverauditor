﻿using System;
using System.Linq;
using Caliburn.Micro;
using MSSQLServerAuditor.Common.Collections;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Resources;

namespace MSSQLServerAuditor.ViewModels
{
	public class QuerySettingsViewModel : Screen
	{
		public event EventHandler ApplyClicked;

		private readonly TemplateNodeInfo _tnInfoCopy;
		private TemplateNodeQueryInfo     _selectedQuery;
		
		public QuerySettingsViewModel(TemplateNodeInfo tnInfo)
		{
			this._tnInfoCopy = MakeClone(tnInfo);

			Queries = CollectQueries();
		}

		public ObservableCollectionEx<TemplateNodeQueryInfo> Queries { get; set; }

		public TemplateNodeQueryInfo SelectedQuery
		{
			get { return this._selectedQuery; }
			set
			{
				if (this._selectedQuery != value)
				{
					this._selectedQuery = value;
					NotifyOfPropertyChange(() => SelectedQuery);
				}
			}
		}

		public TemplateNodeInfo TemplateNodeInfo => this._tnInfoCopy;

		public bool CanApply  => true;
		public bool CanAccept => true;
		public bool CanCancel => true;

		public void Apply()
		{
			OnApplyClicked(EventArgs.Empty);
		}

		public void Accept()
		{
			TryClose(true);
		}

		public void Cancel()
		{
			TryClose(false);
		}

		protected override void OnInitialize()
		{
			base.OnInitialize();

			DisplayName = Strings.QuerySettings;
		}

		private TemplateNodeInfo MakeClone(TemplateNodeInfo tnInfo)
		{
			TemplateNodeInfo clone = tnInfo.XmlClone();

			clone.Queries           = tnInfo.Queries.Select(q => q.Clone()).ToList();
			clone.ConnectionQueries = tnInfo.ConnectionQueries.Select(q => q.Clone()).ToList();
			clone.GroupQueries      = tnInfo.GroupQueries.Select(q => q.Clone()).ToList();

			return clone;
		}

		private ObservableCollectionEx<TemplateNodeQueryInfo> CollectQueries()
		{
			ObservableCollectionEx<TemplateNodeQueryInfo> queries = new ObservableCollectionEx<TemplateNodeQueryInfo>();

			queries.AddRange(this._tnInfoCopy.Queries.Where(q => q.ParameterValues.Any()));
			queries.AddRange(this._tnInfoCopy.GroupQueries.Where(q => q.ParameterValues.Any()));
			queries.AddRange(this._tnInfoCopy.SqlCodeGuardQueries);

			return queries;
		}

		private void OnApplyClicked(EventArgs e)
		{
			EventHandler handler = ApplyClicked;
			handler?.Invoke(this, e);
		}
	}
}
