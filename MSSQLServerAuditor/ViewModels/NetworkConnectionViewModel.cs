﻿using System;
using System.Linq;
using Caliburn.Micro;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Network;
using MSSQLServerAuditor.Core.Querying.Connections;
using MSSQLServerAuditor.Infrastructure;
using MSSQLServerAuditor.Presentation.Validation;
using MSSQLServerAuditor.Resources;
using NLog;
using LogManager = NLog.LogManager;

namespace MSSQLServerAuditor.ViewModels
{
	public class NetworkConnectionViewModel : ValidatingScreen, IConnectionDialog<NetworkInstance>
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private string       _connectionName;
		private string       _remoteHost;
		private ProtocolType _networkProtocol;
		private int          _port = 1;
		private int          _timeoutMillis = 100;

		private readonly IDisposable _validationSuspender;

		private bool _validationSuspenderDisposed;
		private bool _isKeyModificationEnabled = true;

		public NetworkConnectionViewModel()
		{
			ConfigureValidationRules();

			NetworkProtocols = new BindableCollection<ProtocolType>(
				Enum.GetValues(typeof(ProtocolType)).Cast<ProtocolType>()
			);

			// suppress validation till the first attempt to accept
			this._validationSuspender = Validator.SuppressValidation();
			this._validationSuspenderDisposed = false;
		}

		public bool IsKeyModificationEnabled
		{
			get { return this._isKeyModificationEnabled; }
			set
			{
				if (this._isKeyModificationEnabled != value)
				{
					this._isKeyModificationEnabled = value;
					NotifyOfPropertyChange(() => IsKeyModificationEnabled);
				}
			}
		}

		public NetworkInstance Instance
		{
			get
			{
				NetworkInstance instance = new NetworkInstance
				{
					Host           = RemoteHost,
					IsActive       = true,
					ConnectionName = ConnectionName,
					Port           = Port,
					Login          = Login.CreateEmpty(),
					ConnectionType = ConnectionType.NetworkInformation,
					Protocol       = NetworkProtocol.ToString(),
					Timeout        = TimeoutMillis
				};

				return instance;
			}
			set
			{
				Check.NotNull(value, nameof(value));

				NetworkConnectionInfo ncInfo = value.ToNetworkConnectionInfo();

				ConnectionName  = value.ConnectionName;
				RemoteHost      = ncInfo.Host;
				NetworkProtocol = ncInfo.Protocol;
				Port            = ncInfo.Port;
				TimeoutMillis   = ncInfo.Timeout;

				NetworkProtocol = NetworkProtocols.FirstOrDefault(p => p == ncInfo.Protocol);
			}
		}

		ServerInstance IConnectionDialog.Instance
		{
			get { return Instance; }
			set { Instance = (NetworkInstance) value; }
		}

		public IObservableCollection<ProtocolType> NetworkProtocols { get; }

		public string ConnectionName
		{
			get { return this._connectionName; }
			set
			{
				if (this._connectionName != value)
				{
					this._connectionName = value;

					NotifyOfPropertyChange(() => ConnectionName);
					Validator.Validate(nameof(ConnectionName));
				}
			}
		}

		public string RemoteHost
		{
			get { return this._remoteHost; }
			set
			{
				if (this._remoteHost != value)
				{
					this._remoteHost = value;

					NotifyOfPropertyChange(() => RemoteHost);
					Validator.Validate(nameof(RemoteHost));
				}
			}
		}

		public ProtocolType NetworkProtocol
		{
			get { return this._networkProtocol; }
			set
			{
				if (this._networkProtocol != value)
				{
					this._networkProtocol = value;

					NotifyOfPropertyChange(() => NetworkProtocol);
					NotifyOfPropertyChange(() => IsPortEnabled);

					SetDefaultTimeout();

					Validator.Validate(nameof(NetworkProtocol));
				}
			}
		}

		public int Port
		{
			get { return this._port; }
			set
			{
				if (this._port != value)
				{
					this._port = value;

					NotifyOfPropertyChange(() => Port);
					Validator.Validate(nameof(Port));
				}
			}
		}

		public bool IsPortEnabled => NetworkProtocol != ProtocolType.Icmp;

		public int TimeoutMillis
		{
			get { return this._timeoutMillis; }
			set
			{
				if (this._timeoutMillis != value)
				{
					this._timeoutMillis = value;

					NotifyOfPropertyChange(() => TimeoutMillis);
					Validator.Validate(nameof(TimeoutMillis));
				}
			}
		}

		public void Accept()
		{
			EnableValidation();

			ValidationResult validationResult = Validator.ValidateAll();
			if (validationResult.IsValid)
			{
				TryClose(true);
			}
		}

		public void Cancel()
		{
			TryClose(false);
		}

		private void SetDefaultTimeout()
		{
			switch (NetworkProtocol)
			{
				case ProtocolType.Icmp:
					TimeoutMillis = HostPinger.DefaultTimeout;
					break;

				case ProtocolType.Tcp:
					TimeoutMillis = TcpPinger.DefaultTimeout;
					break;

				case ProtocolType.Udp:
					TimeoutMillis = UdpPinger.DefaultTimeout;
					break;
			}
		}

		protected override void OnInitialize()
		{
			base.OnInitialize();

			DisplayName = Strings.NetworkConnection;
		}

		private void ConfigureValidationRules()
		{
			Validator.AddRequiredRule(() => ConnectionName, Strings.ConnectionNameIsRequired);
			Validator.AddRequiredRule(() => RemoteHost,     Strings.HostIsRequired);
			Validator.AddRule(nameof(Port), () =>
			{
				if (Port < 0 || Port > ushort.MaxValue)
				{
					return RuleResult.Invalid(Strings.IncorrectPort);
				}

				return RuleResult.Valid();
			});
		}

		private void EnableValidation()
		{
			if (!this._validationSuspenderDisposed)
			{
				// dispose validation suspender to allow on-fly validation
				this._validationSuspender.Dispose();
				this._validationSuspenderDisposed = true;
			}
		}
	}
}
