using System;
using System.Threading.Tasks;
using MSSQLServerAuditor.Tree;

namespace MSSQLServerAuditor.ViewModels
{
	public interface IWorkspace
	{
		Task UpdateContentAsync(AuditTreeNode auditNode);
		void CancelUpdate();
		bool IsContentNode(AuditTreeNode auditNode);
		bool IsBusy { get; }
		Task RefreshContentAsync();
		
		void SetNextUpdateDate(DateTime? nextUpdate);
	}
}