﻿using System;
using System.Collections.Generic;
using System.Linq;
using MSSQLServerAuditor.Presentation.Validation;
using MSSQLServerAuditor.Resources;

namespace MSSQLServerAuditor.ViewModels
{
	public class ScheduleIdViewModel : ValidatingScreen
	{
		private readonly List<ScheduleViewModel> _schedules;

		private string _id;

		public ScheduleIdViewModel(List<ScheduleViewModel> schedules)
		{
			this._schedules = schedules;

			ConfigureValidationRules();
		}

		public string Id
		{
			get { return this._id; }
			set
			{
				if (value != this._id)
				{
					this._id = value;
					NotifyOfPropertyChange(() => Id);
				}
			}
		}

		public void Accept()
		{
			ValidationResult validationResult = Validator.ValidateAll();

			if (validationResult.IsValid)
			{
				TryClose(true);
			}
		}

		public void Cancel()
		{
			TryClose(false);
		}

		protected override void OnInitialize()
		{
			base.OnInitialize();

			DisplayName = Strings.NewSchedule;
		}

		private void ConfigureValidationRules()
		{
			Validator.AddRequiredRule(() => Id, Strings.ScheduleIdIsRequired);
			Validator.AddRule(
				nameof(Id),
				() =>
				{
					if (this._schedules.Any(s => string.Equals(s.Id, Id, StringComparison.OrdinalIgnoreCase)))
					{
						return RuleResult.Invalid(Strings.ScheduleIdExists);
					}

					return RuleResult.Valid();
				}
			);
		}
	}
}
