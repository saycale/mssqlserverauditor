﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Caliburn.Micro;
using MSSQLServerAuditor.Common.Collections;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Persistence.Services;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Querying.Connections;
using MSSQLServerAuditor.Infrastructure;
using MSSQLServerAuditor.Presentation.Dialogs;
using MSSQLServerAuditor.Presentation.Interactivity.Autocomplete;
using MSSQLServerAuditor.Presentation.Validation;
using MSSQLServerAuditor.Resources;
using NLog;
using LogManager = NLog.LogManager;

namespace MSSQLServerAuditor.ViewModels
{
	public class DatabaseConnectionViewModel : ValidatingScreen, IConnectionDialog<SqlBaseInstance>
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly ConnectionType  _connectionType;
		private readonly IStorageManager _storageManager;
		private readonly IDialogService  _dialogService;

		private bool _isLoadingUsernames;
		private bool _isTestingConnection;

		private readonly IDisposable _validationSuspender;
		private          bool        _validationSuspenderDisposed;
		private          bool        _isKeyModificationEnabled = true;

		private AuthenticationMode _authMode;
		private string             _connectionName;
		private string             _connectionAlias;
		private string             _username;
		private string             _password;
		private string             _environment;
		private string             _domain;
		private string             _host;
		private string             _instanceName;
		private string             _instanceVersion;
		private bool               _isOdbc;
		private string             _connectionString;

		public DatabaseConnectionViewModel(
			IStorageManager storageManager,
			IWindowManager  windowManager,
			ConnectionType  connectionType
		)
		{
			this._storageManager = storageManager;
			this._connectionType = connectionType;

			this._dialogService = new DialogService(windowManager);

			Usernames         = new ObservableCollectionEx<string>();
			AutocompleteItems = new ObservableCollectionEx<AutocompleteItem>();

			ConfigureValidationRules();

			IsTestingConnection = false;

			// suppress validation till the first attempt to accept
			this._validationSuspender = Validator.SuppressValidation();
			this._validationSuspenderDisposed = false;

			AuthMode = AuthenticationMode.Windows;
		}

		protected override void OnInitialize()
		{
			base.OnInitialize();

			DisplayName = Strings.DatabaseConnection;

			UpdateConnectionStringAutocompletion();
		}

		public ObservableCollectionEx<AutocompleteItem> AutocompleteItems { get; private set; }

		public bool IsKeyModificationEnabled
		{
			get { return this._isKeyModificationEnabled; }
			set
			{
				if (this._isKeyModificationEnabled != value)
				{
					this._isKeyModificationEnabled = value;
					NotifyOfPropertyChange(() => IsKeyModificationEnabled);
				}
			}
		}

		public SqlBaseInstance Instance
		{
			get
			{
				Login login = new Login(
					Username,
					Password,
					AuthMode == AuthenticationMode.Windows
				);

				SqlBaseInstance instance;
				switch (this._connectionType)
				{
					case ConnectionType.MSSQL:
						instance = new MssqlInstance();
						break;
					case ConnectionType.TDSQL:
						instance = new TeradataInstance();
						break;
					default:
						throw new ArgumentOutOfRangeException("It is not designed for use with this type of connection.");
				}

				instance.ConnectionName   = ConnectionName;
				instance.Login            = login;
				instance.IsActive         = true;
				instance.ConnectionString = ConnectionString;
				instance.ConnectionType   = this._connectionType;
				instance.Domain           = Domain;
				instance.Environment      = Environment;
				instance.Host             = Host;
				instance.InstanceName     = InstanceName;
				instance.IsOdbc           = IsOdbc;
				instance.Version          = new InstanceVersion(InstanceVersion);

				if (CanEditConnectionAlias)
				{
					instance.ConnectionAlias = ConnectionAlias;
				}

				return instance;
			}
			set
			{
				Check.NotNull(value, nameof(value));

				if (value.ConnectionType != this._connectionType)
				{
					throw new ArgumentException("Dialog's connection type does not match to connection type of server instance");
				}

				ConnectionName   = value.ConnectionName;
				ConnectionString = value.ConnectionString;
				Domain           = value.Domain;
				Environment      = value.Environment;
				Host             = value.Host;
				InstanceName     = value.InstanceName;
				IsOdbc           = value.IsOdbc;
				InstanceVersion  = value.Version.ToString();

				Login login = value.Login;

				AuthMode = login.IsWinAuth ? AuthenticationMode.Windows : AuthenticationMode.SqlServer;
				if (!login.IsWinAuth)
				{
					Username = login.Name;
					Password = login.Password;
				}

				if (CanEditConnectionAlias)
				{
					ConnectionAlias = value.ConnectionAlias;
				}
			}
		}

		ServerInstance IConnectionDialog.Instance
		{
			get { return Instance; }
			set { Instance = (SqlBaseInstance) value; }
		}

		public ObservableCollectionEx<string> Usernames { get; }

		public bool IsLoadingUsernames
		{
			get { return this._isLoadingUsernames; }
			set
			{
				if (this._isLoadingUsernames != value)
				{
					this._isLoadingUsernames = value;
					NotifyOfPropertyChange(() => IsLoadingUsernames);
				}
			}
		}

		public List<AuthenticationMode> AuthModes { get; } =
			Enum.GetValues(typeof(AuthenticationMode)).OfType<AuthenticationMode>().ToList();

		public AuthenticationMode AuthMode
		{
			get { return this._authMode; }
			set
			{
				if (this._authMode != value)
				{
					this._authMode = value;

					Username = this._authMode == AuthenticationMode.SqlServer
						? string.Empty
						: LoggedOnUser;

					Password = string.Empty;

					FetchUsernamesAsync();

					NotifyOfPropertyChange(() => AuthMode);
					Validator.Validate(nameof(AuthMode));

					NotifyOfPropertyChange(() => Password);
					Validator.Validate(nameof(Password));
				}
			}
		}

		public bool CanEditConnectionAlias => this._connectionType == ConnectionType.MSSQL;

		public string ConnectionName
		{
			get { return this._connectionName; }
			set
			{
				if (this._connectionName != value)
				{
					this._connectionName = value;

					NotifyOfPropertyChange(() => ConnectionName);
					Validator.Validate(nameof(ConnectionName));
				}
			}
		}

		public string ConnectionAlias
		{
			get { return this._connectionAlias; }
			set
			{
				if (this._connectionAlias != value)
				{
					this._connectionAlias = value;

					NotifyOfPropertyChange(() => ConnectionAlias);
					Validator.Validate(nameof(ConnectionAlias));
				}
			}
		}

		public string Username
		{
			get { return this._username; }
			set
			{
				if (this._username != value)
				{
					this._username = value;

					NotifyOfPropertyChange(() => Username);
					Validator.Validate(nameof(Username));
				}
			}
		}

		public string Password
		{
			get { return this._password; }
			set
			{
				if (this._password != value)
				{
					this._password = value;

					NotifyOfPropertyChange(() => Password);
					Validator.Validate(nameof(Password));
				}
			}
		}

		public string Environment
		{
			get { return this._environment; }
			set
			{
				if (this._environment != value)
				{
					this._environment = value;

					NotifyOfPropertyChange(() => Environment);
					Validator.Validate(nameof(Environment));
				}
			}
		}

		public string Domain
		{
			get { return this._domain; }
			set
			{
				if (this._domain != value)
				{
					this._domain = value;

					NotifyOfPropertyChange(() => Domain);
					Validator.Validate(nameof(Domain));
				}
			}
		}

		public string Host
		{
			get { return this._host; }
			set
			{
				if (this._host != value)
				{
					this._host = value;

					NotifyOfPropertyChange(() => Host);
					Validator.Validate(nameof(Host));
				}
			}
		}

		public string InstanceName
		{
			get { return this._instanceName; }
			set
			{
				if (this._instanceName != value)
				{
					this._instanceName = value;

					NotifyOfPropertyChange(() => InstanceName);
					Validator.Validate(nameof(InstanceName));
				}
			}
		}

		public string InstanceVersion
		{
			get { return this._instanceVersion; }
			set
			{
				if (this._instanceVersion != value)
				{
					this._instanceVersion = value;

					NotifyOfPropertyChange(() => InstanceVersion);
					Validator.Validate(nameof(InstanceVersion));
				}
			}
		}

		public bool IsOdbc
		{
			get { return this._isOdbc; }
			set
			{
				if (this._isOdbc != value)
				{
					this._isOdbc = value;

					UpdateConnectionStringAutocompletion();

					NotifyOfPropertyChange(() => IsOdbc);
					Validator.Validate(nameof(IsOdbc));
				}
			}
		}

		public string ConnectionString
		{
			get { return this._connectionString; }
			set
			{
				if (this._connectionString != value)
				{
					this._connectionString = value;

					NotifyOfPropertyChange(() => ConnectionString);
					Validator.Validate(nameof(ConnectionString));
				}
			}
		}

		public bool IsTestingConnection
		{
			get { return this._isTestingConnection; }
			set
			{
				if (this._isTestingConnection != value)
				{
					this._isTestingConnection = value;
					NotifyOfPropertyChange(() => IsTestingConnection);
				}
			}
		}

		public void Accept()
		{
			EnableValidation();

			ValidationResult validationResult = Validator.ValidateAll();
			if (validationResult.IsValid)
			{
				TryClose(true);
			}
		}

		public void Cancel()
		{
			TryClose(false);
		}

		public async void TestConnection()
		{
			EnableValidation();

			if (!CanTestConnection)
			{
				return;
			}

			
			IsTestingConnection = true;

			IQueryConnection connection = null;
			Exception        error      = null;
			try
			{
				connection = await Task.Run(() => 
					Instance.OpenNewConnection(this._storageManager.StorageConnectionFactory)
				);
			}
			catch (Exception exc)
			{
				Log.Warn(exc, "Connection test failed");
				error = exc;
			}
			finally
			{
				IsTestingConnection = false;
				connection?.Close();
			}

			Dialog dialog;
			if (error == null)
			{
				dialog = new InformationDialog($"{Strings.ConnectionTestSucceeded}.", Answer.Ok);
			}
			else
			{
				dialog = new ErrorDialog($"{Strings.ConnectionTestFailed}:\n" + error.Message, Answer.Ok);
			}

			this._dialogService.ShowDialog(dialog);
		}

		private void ConfigureValidationRules()
		{
			Validator.AddRequiredRule(() => ConnectionName,   Strings.ConnectionNameIsRequired);
			Validator.AddRequiredRule(() => ConnectionString, Strings.ConnectionStringIsRequired);
			Validator.AddRequiredRule(() => Host,             Strings.HostIsRequired);
			Validator.AddRequiredRule(() => InstanceVersion,  Strings.InstanceVersionIsRequired);
			
			Validator.AddRule(
				nameof(InstanceVersion), () =>
				{
					if (InstanceVersion != null)
					{
						try
						{
							new InstanceVersion(InstanceVersion);
							return RuleResult.Valid();
						}
						catch (Exception)
						{
							return RuleResult.Invalid(Strings.IncorrectVersion);
						}
					}

					return RuleResult.Valid();
				}
			);

			Validator.AddRule(
				nameof(Username),
				() =>
				{
					if (AuthMode == AuthenticationMode.SqlServer)
					{
						return RuleResult.Assert(!string.IsNullOrWhiteSpace(Username), Strings.UsernameIsRequired);
					}

					return RuleResult.Valid();
				}
			);

			Validator.AddRule(
				nameof(Password),
				() =>
				{
					if (AuthMode == AuthenticationMode.SqlServer)
					{
						return RuleResult.Assert(!string.IsNullOrWhiteSpace(Password), Strings.PasswordIsRequired);
					}

					return RuleResult.Valid();
				}
			);
		}

		private void UpdateConnectionStringAutocompletion()
		{
			IEnumerable<string> parameters = ConnectionStringParameters
				.GetOfType(IsOdbc, _connectionType)
				.OrderBy(s => s);

			List<AutocompleteItem> snippets = new List<AutocompleteItem>();

			foreach (string arg in SqlBaseInstance.PlaceholderDictionary.Keys)
			{
				snippets.Add(new AppendAfterText(arg, @"[=\\\.]\s*"));
			}

			foreach (string param in parameters)
			{
				snippets.Add(new AppendAfterText(param, @"(^|;)\s*"));
			}

			AutocompleteItems.ReplaceRange(snippets);
		}

		private bool CanTestConnection
		{
			get
			{
				bool canTest = true;

				// validate properties required for connection test
				canTest &= Validator.Validate(nameof(ConnectionString)).IsValid;
				canTest &= Validator.Validate(nameof(Host)).IsValid;
				canTest &= Validator.Validate(nameof(Password)).IsValid;
				canTest &= Validator.Validate(nameof(Username)).IsValid;
				canTest &= Validator.Validate(nameof(InstanceVersion)).IsValid;

				return canTest;
			}
		}

		private void EnableValidation()
		{
			if (!this._validationSuspenderDisposed)
			{
				// dispose validation suspender to allow on-fly validation
				this._validationSuspender.Dispose();
				this._validationSuspenderDisposed = true;
			}
		}

		private async void FetchUsernamesAsync()
		{
			Usernames.Clear();

			if (AuthMode == AuthenticationMode.SqlServer)
			{
				try
				{
					IsLoadingUsernames = true;

					List<string> usernames = await LoadUsernamesAsync();
					Usernames.ReplaceRange(usernames);
				}
				catch (Exception exc)
				{
					Log.Error(exc, "Failed to load logins from database");
				}
				finally
				{
					IsLoadingUsernames = false;
				}
			}
		}

		private async Task<List<string>> LoadUsernamesAsync()
		{
			return await Task.Run(() => LoadUsernames());
		}

		private List<string> LoadUsernames()
		{
			// load all available usernames for sql server connection type
			ConnectionService cnnService = this._storageManager.MainStorage.Connections;

			return cnnService
				.GetLogins(this._connectionType, false)
				.Select(l => l.Name)
				.ToList();
		}

		private string LoggedOnUser =>
			$"{System.Environment.UserDomainName}\\{System.Environment.UserName}";
	}
}
