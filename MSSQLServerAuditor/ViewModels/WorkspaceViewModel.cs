﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Caliburn.Micro;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Tasks;
using MSSQLServerAuditor.Tasks;
using MSSQLServerAuditor.Tree;

namespace MSSQLServerAuditor.ViewModels
{
	public class WorkspaceViewModel : PropertyChangedBase, IWorkspace
	{
		private readonly IStatusBar _statusBar;

		private ContentViewModel _content;
		private AuditTreeNode    _auditNode;
		private bool             _isBusy;

		private CancellationTokenSource _canceller;

		public WorkspaceViewModel(IQueueTaskManager taskManager, IStatusBar statusBar)
		{
			Check.NotNull(taskManager, nameof(taskManager));
			Check.NotNull(statusBar,   nameof(statusBar));

			this._statusBar = statusBar;

			taskManager.TaskStatusChanged += TaskStatusChanged;

			this._canceller = new CancellationTokenSource();
		}

		private async void TaskStatusChanged(object sender, QueueTaskEventArgs args)
		{
			TreeTask task = args.Task as TreeTask;
			if (task != null)
			{
				switch (task.Status)
				{
					case QueueTaskStatus.Completed:
						AuditTreeNode node = task.Info.TreeNode;

						if (node != null && IsContentNode(node))
						{
							await UpdateContentAsync(node);
						}
						break;
				}
			}
		}

		public async Task RefreshContentAsync()
		{
			await this.UpdateContentAsync(this._auditNode);
		}

		public void SetNextUpdateDate(DateTime? nextUpdate)
		{
			Content.NextUpdateDate = nextUpdate;
			UpdateStatusBar();
		}

		public async Task UpdateContentAsync(AuditTreeNode auditNode)
		{
			if (IsBusy)
			{
				CancelUpdate();
			}

			IsBusy = true;

			try
			{
				if (auditNode == null)
				{
					this.Content = null;
				}
				else
				{
					this.Content = await auditNode.CreateContentAsync(this._canceller.Token);
				}

				this._auditNode = auditNode;
			}
			catch (OperationCanceledException)
			{
				/* skip exception */
			}
			finally
			{
				IsBusy = false;
			}
		}

		public ContentViewModel Content
		{
			get { return this._content; }
			private set
			{
				if (this._content != value)
				{
					ContentViewModel prevContent = this._content;
					this._content = value;

					prevContent?.Release();

					UpdateStatusBar();

					NotifyOfPropertyChange(() => Content);
				}
			}
		}

		public bool IsContentNode(AuditTreeNode auditNode)
		{
			return auditNode == this._auditNode;
		}

		public bool IsBusy
		{
			get { return this._isBusy; }
			set
			{
				if (this._isBusy != value)
				{
					this._isBusy = value;
					NotifyOfPropertyChange(() => IsBusy);
				}
			}
		}

		public void CancelUpdate()
		{
			this._canceller.Cancel();
			this._canceller = new CancellationTokenSource();
		}

		private void UpdateStatusBar()
		{
			this._statusBar.LastUpdateDate     = this._content?.UpdateDate;
			this._statusBar.LastUpdateDuration = this._content?.UpdateDuration;
			this._statusBar.NextUpdateDate     = this._content?.NextUpdateDate;
		}
	}
}
