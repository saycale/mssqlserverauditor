﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using Caliburn.Micro;
using MSSQLServerAuditor.Common;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Resources;

namespace MSSQLServerAuditor.ViewModels
{
	public class AboutViewModel : Screen
	{
		private readonly IStorageManager _storage;
		private readonly IAppSettings    _appSettings;

		private FlowDocument _aboutDocument;

		public AboutViewModel(
			IStorageManager storage,
			IAppSettings    appSettings)
		{
			Check.NotNull(storage,     nameof(storage));
			Check.NotNull(appSettings, nameof(appSettings));

			this._storage     = storage;
			this._appSettings = appSettings;

			PrepareAboutText();
		}

		private void PrepareAboutText()
		{
			Dictionary<string, string> properties = new Dictionary<string, string>
			{
				{ Strings.Product,       Product       },
				{ Strings.Copyright,     Copyright     },
				{ Strings.CompanyName,   CompanyName   },
				{ Strings.SupportEmail,  SupportEmail  },
				{ Strings.WebSite,       WebSite       },
				{ Strings.UserDirectory, UserDirectory },
				{ Strings.ProcessId,     ProcessId     },
				{ Strings.DatabaseInfo,  DatabaseInfo  }
			};

			FlowDocument doc = new FlowDocument();

			Table table = new Table();

			TableColumn propNameCol  = new TableColumn();
			TableColumn propValueCol = new TableColumn { Width = GridLength.Auto };

			table.Columns.Add(propNameCol);
			table.Columns.Add(propValueCol);

			TableRowGroup rowGroup = new TableRowGroup();
			table.RowGroups.Add(rowGroup);

			// used to measure the width of the "property name" column 
			// (TableColumn's auto-size doesn't work)
			TextBlock measureBlock = new TextBlock
			{
				FontFamily = doc.FontFamily,
				FontSize   = doc.FontSize,
			};

			double maxWidth = 0;

			foreach (KeyValuePair<string, string> property in properties)
			{
				string propName  = $"{property.Key}:";
				string propValue = property.Value;

				Inline headerSpan = new Bold(new Run(propName));
				Inline valueSpan  = new Run(propValue);

				// measure "property name" cell width
				measureBlock.Inlines.Clear();
				measureBlock.Inlines.Add(headerSpan);
				measureBlock.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));

				maxWidth = Math.Max(measureBlock.DesiredSize.Width, maxWidth);

				TableRow row = new TableRow();
				
				row.Cells.Add(new TableCell(new Paragraph(headerSpan)));
				row.Cells.Add(new TableCell(new Paragraph(valueSpan)));

				rowGroup.Rows.Add(row);
			}

			propNameCol.Width = new GridLength(maxWidth + 20);

			doc.Blocks.Add(table);
			this._aboutDocument = doc;
		}

		protected override void OnInitialize()
		{
			base.OnInitialize();

			DisplayName = Strings.About;
		}

		public FlowDocument AboutDocument
		{
			get { return this._aboutDocument; }
			set
			{
				if (!Equals(this._aboutDocument, value))
				{
					this._aboutDocument = value;
					NotifyOfPropertyChange(() => AboutDocument);
				}
			}
		}

		public void Accept()
		{
			TryClose(true);
		}

		public string Product => 
			$"{CurrentAssembly.ProductName} {CurrentAssembly.Version} ({AppVersion.Edition})";

		public string Copyright     => CurrentAssembly.Copyright;
		public string CompanyName   => CurrentAssembly.Company;
		public string SupportEmail  => this._appSettings.System.SupportEmail;
		public string WebSite       => this._appSettings.System.SupportUrl;
		public string UserDirectory => this._appSettings.UserSettingsDirectory;
		public string ProcessId     => $"{Process.GetCurrentProcess().ProcessName} (Id: {Process.GetCurrentProcess().Id})";

		public string DatabaseInfo
		{
			get
			{
				StorageConnectionInfo cnnInfo = this._storage.ConnectionInfo;
				switch (cnnInfo.StorageType)
				{
					case StorageType.MSSQL:
						return $"{StorageType.MSSQL}";
					case StorageType.SQLite:
						return $"{StorageType.SQLite} ({cnnInfo.WorkDirectory})";
					default:
						throw new ArgumentOutOfRangeException(cnnInfo.StorageType.ToString());
				}
			}
		}
	}
}
