﻿using System;
using System.Collections.Generic;
using Caliburn.Micro;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Messages;
using MSSQLServerAuditor.Presentation.Dialogs;
using MSSQLServerAuditor.Resources;

namespace MSSQLServerAuditor.ViewModels
{
	public class SettingsViewModel : Screen
	{
		private readonly IAppSettings     _origSettings;
		private readonly UserSettings     _modSettings;
		private readonly IEventAggregator _eventAggregator;
		private readonly DialogService    _dialogService;

		public SettingsViewModel(
			IWindowManager   windowManager,
			IAppSettings     appSettings,
			IEventAggregator eventAggregator)
		{
			this._dialogService   = new DialogService(windowManager);
			this._origSettings    = appSettings;
			this._modSettings     = appSettings.User.XmlClone();
			this._eventAggregator = eventAggregator;
		}

		protected override void OnInitialize()
		{
			base.OnInitialize();

			DisplayName = Strings.Settings;
		}

		public string UiLanguage
		{
			get { return this._modSettings.UiLanguage; }
			set
			{
				if (this._modSettings.UiLanguage != value)
				{
					this._modSettings.UiLanguage = value;
					NotifyOfPropertyChange(() => UiLanguage);
				}
			}
		}

		public string ReportLanguage
		{
			get { return this._modSettings.ReportLanguage; }
			set
			{
				if (this._modSettings.ReportLanguage != value)
				{
					this._modSettings.ReportLanguage = value;
					NotifyOfPropertyChange(() => ReportLanguage);
				}
			}
		}

		public int ConnectionTimeoutSec
		{
			get { return this._modSettings.Timeout; }
			set
			{
				if (this._modSettings.Timeout != value)
				{
					this._modSettings.Timeout = value;
					NotifyOfPropertyChange(() => ConnectionTimeoutSec);
				}
			}
		}

		public int MaxThreads
		{
			get { return this._modSettings.MaxThreads; }
			set
			{
				if (this._modSettings.MaxThreads != value)
				{
					this._modSettings.MaxThreads = value;
					NotifyOfPropertyChange(() => MaxThreads);
				}
			}
		}

		public bool ShowXmlTab
		{
			get { return this._modSettings.ShowXml; }
			set
			{
				if (this._modSettings.ShowXml != value)
				{
					this._modSettings.ShowXml = value;
					NotifyOfPropertyChange(() => ShowXmlTab);
				}
			}
		}

		public bool ShowHtmlTabs
		{
			get { return this._modSettings.ShowHtml; }
			set
			{
				if (this._modSettings.ShowHtml != value)
				{
					this._modSettings.ShowHtml = value;
					NotifyOfPropertyChange(() => ShowHtmlTabs);
				}
			}
		}

		public List<string> UiLanguages     => this._origSettings.System.UiLanguages;
		public List<string> ReportLanguages => this._origSettings.System.ReportLanguages;

		public void Accept()
		{
			UserSettings oldSettings = this._origSettings.User.XmlClone();

			this._origSettings.User.UiLanguage     = this._modSettings.UiLanguage;
			this._origSettings.User.ReportLanguage = this._modSettings.ReportLanguage;
			this._origSettings.User.Timeout        = this._modSettings.Timeout;
			this._origSettings.User.MaxThreads     = this._modSettings.MaxThreads;
			this._origSettings.User.ShowXml        = this._modSettings.ShowXml;
			this._origSettings.User.ShowHtml       = this._modSettings.ShowHtml;

			try
			{
				this._origSettings.SaveUserSettings();

				this._eventAggregator.PublishOnUIThread(
					new SettingsUpdateMessage(oldSettings, this._origSettings.User)
				);

				TryClose(true);
			}
			catch (Exception exc)
			{
				// failed to save settings
				// discard changes and show error
				this._origSettings.ReloadUserSettings();

				ErrorDialog dialog = new ErrorDialog(
					Strings.SavingError,
					string.Format(Strings.SavingErrorMessageTemplate, exc.Message),
					Answer.Ok
				);

				this._dialogService.ShowDialog(dialog);
			}
		}

		public void Cancel()
		{
			TryClose(false);
		}
	}
}
