﻿using System;
using System.IO;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Infrastructure;
using MSSQLServerAuditor.Presentation.Validation;
using MSSQLServerAuditor.Resources;
using NLog;

namespace MSSQLServerAuditor.ViewModels
{
	public class SqliteConnectionViewModel : ValidatingScreen, IConnectionDialog<SqliteInstance>
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();
		
		private string _connectionName;
		private string _connectionAlias;
		private string _databasePath;

		private readonly IDisposable _validationSuspender;

		private bool _validationSuspenderDisposed;
		private bool _isKeyModificationEnabled = true;

		public SqliteConnectionViewModel()
		{
			ConfigureValidationRules();

			// suppress validation till the first attempt to accept
			this._validationSuspender = Validator.SuppressValidation();
			this._validationSuspenderDisposed = false;
		}

		public SqliteInstance Instance
		{
			get
			{
				SqliteInstance instance = new SqliteInstance
				{
					DatabaseFilePath = DatabasePath,
					IsActive         = true,
					ConnectionName   = ConnectionName,
					ConnectionAlias  = ConnectionAlias,
					Login            = Login.CreateEmpty(),
					ConnectionType   = ConnectionType.SQLite
				};


				return instance;
			}
			set
			{
				Check.NotNull(value, nameof(value));

				ConnectionName  = value.ConnectionName;
				DatabasePath    = value.DatabaseFilePath;
				ConnectionAlias = value.ConnectionAlias;
			}
		}

		ServerInstance IConnectionDialog.Instance
		{
			get { return Instance; }
			set { Instance = (SqliteInstance) value; }
		}

		public bool IsKeyModificationEnabled
		{
			get { return this._isKeyModificationEnabled; }
			set
			{
				if (this._isKeyModificationEnabled != value)
				{
					this._isKeyModificationEnabled = value;
					NotifyOfPropertyChange(() => IsKeyModificationEnabled);
				}
			}
		}

		public string ConnectionName
		{
			get { return this._connectionName; }
			set
			{
				if (this._connectionName != value)
				{
					this._connectionName = value;

					NotifyOfPropertyChange(() => ConnectionName);
					Validator.Validate(nameof(ConnectionName));
				}
			}
		}

		public string DatabasePath
		{
			get { return this._databasePath; }
			set
			{
				if (this._databasePath != value)
				{
					this._databasePath = value;

					NotifyOfPropertyChange(() => DatabasePath);
					Validator.Validate(nameof(DatabasePath));
				}
			}
		}

		public string ConnectionAlias
		{
			get { return this._connectionAlias; }
			set
			{
				if (this._connectionAlias != value)
				{
					this._connectionAlias = value;

					NotifyOfPropertyChange(() => ConnectionAlias);
					Validator.Validate(nameof(ConnectionAlias));
				}
			}
		}

		public void Accept()
		{
			EnableValidation();

			ValidationResult validationResult = Validator.ValidateAll();
			if (validationResult.IsValid)
			{
				TryClose(true);
			}
		}

		public void Cancel()
		{
			TryClose(false);
		}

		protected override void OnInitialize()
		{
			base.OnInitialize();

			DisplayName = Strings.SqliteConnectionProperties;
		}

		private void ConfigureValidationRules()
		{
			Validator.AddRequiredRule(() => ConnectionName, Strings.ConnectionNameIsRequired);
			
			Validator.AddRule(
				nameof(DatabasePath),
				() =>
				{
					string dbFilePath = DatabasePath;

					if (string.IsNullOrWhiteSpace(dbFilePath))
					{
						return RuleResult.Invalid(Strings.DatabaseFileIsRequired);
					}

					if (!File.Exists(dbFilePath))
					{
						return RuleResult.Invalid(Strings.DatabaseFileNotExists);
					}

					return RuleResult.Valid();
				}
			);
		}

		private void EnableValidation()
		{
			if (!this._validationSuspenderDisposed)
			{
				// dispose validation suspender to allow on-fly validation
				this._validationSuspender.Dispose();
				this._validationSuspenderDisposed = true;
			}
		}
	}
}
