﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Mail;
using System.Threading.Tasks;
using CronExpressionDescriptor;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Presentation.Validation;
using MSSQLServerAuditor.Resources;

namespace MSSQLServerAuditor.ViewModels
{
	public class ScheduleViewModel : ValidatingViewModel
	{
		private static readonly Options DescOptions = new Options { Use24HourTimeFormat = true };

		private static readonly string DefaultCronExpression = "0 0/1 * * * ?";

		private string    _name;
		private string    _id;
		private bool      _isEnabled;
		private string    _cronExpression;
		private string    _targetMachine;
		private bool      _isServiceOnly;
		private bool      _isEmailEnabled;
		private string    _recipients;
		private bool      _sendNonEmptyNotificationsOnly;
		private string    _notificationLanguage;
		private DateTime? _nextFireTime;
		private bool      _updateHierarchically;
		private int       _updateDeep = -1;

		private readonly IDisposable _validationSuspender;
		private          bool        _validationSuspenderDisposed;

		public ScheduleViewModel(string scheduleId, IAppSettings settings)
		{
			Check.NotNullOrWhiteSpace(scheduleId, nameof(scheduleId));

			Id = scheduleId;

			ConfigureValidationRules();
			Validator.ResultChanged += ValidatorOnResultChanged;

			this._validationSuspender = Validator.SuppressValidation();

			NotificationLanguages = new ObservableCollection<string> { string.Empty };

			foreach (string reportLang in settings.System.ReportLanguages)
			{
				NotificationLanguages.Add(reportLang);
			}

			CronExpression = DefaultCronExpression;
			UpdateDeep     = -1; // infinite update
		}

		public ObservableCollection<string> NotificationLanguages { get; private set; }

		/// <summary>
		/// Enable validation
		/// </summary>
		public void EnableValidation()
		{
			if (!this._validationSuspenderDisposed)
			{
				this._validationSuspender.Dispose();
				this._validationSuspenderDisposed = true;
			}
		}

		public string Id
		{
			get { return this._id; }
			set
			{
				if (this._id != value)
				{
					this._id = value;

					NotifyOfPropertyChange(() => Id);
					Validator.Validate(nameof(Id));
				}
			}
		}

		public string ScheduleName
		{
			get { return this._name; }
			set
			{
				if (this._name != value)
				{
					this._name = value;

					NotifyOfPropertyChange(() => ScheduleName);
					Validator.Validate(nameof(ScheduleName));
				}
			}
		}

		public bool IsScheduleEnabled
		{
			get { return this._isEnabled; }
			set
			{
				if (this._isEnabled != value)
				{
					this._isEnabled = value;

					NotifyOfPropertyChange(() => IsScheduleEnabled);
				}
			}
		}

		public string CronExpression
		{
			get { return this._cronExpression; }
			set
			{
				if (this._cronExpression != value)
				{
					this._cronExpression = value;

					NotifyOfPropertyChange(() => CronExpression);
					NotifyOfPropertyChange(() => CronDescription);
					Validator.Validate(nameof(CronExpression));

					UpdateNextFireTimeAsync();
				}
			}
		}

		public DateTime? NextFireTime
		{
			get { return this._nextFireTime; }
			set
			{
				if (this._nextFireTime != value)
				{
					this._nextFireTime = value;
					NotifyOfPropertyChange(() => NextFireTime);
				}
			}
		}

		public string TargetMachine
		{
			get { return this._targetMachine; }
			set
			{
				if (this._targetMachine != value)
				{
					this._targetMachine = value;

					NotifyOfPropertyChange(() => TargetMachine);
				}
			}
		}

		public bool IsServiceOnly
		{
			get { return this._isServiceOnly; }
			set
			{
				if (this._isServiceOnly != value)
				{
					this._isServiceOnly = value;

					NotifyOfPropertyChange(() => IsServiceOnly);
				}
			}
		}

		public bool IsEmailEnabled
		{
			get { return this._isEmailEnabled; }
			set
			{
				if (this._isEmailEnabled != value)
				{
					this._isEmailEnabled = value;

					NotifyOfPropertyChange(() => IsEmailEnabled);
				}
			}
		}

		public string Recipients
		{
			get { return this._recipients; }
			set
			{
				if (this._recipients != value)
				{
					this._recipients = value;

					NotifyOfPropertyChange(() => Recipients);
					Validator.Validate(nameof(Recipients));
				}
			}
		}

		public bool SendNonEmptyNotificationsOnly
		{
			get { return this._sendNonEmptyNotificationsOnly; }
			set
			{
				if (this._sendNonEmptyNotificationsOnly != value)
				{
					this._sendNonEmptyNotificationsOnly = value;

					NotifyOfPropertyChange(() => SendNonEmptyNotificationsOnly);
				}
			}
		}

		public string NotificationLanguage
		{
			get { return this._notificationLanguage; }
			set
			{
				if (this._notificationLanguage != value)
				{
					this._notificationLanguage = value;

					NotifyOfPropertyChange(() => NotificationLanguage);
				}
			}
		}

		public bool UpdateHierarchically
		{
			get { return this._updateHierarchically; }
			set
			{
				if (this._updateHierarchically != value)
				{
					this._updateHierarchically = value;

					NotifyOfPropertyChange(() => UpdateHierarchically);
				}
			}
		}

		public int UpdateDeep
		{
			get { return this._updateDeep; }
			set
			{
				if (this._updateDeep != value)
				{
					this._updateDeep = value;

					NotifyOfPropertyChange(() => UpdateDeep);
				}
			}
		}

		public string CronDescription
		{
			get
			{
				if (!string.IsNullOrWhiteSpace(CronExpression))
				{
					string cron = CronExpression.Trim();

					try
					{
						if (Quartz.CronExpression.IsValidExpression(cron))
						{
							return ExpressionDescriptor.GetDescription(cron, DescOptions);
						}

						return Strings.InvalidCronExpression;
					}
					catch (Exception exc)
					{
						return exc.Message;
					}
				}

				return string.Empty;
			}
		}

		private void ConfigureValidationRules()
		{
			Validator.AddRequiredRule(() => Id,           Strings.ScheduleIdIsRequired);
			Validator.AddRequiredRule(() => ScheduleName, Strings.ScheduleNameIsRequired);
			Validator.AddRule(
				nameof(CronExpression),
				() =>
				{
					if (!IsValidCronExpression(CronExpression))
					{
						return RuleResult.Invalid(Strings.InvalidCronExpression);
					}

					return RuleResult.Valid();
				}
			);

			Validator.AddRule(
				nameof(Recipients),
				() =>
				{
					if (IsEmailEnabled)
					{
						if (string.IsNullOrWhiteSpace(Recipients))
						{
							return RuleResult.Invalid(Strings.EnterRecipients);
						}

						string[] emails = Recipients.Split(
							new[] { ";" },
							StringSplitOptions.RemoveEmptyEntries
						);

						List<string> invalidEmails = new List<string>();
						foreach (string email in emails)
						{
							try
							{
								new MailAddress(email);
							}
							catch (Exception)
							{
								invalidEmails.Add(email);
							}
						}

						if (invalidEmails.Count != 0)
						{
							string invalidEmailsString = string.Join("; ", invalidEmails);

							return RuleResult.Invalid(
								string.Format($"{Strings.InvalidEmailAddresses}:\n{invalidEmailsString}")
							);
						}
					}

					return RuleResult.Valid();
				}
			);
		}

		private void ValidatorOnResultChanged(object sender, ValidationResultChangedEventArgs args)
		{
			NotifyOfPropertyChange(() => HasErrors);
		}

		private static bool IsValidCronExpression(string cron)
		{
			if (string.IsNullOrWhiteSpace(cron))
			{
				return false;
			}

			try
			{
				return Quartz.CronExpression.IsValidExpression(cron);
			}
			catch (Exception)
			{
				return false;
			}
		}

		private static DateTime? GetNextFireTime(string cronExpression)
		{
			if (IsValidCronExpression(cronExpression))
			{
				Quartz.CronExpression cron = new Quartz.CronExpression(cronExpression);
				DateTimeOffset? offset = cron.GetTimeAfter(DateTimeOffset.UtcNow);

				if (offset != null)
				{
					return offset.Value.LocalDateTime;
				}
			}

			return null;
		}

		private async void UpdateNextFireTimeAsync()
		{
			NextFireTime = await Task.Run(() => GetNextFireTime(CronExpression));
		}
	}
}
