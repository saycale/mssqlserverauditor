﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using MSSQLServerAuditor.Common;
using MSSQLServerAuditor.Resources;

namespace MSSQLServerAuditor.ViewModels
{
	public class ComponentsViewModel : Screen
	{
		private string _componentsInfo;
		private bool   _isBusy = true;

		public bool IsBusy
		{
			get { return this._isBusy; }
			set
			{
				if (this._isBusy != value)
				{
					this._isBusy = value;
					NotifyOfPropertyChange(() => IsBusy);
					NotifyOfPropertyChange(() => CanCopyComponentsInfoToClipboard);
				}
			}
		}

		public string ComponentsInfo
		{
			get { return this._componentsInfo; }
			set
			{
				if (this._componentsInfo != value)
				{
					this._componentsInfo = value;
					NotifyOfPropertyChange(() => ComponentsInfo);
				}
			}
		}

		public void Accept()
		{
			TryClose(true);
		}

		public void CopyComponentsInfoToClipboard()
		{
			Clipboard.SetText(ComponentsInfo);
		}

		public bool CanCopyComponentsInfoToClipboard => !IsBusy;

		protected override async void OnViewLoaded(object view)
		{
			base.OnViewLoaded(view);

			try
			{
				ComponentsInfo = await GetComponentsInfoAsync();
			}
			catch (Exception exc)
			{
				ComponentsInfo = exc.ToString();
			}
			finally
			{
				IsBusy = false;
			}
		}

		protected override void OnInitialize()
		{
			base.OnInitialize();
			
			DisplayName = string.Format(
				Strings.ComponentsOfPattern,
				$"{CurrentAssembly.Title} {AppVersion.Edition}"
			);
		}
		
		private async Task<string> GetComponentsInfoAsync()
		{
			return await Task.Run(() => GetComponentInfo());
		}

		private string GetComponentInfo()
		{
			StringBuilder sb = new StringBuilder();

			sb.AppendLine(GetAssembliesString());
			sb.AppendLine();
			sb.AppendLine(GetFrameworksString());
			sb.AppendLine();
			sb.AppendLine(GetOSVersionString());
			sb.AppendLine();
			sb.AppendLine(GetADOVersionString());
			sb.AppendLine();
			sb.AppendLine(GetIEVersionString());

			return sb.ToString();
		}

		private string GetAssembliesString()
		{
			StringBuilder sb = new StringBuilder();

			sb.AppendLine($"{Strings.AssembliesLoaded}:");
			sb.AppendLine(new string('-', 30));
			sb.AppendLine();

			List<AssemblyName> assemblies = RuntimeEnvironment.LoadedAssemblies
				.OrderBy(a => a.Name)
				.ToList();

			int maxNameLen = 0;
			int maxVersLen = 0;
			foreach (AssemblyName assembly in assemblies)
			{
				maxNameLen = Math.Max(maxNameLen, assembly.Name.Length);
				maxVersLen = Math.Max(maxVersLen, assembly.Version.ToString().Length);
			}

			string formatString = $"{{0, -{maxNameLen + 3}}}{{1, -{maxVersLen + 3}}}{{2,7}}";
			foreach (AssemblyName assembly in assemblies)
			{
				string assemblyLine = string.Format(
					formatString,
					assembly.Name,
					"v." + assembly.Version,
					assembly.ProcessorArchitecture
				);

				sb.AppendLine(assemblyLine);
			}

			return sb.ToString();
		}

		private string GetFrameworksString()
		{
			StringBuilder sb = new StringBuilder();
			List<FrameworkInfo> frameworks = RuntimeEnvironment
				.GetInstalledFrameworks()
				.OrderBy(fi => fi.Version)
				.ToList();

			sb.AppendLine($"{Strings.NetFrameworksInstalled}:");
			sb.AppendLine(new string('-', 30));
			sb.AppendLine();

			foreach (FrameworkInfo framework in frameworks)
			{
				sb.AppendLine(framework.ToString());
			}

			return sb.ToString();
		}

		private string GetIEVersionString()
		{
			return $"{Strings.MicrosoftIE}: {RuntimeEnvironment.IeVersion}";
		}

		private string GetADOVersionString()
		{
			return $"{Strings.ADODatabaseRuntime}: {RuntimeEnvironment.AdoVersion}";
		}

		private string GetOSVersionString()
		{
			return $"{Strings.WindowsVersion}: {RuntimeEnvironment.WinVersion}";
		}
	}
}
