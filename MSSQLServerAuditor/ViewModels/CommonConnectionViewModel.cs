﻿using System;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Infrastructure;
using MSSQLServerAuditor.Presentation.Validation;
using MSSQLServerAuditor.Resources;
using NLog;

namespace MSSQLServerAuditor.ViewModels
{
	public class CommonConnectionViewModel : ValidatingScreen, IConnectionDialog
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly ConnectionType _connectionType;

		private string _connectionName;
		private string _connectionString;

		private readonly IDisposable _validationSuspender;

		private bool _validationSuspenderDisposed;
		private bool _isKeyModificationEnabled = true;

		public CommonConnectionViewModel(ConnectionType connectionType)
		{
			this._connectionType = connectionType;

			ConfigureValidationRules();

			// suppress validation till the first attempt to accept
			this._validationSuspender         = Validator.SuppressValidation();
			this._validationSuspenderDisposed = false;
		}

		public ServerInstance Instance
		{
			get
			{
				ServerInstance instance;
				switch (this._connectionType)
				{
					case ConnectionType.SQLite:
						instance = new SqliteInstance { DatabaseFilePath = ConnectionString };
						break;
					case ConnectionType.ActiveDirectory:
						instance = new LdapInstance { ConnectionPath = ConnectionString };
						break;
					case ConnectionType.EventLog:
						instance = new EventLogInstance { MachineName = ConnectionString };
						break;
					default:
						throw new ArgumentOutOfRangeException($"Dialog is not intended to use for connection type '{this._connectionType}'");
				}
				
				instance.IsActive       = true;
				instance.ConnectionName = ConnectionName;
				instance.Login          = Login.CreateEmpty();
				instance.ConnectionType = this._connectionType;

				return instance;
			}
			set
			{
				Check.NotNull(value, nameof(value));

				ConnectionName = value.ConnectionName;

				switch (this._connectionType)
				{
					case ConnectionType.SQLite:
						ConnectionString = ((SqliteInstance) value).DatabaseFilePath;
						break;
					case ConnectionType.ActiveDirectory:
						ConnectionString = ((LdapInstance) value).ConnectionPath;
						break;
					case ConnectionType.EventLog:
						ConnectionString = ((EventLogInstance) value).MachineName;
						break;
					default:
						throw new ArgumentOutOfRangeException($"Dialog is not intended to use for connection type '{this._connectionType}'");
				}
			}
		}

		public bool IsKeyModificationEnabled
		{
			get { return this._isKeyModificationEnabled; }
			set
			{
				if (this._isKeyModificationEnabled != value)
				{
					this._isKeyModificationEnabled = value;
					NotifyOfPropertyChange(() => IsKeyModificationEnabled);
				}
			}
		}

		public string ConnectionName
		{
			get { return this._connectionName; }
			set
			{
				if (this._connectionName != value)
				{
					this._connectionName = value;

					NotifyOfPropertyChange(() => ConnectionName);
					Validator.Validate(nameof(ConnectionName));
				}
			}
		}

		public string ConnectionString
		{
			get { return this._connectionString; }
			set
			{
				if (this._connectionString != value)
				{
					this._connectionString = value;

					NotifyOfPropertyChange(() => ConnectionString);
					Validator.Validate(nameof(ConnectionString));
				}
			}
		}

		public void Accept()
		{
			EnableValidation();

			ValidationResult validationResult = Validator.ValidateAll();
			if (validationResult.IsValid)
			{
				TryClose(true);
			}
		}

		public void Cancel()
		{
			TryClose(false);
		}

		protected override void OnInitialize()
		{
			base.OnInitialize();

			DisplayName = Strings.ConnectionProperties;
		}

		private void ConfigureValidationRules()
		{
			Validator.AddRequiredRule(() => ConnectionName,   Strings.ConnectionNameIsRequired);
			Validator.AddRequiredRule(() => ConnectionString, Strings.ConnectionStringIsRequired);
		}

		private void EnableValidation()
		{
			if (!this._validationSuspenderDisposed)
			{
				// dispose validation suspender to allow on-fly validation
				this._validationSuspender.Dispose();
				this._validationSuspenderDisposed = true;
			}
		}
	}
}
