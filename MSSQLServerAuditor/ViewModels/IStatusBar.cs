﻿using System;

namespace MSSQLServerAuditor.ViewModels
{
	public interface IStatusBar
	{
		string    GroupName          { get; set; }
		string    Login              { get; set; }
		int?      InstancesCount     { get; set; }
		DateTime? LastUpdateDate     { get; set; }
		TimeSpan? LastUpdateDuration { get; set; }
		DateTime? NextUpdateDate     { get; set; }
		bool      ShowContextInfo    { get; set; }

		void Cleanup();
	}
}