﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Controls;
using Caliburn.Micro;
using MSSQLServerAuditor.Common.Async;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Controls.TreeView;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.Menu;
using MSSQLServerAuditor.Schedule;
using MSSQLServerAuditor.Tasks;
using MSSQLServerAuditor.Tree;
using NLog;
using LogManager = NLog.LogManager;

namespace MSSQLServerAuditor.ViewModels
{
	public class AuditForestViewModel : PropertyChangedBase, IAuditForest
	{
		public event EventHandler CurrentNodeChanged;
		public event EventHandler TreeChanged;

		private readonly IShell _shell;

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private AdvTreeNode _currentNode;
		private AdvTreeNode _origin;

		private readonly GuiJobScheduler _jobScheduler;

		public AuditForestViewModel(IShell shell)
		{
			Check.NotNull(shell, nameof(shell));

			this._shell = shell;
			this.Origin = new AdvTreeNode();

			Origin.Children.CollectionChanged += (sender, args) =>
			{
				OnTreeChanged();
			};

			this._jobScheduler = new GuiJobScheduler(
				this,
				shell.AppSettings,
				shell.XmlLoader
			);

			this._jobScheduler.StartJobScheduler();
		}
	
		public AdvTreeNode Origin
		{
			get { return this._origin; }
			set
			{
				if (this._origin != value)
				{
					this._origin = value;
					NotifyOfPropertyChange(() => Origin);
				}
			}
		}

		public AdvTreeNode CurrentNode
		{
			get { return this._currentNode; }
			set
			{
				if (this._currentNode != value)
				{
					this._currentNode = value;

					AuditTreeNode contentNode = this._currentNode as AuditTreeNode;
					if (contentNode != null)
					{
						UpdateStatusBar(contentNode);

						NotifyTaskCompletion.Create(UpdateContentAsync(contentNode));
					}
					else
					{
						CleanupStatusBar();
						NotifyTaskCompletion.Create(CleanupContentAsync());
					}

					OnCurrentNodeChanged();
				}
			}
		}

		public GuiJobScheduler JobScheduler => this._jobScheduler;

		public async Task CloseTreeAsync(TemplateTreeRoot root)
		{
			AuditTree tree = root.AuditTree;
			if (tree != null)
			{
				await RemoveTreeAsync(root);

				CurrentNode = null;
			}
		}

		public async Task RebuildTreeAsync(TemplateTreeRoot root)
		{
			AuditTree tree = root.AuditTree;
			if (tree != null)
			{
				int index = Origin.Children.IndexOf(root);
				await RemoveTreeAsync(root);

				CurrentNode = null;

				AuditTree newTree = await this._shell.OpenConnectionAsync(tree.Context);

				if (index >= 0)
				{
					await InsertTreeAsync(newTree.Root, index);
				}
				else
				{
					await AddTreeAsync(newTree.Root);
				}
			}
		}

		public async Task AddTreeAsync(TemplateTreeRoot root)
		{
			Origin.Children.Add(root);

			await Task.Run(() => ScheduleTreeJobs(root));
		}

		public async Task InsertTreeAsync(TemplateTreeRoot root, int index)
		{
			Origin.Children.Insert(index, root);

			await Task.Run(() => ScheduleTreeJobs(root));
		}

		public async Task<bool> RemoveTreeAsync(TemplateTreeRoot root)
		{
			bool result = Origin.Children.Remove(root);

			await Task.Run(() => UnscheduleTreeJobs(root));

			return result;
		}

		public async Task ClearForestAsync()
		{
			List<TemplateTreeRoot> rootsCopy = Origin.Children
				.OfType<TemplateTreeRoot>()
				.ToList();

			await Task.WhenAll(rootsCopy.Select(CloseTreeAsync));
		}

		private void ScheduleTreeJobs(TemplateTreeRoot root)
		{
			AuditTree tree = root.AuditTree;

			if (tree != null)
			{
				try
				{
					this._jobScheduler.ScheduleJobs(root);
				}
				catch (Exception exc)
				{
					Log.Error(exc, $"Failed to schedule jobs for connection '{tree.Context.ConnectionGroup.Name}'");
				}
			}
		}

		private void UnscheduleTreeJobs(TemplateTreeRoot root)
		{
			AuditTree tree = root.AuditTree;

			if (tree != null)
			{
				try
				{
					this._jobScheduler.UnscheduleJobs(tree.Context);
				}
				catch (Exception exc)
				{
					Log.Error(exc, $"Failed to unschedule jobs for connection '{tree.Context.ConnectionGroup.Name}'");
				}
			}
		}

		public void OpenTreeContextMenu(object item, ContextMenuEventArgs args)
		{
			AuditTreeNode auditNode = item as AuditTreeNode;
			if (auditNode != null)
			{
				List<ContextMenuItem> menuItems = auditNode.CreateMenuItems();
				if (menuItems.Any())
				{
					MenuItemContext context = new MenuItemContext(
						this._shell,
						auditNode
					);

					ContextMenu contextMenu = new ContextMenu();
					foreach (ContextMenuItem menuItem in menuItems)
					{
						if (menuItem == SeparatorMenuItem.Instance)
						{
							contextMenu.Items.Add(new Separator());
							continue;
						} 

						MenuItem menu = new MenuItem
						{
							Header    = menuItem.GetHeader(context),
							Icon      = new Image { Source = menuItem.GetIcon(context) },
							IsEnabled = menuItem.IsEnabled(context)
						};

						menu.Click += async delegate
						{
							await menuItem.ExecuteAsync(context);
						};

						contextMenu.Items.Add(menu);
					}

					contextMenu.IsOpen = true;
				}
			}
		}

		public void RefreshTrees()
		{
			foreach (AdvTreeNode child in Origin.Children)
			{
				RefreshNode(child);
			}
		}

		public void UpdateCurrentNode()
		{
			AuditTreeNode auditNode = CurrentNode as AuditTreeNode;

			if (auditNode != null)
			{
				AuditTree tree = auditNode.AuditTree;

				TreeTaskInfo taskInfo = new TreeTaskInfo(auditNode)
				{
					FetchMode            = FetchMode.Remote,
					UpdateHierarchically = false
				};

				tree.RequestUpdate(taskInfo);
			}
		}

		private void RefreshNode(AdvTreeNode node)
		{
			// reload node settings from db
			TemplateTreeNode templateNode = node as TemplateTreeNode;
			templateNode?.RefreshSettings();

			node.Refresh();

			foreach (AdvTreeNode child in node.Children)
			{
				RefreshNode(child);
			}
		}

		private void CleanupStatusBar()
		{
			IStatusBar statusBar = this._shell.StatusBar;
			statusBar.Cleanup();
		}

		private async Task CleanupContentAsync()
		{
			await this._shell.Workspace.UpdateContentAsync(null);
		}

		private void UpdateStatusBar(AuditTreeNode contentNode)
		{
			AuditContext context = contentNode.AuditTree?.Context;

			IStatusBar statusBar = this._shell.StatusBar;
			statusBar.ShowContextInfo = context != null;
			if (context != null)
			{
				statusBar.Login          = context.Login;
				statusBar.InstancesCount = context.ServersCount;
				statusBar.GroupName      = context.ConnectionGroup?.Name;
			}
		}

		private async Task UpdateContentAsync(AuditTreeNode contentNode)
		{
			IWorkspace workspace = this._shell.Workspace;

			await workspace.UpdateContentAsync(contentNode);
		}

		private void OnCurrentNodeChanged()
		{
			NotifyOfPropertyChange(() => CurrentNode);

			CurrentNodeChanged?.Invoke(this, EventArgs.Empty);
		}

		private void OnTreeChanged()
		{
			TreeChanged?.Invoke(this, EventArgs.Empty);
		}
	}
}
