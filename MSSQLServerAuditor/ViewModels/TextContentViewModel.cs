﻿namespace MSSQLServerAuditor.ViewModels
{
	public class TextContentViewModel : ContentViewModel
	{
		private string _text;
		private bool   _isError;

		public string Text
		{
			get { return this._text; }
			set
			{
				if (value != this._text)
				{
					this._text = value;
					NotifyOfPropertyChange();
				}
			}
		}

		public bool IsError
		{
			get { return this._isError; }
			set
			{
				if (value != this._isError)
				{
					this._isError = value;
					NotifyOfPropertyChange();
				}
			}
		}
	}
}
