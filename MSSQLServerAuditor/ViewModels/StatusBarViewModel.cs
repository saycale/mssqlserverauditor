﻿using System;
using Caliburn.Micro;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Tasks;

namespace MSSQLServerAuditor.ViewModels
{
	public class StatusBarViewModel : PropertyChangedBase, IStatusBar
	{
		private readonly IQueueTaskManager _taskManager;

		private bool _displayTasks;
		private bool _canShowTasks;
		private bool _canStopTasks;

		private bool   _showContextInfo;
		private string _groupName;
		private string _login;
		private int?   _instancesCount;

		private DateTime? _lastUpdateDate;
		private TimeSpan? _lastUpdateDuration;
		private DateTime? _nextUpdateDate;

		public StatusBarViewModel(IQueueTaskManager taskManager)
		{
			Check.NotNull(taskManager, nameof(taskManager));

			this._taskManager = taskManager;
			TaskMonitor = new TaskMonitorViewModel(taskManager);

			taskManager.TaskStatusChanged   += TaskStatusChanged;
			taskManager.RunningTasksChanged += TaskManagerTasksChanged;
			taskManager.WaitingTasksChanged += TaskManagerTasksChanged;
		}

		public TaskMonitorViewModel TaskMonitor { get; }

		public void Cleanup()
		{
			GroupName          = null;
			InstancesCount     = null;
			Login              = null;
			LastUpdateDate     = null;
			LastUpdateDuration = null;
			NextUpdateDate     = null;

			ShowContextInfo = false;
		}

		public void StopTasks()
		{
			this._taskManager.StopAll();
		}

		public bool ShowContextInfo
		{
			get { return _showContextInfo; }
			set
			{
				if (this._showContextInfo != value)
				{
					this._showContextInfo = value;
					NotifyOfPropertyChange(() => ShowContextInfo);
				}
			}
		}

		public string GroupName
		{
			get { return this._groupName; }
			set
			{
				if (this._groupName != value)
				{
					this._groupName = value;
					NotifyOfPropertyChange(() => GroupName);
				}
			}
		}

		public int? InstancesCount
		{
			get { return this._instancesCount; }
			set
			{
				if (this._instancesCount != value)
				{
					this._instancesCount = value;
					NotifyOfPropertyChange(() => InstancesCount);
				}
			}
		}

		public string Login
		{
			get { return this._login; }
			set
			{
				if (this._login != value)
				{
					this._login = value;
					NotifyOfPropertyChange(() => Login);
				}
			}
		}
		
		public DateTime? LastUpdateDate
		{
			get { return this._lastUpdateDate; }
			set
			{
				if (this._lastUpdateDate != value)
				{
					this._lastUpdateDate = value;
					NotifyOfPropertyChange(() => LastUpdateDate);
				}
			}
		}

		public TimeSpan? LastUpdateDuration
		{
			get { return this._lastUpdateDuration; }
			set
			{
				if (this._lastUpdateDuration != value)
				{
					this._lastUpdateDuration = value;
					NotifyOfPropertyChange(() => LastUpdateDuration);
				}
			}
		}

		public DateTime? NextUpdateDate
		{
			get { return this._nextUpdateDate; }
			set
			{
				if (this._nextUpdateDate != value)
				{
					this._nextUpdateDate = value;
					NotifyOfPropertyChange(() => NextUpdateDate);
				}
			}
		}

		public bool DisplayTasks
		{
			get { return this._displayTasks; }
			set
			{
				if (this._displayTasks != value)
				{
					this._displayTasks = value;
					TaskMonitor.NotifyProgress = value;

					NotifyOfPropertyChange(() => DisplayTasks);
				}
			}
		}

		public bool CanShowTasks
		{
			get { return this._canShowTasks; }
			set
			{
				if (this._canShowTasks != value)
				{
					this._canShowTasks = value;
					NotifyOfPropertyChange(() => CanShowTasks);
				}
			}
		}

		public bool CanStopTasks
		{
			get { return this._canStopTasks; }
			set
			{
				if (this._canStopTasks != value)
				{
					this._canStopTasks = value;
					NotifyOfPropertyChange(() => CanStopTasks);
				}
			}
		}

		public void SwitchDisplayTasks()
		{
			DisplayTasks = !DisplayTasks;
		}

		private void TaskStatusChanged(object sender, QueueTaskEventArgs args)
		{
			QueueTask task = args.Task;
			if (task == null)
			{
				return;
			}

			switch (task.Status)
			{
				case QueueTaskStatus.Waiting:
				case QueueTaskStatus.Running:
					CanShowTasks = true;
					CanStopTasks = true;
					break;
			}
		}

		private void TaskManagerTasksChanged(object sender, EventArgs eventArgs)
		{
			// check if all tasks finished
			if (!this._taskManager.HasAnyTasks)
			{
				CanShowTasks = false;
				CanStopTasks = false;
				DisplayTasks = false;
			}
		}
	}
}
