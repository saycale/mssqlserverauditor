﻿<?xml version="1.0" encoding="utf-8" ?>
<configuration>
	<configSections>
		<section name="entityFramework" type="System.Data.Entity.Internal.ConfigFile.EntityFrameworkSection, EntityFramework, Version=6.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false"/>
		<section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler,Log4net"/>
		<section name="storage" type="MSSQLServerAuditor.Core.Persistence.Config.StorageConfigSection,MSSQLServerAuditor.Core"/>
		<section name="nlog" type="NLog.Config.ConfigSectionHandler, NLog"/>
		<sectionGroup name="userSettings" type="System.Configuration.UserSettingsGroup, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" >
			<section name="MSSQLServerAuditor.Properties.Settings" type="System.Configuration.ClientSettingsSection, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" allowExeDefinition="MachineToLocalUser" requirePermission="false" />
		</sectionGroup>
	</configSections>
	<entityFramework>
		<defaultConnectionFactory type="System.Data.Entity.Infrastructure.LocalDbConnectionFactory, EntityFramework">
			<parameters>
				<parameter value="v11.0"/>
			</parameters>
		</defaultConnectionFactory>
		<providers>
			<provider invariantName="System.Data.SqlClient" type="System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer"/>
			<provider invariantName="System.Data.SQLite" type="System.Data.SQLite.EF6.SQLiteProviderServices, System.Data.SQLite.EF6"/>
			<provider invariantName="System.Data.SQLite.EF6" type="System.Data.SQLite.EF6.SQLiteProviderServices, System.Data.SQLite.EF6"/>
		</providers>
	</entityFramework>
	<runtime>
		<assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
			<probing privatePath="lib"/>
		</assemblyBinding>
	</runtime>
	<system.data>
		<DbProviderFactories>
			<remove invariant="System.Data.SQLite"/>
			<add name="SQLite Data Provider" invariant="System.Data.SQLite" description=".NET Framework Data Provider for SQLite" type="System.Data.SQLite.SQLiteFactory, System.Data.SQLite"/>
			<remove invariant="System.Data.SQLite.EF6"/>
			<add name="SQLite Data Provider (Entity Framework 6)" invariant="System.Data.SQLite.EF6" description=".NET Framework Data Provider for SQLite (Entity Framework 6)" type="System.Data.SQLite.EF6.SQLiteProviderFactory, System.Data.SQLite.EF6"/>
		</DbProviderFactories>
	</system.data>
	<storage>
		<connection type="SQLite" connectionString="data source={0};journal mode=Wal;pooling=True;page size=4096;cache size=10000;synchronous=Off">
			<databases>
				<database alias="current" name="${version}$_current.sqlite"/>
				<database alias="dbfs"    name="${version}$_dbfs.sqlite"/>
				<database alias="report"  name="${version}$_reporting.sqlite"/>
				<database alias="ref"     name="${version}$_reference.sqlite"/>
				<database alias="hist"    name="${version}$_historical.sqlite"/>
				<database alias="hist24"  name="${version}$_historical24.sqlite"/>
			</databases>
		</connection>
		<!--
		<connection type="MSSQL" connectionString="Server=MSSQLSERVER;Database={0};User Id=sa;Password=1;">
			<databases>
				<database alias="current" name="${version}$_current"/>
				<database alias="dbfs"    name="${version}$_dbfs"/>
				<database alias="report"  name="${version}$_reporting"/>
				<database alias="ref"     name="${version}$_reference"/>
				<database alias="hist"    name="${version}$_historical"/>
				<database alias="hist24"  name="${version}$_historical24"/>
			</databases>
		</connection>
		-->
	</storage>
	<nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<variable name="LogDir" value="${specialfolder:folder=ApplicationData}\MSSQLServerAuditor\Logs"/>
		<variable name="OldLogDir" value="${specialfolder:folder=ApplicationData}\MSSQLServerAuditor\OldLogs"/>
		<variable name="LogName" value="${date:format=yyyy-MM-dd}_${processid}.log"/>
		<targets>
			<target name="File" type="File" fileName="${LogDir}\${LogName}"
							layout="${longdate}|${level:uppercase=true}|${logger}|${message}${onexception:inner=${newline}${exception:format=ToString}}"
							maxArchiveFiles="10" archiveNumbering="Sequence" archiveAboveSize="1048576" archiveFileName="${OldLogDir}\{#######}.a"/>
		</targets>
		<rules>
			<logger name="*" minlevel="Debug" writeTo="File" />
		</rules>
	</nlog>
	<startup>
		<supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.5.2" />
	</startup>
	<userSettings>
		<MSSQLServerAuditor.Properties.Settings>
			<setting name="ShowStatusBar" serializeAs="String">
				<value>True</value>
			</setting>
			<setting name="ShowReportTree" serializeAs="String">
				<value>True</value>
			</setting>
			<setting name="ShellViewPlacement" serializeAs="String">
				<value />
			</setting>
			<setting name="ShowMainMenu" serializeAs="String">
				<value>True</value>
			</setting>
		</MSSQLServerAuditor.Properties.Settings>
	</userSettings>
</configuration>