﻿using System.Windows;
using System.Windows.Controls;

namespace MSSQLServerAuditor.Extensions
{
	public class ColumnDefinitionEx : ColumnDefinition
	{
		public static readonly DependencyProperty VisibleProperty;

		public bool Visible
		{
			get { return (bool) GetValue(VisibleProperty); }
			set { SetValue(VisibleProperty, value); }
		}

		static ColumnDefinitionEx()
		{
			VisibleProperty = DependencyProperty.Register(
				nameof(Visible),
				typeof(bool),
				typeof(ColumnDefinitionEx),
				new PropertyMetadata(true, OnVisibleChanged)
			);

			WidthProperty.OverrideMetadata(
				typeof(ColumnDefinitionEx),
				new FrameworkPropertyMetadata(new GridLength(1, GridUnitType.Star), null, CoerceWidth)
			);

			MinWidthProperty.OverrideMetadata(
				typeof(ColumnDefinitionEx),
				new FrameworkPropertyMetadata((double) 0, null, CoerceMinWidth)
			);
		}

		public static void SetVisible(DependencyObject obj, bool nVisible)
		{
			obj.SetValue(VisibleProperty, nVisible);
		}

		public static bool GetVisible(DependencyObject obj)
		{
			return (bool) obj.GetValue(VisibleProperty);
		}

		private static void OnVisibleChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			obj.CoerceValue(WidthProperty);
			obj.CoerceValue(MinWidthProperty);
		}

		private static object CoerceWidth(DependencyObject obj, object nValue)
		{
			return ((ColumnDefinitionEx) obj).Visible ? nValue : new GridLength(0);
		}

		private static object CoerceMinWidth(DependencyObject obj, object nValue)
		{
			return ((ColumnDefinitionEx) obj).Visible ? nValue : (double) 0;
		}
	}
}
