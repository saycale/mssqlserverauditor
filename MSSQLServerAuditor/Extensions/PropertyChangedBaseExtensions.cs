﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Caliburn.Micro;

namespace MSSQLServerAuditor.Extensions
{
	public static class PropertyChangedBaseExtensions
	{
		public static void NotifyOfPropertiesChange(this PropertyChangedBase pcb, params string[] properties)
		{
			if (properties != null && properties.Any())
			{
				foreach (string propName in properties)
				{
					pcb.NotifyOfPropertyChange(propName);
				}
			}
		}

		public static void NotifyOfPropertiesChange(this PropertyChangedBase pcb, params Expression<Func<object>>[] properties)
		{
			if (properties != null && properties.Any())
			{
				foreach (Expression<Func<object>> property in properties)
				{
					string propName = property.GetMemberInfo().Name;
					pcb.NotifyOfPropertyChange(propName);
				}
			}
		}
	}
}
