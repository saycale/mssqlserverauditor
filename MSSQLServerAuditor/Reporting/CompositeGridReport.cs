using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Reporting;

namespace MSSQLServerAuditor.Reporting
{
	public class CompositeGridReport : IVisualReport
	{
		private const int SplitterSize = 5;

		private readonly List<IVisualReport> _reports;

		public CompositeGridReport(
			PreprocessorArea area,
			IPreprocessor    handler,
			ReportData       reportData)
		{
			this._reports = new List<IVisualReport>();

			if (area == null)
			{
				throw new ArgumentNullException(nameof(area));
			}

			if (!IsConfigured(area))
			{
				throw new ArgumentException(nameof(area));
			}

			Grid grid = new Grid();

			float[] rowHeights = area.Rows;
			float[] colWidths  = area.Columns;

			List<ColumnDefinition> colDefs = CreateGridCols(colWidths);
			List<RowDefinition>    rowDefs = CreateGridRows(rowHeights);

			foreach (ColumnDefinition colDef in colDefs)
			{
				grid.ColumnDefinitions.Add(colDef);
			}

			foreach (RowDefinition rowDef in rowDefs)
			{
				grid.RowDefinitions.Add(rowDef);
			}

			AddRowSplitters(grid, rowDefs, colDefs.Count);
			AddColumnSplitters(grid, colDefs, rowDefs.Count);

			foreach (PreprocessorConfig config in area.PreprocessorsConfigs)
			{
				IVisualReport visualReport = handler.CreateControl(reportData, config);
				this._reports.Add(visualReport);
				if (config.VerticalTextAlign != VerticalTextAlign.None)
				{
					string title = handler.GetTitle(reportData, config);

					visualReport = new TitleDecorator(visualReport, title, config);
				}

				int gridRow = 2 * (config.Row - 1);
				int gridCol = 2 * (config.Column - 1);
				int colSpan = 2 * config.ColSpan - 1;
				int rowSpan = 2 * config.RowSpan - 1;

				Grid.SetRow(visualReport.Control, gridRow);
				Grid.SetColumn(visualReport.Control, gridCol);

				if (rowSpan > 0)
				{
					Grid.SetRowSpan(visualReport.Control, rowSpan);
				}

				if (colSpan > 0)
				{
					Grid.SetColumnSpan(visualReport.Control, colSpan);
				}

				grid.Children.Add(visualReport.Control);
			}

			Control = grid;
		}

		public FrameworkElement Control { get; }

		public void Release()
		{
			foreach (IVisualReport report in this._reports)
			{
				report.Release();
			}
		}

		private static List<RowDefinition> CreateGridRows(float[] rowHeights)
		{
			List<RowDefinition> rowDefs = new List<RowDefinition>();

			int rowIndex = 0;
			foreach (float rowHeight in rowHeights)
			{
				RowDefinition rowDef = new RowDefinition
				{
					Height = new GridLength(rowHeight, GridUnitType.Star)
				};

				rowDefs.Add(rowDef);

				if (rowIndex < rowHeights.Length - 1)
				{
					// add row splitter
					RowDefinition splitRowDef = new RowDefinition
					{
						Height = new GridLength(SplitterSize)
					};

					rowDefs.Add(splitRowDef);
				}
				rowIndex++;
			}

			return rowDefs;
		}

		private static List<ColumnDefinition> CreateGridCols(float[] colWidths)
		{
			List<ColumnDefinition> colDefs = new List<ColumnDefinition>();

			int colIndex = 0;
			foreach (float colWidth in colWidths)
			{
				ColumnDefinition colDef = new ColumnDefinition
				{
					Width = new GridLength(colWidth, GridUnitType.Star)
				};

				colDefs.Add(colDef);

				if (colIndex < colWidths.Length - 1)
				{
					// add column splitter
					ColumnDefinition splitColDef = new ColumnDefinition
					{
						Width = new GridLength(SplitterSize)
					};

					colDefs.Add(splitColDef);
				}
				colIndex++;
			}

			return colDefs;
		}

		private static void AddRowSplitters(Grid grid, List<RowDefinition> rowDefs, int span)
		{
			for (int splitterRow = 1; splitterRow < rowDefs.Count - 1; splitterRow += 2)
			{
				GridSplitter splitter = CreateGridSplitter();

				Grid.SetRow(splitter, splitterRow);
				Grid.SetColumn(splitter, 0);
				Grid.SetColumnSpan(splitter, span);

				grid.Children.Add(splitter);
			}
		}

		private static void AddColumnSplitters(Grid grid, List<ColumnDefinition> colDefs, int span)
		{
			for (int splitterCol = 1; splitterCol < colDefs.Count - 1; splitterCol += 2)
			{
				GridSplitter splitter = CreateGridSplitter();

				Grid.SetRow(splitter, 0);
				Grid.SetColumn(splitter, splitterCol);
				Grid.SetRowSpan(splitter, span);

				grid.Children.Add(splitter);
			}
		}

		private static GridSplitter CreateGridSplitter()
		{
			GridSplitter splitter = new GridSplitter
			{
				VerticalAlignment   = VerticalAlignment.Stretch,
				HorizontalAlignment = HorizontalAlignment.Stretch,
				ResizeBehavior      = GridResizeBehavior.PreviousAndNext
			};

			return splitter;
		}

		private static bool IsConfigured(PreprocessorArea area)
		{
			if (area.PreprocessorsConfigs == null)
			{
				return false;
			}

			if (area.HasSplitter)
			{
				return !area.Rows.IsNullOrEmpty() && !area.Columns.IsNullOrEmpty();
			}

			return true;
		}
	}
}