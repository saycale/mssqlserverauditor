﻿using System.Windows;
using MSSQLServerAuditor.Controls;
using MSSQLServerAuditor.Core.Reporting;

namespace MSSQLServerAuditor.Reporting
{
	public class TitleDecorator : IVisualReport
	{
		private readonly IVisualReport _report;

		public TitleDecorator(
			IVisualReport      report,
			string             title,
			PreprocessorConfig config
		)
		{
			this._report = report;

			Control = WrapWithTitle(config, title, report.Control);
		}

		public FrameworkElement Control { get; }

		public void Release()
		{
			this._report.Release();
		}

		private static FrameworkElement WrapWithTitle(
			PreprocessorConfig config,
			string             title,
			FrameworkElement   control)
		{
			TitleFrame titleFrame = new TitleFrame { ContentControl = control };

			if (config.VerticalTextAlign == VerticalTextAlign.Top)
			{
				titleFrame.HeaderVisibility = Visibility.Visible;
				titleFrame.FooterVisibility = Visibility.Collapsed;
				titleFrame.Header           = title;
			}
			else
			{
				titleFrame.HeaderVisibility = Visibility.Collapsed;
				titleFrame.FooterVisibility = Visibility.Visible;
				titleFrame.Footer           = title;
			}

			switch (config.TextAlign)
			{
				case TextAlign.Center:
					titleFrame.HeaderAlignment = titleFrame.FooterAlignment = TextAlignment.Center;
					break;
				case TextAlign.Left:
					titleFrame.HeaderAlignment = titleFrame.FooterAlignment = TextAlignment.Left;
					break;
				case TextAlign.Right:
					titleFrame.HeaderAlignment = titleFrame.FooterAlignment = TextAlignment.Right;
					break;
			}

			return titleFrame;
		}
	}
}
