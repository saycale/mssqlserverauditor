using System.Windows;
using System.Windows.Controls;
using MSSQLServerAuditor.Core.Reporting;

namespace MSSQLServerAuditor.Reporting
{
	public class XmlVisualReport : IVisualReport
	{
		public XmlVisualReport(XmlReport xml)
		{
			Control = new TextBox
			{
				Text                          = xml.XmlString,
				IsReadOnly                    = true,
				HorizontalScrollBarVisibility = ScrollBarVisibility.Auto,
				VerticalScrollBarVisibility   = ScrollBarVisibility.Auto
			};
		}

		public FrameworkElement Control { get; }

		public void Release()
		{

		}
	}
}