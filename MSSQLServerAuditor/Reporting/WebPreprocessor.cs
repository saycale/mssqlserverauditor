﻿using System;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Xml;
using System.Xml.Xsl;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Common.Utils;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Reporting;
using MSSQLServerAuditor.Core.Reporting.Attributes;
using MSSQLServerAuditor.Core.Settings;
using NLog;

namespace MSSQLServerAuditor.Reporting
{
	[Handles("WebPreprocessor")]
	public class WebPreprocessor : IPreprocessor
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly string          _hostUrl;
		private readonly IStorageManager _storage;

		public WebPreprocessor(string hostUrl, IStorageManager storage)
		{
			this._storage = storage;
			this._hostUrl = hostUrl;
		}

		public void Prepare(ReportData data, PreprocessorConfig config)
		{
			SaveHtmlContent(data, config);
		}

		public IVisualReport CreateControl(ReportData data, PreprocessorConfig config)
		{
			string htmlPath = GetHtmlPath(data, config);

			return CreateControl(htmlPath);
		}

		public EmailContent CreateMailContent(ReportData reportData, PreprocessorConfig preprocessor)
		{
			throw new NotImplementedException("Not intended for email sending");
		}

		public string GetTitle(ReportData data, PreprocessorConfig config)
		{
			string html = XslTransformHelper.Transform(data, config.Configuration);

			return HtmlUtils.GetTitle(html, config.Id);
		}

		public string GetTitle(ReportData data, PreprocessorArea area)
		{
			string html = XslTransformHelper.Transform(data, area.Configuration);

			return HtmlUtils.GetTitle(html, area.Id);
		}

		private string GetHtmlPath(ReportData data, PreprocessorConfig config)
		{
			TemplateNodeInfo tnInfo = data.TemplateNodeInfo;

			Check.NotNull(tnInfo, nameof(tnInfo));

			string dbfsFolder = GetFolder(tnInfo, data.ConnectionGroupId);
			return Path.Combine(dbfsFolder, config.Id + ".html");
		}

		private IVisualReport CreateControl(string htmlFileName)
		{
			return new WebReport(GetResultWebPath(htmlFileName));
		}

		public void HideScriptErrors(WebBrowser browser)
		{
			// Sets the .Silent property on the underlying activeX object.
			// Which is the same as the .ScriptErrorsSuppressed property which is the Windows forms equivalent.

			FieldInfo fiComBrowser = typeof(WebBrowser)
				.GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);

			if (fiComBrowser == null)
			{
				return;
			}

			object comBrowser = fiComBrowser.GetValue(browser);
			if (comBrowser == null)
			{
				browser.Loaded += BrowserOnLoaded; //In case we are to early
				return;
			}

			comBrowser.GetType().InvokeMember(
				"Silent",
				BindingFlags.SetProperty,
				null,
				comBrowser,
				new object[] { true }
			);
		}

		private void BrowserOnLoaded(object sender, RoutedEventArgs routedEventArgs)
		{
			WebBrowser browser = sender as WebBrowser;
			if (browser != null)
			{
				HideScriptErrors(browser);
				browser.Loaded -= BrowserOnLoaded;
			}
		}

		private void BrowserOnUnloaded(object sender, RoutedEventArgs routedEventArgs)
		{
			WebBrowser browser = sender as WebBrowser;
			if (browser != null)
			{
				browser.Unloaded -= BrowserOnUnloaded;

				try
				{
					//I found that navigating to a blank page before disposing reduces the handle leak
					browser.Source = null;
				}
				catch
				{
					// Multiple exceptions can happen here (ArgumentException, COMException, ... so catch everything)
				}
				
				try
				{
					IKeyboardInputSite keyboardInputSite = ((IKeyboardInputSink)browser).KeyboardInputSite;
					if (keyboardInputSite != null)
					{
						keyboardInputSite.Unregister();

						Type type = keyboardInputSite.GetType();
						FieldInfo fieldInfo = type.GetField("_sinkElement", BindingFlags.NonPublic | BindingFlags.Instance);
						fieldInfo?.SetValue(keyboardInputSite, null);
					}
				}
				catch
				{
					//Catch everything, this is a nasty hack to dispose the WebBrowser
				}

				browser.Dispose();
			}
		}

		private Uri GetResultWebPath(string path)
		{
			string url = string.Format(
				"{0}/result/{1}",
				this._hostUrl,
				path.Replace(Path.DirectorySeparatorChar, '/')
					.Replace(@"\?\", string.Empty)
					.Replace(@"/?/", string.Empty)
			);

			Log.Debug($"url:{url}");

			return new Uri(url);
		}

		private XmlReader GetXmlReader(string xmlString)
		{
			XmlDocument document = new XmlDocument();

			try
			{
				document.LoadXml(xmlString);
			}
			catch (Exception ex)
			{
				Log.Error(ex);
			}

			return new XmlNodeReader(document);
		}

		private void SaveHtmlContent(
			ReportData         data,
			PreprocessorConfig config
		)
		{
			string               htmlPath = GetHtmlPath(data, config);
			XslCompiledTransform xsl      = new XslCompiledTransform();

			string xmlConfig = config.Configuration.Replace(
				"$JS_FOLDER$",
				"/file"
			);

			using (XmlReader xmlReader = GetXmlReader(xmlConfig))
			{
				xsl.Load(xmlReader);
			}

			try
			{
				XsltArgumentList list    = new XsltArgumentList();
				XmlDocument      xmlData = data.XmlReport.ResultXml;

				using (MemoryStream stream = new MemoryStream())
				{
					XmlNode docElement;

					if (xmlData != null)
					{
						docElement = xmlData.DocumentElement;
					}
					else
					{
						docElement = new XmlDocument();
					}

					using (XmlReader reader = new XmlNodeReader(docElement))
					{
						xsl.Transform(reader, list, stream);
					}

					stream.Position = 0;

					this._storage.FsStorage.SaveReportStream(htmlPath, stream);
				}
			}
			catch (PathTooLongException ex)
			{
				Log.Error(ex,
					"PathTooLongException:htmlFileName:{0};Exception:{1}",
					htmlPath,
					ex
				);

				throw;
			}
		}

		private string GetFolder(
			TemplateNodeInfo tnInfo,
			long             groupId
		)
		{
			string folder = Path.GetDirectoryName(
				GetFileName(tnInfo, groupId)
			);

			return folder;
		}

		private string ComposeFileName(
			string directory,
			long   groupId
		)
		{
			string filePath = $"MSSQLServerAuditor.{groupId}";

			return Path.Combine(
				directory,
				filePath
			);
		}

		private string GetFileName(
			TemplateNodeInfo tnInfo,
			long             groupId
		)
		{
			string rootFolder = string.Empty;

			TemplateNodeInfo tnInfoTemp = tnInfo;
			do
			{
				string filePath = $"{tnInfoTemp.NodeInstanceId}";

				rootFolder = Path.Combine(
					filePath.GetValidFileName(),
					rootFolder
				);

				tnInfoTemp = tnInfoTemp.Parent;

			} while (tnInfoTemp != null);

			rootFolder = Path.Combine(tnInfo.NodeInstanceId.ToString(), rootFolder);

			return ComposeFileName(rootFolder, groupId);
		}

		public bool IsEnabled(IAppSettings appSettings)
		{
			return true; // enabled always
		}
	}

}
