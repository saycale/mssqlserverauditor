﻿using System;
using System.Collections.Generic;
using System.Configuration.Install;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using CommandLine;
using CommandLine.Text;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace MSSQLServerAuditor.Service
{
	static class Program
	{
		public const string LogTargetConsole = "Console";
		public const string LogTargetFile    = "File";

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main(string[] args)
		{
			if (Environment.UserInteractive)
			{
				Options options = new Options();

				if (Parser.Default.ParseArguments(args, options))
				{
					if (options.Install)
					{
						InstallService(options.ServiceName, options.DisplayName);
					}
					else if (options.Uninstall)
					{
						UninstallService(options.ServiceName);
					}
					else if (options.Console)
					{
						StartConsole();
					}
					else
					{
						DisplayHelp(options);
					}
				}
				else
				{
					DisplayHelp(options);
				}
			}
			else
			{
				StartService();
			}
		}

		private static void DisplayHelp(Options options)
		{
			Console.WriteLine(HelpText.AutoBuild(options));
		}

		private static void UninstallService(string name)
		{
			List<string> args = new List<string>();

			if (!string.IsNullOrEmpty(name))
			{
				args.Add($"/servicename={name}");
			}

			args.Add("/u");
			args.Add(Assembly.GetExecutingAssembly().Location);

			ManagedInstallerClass.InstallHelper(args.ToArray());
		}

		private static void InstallService(string name, string displayName)
		{
			List<string> args = new List<string>();

			if (!string.IsNullOrEmpty(name))
			{
				args.Add($"/servicename={name}");
			}

			if (!string.IsNullOrEmpty(displayName))
			{
				args.Add($"/servicedisplayname={displayName}");
			}

			args.Add(Assembly.GetExecutingAssembly().Location);

			ManagedInstallerClass.InstallHelper(args.ToArray());
		}

		private static void StartService()
		{
			DisableLogTarget(LogTargetConsole);

			ServiceBase.Run(new AuditorService());
		}

		private static void StartConsole()
		{
			DisableLogTarget(LogTargetFile);

			AuditorService service = new AuditorService();
			service.Run();

			Console.ReadKey();
			service.Shutdown();
		}

		private static void DisableLogTarget(string targetName)
		{
			LoggingConfiguration config = LogManager.Configuration;

			Target target = config.FindTargetByName(targetName);
			if (target != null)
			{
				foreach (LoggingRule loggingRule in config.LoggingRules)
				{
					if (loggingRule.Targets.Any(t => t == target))
					{
						loggingRule.Targets.Remove(target);
					}
				}

				config.Reload();
			}
		}
	}
}
