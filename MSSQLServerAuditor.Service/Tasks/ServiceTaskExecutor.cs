﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Common.Utils;
using MSSQLServerAuditor.Core;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Notification;
using MSSQLServerAuditor.Core.Persistence;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Querying;
using MSSQLServerAuditor.Core.Querying.Data;
using MSSQLServerAuditor.Core.Reporting;
using MSSQLServerAuditor.Core.Reporting.Preprocessors;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Core.Xml;
using NLog;

namespace MSSQLServerAuditor.Service.Tasks
{
	public class ServiceTaskExecutor
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly IStorageManager  _storageManager;
		private readonly IAppSettings     _appSettings;
		private readonly QueryExecutor    _queryExecutor;
		private readonly XmlLoader        _xmlLoader;
		private readonly AuditCoordinator _auditCoordinator;

		private readonly NotificationService _notificationService;

		public ServiceTaskExecutor(
			IStorageManager storageManager,
			IAppSettings    appSettings,
			XmlLoader       xmlLoader
		)
		{
			Check.NotNull(storageManager, nameof(storageManager));
			Check.NotNull(appSettings,    nameof(appSettings));
			Check.NotNull(xmlLoader,      nameof(xmlLoader));

			this._storageManager = storageManager;
			this._appSettings    = appSettings;
			this._xmlLoader      = xmlLoader;

			this._queryExecutor = new QueryExecutor(
				appSettings,
				this._storageManager.StorageConnectionFactory,
				storageManager.QueryFormatter
			);

			this._notificationService = CreateNotificationService();

			this._auditCoordinator = new AuditCoordinator(
				storageManager,
				this._queryExecutor,
				xmlLoader
			);
		}

		public async Task ExecuteUpdateAsync(ServiceTaskInfo taskInfo, CancellationToken cancelToken)
		{
			if (taskInfo.UpdateHierarchically && taskInfo.UpdateDeep != 0)
			{
				await InstantiateChildrenAsync(
					taskInfo.AuditContext,
					taskInfo.TemplateNodeInfo,
					cancelToken
				);
			}

			await Task.Run(
				() => ExecuteUpdate(taskInfo, cancelToken),
				cancelToken
			);
		}

		private async Task InstantiateChildrenAsync(AuditContext context, TemplateNodeInfo tnInfo, CancellationToken cancel)
		{
			await Task.Run(
				() => InstantiateChildren(context, tnInfo, cancel),
				cancel
			);
		}

		private void InstantiateChildren(AuditContext context, TemplateNodeInfo tnInfo, CancellationToken cancel)
		{
			if (tnInfo.Template.Children.Any() || tnInfo.HasDynamicChildren())
			{
				tnInfo.Children.Clear();
				tnInfo.Children.AddRange(
					 tnInfo.Template.Children
						.Where(templateNode => string.IsNullOrWhiteSpace(templateNode.GroupSelectId))
						.Select(templateNode =>
							templateNode.Instantiate(null, tnInfo)
						)
					);

				if (!tnInfo.IsSaved)
				{
					SaveNode(context, tnInfo);
				}

				DisableChildren(tnInfo);
				InitializeStaticNodes(context, tnInfo);

				if (tnInfo.HasDynamicChildren())
				{
					CreateDynamicNodes(context, tnInfo, cancel);
				}

				SetChildrenProcessed(tnInfo);
			}
		}

		private void CreateDynamicNodes(AuditContext context, TemplateNodeInfo parentNodeInfo, CancellationToken token)
		{
			foreach (TemplateNodeQueryInfo queryInfo in parentNodeInfo.GroupQueries)
			{
				List<TemplateNodeInfo> replicaTemplates = parentNodeInfo.GetGroupQueryNodes(queryInfo);
				List<AuditSession>     sessions         = context.CreateSessions();

				lock (parentNodeInfo.Children)
				{
					parentNodeInfo.Children.RemoveAll(tn => tn.ReplicaQuery == queryInfo);
				}

				foreach (AuditSession auditSession in sessions)
				{
					List<NodeAttributes> dynamicNodesAttributes = QueryDynamicNodes(auditSession, queryInfo, token);

					foreach (NodeAttributes dynamicNodeAttributes in dynamicNodesAttributes)
					{
						foreach (TemplateNodeInfo replicaTemplate in replicaTemplates)
						{
							ReplicateNode(
								context,
								parentNodeInfo,
								replicaTemplate,
								dynamicNodeAttributes,
								queryInfo
							);
						}
					}
				}
			}
		}

		private void ReplicateNode(
			AuditContext          context,
			TemplateNodeInfo      parentNode,
			TemplateNodeInfo      replicaTemplate,
			NodeAttributes        nodeAttributes,
			TemplateNodeQueryInfo replicaQuery
		)
		{
			TemplateNodeInfo replicaNode = replicaTemplate.Replicate(
				nodeAttributes,
				replicaQuery,
				parentNode
			);

			lock (parentNode.Children)
			{
				parentNode.Children.Add(replicaNode);
			}

			if (!replicaNode.IsSaved)
			{
				SaveNode(context, replicaNode);
			}

			LoadNodeSettings(context, replicaNode);
		}

		private List<NodeAttributes> QueryDynamicNodes(
			AuditSession          session,
			TemplateNodeQueryInfo tnQueryInfo,
			CancellationToken     token
		)
		{
			return this._auditCoordinator.QueryDynamicNodes(
				session,
				tnQueryInfo,
				token
			);
		}

		private void SaveNode(AuditContext context, TemplateNodeInfo tnInfo)
		{
			MainStorage.Nodes.SaveNode(
				context.Template,
				context.ConnectionGroup,
				tnInfo
			);
		}

		private void InitializeStaticNodes(AuditContext context, TemplateNodeInfo tnInfo)
		{
			foreach (TemplateNodeInfo childInfo in tnInfo.Children)
			{
				if (!childInfo.IsSaved)
				{
					SaveNode(context, childInfo);
				}

				LoadNodeSettings(context, childInfo);
			}
		}

		private void LoadNodeSettings(
			AuditContext context,
			TemplateNodeInfo tnInfo
		)
		{
			MainStorage.Nodes.LoadNodeSettings(
				context,
				tnInfo,
				this._appSettings.User.UiLanguage
			);

			tnInfo.HasActiveSchedules = HasActiveSchedules(tnInfo);
		}

		private bool HasActiveSchedules(TemplateNodeInfo tnInfo)
		{
			return MainStorage.Settings.HasActiveSchedules(tnInfo);
		}

		private void DisableChildren(TemplateNodeInfo tnInfo)
		{
			MainStorage.Nodes.DisableChildren(tnInfo.NodeInstanceId);

			MainStorage.Nodes.SetChildrenNotProcessed(
				tnInfo.NodeInstanceId,
				false
			);
		}

		private void SetChildrenProcessed(TemplateNodeInfo tnInfo)
		{
			MainStorage.Nodes.SetChildrenNotProcessed(
				tnInfo.NodeInstanceId,
				false
			);
		}

		private void ExecuteUpdate(ServiceTaskInfo taskInfo, CancellationToken cancel)
		{
			UpdateNode(taskInfo, cancel);

			cancel.ThrowIfCancellationRequested();

			if (taskInfo.SendMessage == true)
			{
				SendNotification(taskInfo);
			}
		}

		private void UpdateNode(ServiceTaskInfo taskInfo, CancellationToken cancel)
		{
			AuditContext     context = taskInfo.AuditContext;
			TemplateNodeInfo tnInfo  = taskInfo.TemplateNodeInfo;

			Log.Debug(
				"Started query execution: Connection: '{0}'; nodeId: '{1}'; Query: '{2}'",
				context.ConnectionGroup.Name,
				tnInfo.NodeInstanceId,
				tnInfo.Name
			);

			MainStorage.Nodes.LoadQueryParameters(context, tnInfo);

			cancel.ThrowIfCancellationRequested();

			// execute node's queries and save results
			this._auditCoordinator.ProcessQueries(context, tnInfo, cancel);
		}

		private NotificationService CreateNotificationService()
		{
			IPreprocessManager processorManager = new PreprocessManager();

			processorManager.AllPreprocessors.Add(new HtmlPreprocessor());

			NotificationService notificationService = new NotificationService(
				this._appSettings,
				this._xmlLoader,
				processorManager
			);

			return notificationService;
		}

		private MainStorage MainStorage => this._storageManager.MainStorage;

		private void SendNotification(ServiceTaskInfo taskInfo)
		{
			AuditContext     context = taskInfo.AuditContext;
			TemplateNodeInfo tnInfo  = taskInfo.TemplateNodeInfo;

			QueryResultSet savedResult;

			try
			{
				QueryResultReader resultReader = new QueryResultReader(
					this._storageManager,
					this._auditCoordinator,
					this._xmlLoader
				);

				savedResult = resultReader.LoadResults(context, tnInfo);
			}
			catch (Exception exc)
			{
				Log.Error(
					exc,
					"Failed to read data for notification for the node " +
					$"'name: {tnInfo.Name}, " +
					$"id: {tnInfo.Id}, " +
					$"connection: {context.ConnectionGroup.Name}'"
				);

				return;
			}

			
			if (taskInfo.SendMessageNonEmptyOnly && savedResult.IsEmpty)
			{
				Log.Info(
					"Query results for node 'name: {0}, id: {1}, connection: {2}' are empty. Skip notification sending",
					tnInfo.Name,
					tnInfo.Id,
					context.ConnectionGroup.Name
				);

				return;
			}

			string reportLang = StringUtils.FirstNonEmpty(
				taskInfo.MessageLanguage,
				this._appSettings.User.ReportLanguage,
				"en"
			);

			IXmlTransformer transformer = new XmlTransformer(reportLang);
			XmlDocument     xmlDocument = transformer.Transform(savedResult);
			XmlReport       xmlReport   = new XmlReport(savedResult, xmlDocument);

			ReportData reportData = new ReportData
			{
				TemplateNodeInfo  = tnInfo,
				XmlReport         = xmlReport,
				DateTime          = xmlReport.SourceData.UpdateTime,
				Duration          = xmlReport.SourceData.UpdateDuration,
				ConnectionGroupId = context.ConnectionGroup.Id
			};

			try
			{
				this._notificationService.SendNotification(
					taskInfo.MessageRecipients,
					reportData
				);
			}
			catch (Exception exc)
			{
				Log.Error(exc, "Failed to send email notificaion.");
			}
		}
	}
}
