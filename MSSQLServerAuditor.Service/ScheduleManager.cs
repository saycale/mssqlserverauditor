﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Schedule;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Core.Tasks;
using MSSQLServerAuditor.Core.Xml;
using MSSQLServerAuditor.Service.Tasks;
using NLog;

namespace MSSQLServerAuditor.Service
{
	public class ScheduleManager
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();
		
		private readonly IStorageManager          _storageManager;
		private readonly XmlLoader                _xmlLoader;
		private readonly ServiceJobScheduler      _jobScheduler;
		private Dictionary<long, ScheduleDetails> _schedules;

		private readonly object _schedulesLock = new object();
		private readonly string _winUser;

		public ScheduleManager(
			IStorageManager storageManager,
			IAppSettings    appSettings,
			XmlLoader       xmlLoader
		)
		{
			Check.NotNull(storageManager, nameof(storageManager));
			Check.NotNull(appSettings,    nameof(appSettings));
			Check.NotNull(xmlLoader,      nameof(xmlLoader));

			this._storageManager = storageManager;
			this._xmlLoader      = xmlLoader;

			ServiceTaskExecutor taskExecutor = new ServiceTaskExecutor(storageManager, appSettings, xmlLoader);
			QueueTaskManager    taskManager  = new QueueTaskManager(appSettings.User);
			
			this._jobScheduler = new ServiceJobScheduler(taskManager, taskExecutor);

			this._schedules    = new Dictionary<long, ScheduleDetails>();
			this._winUser      = GetCurrentWinUser();
		}

		public void StartScheduler()
		{
			Log.Debug("Starting scheduler...");
			Log.Debug("Logged on user: {0}", this._winUser);

			lock (this._schedulesLock)
			{
				this._schedules.Clear();
				this._schedules = new Dictionary<long, ScheduleDetails>(LoadSchedules());
			}

			this._jobScheduler.StartJobScheduler();

			Log.Debug($"Scheduler started. Active schedules: '{this._schedules.Count}'");

			StartSchedules();
		}

		private MainStorage MainStorage => this._storageManager.MainStorage;

		private void StartSchedules()
		{
			if (this._schedules == null || this._schedules.Count < 1)
			{
				return;
			}

			Task runJobsTask = Task.Run(() => RunJobs());

			Task.WaitAll(runJobsTask);
		}

		public void Shutdown()
		{
			this._jobScheduler.Shutdown();
		}

		private void RunJobs()
		{
			lock (this._schedulesLock)
			{
				foreach (ScheduleDetails scheduleDetails in this._schedules.Values)
				{
					ScheduleJob(scheduleDetails);
				}
			}
		}

		private AuditContext GetAuditContext(
			ScheduleDetails scheduleDetails,
			string          winUser
		)
		{
			long groupId = scheduleDetails.ConnectionGroupId;

			ConnectionGroup group = MainStorage.Connections
				.GetConnectionGroup(groupId, winUser);

			AuditContext context = new AuditContext(
				group,
				new Template
				{
					Id        = scheduleDetails.TemplateId,
					Directory = scheduleDetails.TemplateDirectory,
					Name      = scheduleDetails.TemplateName,
					UId       = scheduleDetails.TemplateUId
				}
			);
			
			return context;
		}

		private TemplateNodeInfo GetNodeInfo(
			TemplateInfo    template,
			ScheduleDetails scheduleDetails
		)
		{
			IEnumerable<TemplateNodeInfo> nodes = GetTreeNodesList(template.CreateRoot());
			
			TemplateNodeInfo tnInfo = nodes.FirstOrDefault(n =>
				n.Id   == scheduleDetails.TemplateNodeUId &&
				n.Name == scheduleDetails.TemplateNodeName
			);

			if (tnInfo != null)
			{
				tnInfo.NodeInstanceId = scheduleDetails.NodeInstanceId;
			}

			return tnInfo;
		}

		/// <summary>
		/// Get current machine name.
		/// </summary>
		/// <returns>A string containing the name of this machine.
		/// Return <c>null</c> when machine name can't be obtained.</returns>
		private string GetCurrentMachineNameSafe()
		{
			string currentMachine;

			try
			{
				currentMachine = Environment.MachineName;
			}
			catch (Exception exc)
			{
				Log.Error(
					exc,
					"The name of this computer cannot be obtained."
				);

				return null;
			}

			if (string.IsNullOrWhiteSpace(currentMachine))
			{
				Log.Error("The name of this computer is null or empty.");

				return null;
			}

			return currentMachine;
		}

		private void ScheduleJob(ScheduleDetails scheduleDetails)
		{
			AuditContext context = GetAuditContext(
				scheduleDetails,
				this._winUser
			);

			if (!context.ConnectionGroup.ServerInstances.Any())
			{
				Log.Debug("Job (id: {0}, name: {1}) is not scheduled as no connections for user '{2}'",
					scheduleDetails.UId,
					scheduleDetails.Name,
					this._winUser
				);

				return;
			}

			string templatePath = Path.Combine(
				context.Template.Directory,
				context.Template.Name
			);

			TemplateInfo templateInfo = this._xmlLoader.LoadTemplate(templatePath);

			TemplateNodeInfo tnInfo = GetNodeInfo(templateInfo, scheduleDetails);

			if (tnInfo != null)
			{
				TemplateNodeInfo tnInstance = tnInfo.Instantiate(null, null);

				if (tnInfo.IsSaved)
				{
					tnInstance.NodeInstanceId = tnInfo.NodeInstanceId;
				}

				MainStorage.Nodes.LoadQueryParameters(context, tnInstance);

				JobInfo jobInfo = MainStorage.Settings.CreateJob(tnInstance, scheduleDetails);

				bool scheduled = this._jobScheduler.ScheduleJob(
					context,
					jobInfo
				);

				if (scheduled)
				{
					Log.Debug("Scheduled job (id: {0}, name: {1})",
						scheduleDetails.UId,
						scheduleDetails.Name
					);
				}
			}
		}

		private void UnscheduleJob(ScheduleDetails scheduleDatails)
		{
			long groupId = scheduleDatails.ConnectionGroupId;
			if (groupId != 0)
			{
				this._jobScheduler.RemoveJob(
					scheduleDatails.Id,
					groupId
				);
			}
		}

		private void RescheduleJob(ScheduleDetails scheduleDetails)
		{
			UnscheduleJob(scheduleDetails);
			ScheduleJob(scheduleDetails);
		}

		private IEnumerable<TemplateNodeInfo> GetTreeNodesList(TemplateNodeInfo node)
		{
			List<TemplateNodeInfo> result = new List<TemplateNodeInfo> { node };

			foreach (TemplateNodeInfo child in node.Children)
			{
				result.AddRange(GetTreeNodesList(child));
			}

			return result;
		}

		/// <summary>
		/// Check schedules updates
		/// </summary>
		public void UpdateSchedules()
		{
			List<ScheduleDetails> addedSchedules   = new List<ScheduleDetails>();
			List<ScheduleDetails> deletedSchedules = new List<ScheduleDetails>();
			List<ScheduleDetails> updatedSchedules = new List<ScheduleDetails>();

			lock (this._schedulesLock)
			{
				Dictionary<long, ScheduleDetails> currentSchedules = LoadSchedules();

				Log.Debug($"Updating schedules... Active schedules: '{this._schedules.Count}'");

				foreach (KeyValuePair<long, ScheduleDetails> schedule in this._schedules)
				{
					long scheduleId = schedule.Key;
					ScheduleDetails cachedSettings = schedule.Value;
					ScheduleDetails settings;

					if (currentSchedules.TryGetValue(scheduleId, out settings))
					{
						DateTime? newDate = settings.DateUpdated;
						DateTime? oldDate = cachedSettings.DateUpdated;

						// check if schedule settings were modified
						if (Nullable.Compare(newDate, oldDate) > 0)
						{
							updatedSchedules.Add(settings);
						}
					}
					else
					{
						// schedule has been removed
						deletedSchedules.Add(cachedSettings);
					}
				}

				// update modified schedules in the cache
				foreach (ScheduleDetails settings in updatedSchedules)
				{
					this._schedules[settings.Id] = settings;
				}

				// remove disabled/deleted schedules from the cache
				foreach (ScheduleDetails settings in deletedSchedules)
				{
					this._schedules.Remove(settings.Id);
				}

				// cache new schedules
				foreach (KeyValuePair<long, ScheduleDetails> schedule in currentSchedules)
				{
					if (!this._schedules.ContainsKey(schedule.Key))
					{
						addedSchedules.Add(schedule.Value);

						this._schedules[schedule.Key] = schedule.Value;
					}
				}
			}

			Log.Debug("Schedules amended:'{0}', added:'{1}', deleted:'{2}'",
				updatedSchedules.Count,
				addedSchedules.Count,
				deletedSchedules.Count
			);

			try
			{
				this._jobScheduler.Pause();

				RescheduleUpdatedJobs(updatedSchedules);
				ScheduleNewJobs(addedSchedules);
				UnscheduleDeletedJobs(deletedSchedules);
			}
			catch (Exception exc)
			{
				Log.Error(exc, "Failed to reschedule jobs");
			}
			finally
			{
				this._jobScheduler.Resume();
			}
		}

		private void UnscheduleDeletedJobs(IEnumerable<ScheduleDetails> deletedSchedules)
		{
			foreach (ScheduleDetails schedule in deletedSchedules)
			{
				UnscheduleJob(schedule);
			}
		}

		private void ScheduleNewJobs(IEnumerable<ScheduleDetails> addedSchedules)
		{
			foreach (ScheduleDetails schedule in addedSchedules)
			{
				ScheduleJob(schedule);
			}
		}

		private void RescheduleUpdatedJobs( IEnumerable<ScheduleDetails> updatedSchedules)
		{
			foreach (ScheduleDetails schedule in updatedSchedules)
			{
				RescheduleJob(schedule);
			}
		}

		private Dictionary<long, ScheduleDetails> LoadSchedules()
		{
			string machineName = GetCurrentMachineNameSafe();

			if (!string.IsNullOrEmpty(machineName))
			{
				machineName = machineName.Trim();
			}

			return MainStorage.Settings.LoadScheduleDetails(machineName);
		}

		private static string GetCurrentWinUser()
		{
			return $"{Environment.UserDomainName}\\{Environment.UserName}";
		}
	}
}
