﻿using System;
using System.Collections.Generic;
using System.Linq;
using MSSQLServerAuditor.Common;
using MSSQLServerAuditor.Common.Collections;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Querying.Data;
using MSSQLServerAuditor.Core.Session;
using NLog;

namespace MSSQLServerAuditor.Core.ETL
{
	public class ETLExecutor
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly IStorageManager _storageManager;

		public ETLExecutor(IStorageManager storageManager)
		{
			Check.NotNull(storageManager, nameof(storageManager));

			this._storageManager = storageManager;
		}

		/// <summary>
		/// Extract, transform and publish results to remote databases
		/// </summary>
		/// <param name="queryResults">Source query results</param>
		public List<ETLResult> Execute(QueryResultSet queryResults)
		{
			if (queryResults.IsEmpty)
			{
				return Lists.Empty<ETLResult>();
			}

			List<ETLResult> etlResults = new List<ETLResult>();
			foreach (QueryResult queryResult in queryResults.Results)
			{
				List<ETLResult> results = ProcessQueryResult(queryResult);
				etlResults.AddRange(results);
			}

			return etlResults;
		}

		private List<ETLResult> ProcessQueryResult(QueryResult queryResult)
		{
			List<ETLResult> metaResults = new List<ETLResult>();
			foreach (KeyValuePair<AuditSession, SessionResult> srPair in queryResult.SessionResults)
			{
				AuditSession  session       = srPair.Key;
				SessionResult sessionResult = srPair.Value;

				if (sessionResult.IsEmpty)
				{
					continue;
				}

				long? queryId = this._storageManager.MainStorage.Nodes.GetQuery(
					session,
					queryResult.QueryInfo,
					DefaultValues.Date.Minimum,
					true
				)?.Id;

				ETLQueryParameters etlQueryParams = new ETLQueryParameters
				{
					QueryId           = queryId,
					ConnectionGroupId = session.Group.Id,
					ServerInstanceId  = session.Server.Id,
					LoginId           = session.Server.Login.Id,
					TemplateId        = session.Template.Id
				};

				List<ETLResult> subResults = ProcessSessionResult(sessionResult, etlQueryParams);

				metaResults.AddRange(subResults);
			}

			return metaResults;
		}

		private List<ETLResult> ProcessSessionResult(SessionResult sessionResult, ETLQueryParameters etlQueryParams)
		{
			List<ETLResult> etlResults = new List<ETLResult>();
			foreach (DatabaseResult dbResult in sessionResult.DatabaseResults.Values)
			{
				QueryInfo          queryInfo     = dbResult.QueryItemInfo.Parent;
				List<ETLStatement> etlStatements = queryInfo?.ETLStatements;

				if (etlStatements != null && !dbResult.IsEmpty)
				{
					foreach (ETLStatement etlStatement in etlStatements)
					{
						string connection = etlStatement.ConnectionName;
						if (string.IsNullOrWhiteSpace(connection))
						{
							// no ETL connection specified
							continue;
						}

						// load ETL instances from database
						List<ServerInstance> etlTargets = this._storageManager.MainStorage.Connections
							.GetServerInstancesByAlias(connection);

						foreach (ServerInstance etlTarget in etlTargets)
						{
							// get scripts of ETL target's connection type
							IEnumerable<ETLScript> matchingScripts = etlStatement.ETLScripts
								.Where(s => s.ConnectionType == etlTarget.ConnectionType || s.ConnectionType == null);

							foreach (ETLScript etlScript in matchingScripts)
							{
								// get target database
								ETLDatabase dbTarget = ETLDatabaseFactory.CreateDatabase(etlTarget);

								// transform data to publish
								ETLData etlData = TransformResult(dbResult, etlQueryParams, etlScript);

								ETLResult meta = new ETLResult
								{
									TargetServerInstanceId = etlTarget.Id,
									QueryId                = etlQueryParams.QueryId
								};

								try
								{
									// publish data to target database
									long rowsAdded = Publish(etlData, dbTarget);
									meta.Rows      = rowsAdded;
								}
								catch (Exception exc)
								{
									Log.Error(exc, 
										$"Error executing ETL query. " +
										$"Query id: '{meta.QueryId}', " +
										$"Server id: '{meta.TargetServerInstanceId}'"
									);

									ErrorInfo error = new ErrorInfo(exc);

									meta.ErrorCode    = error.Code;
									meta.ErrorMessage = error.Message;
									meta.ErrorId      = error.Number;
								}

								etlResults.Add(meta);
							}
							
						}
					}
				}
			}

			return etlResults;
		}

		private static ETLData TransformResult(
			DatabaseResult     dbResult,
			ETLQueryParameters etlQueryParams,
			ETLScript          etlScript
		)
		{
			ETLTransform transform = new ETLTransform(etlScript.TableMapping);

			return transform.Apply(dbResult.DataTables, etlQueryParams);
		}

		private static long Publish(ETLData etlData, ETLDatabase dbTarget)
		{
			long rowsAdded = 0;
			
			if (etlData.Rows.Any())
			{
				rowsAdded = dbTarget.InsertRows(etlData.TableInfo, etlData.Rows);
			}

			return rowsAdded;
		}
	}
}
