﻿using System;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Settings
{
	[Serializable]
	public class SmtpSettings
	{
		[XmlElement(ElementName = "ServerAddress")]
		public string ServerAddress { get; set; }

		[XmlElement(ElementName = "Port")]
		public int Port { get; set; }

		[XmlElement(ElementName = "AuthRequired")]
		public bool AuthenticationRequired { get; set; }

		[XmlElement(ElementName = "SslEnabled")]
		public bool SslEnabled { get; set; }

		[XmlElement(ElementName = "SmtpCredentials")]
		public SmtpCredentials SmtpCredentials { get; set; }

		[XmlElement(ElementName = "SenderEmail")]
		public string SenderEmail { get; set; }
	}
}