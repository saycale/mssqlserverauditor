﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using MSSQLServerAuditor.Common.Collections;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.ETL;
using MSSQLServerAuditor.Core.Persistence;
using MSSQLServerAuditor.Core.Persistence.Services;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Querying;
using MSSQLServerAuditor.Core.Querying.Data;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.Core.Xml;
using MSSQLServerAuditor.DataAccess.Entity.Models;
using NLog;

namespace MSSQLServerAuditor.Core
{
	public class AuditCoordinator
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly IStorageManager _storageManager;
		private readonly QueryExecutor   _queryExecutor;
		private readonly IQueryLoader    _queryLoader;

		public AuditCoordinator(
			IStorageManager storageManager,
			QueryExecutor   queryExecutor,
			IQueryLoader    queryLoader
		)
		{
			Check.NotNull(storageManager, nameof(storageManager));
			Check.NotNull(queryExecutor,  nameof(queryExecutor));
			Check.NotNull(queryLoader,    nameof(queryLoader));

			this._storageManager = storageManager;
			this._queryExecutor  = queryExecutor;
			this._queryLoader    = queryLoader;
		}

		public QueryResultSet ProcessQueries(
			AuditContext      context,
			TemplateNodeInfo  tnInfo,
			CancellationToken token
		)
		{
			DateTime  startTime     = DateTime.Now;
			Stopwatch durationWatch = new Stopwatch();

			durationWatch.Start();

			// execute query to a remote source
			QueryResultSet results = Execute(
				context,
				tnInfo,
				token
			);

			QueryResultWriter writer = new QueryResultWriter(
				this._storageManager,
				this._queryLoader
			);

			// save result to local storage
			long? rows = writer.SaveResults(results);

			tnInfo.Counter = rows;

			// update row counts and timers
			NodeService nodeService = this._storageManager.MainStorage.Nodes;
			nodeService.SaveNodeCounter(tnInfo);

			// execute ETL statements
			ETLExecutor     etlExecutor = new ETLExecutor(this._storageManager);
			List<ETLResult> etlResults  = etlExecutor.Execute(results);

			if (etlResults.Any())
			{
				SaveETLResults(etlResults);
			}

			durationWatch.Stop();

			nodeService.SaveUpdateTimings(
				tnInfo,
				startTime,
				(long) durationWatch.Elapsed.TotalMilliseconds
			);

			return results;
		}

		private void SaveETLResults(List<ETLResult> etlResults)
		{
			foreach (ETLResult etlResult in etlResults)
			{
				try
				{
					this._storageManager.MainStorage.MetaResults.SaveETLResult(etlResult);
				}
				catch (Exception exc)
				{
					Log.Error(exc, "Failed to save ETL results");
				}
			}
		}

		public List<NodeAttributes> QueryDynamicNodes(
			AuditSession          session,
			TemplateNodeQueryInfo tnQueryInfo,
			CancellationToken     token
		)
		{
			List<QueryInfo> queries = this._queryLoader.LoadQueries(tnQueryInfo);

			ConnectionType type = session.ConnectionType;

			QueryInfo query = queries.FirstOrDefault(q =>
				q.ConnectionType == type || q.ConnectionType.IsInternal()
			);

			if (query == null)
			{
				Log.Error($"Unable to get dynamic nodes. No query defined for template node: {tnQueryInfo.TemplateNode.Name}");
				return Lists.Empty<NodeAttributes>();
			}

			QueryItemInfo queryItem = session.Server.ResolveQuery(query.Items);

			string defaultDatabase = tnQueryInfo.TemplateNode.GetDefaultDatabase();

			ErrorInfo       error;
			List<DataTable> tables;
			try
			{
				tables = CreateSessionExecutor(session).ExecuteQuery(
					queryItem,
					token,
					defaultDatabase,
					query.Parameters,
					tnQueryInfo.ParameterValues
				);

				error = null;
			}
			catch (Exception ex)
			{
				Log.Error(ex,
					"Error executing query for dynamic nodes\r\n" +
					$"Query: {tnQueryInfo}\r\n" +
					$"Connection: {session}"
				);

				error  = new ErrorInfo(ex);
				tables = null;
			}

			MainStorage storage = this._storageManager.MainStorage;

			DBQueryGroup queryGroup = storage.Nodes.GetQueryGroup(
				session,
				tnQueryInfo,
				DateTime.Now,
				false
			);

			 this._storageManager.MainStorage.MetaResults.SaveMeta(
				queryGroup.Id,
				error,
				tables
			);

			if (tables != null && tables.Count < 0)
			{
				throw new DataException(query + " returned no recordsets.");
			}

			List<NodeAttributes> dynamicNodes = new List<NodeAttributes>();
			if (tables != null && tables.Any())
			{
				foreach (DataTable table in tables)
				{
					foreach (DataRow row in table.Rows)
					{
						NodeAttributes attributes = new NodeAttributes(row);

						dynamicNodes.Add(attributes);
					}
				}
			}

			return dynamicNodes;
		}

		public QueryResultSet Execute(
			AuditContext      context,
			TemplateNodeInfo  tnInfo,
			CancellationToken token,
			bool              checkHist = false)
		{
			List<TemplateNodeQueryInfo> queryList = tnInfo.Queries;

			ConnectionGroup group = context.ConnectionGroup;
			ConnectionType  type  = group.ConnectionType;

			QueryResultSet results = new QueryResultSet();
			if (queryList.Any())
			{
				foreach (TemplateNodeQueryInfo tnQueryInfo in queryList)
				{
					if (token.IsCancellationRequested)
					{
						break;
					}

					QueryResult queryResult;
					List<QueryInfo> queries = this._queryLoader.LoadQueries(tnQueryInfo);

					if (checkHist)
					{
						queries.RemoveAll(q => !q.ConnectionType.IsInternal());
					}
					else
					{
						queries.RemoveAll(q => q.ConnectionType.IsInternal());
					}

					string connectionsSelectId = tnQueryInfo.ConnectionsSelectId;
					if (connectionsSelectId == null)
					{
						if (!queries.Any(q => q.ConnectionType == type || q.ConnectionType.IsInternal()))
						{
							continue;
						}

						queryResult = this._queryExecutor.ExecuteQueries(
							context,
							tnQueryInfo,
							queries,
							token
						);
					}
					else
					{
						queryResult = ExecuteConnectionsSelectQuery(
							context,
							tnQueryInfo,
							queries,
							token
						);
					}

					results.Add(queryResult);
				}
			}

			return results;
		}

		private QueryResult ExecuteConnectionsSelectQuery(
			AuditContext          context,
			TemplateNodeQueryInfo tnQueryInfo,
			List<QueryInfo>       queries,
			CancellationToken     token
		)
		{
			TemplateNodeQueryInfo connectionsQueryInfo = tnQueryInfo.GetParentConnectionSelectQuery();
			List<QueryInfo>       connectionsQueries   = this._queryLoader.LoadQueries(connectionsQueryInfo);

			QueryResult connectionsQueryResult = this._queryExecutor.ExecuteQueries(
				context,
				connectionsQueryInfo,
				connectionsQueries,
				token
			);

			QueryResult queryResult = new QueryResult(tnQueryInfo);

			foreach (KeyValuePair<AuditSession, SessionResult> sessionResultPair in connectionsQueryResult.SessionResults)
			{
				MainStorage       mainStorage = this._storageManager.MainStorage;
				ConnectionService connections = mainStorage.Connections;

				SessionResult sessionResult = sessionResultPair.Value;

				if (sessionResult.ErrorInfo == null)
				{
					AuditSession session = sessionResultPair.Key;

					DBQuery parentQuery = mainStorage.Nodes.GetQuery(
						session,
						tnQueryInfo,
						DateTime.Now,
						false
					);

					List<DBDynamicConnection> dynamicConnections = new List<DBDynamicConnection>();

					foreach (KeyValuePair<string, DatabaseResult> databaseResultInfo in sessionResult.DatabaseResults)
					{
						DatabaseResult  dbResult   = databaseResultInfo.Value;
						List<DataTable> dataTables = dbResult.DataTables;

						if (dataTables != null)
						{
							foreach (DataTable dataTable in dataTables)
							{
								List<ServerInstance> dynamicInstances = ReadDynamicConnections(dataTable);

								foreach (ServerInstance dynamicInstance in dynamicInstances)
								{
									QueryInfo query = queries.FirstOrDefault(
										x => x.ConnectionType == dynamicInstance.ConnectionType || x.ConnectionType.IsInternal()
									);

									if (query == null)
									{
										continue;
									}

									QueryItemInfo queryItem = dynamicInstance.ResolveQuery(query.Items);

									AuditSession dynamicSession = new AuditSession(
										session.Group,
										dynamicInstance,
										session.Template
									);

									SessionExecutor executor = CreateSessionExecutor(dynamicSession);
									SessionResult connectionSelectResult = executor.ExecuteConnectionSelectQuery(
										tnQueryInfo,
										queryItem,
										token
									);

									queryResult.AddSessionResult(connectionSelectResult);

									DBQuery dynamicQuery = mainStorage.Nodes.GetQuery(
										connectionSelectResult.Session,
										tnQueryInfo,
										DateTime.Now,
										false
									);

									if (parentQuery != null && dynamicQuery != null)
									{
										long? dynamicInstanceId = dynamicQuery.ServerInstanceId;

										if (dynamicInstanceId.HasValue)
										{
											DBDynamicConnection dynamicConnection = new DBDynamicConnection
											{
												QueryId  = dynamicQuery.Id,
												ServerId = dynamicInstanceId.Value
											};

											dynamicConnections.Add(dynamicConnection);
										}
									}
								}
							}
						}
					}

					if (dynamicConnections.Count > 0 && parentQuery != null)
					{
						long pqId = parentQuery.Id;

						connections.DeleteDynamicConnections(pqId);
						connections.UpdateDynamicConnections(pqId, dynamicConnections);
					}
				}
			}

			return queryResult;
		}

		private List<ServerInstance> ReadDynamicConnections(DataTable table)
		{
			List<ServerInstance> dynamicInstances = new List<ServerInstance>();

			ConnectionService connections = this._storageManager.MainStorage.Connections;

			foreach (DataRow row in table.Rows)
			{
				string         typeString     = row.GetValue(DBConnectionType.FieldName, ConnectionType.MSSQL.ToString());
				ConnectionType connectionType = ConnectionTypeHelper.Parse(typeString);

				ServerInstance dynInstance = ServerInstanceFactory.CreateInstance(connectionType);

				dynInstance.ReadFrom(row);

				dynInstance.IsDynamic = true;
				connections.SaveServerInstance(dynInstance);

				dynamicInstances.Add(dynInstance);
			}

			return dynamicInstances;
		}

		private SessionExecutor CreateSessionExecutor(AuditSession session)
		{
			return this._queryExecutor.CreateSessionExecutor(session);
		}
	}
}
