﻿using System;
using MSSQLServerAuditor.Core.Domain.Models;
using Quartz;

namespace MSSQLServerAuditor.Core.Schedule
{
	public class UpdateJob
	{
		public static UpdateJob Create(
			JobInfo jobInfo,
			string  groupName,
			Action  updateAction
		)
		{
			return new UpdateJob(
				jobInfo,
				groupName,
				updateAction
			);
		}

		private UpdateJob(
			JobInfo jobInfo,
			string  groupName,
			Action  updateAction
		)
		{
			GroupName = groupName;
			JobDetail = CreateUpdateJob(jobInfo, updateAction);
			Trigger   = CreateUpdateTrigger(jobInfo);
		}

		public string     GroupName { get; private set; }
		public IJobDetail JobDetail { get; private set; }
		public ITrigger   Trigger   { get; private set; }

		private IJobDetail CreateUpdateJob(
			JobInfo jobInfo,
			Action  updateAction
		)
		{
			JobKey jobKey = jobInfo.GetJobKey(GroupName);

			IJobDetail jobDetail = JobBuilder
				.Create<ActionJob>()
				.WithIdentity(jobKey)
				.SetJobData(new JobDataMap
				{
					{ ActionJob.ParamAction, updateAction }
				})
				.Build();

			return jobDetail;
		}

		private ITrigger CreateUpdateTrigger(JobInfo jobInfo)
		{
			TriggerKey       triggerKey = jobInfo.GetTriggerKey(GroupName);
			ScheduleSettings settings   = jobInfo.ScheduleDetails;

			return CreateTrigger(triggerKey, settings);
		}

		private ITrigger CreateTrigger(TriggerKey triggerKey, ScheduleSettings settings)
		{
			IScheduleBuilder scheduleBuilder = CreateCronSchedule(settings);

			TriggerBuilder builder = TriggerBuilder.Create()
				.WithIdentity(triggerKey)
				.WithSchedule(scheduleBuilder);

			return builder.Build();
		}

		private IScheduleBuilder CreateCronSchedule(ScheduleSettings settings)
		{
			CronExpression      cronExpression      = GetCronExpression(settings);
			CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.CronSchedule(cronExpression);

			return cronScheduleBuilder;
		}

		private CronExpression GetCronExpression(ScheduleSettings settings)
		{
			return new CronExpression(settings.CronExpression);
		}
	}
}
