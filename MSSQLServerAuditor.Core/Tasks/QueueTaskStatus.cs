namespace MSSQLServerAuditor.Core.Tasks
{
	public enum QueueTaskStatus
	{
		Waiting,
		Running,
		Completed,
		Cancelled,
		Failed
	}
}