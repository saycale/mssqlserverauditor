﻿namespace MSSQLServerAuditor.Core.Tasks
{
	public class QueueTaskEventArgs
	{
		public QueueTaskEventArgs(QueueTask task)
		{
			this.Task = task;
		}

		public QueueTask Task { get; }
	}
}
