﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MSSQLServerAuditor.Common;

namespace MSSQLServerAuditor.Core.Querying.Data
{
	/// <summary>
	/// Template node query result set
	/// </summary>
	public class QueryResultSet
	{
		private readonly object            _lock;
		private readonly List<QueryResult> _results;

		/// <summary>
		/// Default constructor
		/// </summary>
		public QueryResultSet()
		{
			this._lock    = new object();
			this._results = new List<QueryResult>();

			this.UpdateTime     = null;
			this.UpdateDuration = null;
		}

		/// <summary>
		/// List of template node results
		/// </summary>
		public ReadOnlyCollection<QueryResult> Results
		{
			get
			{
				lock (this._lock)
				{
					return this._results
						.ToList()
						.AsReadOnly();
				}
			}
		}

		/// <summary>
		/// Results date-time
		/// </summary>
		public DateTime Timestamp
		{
			get
			{
				DateTime timestamp = DefaultValues.Date.Minimum;
				lock (this._lock)
				{
					foreach (QueryResult queryResult in _results)
					{
						if (queryResult.SessionResults.Any())
						{
							foreach (SessionResult sessionResult in queryResult.SessionResults.Values)
							{
								if (sessionResult.Timestamp > timestamp)
								{
									timestamp = sessionResult.Timestamp;
								}
							}
						}
					}
				}

				return timestamp <= DefaultValues.Date.Minimum
					? DateTime.Now
					: timestamp;
			}
		}

		/// <summary>
		/// Update datetime
		/// </summary>
		public DateTime? UpdateTime { get; set; }

		/// <summary>
		/// Update node duration
		/// </summary>
		public TimeSpan? UpdateDuration { get; set; }

		/// <summary>
		/// Add template node result.
		/// </summary>
		/// <param name="queryResult">template node query result.</param>
		public void Add(QueryResult queryResult)
		{
			lock (this._lock)
			{
				this._results.Add(queryResult);
			}
		}

		public bool IsSavable
		{
			get
			{
				lock (this._lock)
				{
					return this._results.Any(qr => qr.IsSavable);
				}
			}
		}

		public bool IsEmpty
		{
			get
			{
				lock (this._lock)
				{
					if (!this.Results.Any())
					{
						return true;
					}

					foreach (QueryResult queryResult in this.Results)
					{
						foreach (SessionResult instanceResult in queryResult.SessionResults.Values)
						{
							// check if any instance result is not empty
							if (!instanceResult.IsEmpty)
							{
								return false;
							}
						}
					}
				}

				return true;
			}
		}
	}
}
