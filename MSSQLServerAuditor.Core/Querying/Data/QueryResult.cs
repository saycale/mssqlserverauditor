﻿using System.Collections.Concurrent;
using System.Linq;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Session;

namespace MSSQLServerAuditor.Core.Querying.Data
{
	/// <summary>
	/// Result for query
	/// </summary>
	public class QueryResult
	{
		/// <summary>
		/// Default constructor
		/// </summary>
		/// <param name="tnQueryInfo">Tempate node query info</param>
		public QueryResult(TemplateNodeQueryInfo tnQueryInfo)
		{
			this.QueryInfo      = tnQueryInfo;
			this.SessionResults = new ConcurrentDictionary<AuditSession, SessionResult>();
			this.ErrorInfo      = null;
		}

		/// <summary>
		/// Constructor with exception thrown
		/// </summary>
		/// <param name="tnQueryInfo">Tempate node query info</param>
		/// <param name="errorInfo">Error info</param>
		public QueryResult(
			TemplateNodeQueryInfo tnQueryInfo,
			ErrorInfo             errorInfo) : this(tnQueryInfo)
		{
			this.ErrorInfo = errorInfo;
		}

		/// <summary>
		/// Dictionary for session results
		/// </summary>
		public ConcurrentDictionary<AuditSession, SessionResult> SessionResults { get; }

		/// <summary>
		/// Add session result
		/// </summary>
		/// <param name="sessionResult">Session result</param>
		public void AddSessionResult(SessionResult sessionResult)
		{
			this.SessionResults.TryAdd(
				sessionResult.Session,
				sessionResult
			);
		}

		/// <summary>
		/// Error info if exception was thrown
		/// </summary>
		public ErrorInfo ErrorInfo { get; }

		/// <summary>
		/// Template node query info
		/// </summary>
		public TemplateNodeQueryInfo QueryInfo { get; }

		public bool IsSavable => SessionResults.Values.Any(dr => dr.IsSavable);
	}
}
