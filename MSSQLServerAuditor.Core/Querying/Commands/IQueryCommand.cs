﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Core.Domain;

namespace MSSQLServerAuditor.Core.Querying.Commands
{
	public interface IQueryCommand : IDisposable
	{
		void AssignParameters(
			List<QueryParameterInfo> parameters,
			List<ParameterValue>     parameterValues
		);

		Task<IDataReader> ExecuteReaderAsync(CancellationToken cancellationToken);
	}
}
