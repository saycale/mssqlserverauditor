﻿using System;
using System.Collections.Generic;
using System.Linq;
using MSSQLServerAuditor.Common.Collections;
using MSSQLServerAuditor.Core.Domain;

namespace MSSQLServerAuditor.Core.Querying.Commands
{
	public class CommandData
	{
		public CommandData(string commandText, string commandNamespace, int timeout)
		{
			Text      = commandText;
			Namespace = commandNamespace;
			Timeout   = timeout;

			Aliases   = new Dictionary<string, List<string>>(StringComparer.OrdinalIgnoreCase);
		}

		public string Text      { get; set; }
		public string Namespace { get; set; }
		public int    Timeout   { get; set; }

		public Dictionary<string, List<string>> Aliases { get; }

		public void AddAliases(List<AliasInfo> aliases)
		{
			if (aliases != null && aliases.Any())
			{
				foreach (AliasInfo alias in aliases)
				{
					if (Aliases.ContainsKey(alias.Name))
					{
						Aliases[alias.Name].Add(alias.Alias);
					}
					else
					{
						Aliases[alias.Name] = Lists.Of(alias.Alias);
					}
				}
			}
		}
	}
}
