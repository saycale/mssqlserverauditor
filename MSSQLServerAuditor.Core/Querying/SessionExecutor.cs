﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common;
using MSSQLServerAuditor.Common.Collections;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Exceptions;
using MSSQLServerAuditor.Core.Querying.Commands;
using MSSQLServerAuditor.Core.Querying.Connections;
using MSSQLServerAuditor.Core.Querying.Data;
using MSSQLServerAuditor.Core.Querying.Handlers;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.Core.Settings;
using NLog;

namespace MSSQLServerAuditor.Core.Querying
{
	public class SessionExecutor
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly IAppSettings            _settings;
		private readonly QueryFormatter          _queryFormatter;
		private readonly IQueryConnectionFactory _connectionFactory;

		public SessionExecutor(
			IAppSettings            settings,
			IQueryConnectionFactory connectionFactory,
			QueryFormatter          queryFormatter,
			AuditSession            session)
		{
			this._settings          = settings;
			this._connectionFactory = connectionFactory;
			this._queryFormatter    = queryFormatter;

			Session = session;
		}

		public AuditSession Session { get; }

		public SessionResult ExecuteConnectionSelectQuery(
			TemplateNodeQueryInfo tnQueryInfo,
			QueryItemInfo         queryItem,
			CancellationToken     token
		)
		{
			QueryInfo queryInfo = queryItem.Parent;

			DatabaseResult dbResult;
			try
			{
				List<DataTable> tables = ExecuteQuery(
					queryItem,
					token,
					tnQueryInfo.TemplateNode.GetDefaultDatabase(),
					queryInfo.Parameters,
					tnQueryInfo.ParameterValues
				);

				if (tables != null && tables.IsNullOrEmpty())
				{
					throw new InvalidTemplateException(tnQueryInfo + " returned no recordsets.");
				}

				dbResult = new DatabaseResult(tables, queryItem);
			}
			catch (Exception exc)
			{
				ErrorInfo error = new ErrorInfo(exc);

				dbResult = new DatabaseResult(error, queryItem);
			}

			SessionResult sessionResult = new SessionResult(Session, DateTime.Now);
			sessionResult.AddDatabaseResult(dbResult);

			return sessionResult;
		}

		public SessionResult Execute(
			TemplateNodeQueryInfo tnQueryInfo,
			QueryInfo             query,
			CancellationToken     token)
		{
			try
			{
				QueryScopeHandlerFactory factory = new QueryScopeHandlerFactory(this, token);
				QueryScopeHandler        handler = factory.GetHandler(query.Scope);

				List<GroupInfo> groups = handler.QueryGroups(
					Session.Server,
					query,
					tnQueryInfo.ParameterValues
				);

				SessionResult result    = new SessionResult(Session, DefaultValues.Date.Minimum);
				QueryItemInfo queryItem = Session.Server.ResolveQuery(
					query.Items,
					query.ConnectionType
				);

				ExecuteQuery(
					tnQueryInfo.ParameterValues,
					tnQueryInfo.TemplateNode.GetDefaultDatabase(),
					groups,
					result,
					queryItem,
					token
				);

				result.RefreshTimestamp(DateTime.Now);

				return result;
			}
			catch (OperationCanceledException ex)
			{
				return new SessionResult(new ErrorInfo(ex), Session, DateTime.Now);
			}
			catch (Exception ex)
			{
				Log.Error(ex, query.ToString());

				return new SessionResult(new ErrorInfo(ex), Session, DateTime.Now);
			}
		}

		private void ExecuteQuery(
			List<ParameterValue> parameters,
			string               defaultDatabase,
			List<GroupInfo>      groups,
			SessionResult        result,
			QueryItemInfo        queryItem,
			CancellationToken    token
		)
		{
			QueryInfo query = queryItem.Parent;

			if (groups.Any())
			{
				DatabaseResult dbResult = ExecuteQuery(
					parameters,
					defaultDatabase,
					queryItem,
					GroupInfo.Empty,
					token
				);

				result.AddDatabaseResult(dbResult);
			}
			else
			{
				QueryScopeHandlerFactory factory = new QueryScopeHandlerFactory(this, token);
				QueryScopeHandler        handler = factory.GetHandler(query.Scope);

				foreach (GroupInfo group in groups)
				{
					if (group.ChildGroups.Count > 0)
					{
						List<ParameterValue> tempParams = new List<ParameterValue>(parameters);

						tempParams.AddRange(
							handler.GetGroupParameters(group)
						);

						ExecuteQuery(
							tempParams,
							defaultDatabase,
							group.ChildGroups,
							result,
							queryItem,
							token
						);
					}

					DatabaseResult dbResult = ExecuteQuery(
						parameters,
						defaultDatabase,
						queryItem,
						group,
						token
					);

					result.AddDatabaseResult(dbResult);
				}
			}
		}

		private DatabaseResult ExecuteQuery(
			List<ParameterValue> parameters,
			string               defaultDatabase,
			QueryItemInfo        queryItem,
			GroupInfo            group,
			CancellationToken    token
		)
		{
			QueryInfo query = queryItem.Parent;

			string database = string.IsNullOrWhiteSpace(group.Name) ? defaultDatabase : group.Name;
			
			DatabaseResult dbResult = QueryDatabase(
				queryItem,
				database,
				group.Id,
				query.Parameters,
				parameters,
				token
			);
			
			dbResult.Database = group.Name;

			return dbResult;
		}

		private DatabaseResult QueryDatabase(
			QueryItemInfo            queryItem,
			string                   database,
			string                   databaseId,
			List<QueryParameterInfo> parameters,
			List<ParameterValue>     parameterValues,
			CancellationToken        token
		)
		{
			try
			{
				List<DataTable> tables = ExecuteQuery(
					queryItem,
					token,
					database,
					parameters,
					parameterValues
				);

				return new DatabaseResult(
					tables,
					queryItem,
					database, 
					databaseId
				);
			}
			catch (Exception ex)
			{
				if (!(ex is OperationCanceledException))
				{
					Log.Error(ex, $"Query: {queryItem}, Connection: {Session}");
				}

				return new DatabaseResult(
					new ErrorInfo(ex),
					queryItem,
					database,
					databaseId
				);
			}
		}

		internal List<DataTable> ExecuteQuery(
			QueryItemInfo            query,
			CancellationToken        token,
			string                   database        = null,
			List<QueryParameterInfo> parameters      = null,
			List<ParameterValue>     parameterValues = null
		)
		{
			if (query == null)
			{
				Log.Warn("There is no query statement to execute (QueryItemInfo == null).");

				return Lists.Empty<DataTable>();
			}

			QueryInfo parentQuery = query.Parent;

			token.ThrowIfCancellationRequested();

			string condition = query.ExecuteIfSqlText
				.TrimmedOrEmpty();

			if (!condition.IsNullOrEmpty())
			{
				QueryItemInfo clone = query.Clone();

				clone.ExecuteIfSqlText = null;
				clone.Text             = condition;
				clone.Parent           = query.Parent;

				DataTable conditionTable = ExecuteQuery(
					clone,
					token,
					database,
					parameters,
					parameterValues
				).FirstOrDefault();
				
				if (conditionTable != null && conditionTable.Rows.Count > 0)
				{
					bool shouldExecute = (int) conditionTable.Rows[0][0] == 1;

					if (!shouldExecute)
					{
						return Lists.Empty<DataTable>();
					}
				}
			}

			List<DataTable> tables = new List<DataTable>();

			// trim whitespaces
			string queryText = query.Text.TrimmedOrEmpty();

			// check if sql query is empty
			if (string.IsNullOrEmpty(queryText))
			{
				// return empty result without executing the query
				Log.Warn("Sql statement is empty, nothing to execute.");

				return Lists.Empty<DataTable>();
			}

			queryText = this._queryFormatter.Format(queryText);

			using (IQueryConnection connection = this._connectionFactory.OpenNewConnection(Session, query.Parent.ConnectionType))
			{
				connection.ChangeDatabase(database);

				CommandData commandData = new CommandData(
					queryText,
					parentQuery?.Namespace,
					this._settings.User.Timeout
				);

				commandData.AddAliases(parentQuery?.Aliases);

				using (IQueryCommand queryCommand = connection.GetCommand(commandData))
				{
					queryCommand.AssignParameters(parameters, parameterValues);

					try
					{
						Task<IDataReader> executeReaderTask = queryCommand.ExecuteReaderAsync(token);

						using (IDataReader dataReader = executeReaderTask.GetAwaiter().GetResult())
						{
							while (!dataReader.IsClosed)
							{
								DataTable table = new DataTable();

								table.Load(dataReader, LoadOption.OverwriteChanges, ExecuteSqlFillErrorHandler);
								tables.Add(table);
							}
						}
					}
					catch (Exception)
					{
						// check if the error occurred because of cancellation request
						if (token.IsCancellationRequested)
						{
							// throw cancellation exception
							token.ThrowIfCancellationRequested();
						}

						// otherwise, rethrow the original exception
						throw;
					}
				}
			}

			return tables;
		}

		private void ExecuteSqlFillErrorHandler(object sender, FillErrorEventArgs e)
		{
			Log.Error(e.Errors, "Error when filling table with query results");

			// Setting e.Continue to True tells the Load method to continue trying. Setting it to False
			// indicates that an error has occurred, and the Load method raises the exception that got
			// you here.
			e.Continue = true;
		}
	}
}
