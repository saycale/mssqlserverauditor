﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using MSSQLServerAuditor.Common.Collections;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Querying.Data;
using NLog;

namespace MSSQLServerAuditor.Core.Querying.Handlers
{
	public class DatabaseScopeHandler : QueryScopeHandler
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		public DatabaseScopeHandler(
			SessionExecutor   sessionExecutor,
			CancellationToken token) : base(sessionExecutor, token)
		{
		}

		public override List<GroupInfo> QueryGroups(
			ServerInstance       serverInstance,
			QueryInfo            query,
			List<ParameterValue> parameters)
		{
			List<GroupInfo> allGroups = new List<GroupInfo>();

			QueryItemInfo groupQuery = GetGroupQuery(serverInstance, query);

			if (groupQuery != null)
			{
				try
				{
					List<DataTable> groupsTabels = SessionExecutor.ExecuteQuery(
						groupQuery,
						this.CancelToken,
						null,
						query.Parameters,
						parameters
					);

					foreach (DataTable groupTable in groupsTabels)
					{
						List<GroupInfo> groups = ReadGroups(groupTable);
						allGroups.AddRange(groups);
					}

					if (groupQuery.ChildGroups.IsNullOrEmpty())
					{
						return allGroups;
					}

					foreach (GroupInfo group in allGroups)
					{
						GetChildGroups(
							serverInstance,
							query,
							groupQuery,
							group,
							parameters
						);
					}
				}
				catch (OperationCanceledException ex)
				{
					Log.Error(ex);
					throw;
				}
				catch (Exception ex)
				{
					Log.Error(ex, $"Failed to query groups for connection={this.SessionExecutor.Session}, query={query}");
					throw;
				}
			}

			return allGroups;
		}

		public override List<ParameterValue> GetGroupParameters(GroupInfo groupRec)
		{
			ParameterValue parameter = new ParameterValue
			{
				Name         = groupRec.Name,
				DefaultValue = groupRec.Id
			};

			return Lists.Of(parameter);
		}

		private void GetChildGroups(
			ServerInstance       serverInstance,
			QueryInfo            query,
			QueryItemInfo        parent,
			GroupInfo            parentGroup,
			List<ParameterValue> parameterValues
		)
		{
			QueryItemInfo childGroupQuery = serverInstance.ResolveQuery(
				parent.ChildGroups,
				query.ConnectionType
			);

			List<ParameterValue> tempParams = parameterValues.ToList();
			tempParams.AddRange(GetGroupParameters(parentGroup));

			try
			{
				List<DataTable> groupTables = SessionExecutor.ExecuteQuery(
					childGroupQuery,
					this.CancelToken,
					null,
					childGroupQuery.Parameters,
					tempParams
				);

				foreach (DataTable groupTable in groupTables)
				{
					List<GroupInfo> groups = ReadGroups(groupTable);
					parentGroup.ChildGroups.AddRange(groups);
				}

				if (childGroupQuery.ChildGroups == null || childGroupQuery.ChildGroups.Count <= 0)
				{
					return;
				}

				foreach (GroupInfo childGroup in parentGroup.ChildGroups)
				{
					GetChildGroups(
						serverInstance,
						query,
						childGroupQuery,
						childGroup,
						tempParams
					);
				}
			}
			catch (OperationCanceledException ex)
			{
				Log.Error(ex);
				throw;
			}
			catch (Exception ex)
			{
				Log.Error(ex,
					$"Failed to query groups for connection={SessionExecutor.Session}, query={childGroupQuery}"
				);

				throw;
			}
		}

		private List<GroupInfo> ReadGroups(DataTable groupTable)
		{
			if (groupTable.Columns.Count < 2)
			{
				throw new ArgumentException(
					"Table with groups must contains minimum two columns. GroupName and GroupId."
				);
			}

			List<GroupInfo> groups = new List<GroupInfo>();

			List<DataRow> rows = groupTable.Rows.Cast<DataRow>().ToList();
			foreach (DataRow row in rows)
			{
				string name = row[0].ToString();
				string id   = row[1].ToString();

				GroupInfo group = new GroupInfo(name, id)
				{
					Scope = QueryScope.Database
				};

				groups.Add(group);
			};

			return groups;
		}

		private QueryItemInfo GetGroupQuery(
			ServerInstance serverInstance,
			QueryInfo      query
		)
		{
			List<QueryItemInfo> queryItems = !query.GroupSelect.IsNullOrEmpty()
				? query.GroupSelect
				: query.DatabaseSelect;

			return serverInstance.ResolveQuery(
				queryItems,
				query.ConnectionType
			);
		}
	}
}
