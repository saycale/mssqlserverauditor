﻿using System.Collections.Generic;
using System.Threading;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Querying.Data;

namespace MSSQLServerAuditor.Core.Querying.Handlers
{
	public abstract class QueryScopeHandler
	{
		public QueryScopeHandler(
			SessionExecutor   sessionExecutor,
			CancellationToken token)
		{
			Check.NotNull(sessionExecutor, nameof(sessionExecutor));
			Check.NotNull(token,           nameof(token));

			this.SessionExecutor = sessionExecutor;
			this.CancelToken     = token;
		}

		protected SessionExecutor   SessionExecutor { get; }
		protected CancellationToken CancelToken     { get; }

		public abstract List<GroupInfo> QueryGroups(
			ServerInstance       serverInstance,
			QueryInfo            query,
			List<ParameterValue> parameters
		);

		public abstract List<ParameterValue> GetGroupParameters(
			GroupInfo groupRec
		);
	}
}
