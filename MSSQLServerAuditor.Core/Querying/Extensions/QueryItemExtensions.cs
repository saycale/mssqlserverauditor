﻿using System.Collections.Generic;
using System.Linq;
using MSSQLServerAuditor.Core.Domain;

namespace MSSQLServerAuditor.Core.Querying.Extensions
{
	public static class QueryItemExtensions
	{
		public static QueryItemInfo GetQueryItemForVersion(
			this IEnumerable<QueryItemInfo> list,
			ConnectionType                  connectionType,
			InstanceVersion                 version)
		{
			IEnumerable<QueryItemInfo> queryItems = 
				from itemInfo in list
				where itemInfo.IsApplicableVersion(connectionType, version)
				select itemInfo;

			return queryItems.FirstOrDefault();
		}

		public static bool IsApplicableVersion(
			this QueryItemInfo queryItem,
			ConnectionType     connectionType,
			InstanceVersion    version)
		{
			if (connectionType.IsInternal())
			{
				return true;
			}

			InstanceVersion minVersion = InstanceVersion.GetMinVersion(queryItem.MinVersion);
			InstanceVersion maxVersion = InstanceVersion.GetMaxVersion(queryItem.MaxVersion);

			return
				version.CompareTo(minVersion) >= 0 && 
				version.CompareTo(maxVersion) <= 0;
		}
	}
}
