﻿using System;
using System.Data;
using MSSQLServerAuditor.Core.Querying.Commands;

namespace MSSQLServerAuditor.Core.Querying.Connections
{
	public interface IQueryConnection : IDisposable
	{
		void Open();

		void ChangeDatabase(string database);

		void Close();

		ConnectionState? State { get; }

		IQueryCommand GetCommand(CommandData commandData);
	}
}
