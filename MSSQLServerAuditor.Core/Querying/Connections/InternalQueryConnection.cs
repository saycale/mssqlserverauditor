﻿using System.Data;
using System.Data.Common;
using MSSQLServerAuditor.Core.Querying.Commands;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.DataAccess.Connections;
using MSSQLServerAuditor.DataAccess.Entity.Models;
using MSSQLServerAuditor.DataAccess.Extensions;
using MSSQLServerAuditor.DataAccess.Structure;
using NLog;

namespace MSSQLServerAuditor.Core.Querying.Connections
{
	public class InternalConnectionParameters
	{
		public static InternalConnectionParameters FromSession(AuditSession session)
		{
			return new InternalConnectionParameters(
				session.Group.Id,
				session.Server.Id,
				session.Template.Id,
				session.Server.Login.Id
			);
		}

		public InternalConnectionParameters(
			long? groupId,
			long? serverIntanceId,
			long? templateId,
			long? loginId
		)
		{
			this.GroupId          = groupId;
			this.ServerInstanceId = serverIntanceId;
			this.TemplateId       = templateId;
			this.LoginId          = loginId;
		}

		public long? GroupId          { set; get;}
		public long? ServerInstanceId { set; get;}
		public long? TemplateId       { set; get;}
		public long? LoginId          { set; get;}
	}

	internal class InternalQueryConnection : JointQueryConnection
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly InternalConnectionParameters _connectionParameters;

		public InternalQueryConnection(
			IStorageConnectionFactory    connectionFactory,
			InternalConnectionParameters connectionParameters
		) : base(connectionFactory)
		{
			this._connectionParameters = connectionParameters;
		}

		public override IQueryCommand GetCommand(CommandData commandData)
		{
			DbCommand dbCommand = base.Connection.CreateCommand();

			dbCommand.CommandText    = commandData.Text;
			dbCommand.CommandTimeout = commandData.Timeout;

			return new DbInternalQueryCommand(
				dbCommand,
				this._connectionParameters
			);
		}

		private class DbInternalQueryCommand : DbQueryCommand
		{
			private readonly InternalConnectionParameters _parameters;

			public DbInternalQueryCommand(
				DbCommand                    command,
				InternalConnectionParameters parameters
			) : base(
				command
			)
			{
				this._parameters = parameters;
			}

			protected override void AssignDefaultParameters()
			{
				DbCommand.AddParameter(
					this._parameters.GroupId,
					new FieldInfo(DBConnectionGroup.FieldId, DbType.Int64, FieldInfoFlag.Identity)
				);

				DbCommand.AddParameter(
					this._parameters.ServerInstanceId,
					new FieldInfo(DBServerInstance.FieldId, DbType.Int64, FieldInfoFlag.Identity)
				);

				DbCommand.AddParameter(
					this._parameters.LoginId,
					new FieldInfo(DBLogin.FieldId, DbType.Int64, FieldInfoFlag.Identity)
				);

				DbCommand.AddParameter(
					this._parameters.TemplateId,
					new FieldInfo(DBTemplate.FieldId, DbType.Int64, FieldInfoFlag.Identity)
				);
			}
		}
	}
}
