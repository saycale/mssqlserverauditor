﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Network;
using MSSQLServerAuditor.Core.Querying.Commands;

namespace MSSQLServerAuditor.Core.Querying.Connections
{
	public class NetworkQueryConnection : IQueryConnection
	{
		private readonly NetworkConnectionInfo _networkConnectionInfo;

		public NetworkQueryConnection(NetworkConnectionInfo connectionInfo)
		{
			this._networkConnectionInfo = connectionInfo;
		}

		public void Dispose()
		{
		}

		public void Open()
		{
		}

		public void ChangeDatabase(string database)
		{
		}

		public void Close()
		{
		}

		public ConnectionState? State => ConnectionState.Open;

		public IQueryCommand GetCommand(CommandData commandData)
		{
			return new NetworkInformationCommand(this._networkConnectionInfo);
		}

		private class NetworkInformationCommand : IQueryCommand
		{
			private const string ColHost          = "Host";
			private const string ColAddress       = "Address";
			private const string ColStatus        = "NetworkStatus";
			private const string ColRoundtripTime = "RoundtripTime";
			private const string ColEventTime     = "EventTime";

			private readonly NetworkConnectionInfo _connectionInfo;
			private readonly IPinger _pinger;

			public NetworkInformationCommand(
				NetworkConnectionInfo connectionInfo
			)
			{
				this._connectionInfo = connectionInfo;
				this._pinger         = CreatePinger(connectionInfo);
			}

			public void Dispose()
			{
			}

			public void AssignParameters(
				List<QueryParameterInfo> parameters,
				List<ParameterValue>     parameterValues
			)
			{
			}

			public async Task<IDataReader> ExecuteReaderAsync(CancellationToken cancellationToken)
			{
				PingResult pingResult = await this._pinger.PingAsync(cancellationToken);

				DataTable dt = new DataTable();

				dt.Columns.Add(new DataColumn(ColHost,          typeof(string)));
				dt.Columns.Add(new DataColumn(ColAddress,       typeof(string)));
				dt.Columns.Add(new DataColumn(ColStatus,        typeof(string)));
				dt.Columns.Add(new DataColumn(ColRoundtripTime, typeof(string)));
				dt.Columns.Add(new DataColumn(ColEventTime,     typeof(DateTime)));

				DataRow row = dt.NewRow();

				row[ColHost]          = this._connectionInfo.Host;
				row[ColAddress]       = pingResult.IpAddress.ToSafeString();
				row[ColStatus]        = pingResult.Status.ToSafeString();
				row[ColRoundtripTime] = pingResult.ElapsedMillis.ToSafeString();
				row[ColEventTime]     = DateTime.Now;

				dt.Rows.Add(row);

				return dt.CreateDataReader();
			}

			private IPinger CreatePinger(NetworkConnectionInfo connectionInfo)
			{
				switch (connectionInfo.Protocol)
				{
					case ProtocolType.Tcp:
						return new TcpPinger(connectionInfo.Host, connectionInfo.Port, connectionInfo.Timeout);

					case ProtocolType.Udp:
						return new UdpPinger(connectionInfo.Host, connectionInfo.Port, connectionInfo.Timeout);

					default:
						return new HostPinger(connectionInfo.Host, connectionInfo.Timeout);
				}
			}
		}
	}
}
