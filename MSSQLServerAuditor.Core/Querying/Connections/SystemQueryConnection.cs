﻿using System.Data.Common;
using MSSQLServerAuditor.Core.Querying.Commands;
using MSSQLServerAuditor.DataAccess.Connections;

namespace MSSQLServerAuditor.Core.Querying.Connections
{
	public class SystemQueryConnection : JointQueryConnection
	{
		public SystemQueryConnection(IStorageConnectionFactory factory)
			: base(factory)
		{
		}

		public override IQueryCommand GetCommand(CommandData commandData)
		{
			DbCommand dbCommand = base.Connection.CreateCommand();

			dbCommand.CommandText    = commandData.Text;
			dbCommand.CommandTimeout = commandData.Timeout;

			return new SystemQueryCommand(dbCommand);
		}

		private class SystemQueryCommand : DbQueryCommand
		{
			public SystemQueryCommand(DbCommand dbCommand) : base(dbCommand)
			{
			}

			protected override void AssignDefaultParameters()
			{
				// no parameters to assign
			}
		}
	}
}
