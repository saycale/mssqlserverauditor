﻿using System.Collections.Generic;
using System.ComponentModel;
using MSSQLServerAuditor.Common.Contracts;

namespace MSSQLServerAuditor.Core.Querying.Connections
{
	public enum ProtocolType
	{
		[Description("ICMP (Ping)")]
		Icmp,

		[Description("TCP")]
		Tcp,

		[Description("UDP")]
		Udp
	}

	public class NetworkConnectionInfo
	{
		public NetworkConnectionInfo(
			string       host,
			int          port,
			ProtocolType protocol,
			int          timeout
		)
		{
			Check.NotNullOrWhiteSpace(host, nameof(host));
			Check.Assert(port >= 0 && port <= short.MaxValue, "Port value should be equal or greater than zero.");
			Check.Assert(timeout >= 0, "Timeout should be equal or greater than zero.");

			this.Host     = host;
			this.Port     = port;
			this.Protocol = protocol;
			this.Timeout  = timeout;
		}

		/// <summary>
		/// Remote host
		/// </summary>
		public string Host { get; }

		/// <summary>
		/// Remote server port
		/// </summary>
		public int Port { get; }

		/// <summary>
		/// Specifies network protocol to check availability of the remote server
		/// </summary>
		public ProtocolType Protocol { get; }

		/// <summary>
		/// Timeout (in milliseconds)
		/// </summary>
		public int Timeout { get; }

		public string ConnectionString
		{
			get
			{
				List<string> parts = new List<string>();

				parts.Add("host="     + Host);
				parts.Add("protocol=" + Protocol.ToString().ToLower());

				if (Protocol != ProtocolType.Icmp)
				{
					parts.Add("port=" + Port);
				}

				if (Timeout != 0)
				{
					parts.Add("timeout=" + Timeout);
				}

				return string.Join(",", parts);
			}
		}

		public override string ToString()
		{
			return ConnectionString;
		}
	}
}
