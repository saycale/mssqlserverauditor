﻿using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.DataAccess.Connections;

namespace MSSQLServerAuditor.Core.Querying.Connections
{
	public class QueryConnectionFactory : IQueryConnectionFactory
	{
		private readonly IStorageConnectionFactory _factory;

		public QueryConnectionFactory(IStorageConnectionFactory factory)
		{
			Check.NotNull(factory, nameof(factory));

			this._factory = factory;
		}

		public IQueryConnection OpenNewConnection(AuditSession session, ConnectionType connectionType)
		{
			if (connectionType == ConnectionType.Internal && session.Server.ConnectionType != connectionType)
			{
				InternalConnectionParameters parameters = InternalConnectionParameters.FromSession(session);
				return new InternalQueryConnection(this._factory, parameters);
			}

			ServerInstance instance = session.Server;
			return instance.OpenNewConnection(this._factory);
		}
	}
}