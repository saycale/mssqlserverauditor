﻿using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Common.Utils;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Querying.Commands;

namespace MSSQLServerAuditor.Core.Querying.Connections
{
	public class OdbcQueryConnection : DbQueryConnection<OdbcConnection>
	{
		public OdbcQueryConnection(OdbcConnection odbcConnection) : base(odbcConnection)
		{
		}

		public override IQueryCommand GetCommand(CommandData commandData)
		{
			OdbcCommand cmd = new OdbcCommand
			{
				Connection     = base.Connection,
				CommandText    = commandData.Text,
				CommandTimeout = commandData.Timeout
			};

			return new OdbcQueryCommand(cmd);
		}

		private class OdbcQueryCommand : IQueryCommand
		{
			private readonly OdbcCommand _command;

			public OdbcQueryCommand(OdbcCommand command)
			{
				this._command = command;
			}

			public void Dispose()
			{
				this._command?.Dispose();
			}

			public void AssignParameters(
				List<QueryParameterInfo> parameters,
				List<ParameterValue>     parameterValues
			)
			{
				SortedList<int, string> paramIndexes = new SortedList<int, string>();

				string commandText = this._command.CommandText;
				foreach (QueryParameterInfo parameter in parameters)
				{
					Regex           regex   = new Regex(parameter.Name);
					MatchCollection matches = regex.Matches(commandText);

					foreach (Match match in matches)
					{
						paramIndexes.Add(match.Index, match.Value);
					}

					commandText = commandText.Replace(parameter.Name, "?");
				}

				if (paramIndexes.Any())
				{
					foreach (string paramName in paramIndexes.Values)
					{
						QueryParameterInfo parameterInfo = parameters
							.FirstOrDefault(p => p.Name == paramName);

						if (parameterInfo != null)
						{
							ParameterValue pValue = parameterValues
								.FirstOrDefault(p => p.Name == parameterInfo.Name);

							AddParameter(parameterInfo, pValue);
						}
					}
				}
				else
				{
					foreach (QueryParameterInfo parameterInfo in parameters)
					{
						ParameterValue pValue = parameterValues
								.FirstOrDefault(p => p.Name == parameterInfo.Name);

						AddParameter(parameterInfo, pValue);
					}
				}
			}

			private void AddParameter(
				QueryParameterInfo parameterInfo,
				ParameterValue     pValue)
			{
				OdbcParameter odbcParam = this._command.CreateParameter();
				odbcParam.ParameterName = parameterInfo.Name;
				odbcParam.IsNullable    = true;
				odbcParam.DbType        = parameterInfo.Type.ToDbType();

				object val = parameterInfo.GetDefaultValue();
				if (pValue != null)
				{
					val = ConvertString.ToSqlDbType(
						pValue.UserValue ?? pValue.DefaultValue,
						pValue.IsNull,
						odbcParam.DbType.ToSqlDbType()
					);	
				}

				odbcParam.Value = val;

				this._command.Parameters.Add(odbcParam);
			}

			public async Task<IDataReader> ExecuteReaderAsync(CancellationToken cancellationToken)
			{
				return await this._command.ExecuteReaderAsync(cancellationToken);
			}

			public void Cancel()
			{
				this._command?.Cancel();
			}
		}
	}
}
