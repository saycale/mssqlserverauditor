﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Management;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Common.Utils;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Querying.Commands;

namespace MSSQLServerAuditor.Core.Querying.Connections
{
	public class WmiConnectionInfo
	{
		public string               ComputerName   { get; set; }
		public string               Authority      { get; set; }
		public string               Username       { get; set; }
		public string               Password       { get; set; }
		public ImpersonationLevel?  Impersonation  { get; set; }
		public AuthenticationLevel? Authentication { get; set; }

		public bool IsValid => !string.IsNullOrWhiteSpace(ComputerName);

		public string ConnectionString
		{
			get
			{
				Dictionary<string, string> parts = new Dictionary<string, string>
				{
					{"computername",  ComputerName},
					{"authority",     Authority},
					{"username",      Username},
					{"password",      Password},
					{"auth",          Authentication?.ToString()},
					{"impersonation", Impersonation?.ToString()}
				};

				string connectionString = string.Join(";", parts
					.Where(x => !string.IsNullOrWhiteSpace(x.Value))
					.Select(x => x.Key + "=" + x.Value)
				);

				return connectionString;
			}
		}

		public override string ToString()
		{
			return ConnectionString;
		}
	}

	public class WmiQueryConnection : IQueryConnection
	{
		private readonly WmiConnectionInfo _connectionInfo;

		public WmiQueryConnection(WmiConnectionInfo connectionInfo)
		{
			Check.NotNull(connectionInfo, nameof(connectionInfo));

			this._connectionInfo = connectionInfo;
		}

		public void Dispose()
		{
		}

		public void Open()
		{
		}

		public void ChangeDatabase(string database)
		{
		}

		public void Close()
		{
		}

		public ConnectionState? State => ConnectionState.Open;

		public IQueryCommand GetCommand(CommandData commandData)
		{
			return new WmiQueryCommand(
				this._connectionInfo,
				commandData.Text,
				commandData.Namespace,
				commandData.Aliases
			);
		}

		private class WmiQueryCommand : IQueryCommand
		{
			private const string NamespaceDefault = "root\\cimv2";

			private readonly WmiConnectionInfo          _connectionInfo;
			private readonly string                     _queryText;
			private readonly string                     _queryNamespace;
			private readonly Dictionary<string, string> _parameters;

			private readonly Dictionary<string, List<string>> _aliases;

			public WmiQueryCommand(
				WmiConnectionInfo                connectionInfo,
				string                           queryText,
				string                           queryNamespace,
				Dictionary<string, List<string>> aliases
			)
			{
				Check.NotNull(connectionInfo, nameof(connectionInfo));

				this._connectionInfo = connectionInfo;
				this._queryText      = queryText;
				this._queryNamespace = queryNamespace;
				this._aliases        = aliases ?? new Dictionary<string, List<string>>();
				this._parameters     = new Dictionary<string, string>();
			}

			public void Dispose()
			{
				
			}

			public void AssignParameters(
				List<QueryParameterInfo> parameters,
				List<ParameterValue>     parameterValues
			)
			{
				this._parameters.Clear();

				foreach (QueryParameterInfo paramInfo in parameters)
				{
					ParameterValue paramValue = parameterValues.FirstOrDefault(
						pv => string.Equals(pv.Name, paramInfo.Name, StringComparison.OrdinalIgnoreCase)
					);

					if (paramValue != null)
					{
						this._parameters[paramInfo.Name] = paramValue.UserValue ?? paramValue.DefaultValue;
					}
				}
			}

			public async Task<IDataReader> ExecuteReaderAsync(CancellationToken cancellationToken)
			{
				return await Task.Run(
					() => ExecuteReader(),
					cancellationToken
				).ConfigureAwait(false);
			}

			private IDataReader ExecuteReader()
			{
				ConnectionOptions options = new ConnectionOptions();

				if (this._connectionInfo.Impersonation.HasValue)
				{
					options.Impersonation = this._connectionInfo.Impersonation.Value;
				}

				if (this._connectionInfo.Authentication.HasValue)
				{
					options.Authentication = this._connectionInfo.Authentication.Value;
				}

				if (!string.IsNullOrWhiteSpace(this._connectionInfo.Username))
				{
					options.Username = this._connectionInfo.Username;
				}

				if (!string.IsNullOrWhiteSpace(this._connectionInfo.Password))
				{
					options.Password = this._connectionInfo.Password;
				}

				if (!string.IsNullOrWhiteSpace(this._connectionInfo.Authority))
				{
					options.Authority = this._connectionInfo.Authority;
				}

				string nmspace = StringUtils.FirstNonEmpty(
					this._queryNamespace,
					NamespaceDefault
				);

				string pathFormat = $@"\\{this._connectionInfo.ComputerName}\{nmspace}";
				string path       = FillParameters(pathFormat);

				ManagementScope scope = new ManagementScope(
					path,
					options
				);

				scope.Connect();

				string      queryText = FillParameters(this._queryText);
				ObjectQuery query     = new ObjectQuery(queryText);

				using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query))
				{
					using (ManagementObjectCollection managementObjects = searcher.Get())
					{
						List<ManagementObject> managementList = managementObjects.Cast<ManagementObject>().ToList();

						DataTable dt = new DataTable();

						if (this._aliases.Any())
						{
							foreach (KeyValuePair<string, List<string>> aliasList in this._aliases)
							{
								foreach (string alias in aliasList.Value)
								{
									AddColumnIfNotExists(dt, alias);
								}
							}
						}

						ManagementObject firstInstance = managementList.FirstOrDefault();

						if (firstInstance != null)
						{
							// Set datatable structure
							foreach (PropertyData property in firstInstance.Properties)
							{
								string propName = property.Name;

								List<string> aliases;
								if (!this._aliases.TryGetValue(propName, out aliases))
								{
									AddColumnIfNotExists(dt, propName);
								}
							}

							// Loop through all instances
							foreach (ManagementObject instance in managementList)
							{
								try
								{
									DataRow row = dt.NewRow();

									foreach (PropertyData property in instance.Properties)
									{
										string propName = property.Name;

										List<string> aliases;
										if (this._aliases.TryGetValue(propName, out aliases))
										{
											foreach (string alias in aliases)
											{
												row[alias] = property.Value?.ToString();
											}
										}
										else
										{
											AddColumnIfNotExists(dt, propName);

											row[propName] = property.Value?.ToString();
										}
									}

									dt.Rows.Add(row);
								}
								finally
								{
									instance.Dispose();
								}
							}
						}

						return dt.CreateDataReader();
					}
				}
			}

			private static void AddColumnIfNotExists(DataTable dt, string column)
			{
				if (!dt.Columns.Contains(column))
				{
					dt.Columns.Add(column);
				}
			}

			private string FillParameters(string expression)
			{
				return this._parameters.Aggregate(
					expression,
					(current, parameter) => current.Replace($"${{{parameter.Key}}}", parameter.Value)
				);
			}
		}
	}
}
