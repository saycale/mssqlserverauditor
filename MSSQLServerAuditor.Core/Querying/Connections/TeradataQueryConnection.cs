﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Common.Utils;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Querying.Commands;
using Teradata.Client.Provider;

namespace MSSQLServerAuditor.Core.Querying.Connections
{
	public class TeradataSqlQueryConnection : DbQueryConnection<TdConnection>
	{
		public TeradataSqlQueryConnection(TdConnection tdConnection) : base(tdConnection)
		{
		}

		public override IQueryCommand GetCommand(CommandData commandData)
		{
			TdCommand cmd = new TdCommand
			{
				Connection     = base.Connection,
				CommandText    = commandData.Text,
				CommandTimeout = commandData.Timeout
			};

			return new TeradataCommand(cmd);
		}

		private class TeradataCommand : IQueryCommand
		{
			private readonly TdCommand _command;

			public TeradataCommand(TdCommand command)
			{
				this._command = command;
			}

			public void Dispose()
			{
				this._command?.Dispose();
			}

			public void AssignParameters(
				List<QueryParameterInfo> parameters,
				List<ParameterValue>     parameterValues
			)
			{
				if (parameters != null)
				{
					foreach (QueryParameterInfo parameter in parameters)
					{
						TdParameter tdParam   = this._command.CreateParameter();
						tdParam.ParameterName = parameter.Name;
						tdParam.IsNullable    = true;
						tdParam.DbType        = parameter.Type.ToDbType();
						tdParam.Value         = parameter.GetDefaultValue();

						this._command.Parameters.Add(tdParam);
					}
				}

				if (parameterValues != null)
				{
					foreach (ParameterValue param in parameterValues)
					{
						TdParameter tdParam = (
							from TdParameter p
							in this._command.Parameters
							where p.ParameterName == param.Name
							select p
						).FirstOrDefault();

						if (tdParam != null)
						{
							tdParam.Value = ConvertString.ToSqlDbType(
								param.UserValue ?? param.DefaultValue,
								param.IsNull,
								tdParam.DbType.ToSqlDbType()
							);
						}
					}
				}
			}

			public async Task<IDataReader> ExecuteReaderAsync(CancellationToken cancellationToken)
			{
				return await _command.ExecuteReaderAsync(cancellationToken);
			}

			public void Cancel()
			{
				this._command?.Cancel();
			}
		}
	}
}
