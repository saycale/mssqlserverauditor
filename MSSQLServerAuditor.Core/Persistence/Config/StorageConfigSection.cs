﻿using System.Configuration;

namespace MSSQLServerAuditor.Core.Persistence.Config
{
	// ReSharper disable once ClassNeverInstantiated.Global
	public class StorageConfigSection : ConfigurationSection
	{
		[ConfigurationProperty("connection")]
		public ConnectionElement Connection
		{
			get { return (ConnectionElement) this["connection"]; }
			set { this["connection"] = value; }
		}
	}
}
