﻿using System;
using System.Collections.Concurrent;
using MSSQLServerAuditor.Core.Domain.Mapping;
using MSSQLServerAuditor.Core.Persistence.Services;
using MSSQLServerAuditor.DataAccess.Entity;
using MSSQLServerAuditor.DataAccess.Entity.Context;
using MSSQLServerAuditor.DataAccess.Scope.Implementations;

namespace MSSQLServerAuditor.Core.Persistence.Storages
{
	public class MainStorage
	{
		public MainStorage(
			StorageContextFactory storageContextFactory,
			Mapper                mapper,
			string                dbName)
		{
			StorageContextFactory = storageContextFactory;
			DbName                = dbName;

			AmbientDbContextLocator contextLocator = new AmbientDbContextLocator();

			DbContextScopeFactory<StorageContext> contextScopeFactory =
				new DbContextScopeFactory<StorageContext>(storageContextFactory);

			ConcurrentDictionary<Type, EntityCache> cache =
				new ConcurrentDictionary<Type, EntityCache>();

			RepositoryCatalog repositories = new RepositoryCatalog(contextLocator);

			Connections = new ConnectionService(contextScopeFactory, cache, repositories, mapper);
			Templates   = new TemplateService(contextScopeFactory, cache, repositories, mapper);
			Settings    = new SettingsService(contextScopeFactory, cache, repositories, mapper);
			Nodes       = new NodeService(contextScopeFactory, cache, repositories, mapper, Templates, Settings);
			Queries     = new QueryService(contextScopeFactory, cache, repositories, mapper);
			MetaResults = new MetaResultService(contextScopeFactory, cache, repositories, mapper, Nodes);
		}
		
		public StorageContextFactory StorageContextFactory { get; private set; }

		public ConnectionService Connections { get; private set; }
		public NodeService       Nodes       { get; private set; }
		public SettingsService   Settings    { get; private set; }
		public TemplateService   Templates   { get; private set; }
		public QueryService      Queries     { get; private set; }
		public MetaResultService MetaResults { get; private set; }

		public string DbName { get; private set; }

		public void Initialize()
		{
			using (StorageContext context = StorageContextFactory.CreateDbContext())
			{
				context.Database.Initialize(false);
			}
		}
	}
}
