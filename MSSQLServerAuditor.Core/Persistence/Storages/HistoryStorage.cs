﻿using MSSQLServerAuditor.DataAccess;
using MSSQLServerAuditor.DataAccess.Commands.Interfaces;
using Perceiveit.Data;

namespace MSSQLServerAuditor.Core.Persistence.Storages
{
	public class HistoryStorage : Storage
	{
		public HistoryStorage(
			DBDatabase       dbDatabase,
			ICommandExecutor commandExecutor,
			string           dbName,
			string           alias
		) : base(
			dbDatabase,
			commandExecutor,
			dbName
		)
		{
			this.Alias = alias;
		}

		public string Alias { get; private set; }
	}
}
