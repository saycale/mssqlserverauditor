﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.DataAccess;
using MSSQLServerAuditor.DataAccess.Commands;
using MSSQLServerAuditor.DataAccess.Commands.Interfaces;
using MSSQLServerAuditor.DataAccess.Extensions;
using MSSQLServerAuditor.DataAccess.Structure;
using MSSQLServerAuditor.DataAccess.Tables;
using Perceiveit.Data;
using Perceiveit.Data.Query;

namespace MSSQLServerAuditor.Core.Persistence.Storages
{
	public class CommonReportStorage : Storage, IReportStorage
	{
		protected static readonly FieldInfo FieldId        = new FieldInfo("id",            DbType.Int64,  FieldInfoFlag.Identity);
		protected static readonly FieldInfo FieldRowOrder  = new FieldInfo("s__row_order",  DbType.Int64,  FieldInfoFlag.None);
		protected static readonly FieldInfo FieldRowHash   = new FieldInfo("s__row_hash",   DbType.String, FieldInfoFlag.IsNotNull, 200);
		protected static readonly FieldInfo FieldSessionId = new FieldInfo("s__session_id", DbType.Int64,  FieldInfoFlag.IsNotNull);
		protected static readonly FieldInfo FieldQueryId   = new FieldInfo("s__d_Query_id", DbType.Int64,  FieldInfoFlag.IsNotNull);

		protected readonly Dictionary<NormalizeInfo, ReportTable> _tables;
		protected readonly object                                 _tablesLock;
		protected readonly ThreadLocal<HashAlgorithm>             _hashLocal;

		private readonly ConcurrentDictionary<string, object> _memoryLocks =
			new ConcurrentDictionary<string, object>();

		private readonly ConcurrentDictionary<string, NormalizeInfo> _dbStructures;

		public CommonReportStorage(DBDatabase dbDatabase, ICommandExecutor commandExecutor, string alias)
			: base(dbDatabase, commandExecutor, alias)
		{
			this._tables     = new Dictionary<NormalizeInfo, ReportTable>();
			this._tablesLock = new object();
			this._hashLocal  = new ThreadLocal<HashAlgorithm>(SHA1.Create);

			this._dbStructures  = new ConcurrentDictionary<string, NormalizeInfo>();
		}

		public long? SaveResults(
			long          queryId,
			NormalizeInfo dbStructure,
			DataTable     queryData,
			bool          recreateSchema
		)
		{
			const long sessionId    = 1L;
			long?      affectedRows = null;

			lock (GetMemoryTableSync(dbStructure))
			{
				ClearData(queryId, dbStructure, sessionId);

				if (queryData != null && queryData.Columns.Count > 0)
				{
					affectedRows = SaveQueryData(queryId, dbStructure, queryData, sessionId, recreateSchema);
				}
			}

			return affectedRows;
		}

		public void ClearData(long queryId, NormalizeInfo dbStructure, long sessionId)
		{
			bool tableExists = DbDatabase.Execute(context =>
			{
				CheckTableExistsCommand existsCommand = new CheckTableExistsCommand(
					context,
					dbStructure.TableName
				);

				return existsCommand.Execute(CommandExecutor);
			});

			if (tableExists)
			{
				DBDeleteQuery deleteQuery = DBQuery.DeleteFrom(dbStructure.TableName);

				List<DBComparison> whereClauses = new List<DBComparison>
				{
					GetClausePair(FieldSessionId.Name),
					GetClausePair(FieldQueryId.Name)
				};

				List<DbCommandParameter> parameters = new List<DbCommandParameter>
				{
					new DbCommandParameter(queryId,   FieldQueryId),
					new DbCommandParameter(sessionId, FieldSessionId)
				};

				deleteQuery.WhereAll(whereClauses.ToArray());

				DbDatabase.Execute(context =>
					new DeleteFromTableCommand(context, deleteQuery, parameters)
						.Execute(CommandExecutor));
			}
		}

		public DataTable ReadResult(NormalizeInfo dbStructure, long queryId, bool readServiceColumns = false)
		{
			if (dbStructure == null)
			{
				return null;
			}

			bool tableExists = DbDatabase.Execute(context =>
			{
				CheckTableExistsCommand existsCommand = new CheckTableExistsCommand(
					context,
					dbStructure.TableName
				);

				return existsCommand.Execute(CommandExecutor);
			});

			if (!tableExists)
			{
				return null;
			}

			const long sessionId = 1L;

			DBSelectQuery selectQuery;

			if (dbStructure.Fields.Count > 0 && dbStructure.Fields[0].Name != "*")
			{
				string[] fields = dbStructure.Fields
					.Select(fi => fi.Name)
					.ToArray();

				selectQuery = DBQuery.SelectFields(fields)
					.From(dbStructure.TableName);
			}
			else
			{
				selectQuery = DBQuery.SelectAll().From(dbStructure.TableName);
			}

			List<DBComparison> whereClauses = new List<DBComparison>
			{
				GetClausePair(FieldSessionId.Name),
				GetClausePair(FieldQueryId.Name)
			};

			List<DbCommandParameter> parameters = new List<DbCommandParameter>
			{
				new DbCommandParameter(queryId,   FieldQueryId),
				new DbCommandParameter(sessionId, FieldSessionId)
			};

			selectQuery.WhereAll(whereClauses.ToArray())
				.OrderBy(FieldRowOrder.Name, Order.Ascending);

			DataTable tableResult = DbDatabase.Execute(context => new ReadTableCommand(context, selectQuery, parameters)
				.Execute(this.CommandExecutor));

			if (!readServiceColumns)
			{
				RemoveServiceColumns(tableResult);
			}

			return tableResult;
		}

		public static void ModifyTableRow(ITableRow row, long queryId, long sessionId, long rowOrder, string rowhash)
		{
			row.Values.Add(FieldQueryId.Name,   queryId);
			row.Values.Add(FieldSessionId.Name, sessionId);
			row.Values.Add(FieldRowOrder.Name,  rowOrder);

			row.Values[FieldRowHash.Name] = rowhash;
		}


		public static void ModifyTableInfo(TableInfo tableInfo)
		{
			tableInfo
				.AddField(FieldQueryId)
				.AddField(FieldSessionId)
				.AddField(FieldRowHash)
				.AddField(FieldRowOrder)
				.AddDateCreatedField()
				.AddDateUpdatedField()
				.SetPrimaryKey(
					"primary_key",
					FieldQueryId.Name,
					FieldSessionId.Name,
					FieldRowHash.Name
				);
		}

		public string GetFieldsString(NormalizeInfo tableStructure, string alias = "")
		{
			List<string> fieldList = new List<string>();

			lock (this._tablesLock)
			{
				List<FieldInfo> fields = this._tables[tableStructure].TableInfo.Fields;
				if (fields.Any())
				{
					if (!string.IsNullOrEmpty(alias))
					{
						alias = alias + ".";
					}

					foreach (FieldInfo fieldInfo in fields)
					{
						//
						// "DateCreated" column value is populated with a "default" value
						//
						if (fieldInfo.Name.EqualsIgnoreCase(TableInfo.FieldDateCreated))
						{
							continue;
						}

						//
						// "DateUpdated" column value is populated by after update trigger
						//
						if (fieldInfo.Name.EqualsIgnoreCase(TableInfo.FieldDateUpdated))
						{
							continue;
						}

						fieldList.Add(alias + fieldInfo.Name.AsDbName());
					}

					return fieldList.Join(",");
				}
			}

			return string.Empty;
		}

		private object GetMemoryTableSync(NormalizeInfo dbStructure)
		{
			string key = dbStructure.TableName;
			return this._memoryLocks.GetOrAdd(key, new object());
		}

		protected virtual long? SaveQueryData(
			long          queryId,
			NormalizeInfo dbStructure,
			DataTable     queryData,
			long          sessionId,
			bool          recreateSchema)
		{
			PrepareTables(dbStructure, recreateSchema);

			ReportTable table;

			lock (this._tablesLock)
			{
				if (!this._tables.TryGetValue(dbStructure, out table))
				{
					return null;
				}
			}

			int                     rowOrder   = 0;
			List<ITableRow>         rows       = new List<ITableRow>();
			Dictionary<string, int> insertDict = new Dictionary<string, int>();

			lock (this._tablesLock)
			{
				foreach (DataRow row in queryData.Rows)
				{
					string hash = GetRowHash(row);

					if (!insertDict.ContainsKey(hash))
					{
						insertDict.Add(hash, rowOrder);

						ITableRow tableRow;

						ProcessRowForWrite(row, dbStructure, out tableRow, false);

						ModifyTableRow(tableRow, queryId, sessionId, rowOrder, hash);

						rows.Add(tableRow);

						rowOrder++;
					}
				}

				return DbDatabase.ExecuteTransactionally(context =>
				{
					InsertRowCommand insertCommand = new InsertRowCommand(context, table.TableInfo);

					foreach (ITableRow tableRow in rows)
					{
						insertCommand.AddRowToInsert(tableRow);
					}

					return insertCommand.Execute(CommandExecutor);
				});
			}
		}

		protected long? ProcessRowForWrite(
			DataRow       row,
			NormalizeInfo dbStructure,
			out ITableRow tableRow,
			bool          writeToDb
		)
		{
			ReportTable table;

			lock (this._tablesLock)
			{
				table = this._tables[dbStructure];
			}

			tableRow = new TableRow(table.TableInfo);

			foreach (DataColumn column in row.Table.Columns)
			{
				string columnName = column.ColumnName.RemoveXmlSpecialChars();

				if (dbStructure.Fields.Any(field => field.Name == columnName))
				{
					tableRow.Values.Add(columnName, row[column.ColumnName]);
				}
			}

			foreach (NormalizeInfo childDirectory in dbStructure.ChildDirectories)
			{
				ITableRow tmpTableRow;

				long? childId = this.ProcessRowForWrite(
					row, childDirectory, out tmpTableRow, true
				);

				tableRow.Values.Add(
					childDirectory.TableName.AsFk(),
					childId
				);
			}

			return writeToDb ? InsertOrUpdateRow(table, tableRow) : -1L;
		}

		protected virtual long? InsertOrUpdateRow(
			ReportTable reportTable,
			ITableRow   tableRow
		)
		{
			return reportTable.InsertOrUpdateRow(tableRow);
		}

		private string GetRowHash(DataRow row)
		{
			StringBuilder result = new StringBuilder();

			if (row != null)
			{
				foreach (var colValue in row.ItemArray)
				{
					result.Append(colValue).Append("_");
				}
			}

			return GetHashString(result.ToString());
		}

		protected string GetHashString(string inputString)
		{
			StringBuilder sb = new StringBuilder();

			foreach (byte b in GetHash(inputString))
			{
				sb.Append(b.ToString("X2"));
			}

			return sb.ToString();
		}

		private byte[] GetHash(string inputString)
		{
			HashAlgorithm algorithm = this._hashLocal.Value;

			return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
		}

		protected virtual void PrepareTables(
			NormalizeInfo normalizeInfo,
			bool          recreateSchema,
			bool          mainTable = true
		)
		{
			if (normalizeInfo != null)
			{
				TableInfo tableInfo = GetTableInfo(normalizeInfo);

				lock (this._tablesLock)
				{
					if (!this._tables.ContainsKey(normalizeInfo) || recreateSchema)
					{
						if (mainTable)
						{
							ModifyTableInfo(tableInfo);
						}

						if (tableInfo.Fields.Any())
						{
							ReportTable table = new ReportTable(this, tableInfo);

							if (recreateSchema)
							{
								table.DropAndCreate();
							}
							else
							{
								table.CreateIfNotExists();
							}

							this._tables[normalizeInfo] = table;
						}

						foreach (NormalizeInfo childDirectory in normalizeInfo.ChildDirectories)
						{
							PrepareTables(childDirectory, recreateSchema, false);
						}
					}
				}
			}
		}

		protected TableInfo GetTableInfo(NormalizeInfo normalizeInfo)
		{
			TableInfo tableInfo = new TableInfo(normalizeInfo.TableName);

			foreach (NormalizeFieldInfo field in normalizeInfo.Fields)
			{
				FieldInfoFlag flags = FieldInfoFlag.None;

				if (field.IsUnique)
				{
					flags |= FieldInfoFlag.Unique;
				}

				if (field.IsNotNull)
				{
					flags |= FieldInfoFlag.IsNotNull;
				}

				tableInfo.AddField(new FieldInfo(field.Name, field.Type.ToDbType(), flags));
			}

			foreach (NormalizeInfo childDirectory in normalizeInfo.ChildDirectories)
			{
				tableInfo.AddField(new FieldInfo(
					childDirectory.TableName.AsFk(),
					DbType.Int64,
					FieldInfoFlag.Unique | FieldInfoFlag.IsNotNull
				));
			}

			if (!string.IsNullOrEmpty(normalizeInfo.IndexFields))
			{
				string[] tableIndexFieldsList = normalizeInfo.IndexFields.Split(',')
					.Select(x => x.Trim())
					.Where(x => !string.IsNullOrWhiteSpace(x))
					.ToArray();

				tableInfo.Indexes.Add(
					new IndexInfo(
						$"idx_{normalizeInfo.TableName}_config_fields",
						false,
						tableIndexFieldsList
					)
				);
			}

			return tableInfo;
		}

		protected virtual void RemoveServiceColumns(DataTable dataTable)
		{
			List<string> columnsToRemove = new List<string>
			{
				FieldQueryId.Name,
				FieldSessionId.Name,
				TableInfo.FieldDateCreated,
				TableInfo.FieldDateUpdated,
				FieldRowOrder.Name,
				FieldRowHash.Name
			};

			RemovePrimaryKey(dataTable);

			foreach (string columnName in columnsToRemove)
			{
				if (dataTable.Columns.Contains(columnName))
				{
					dataTable.Columns.Remove(columnName);
				}
			}
		}

		protected static void RemovePrimaryKey(DataTable dataTable)
		{
			dataTable.PrimaryKey = null;

			foreach (DataColumn column in dataTable.Columns)
			{
				column.Unique = false;
			}
		}

		private DBComparison GetClausePair(string fieldName)
		{
			return DBComparison.Compare(
				DBField.Field(fieldName),
				Compare.Equals,
				DBParam.Param(fieldName)
			);
		}

		public NormalizeInfo GetDbStucture(
			string    tableName,
			long      recordSet,
			bool      recreateSchema,
			DataTable dataTable = null
		)
		{
			NormalizeInfo structure;
			if (!recreateSchema && this._dbStructures.TryGetValue(tableName, out structure))
			{
				return structure;
			}

			if (dataTable == null)
			{
				NormalizeInfo      result = new NormalizeInfo();
				NormalizeFieldInfo field  = new NormalizeFieldInfo();

				result.TableName = tableName;
				result.Fields    = new List<NormalizeFieldInfo>();
				field.Name       = "*";
				field.Type       = SqlDbType.NVarChar;

				result.Fields.Add(field);

				return result;
			}

			structure = new NormalizeInfo
			{
				TableName        = tableName,
				Fields           = new List<NormalizeFieldInfo>(),
				ChildDirectories = new List<NormalizeInfo>(),
				RecordSet        = recordSet
			};

			foreach (DataColumn column in dataTable.Columns)
			{
				NormalizeFieldInfo field = new NormalizeFieldInfo
				{
					Name     = column.ColumnName,
					Type     = column.DataType.ToSqlDbType(),
					IsUnique = true
				};

				structure.Fields.Add(field);
			}

			this._dbStructures[tableName] = structure;

			return structure;
		}

		public string GetTableName(
			long                  templateNodeId,
			string                templateUid,
			TemplateNodeQueryInfo tnQueryInfo,
			long                  recordSet
		)
		{
			string tableName = string.Format(
				"t_{0}_{1}_{2}_q{3}_r{4}",
				templateUid ?? string.Empty,
				templateNodeId,
				tnQueryInfo.QueryName,
				tnQueryInfo.Id ?? "0",
				recordSet
			);

			return tableName;
		}
	}
}
