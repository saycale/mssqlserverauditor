﻿using System.Collections.Generic;
using System.IO;
using MSSQLServerAuditor.Common;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Cryptography;
using MSSQLServerAuditor.Core.Domain.Mapping;
using MSSQLServerAuditor.Core.Persistence.Providers;
using MSSQLServerAuditor.Core.Persistence.Storages.Sqlite.Storages;
using MSSQLServerAuditor.Core.Xml;
using MSSQLServerAuditor.DataAccess.Commands.Interfaces;
using MSSQLServerAuditor.DataAccess.Connections;
using MSSQLServerAuditor.DataAccess.Entity.Context;
using MSSQLServerAuditor.DataAccess.Vendors.Sqlite;
using MSSQLServerAuditor.DataAccess.Vendors.Sqlite.Connections;
using NLog;
using Perceiveit.Data;

namespace MSSQLServerAuditor.Core.Persistence.Storages.Sqlite
{
	public class SqliteStorageProvider : StorageProviderBase
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		public SqliteStorageProvider(StorageConnectionInfo connectionInfo, XmlLoader xmlLoader)
			: base(connectionInfo, new SqliteProviderFactory(), xmlLoader)
		{
		}

		public override MainStorage CreateCurrentStorage()
		{
			string pwd = $"@{CurrentAssembly.ProductName}@"; // use project name for password base
			ICryptoService cryptoService = new CachingCryptoService(pwd);

			Mapper mapper = new Mapper(cryptoService);

			string connectionString = GetDbConnectionString(this.ConnectionInfo.CurrentDatabaseInfo);

			List<string> currentStorageScripts = XmlLoader.GetScripts(
				ConnectionInfo.CurrentDatabaseInfo.ScriptPath,
				ConnectionInfo.StorageType
			);

			StorageContextFactory sqliteContextFactory = new StorageContextFactory(
				this.ConnectionFactory,
				new SqliteStorageInitializerFactory(currentStorageScripts),
				connectionString
			);

			MainStorage controller = new MainStorage(
				sqliteContextFactory,
				mapper,
				"current"
			);

			return controller;
		}

		public override IReportStorage CreateReportStorage()
		{
			string connectionString = GetDbConnectionString(ConnectionInfo.ReportDatabaseInfo);

			return SqliteReportStorage.CreateFileStorage(connectionString, "report");
		}

		public override List<HistoryStorage> CreateExtraStorages()
		{
			List<HistoryStorage> historyStorages = new List<HistoryStorage>();
			List<DatabaseInfo>   dbInfoList      = ConnectionInfo.Databases;

			if (dbInfoList.IsNullOrEmpty())
			{
				Log.Error("No history storages to be created.");

				return historyStorages;
			}

			foreach (DatabaseInfo dbInfo in dbInfoList)
			{
				if (string.IsNullOrEmpty(dbInfo.Name) || string.IsNullOrEmpty(dbInfo.Alias))
				{
					Log.Error("History storage filename or alias is not defined.");

					continue;
				}

				string connectionString = GetDbConnectionString(dbInfo);
				DBDatabase historyBase = this.ProviderFactory.CreateDatabase(connectionString);
				ICommandExecutor cmdExecutor = this.ProviderFactory.CreateCommandExecutor();

				HistoryStorage histStorage = new HistoryStorage(
					historyBase,
					cmdExecutor,
					dbInfo.Alias,
					dbInfo.Alias
				);

				if (this.ConnectionInfo.UpdateHistory)
				{
					RunScripts(histStorage, dbInfo.ScriptPath);
				}

				historyStorages.Add(histStorage);
			}

			return historyStorages;
		}

		public override FsStorage CreateFsStorage()
		{
			string           connectionString = GetDbConnectionString(ConnectionInfo.FsDatabaseInfo);
			DBDatabase       fsBase           = this.ProviderFactory.CreateDatabase(connectionString);
			ICommandExecutor cmdExecutor      = this.ProviderFactory.CreateCommandExecutor();

			return new FsStorage(fsBase, cmdExecutor, "dbfs");
		}

		public override IStorageConnectionFactory CreateStorageConnectionFactory()
		{
			Dictionary<string, string> databasesToAttach = new Dictionary<string, string>
			{
				{ GetDbFilePath(ConnectionInfo.ReportDatabaseInfo), "report" }
			};

			if (ConnectionInfo.Databases != null)
			{
				foreach (DatabaseInfo dbInfo in ConnectionInfo.Databases)
				{
					string dbFilePath = GetDbFilePath(dbInfo);

					databasesToAttach.Add(dbFilePath, dbInfo.Alias);
				}
			}

			return new SqliteStorageConnectionFactory(
				GetDbConnectionString(ConnectionInfo.CurrentDatabaseInfo),
				databasesToAttach
			);
		}

		private string GetDbConnectionString(DatabaseInfo database)
		{
			return string.Format(
				this.ConnectionInfo.ConnectionStringPattern,
				GetDbFilePath(database)
			);
		}

		private string GetDbFilePath(DatabaseInfo database)
		{
			string folder = this.ConnectionInfo.WorkDirectory;

			string dbName = database.Name.Replace("${version}$", $"{CurrentAssembly.Version}");

			if (Path.HasExtension(dbName))
			{
				return Path.Combine(folder, dbName);
			}

			return Path.Combine(folder, $"{dbName}.sqlite");
		}
	}
}
