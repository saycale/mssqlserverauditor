﻿using System.Collections.Generic;

namespace MSSQLServerAuditor.Core.Persistence.Storages
{
	public class StorageConnectionInfo
	{
		public StorageConnectionInfo()
		{
			Databases = new List<DatabaseInfo>();
		}

		public StorageType StorageType             { get; set; }
		public string      ConnectionStringPattern { get; set; }

		public bool   UpdateHistory { get; set; }
		public string WorkDirectory { get; set; }

		public DatabaseInfo CurrentDatabaseInfo { get; set; }
		public DatabaseInfo ReportDatabaseInfo  { get; set; }
		public DatabaseInfo FsDatabaseInfo      { get; set; }

		public List<DatabaseInfo> Databases { get; }
	}
}
