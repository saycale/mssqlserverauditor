﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Persistence.Config;
using MSSQLServerAuditor.Core.Settings;

namespace MSSQLServerAuditor.Core.Persistence.Storages
{
	public static class StorageInfoProvider
	{
		public static StorageConnectionInfo GetConnectionInfo(IAppSettings appSettings)
		{
			StorageConfigSection storageConfig = (StorageConfigSection)
				System.Configuration.ConfigurationManager.GetSection("storage");

			string workDirectory = Path.Combine(
				Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
				"MSSQLServerAuditor"
			);

			StorageConnectionInfo connectionInfo = new StorageConnectionInfo
			{
				StorageType             = storageConfig.Connection.Type,
				ConnectionStringPattern = storageConfig.Connection.ConnectionString,
				UpdateHistory           = true,
				WorkDirectory           = workDirectory
			};

			// read database list from app config
			DatabaseCollection databaseCollection = storageConfig.Connection.Databases;
			foreach (DatabaseElement databaseElement in databaseCollection)
			{
				DatabaseInfo database = new DatabaseInfo(databaseElement.Alias, databaseElement.Name);

				switch (database.Alias.ToLower())
				{
					case "current":
						connectionInfo.CurrentDatabaseInfo = database;
						break;
					case "report":
						connectionInfo.ReportDatabaseInfo = database;
						break;
					case "dbfs":
						connectionInfo.FsDatabaseInfo = database;
						break;
					default:
						connectionInfo.Databases.Add(database);
						break;
				}
			}

			// read post build scripts from settings
			if (appSettings?.System != null)
			{
				SystemSettings        systemSettings = appSettings.System;
				List<PostBuildScript> scripts        = systemSettings.PostBuildScripts;

				// find and set post build script for current database
				DatabaseInfo currentDbInfo = connectionInfo.CurrentDatabaseInfo;
				if (currentDbInfo != null)
				{
					currentDbInfo.ScriptPath = FormatPath(systemSettings.PostBuildCurrentDbScript, connectionInfo);
				}

				// find and set post build script for "report" database
				DatabaseInfo reportDbInfo = connectionInfo.ReportDatabaseInfo;
				if (reportDbInfo != null)
				{
					reportDbInfo.ScriptPath = FindPostBuildScript(scripts, reportDbInfo.Alias, connectionInfo);
				}

				// find and set post build script for "dbfs" database
				DatabaseInfo fsDbInfo = connectionInfo.FsDatabaseInfo;
				if (fsDbInfo != null)
				{
					fsDbInfo.ScriptPath = FindPostBuildScript(scripts, fsDbInfo.Alias, connectionInfo);
				}

				foreach (PostBuildScript script in systemSettings.PostBuildScripts)
				{
					DatabaseInfo dbInfo = connectionInfo.Databases.FirstOrDefault(db =>
						string.Equals(db.Alias, script.Alias, StringComparison.OrdinalIgnoreCase)
					);

					if (dbInfo != null)
					{
						dbInfo.ScriptPath = FormatPath(script.File, connectionInfo);
					}
				}
			}

			return connectionInfo;
		}

		private static string FormatPath(string pathPattern, StorageConnectionInfo cnnInfo)
		{
			if (pathPattern == null)
			{
				return null;
			}

			return string.Format(pathPattern, cnnInfo.StorageType);
		}

		private static string FindPostBuildScript(
			List<PostBuildScript> scripts,
			string                dbAlias,
			StorageConnectionInfo cnnInfo
		)
		{
			string pathPattern = scripts.FirstOrDefault(
				pbScript => string.Equals(pbScript.Alias, dbAlias, StringComparison.OrdinalIgnoreCase)
			)?.File;

			return FormatPath(pathPattern, cnnInfo);
		}
	}
}