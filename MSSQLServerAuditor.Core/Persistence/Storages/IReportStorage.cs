﻿using System.Data;
using MSSQLServerAuditor.Core.Domain;
using Perceiveit.Data;

namespace MSSQLServerAuditor.Core.Persistence.Storages
{
	public interface IReportStorage
	{
		string     DbName     { get; }
		DBDatabase DbDatabase { get; }

		long? SaveResults(
			long          queryId,
			NormalizeInfo dbStructure,
			DataTable     queryData,
			bool          recreateSchema
		);

		DataTable ReadResult(
			NormalizeInfo dbStructure,
			long          queryId,
			bool          readServiceColumns = false
		);

		string GetTableName(
			long                  templateNodeId,
			string                templateUid,
			TemplateNodeQueryInfo tnQueryInfo,
			long                  recordSet
		);

		NormalizeInfo GetDbStucture(
			string    tableName,
			long      recordSet,
			bool      recreateSchema,
			DataTable dataTable = null
		);
	}
}
