﻿using System.Collections.Concurrent;
using System.Globalization;
using System.IO;
using MSSQLServerAuditor.DataAccess;
using MSSQLServerAuditor.DataAccess.Commands.Interfaces;
using MSSQLServerAuditor.DataAccess.Tables;
using Perceiveit.Data;

namespace MSSQLServerAuditor.Core.Persistence.Storages
{
	public class FsStorage : Storage
	{
		private class FileLocation
		{
			public long   ConnectionId { get; }
			public string Path         { get; }

			public FileLocation(string filePath)
			{
				string[] pathParts = filePath.Split(System.IO.Path.DirectorySeparatorChar);

				ConnectionId = long.Parse(pathParts[0]);

				Path = string.Join(
					System.IO.Path.DirectorySeparatorChar.ToString(CultureInfo.InvariantCulture),
					pathParts,
					1,
					pathParts.Length - 1
				);
			}
		}

		private readonly ConcurrentDictionary<long, FsTables> _tables;

		public FsStorage(DBDatabase dbDatabase, ICommandExecutor commandExecutor, string alias)
			: base(dbDatabase, commandExecutor, alias)
		{
			this._tables = new ConcurrentDictionary<long, FsTables>();
		}

		public void SaveReportStream(string htmlPath, Stream stream)
		{
			WriteStream(htmlPath, stream);
		}

		public byte[] ReadReport(string htmlPath)
		{
			return ReadFile(
				htmlPath.Replace('/', Path.DirectorySeparatorChar)
			);
		}

		internal void WriteFile(string fileName, byte[] raw)
		{
			FileLocation location = GetLocation(fileName);
			FsTables     storage  = GetStorage(location.ConnectionId);
			string       path     = location.Path;
			string       folder   = Path.GetDirectoryName(path);
			string       file     = Path.GetFileName(path);
			long?        folderId = GetFolderId(storage.FolderTable, folder);

			storage.FileTable.WriteFile(raw, folderId, file);
		}

		/// <summary>
		/// Read file from DB
		/// </summary>
		/// <param name="fileName">File name</param>
		/// <returns></returns>
		internal byte[] ReadFile(string fileName)
		{
			FileLocation location = GetLocation(fileName);
			FsTables storage      = GetStorage(location.ConnectionId);
			string path           = location.Path;
			string folder         = Path.GetDirectoryName(path);
			string file           = Path.GetFileName(path);
			long? folderId        = GetFolderId(storage.FolderTable, folder);

			return storage.FileTable.ReadFile(folderId, file);
		}

		/// <summary>
		/// Write stream to DB
		/// </summary>
		/// <param name="fileName">File name</param>
		/// <param name="stream">Stream</param>
		internal void WriteStream(string fileName, Stream stream)
		{
			var buffer = new byte[stream.Length];

			stream.Read(buffer, 0, buffer.Length);
			this.WriteFile(fileName, buffer);
		}

		private FileLocation GetLocation(string filePath)
		{
			return new FileLocation(filePath);
		}

		private FsTables GetStorage(long connectionGroupId)
		{
			return this._tables.GetOrAdd(connectionGroupId,
				groupId => FsTables.CreateTables(this, groupId));
		}

		private long? GetFolderId(FsFolderTable folderTable, string path)
		{
			long? folderId = folderTable?.GetFolderId(path);

			return folderId;
		}
	}
}
