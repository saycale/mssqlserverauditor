﻿using System.Collections.Generic;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Querying;
using MSSQLServerAuditor.DataAccess.Commands.Interfaces;
using MSSQLServerAuditor.DataAccess.Connections;

namespace MSSQLServerAuditor.Core.Persistence.Storages
{
	public class StorageManager : IStorageManager
	{
		public MainStorage               MainStorage              { get; set; }
		public IReportStorage            ReportStorage            { get; set; }
		public List<HistoryStorage>      HistoryStorages          { get; set; }
		public FsStorage                 FsStorage                { get; set; }
		public IStorageConnectionFactory StorageConnectionFactory { get; set; }
		public ICommandExecutor          CommandExecutor          { get; set; }
		public StorageConnectionInfo     ConnectionInfo           { get; set; }
		public QueryFormatter            QueryFormatter           { get; set; }

		public StorageManager(StorageConnectionInfo connectionInfo)
		{
			Check.NotNull(connectionInfo, nameof(connectionInfo));

			this.ConnectionInfo = connectionInfo;
		}
	}
}
