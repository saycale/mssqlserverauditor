namespace MSSQLServerAuditor.Core.Persistence.Storages
{
	public class DatabaseInfo
	{
		public DatabaseInfo(string alias, string name)
		{
			this.Alias = alias;
			this.Name  = name;
		}

		public string Alias { get; set; }
		public string Name  { get; set; }

		public string ScriptPath { get; set; }
	}
}