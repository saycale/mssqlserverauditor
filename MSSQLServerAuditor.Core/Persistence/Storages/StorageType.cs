﻿using System;

namespace MSSQLServerAuditor.Core.Persistence.Storages
{
	[Flags]
	public enum StorageType
	{
		SQLite = 0,
		MSSQL  = 1
	}
}
