﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Mapping;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Persistence.Services.Base;
using MSSQLServerAuditor.Core.Schedule;
using MSSQLServerAuditor.DataAccess.Entity;
using MSSQLServerAuditor.DataAccess.Entity.Context;
using MSSQLServerAuditor.DataAccess.Entity.Models;
using MSSQLServerAuditor.DataAccess.Scope.Interfaces;

namespace MSSQLServerAuditor.Core.Persistence.Services
{
	public class SettingsService : CachingService
	{
		public SettingsService(
			IDbContextScopeFactory<StorageContext>  scopeFactory,
			ConcurrentDictionary<Type, EntityCache> cache,
			RepositoryCatalog                       repositories,
			Mapper                                  mapper,
			bool                                    isReadOnly = false) 
			: base(scopeFactory, cache, repositories, mapper, isReadOnly)
		{
		}

		public void SaveSettings(TemplateNodeInfo tnInfo, string lang)
		{
			DBNodeSettings settings = new DBNodeSettings
			{
				NodeInstanceId = tnInfo.NodeInstanceId,
				IsEnabled      = !tnInfo.IsDisabled,
				FontColor      = tnInfo.FontColor,
				Icon           = tnInfo.UIcon,
				Name           = tnInfo.UName,
				Language       = lang
			};

			Save(settings);
		}

		public void LoadUserSettings(TemplateNodeInfo tnInfo, string lang)
		{
			DBNodeSettings settings = Get<DBNodeSettings>(s =>
				s.NodeInstanceId == tnInfo.NodeInstanceId &&
				s.Language       == lang
			);

			if (settings != null)
			{
				tnInfo.FontColor  = settings.FontColor;
				tnInfo.FontStyle  = settings.FontStyle;
				tnInfo.UIcon      = settings.Icon;
				tnInfo.UName      = settings.Name;
				tnInfo.IsDisabled = !(settings.IsEnabled ?? true);
			}
		}

		public void ResetSettings(TemplateNodeInfo tnInfo, string lang)
		{
			DBNodeSettings defaultSettings = new DBNodeSettings
			{
				NodeInstanceId = tnInfo.NodeInstanceId,
				Name           = string.Empty,
				Language       = lang
			};

			Save(defaultSettings);
		}

		public void SaveScheduleSettings(
			TemplateNodeInfo       tnInfo,
			List<ScheduleSettings> schedules
		)
		{
			foreach (ScheduleSettings scheduleSettings in schedules)
			{
				SaveScheduleSettings(tnInfo, scheduleSettings);
			}
		}

		public void SaveScheduleSettings(
			TemplateNodeInfo tnInfo,
			ScheduleSettings scheduleSettings
		)
		{
			DBNodeScheduleSettings dbScheduleSettings = Mapper.ToDb(scheduleSettings);

			if (dbScheduleSettings.NodeInstanceId != 0)
			{
				dbScheduleSettings.NodeInstanceId = tnInfo.NodeInstanceId;
			}

			Save(dbScheduleSettings);

			scheduleSettings.Id = dbScheduleSettings.Id;
		}

		public List<ScheduleSettings> GetScheduleSettings(TemplateNodeInfo tnInfo)
		{
			List<DBNodeScheduleSettings> dbSchedules = GetAll<DBNodeScheduleSettings>(
				settings => settings.NodeInstanceId == tnInfo.NodeInstanceId
			);

			List<ScheduleSettings> schedules = new List<ScheduleSettings>();

			foreach (DBNodeScheduleSettings dbSchedule in dbSchedules)
			{
				schedules.Add(Mapper.FromDb(dbSchedule));
			}

			return schedules;
		}

		public List<JobInfo> GetJobs(TemplateNodeInfo tnInfo)
		{
			List<DBNodeScheduleSettings> dbSchedules = GetAll<DBNodeScheduleSettings>(
				settings => settings.NodeInstanceId == tnInfo.NodeInstanceId,

				settings => settings.NodeInstance.TemplateNode.Template
			);

			List<JobInfo> jobs = new List<JobInfo>();
			foreach (DBNodeScheduleSettings dbSchedule in dbSchedules)
			{
				if (dbSchedule != null)
				{
					ScheduleDetails schedule = Mapper.MapScheduleDetails(dbSchedule);
					JobInfo jobInfo = CreateJob(tnInfo, schedule);

					jobs.Add(jobInfo);
				}
			}

			return jobs;
		}

		public bool HasSchedules(TemplateNodeInfo tnInfo)
		{
			DBNodeScheduleSettings dbFirstSchedule = Get<DBNodeScheduleSettings>(settings =>
				settings.NodeInstanceId == tnInfo.NodeInstanceId
			);

			return dbFirstSchedule != null;
		}

		public bool HasActiveSchedules(TemplateNodeInfo tnInfo)
		{
			DBNodeScheduleSettings dbFirstSchedule = Get<DBNodeScheduleSettings>(settings => 
				settings.NodeInstanceId == tnInfo.NodeInstanceId &&
				settings.IsEnabled      == true
			);

			return dbFirstSchedule != null;
		}

		public Dictionary<long, ScheduleDetails> LoadScheduleDetails(string targetMachine)
		{
			if (string.IsNullOrWhiteSpace(targetMachine))
			{
				return LoadScheduleSettings(s =>
						s.IsEnabled                  == true &&
						s.NodeInstance.IsEnabled     == true &&
						(s.TargetMachine == null || s.TargetMachine.Equals(""))
					)
					.Select(Mapper.MapScheduleDetails)
					.ToDictionary(details => details.Id);
			}

			return LoadScheduleSettings(s =>
					s.IsEnabled              == true &&
					s.NodeInstance.IsEnabled == true &&
					(s.TargetMachine.Equals(targetMachine, StringComparison.OrdinalIgnoreCase) || s.TargetMachine == null || s.TargetMachine.Equals(""))
				)
				.Select(Mapper.MapScheduleDetails)
				.ToDictionary(details => details.Id);
		}

		private List<DBNodeScheduleSettings> LoadScheduleSettings(
			Expression<Func<DBNodeScheduleSettings, bool>> filter
		)
		{
			return GetAll(
				filter,

				// query related entities
				s => s.NodeInstance,
				s => s.NodeInstance.TemplateNode,
				s => s.NodeInstance.TemplateNode.Template,
				s => s.NodeInstance.ConnectionGroup
			);
		}

		public JobInfo CreateJob(
			TemplateNodeInfo tnInfo,
			ScheduleDetails  scheduleDetails
		)
		{
			return new JobInfo(tnInfo, scheduleDetails);
		}
	}
}
