using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain.Mapping;
using MSSQLServerAuditor.DataAccess.Entity;
using MSSQLServerAuditor.DataAccess.Entity.Context;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;
using MSSQLServerAuditor.DataAccess.Entity.Repositories;
using MSSQLServerAuditor.DataAccess.Scope.Interfaces;

namespace MSSQLServerAuditor.Core.Persistence.Services.Base
{
	public abstract class AbstractService
	{
		protected AbstractService(
			IDbContextScopeFactory<StorageContext> scopeFactory,
			RepositoryCatalog                      repositories,
			Mapper                                 mapper,
			bool                                   isReadOnly = false
		)
		{
			Check.NotNull(scopeFactory, nameof(scopeFactory));
			Check.NotNull(repositories, nameof(repositories));
			Check.NotNull(mapper,       nameof(mapper));

			this.ContextScopeFactory = scopeFactory;
			this.Repositories        = repositories;
			this.Mapper              = mapper;
			this.IsReadOnly          = isReadOnly;
		}

		protected IDbContextScopeFactory<StorageContext> ContextScopeFactory { get; private set; }

		protected RepositoryCatalog Repositories  { get; private set; }
		protected bool              IsReadOnly    { get; private set; }
		protected Mapper            Mapper        { get; private set; }

		protected T Execute<T>(Func<T> factory)
		{
			using (ContextScopeFactory.CreateReadOnly())
			{
				return factory();
			}
		}

		#region Generic CRUD methods for repositories

		protected virtual TEntity Get<TEntity>(
			long                                       id,
			params Expression<Func<TEntity, object>>[] includes
		) where TEntity : class, IEntity
		{
			return Get(entity => entity.Id == id, includes);
		}

		protected virtual TEntity Get<TEntity>(TEntity specification)
			where TEntity : class, IEntity
		{
			using (ContextScopeFactory.CreateReadOnly())
			{
				TEntity entity = GetRepository<TEntity>()
					.FindByUniqueKey(specification);

				return entity;
			}
		}

		protected virtual TEntity GetIncluding<TEntity>(
			TEntity                                    specification,
			params Expression<Func<TEntity, object>>[] includes)
			where TEntity : class, IEntity
		{
			using (ContextScopeFactory.CreateReadOnly())
			{
				IQueryable<TEntity> query = GetRepository<TEntity>()
					.QueryByUniqueKey(specification);

				if (includes != null)
				{
					query = includes.Aggregate(query,
						(current, include) => current.Include(include));
				}

				return query.FirstOrDefault();
			}
		}

		protected virtual TEntity FilterFirst<TEntity>(
			Func<IQueryable<TEntity>, IQueryable<TEntity>> queryAllFilter,
			params Expression<Func<TEntity, object>>[]     includes
		) where TEntity : class, IEntity
		{
			using (ContextScopeFactory.CreateReadOnly())
			{
				IQueryable<TEntity> query = queryAllFilter(
					GetRepository<TEntity>().All());

				if (includes != null)
				{
					query = includes.Aggregate(query,
						(current, include) => current.Include(include));
				}

				return query.FirstOrDefault();
			}
		}

		protected virtual List<TEntity> FilterAll<TEntity>(
			Func<IQueryable<TEntity>, IQueryable<TEntity>> queryAllFilter,
			params Expression<Func<TEntity, object>>[]     includes
		) where TEntity : class, IEntity
		{
			using (ContextScopeFactory.CreateReadOnly())
			{
				IQueryable<TEntity> query = queryAllFilter(
					GetRepository<TEntity>().All());

				if (includes != null)
				{
					query = includes.Aggregate(query,
						(current, include) => current.Include(include));
				}

				return query.ToList();
			}
		}

		protected virtual List<TOutEntity> FilterAll<TInEntity, TOutEntity>(
			Func<IQueryable<TInEntity>, IQueryable<TOutEntity>> queryAllFilter,
			params Expression<Func<TOutEntity, object>>[] includes
		)
			where TOutEntity : class, IEntity
			where TInEntity  : class, IEntity
		{
			using (ContextScopeFactory.CreateReadOnly())
			{
				IQueryable<TOutEntity> query = queryAllFilter(
					GetRepository<TInEntity>().All());

				if (includes != null)
				{
					query = includes.Aggregate(query,
						(current, include) => current.Include(include));
				}

				return query.ToList();
			}
		}

		protected virtual List<TEntity> GetAll<TEntity>(
			Expression<Func<TEntity, bool>>            filter = null,
			params Expression<Func<TEntity, object>>[] includes
		) where TEntity : class, IEntity
		{
			using (ContextScopeFactory.CreateReadOnly())
			{
				return GetRepository<TEntity>()
					.GetIncluding(filter, null, includes)
					.ToList();
			}
		}

		protected virtual List<TEntity> GetAllOrdered<TEntity>(
			Expression<Func<TEntity, bool>>                       filter = null,
			Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
			params Expression<Func<TEntity, object>>[]            includes
		) where TEntity : class, IEntity
		{
			using (ContextScopeFactory.CreateReadOnly())
			{
				return GetRepository<TEntity>()
					.GetIncluding(filter, orderBy, includes)
					.ToList();
			}
		}

		protected virtual TEntity Get<TEntity>(
			Expression<Func<TEntity, bool>>                       filter = null,
			Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
			params Expression<Func<TEntity, object>>[]            includes
		) where TEntity : class, IEntity
		{
			using (ContextScopeFactory.CreateReadOnly())
			{
				return GetRepository<TEntity>()
					.GetIncluding(filter, orderBy, includes)
					.FirstOrDefault();
			}
		}

		protected virtual TEntity Get<TEntity>(
			Expression<Func<TEntity, bool>>            filter = null,
			params Expression<Func<TEntity, object>>[] includes
		) where TEntity : class, IEntity
		{
			using (ContextScopeFactory.CreateReadOnly())
			{
				return GetRepository<TEntity>()
					.GetIncluding(filter, null, includes)
					.FirstOrDefault();
			}
		}

		protected virtual void Save<TEntity>(TEntity entity)
			where TEntity : class, IEntity
		{
			if (this.IsReadOnly)
			{
				return;
			}

			lock (GetLock<TEntity>())
			{
				using (IDbContextScope<StorageContext> scope = ContextScopeFactory.Create())
				{
					GetRepository<TEntity>().InsertOrUpdate(entity);

					scope.SaveChanges();
				}
			}
		}

		protected virtual TEntity GetOrAdd<TEntity>(TEntity specification)
			where TEntity : class, IEntity
		{
			TEntity entity = Get(specification);

			if (this.IsReadOnly)
			{
				return entity;
			}

			if (entity == null)
			{
				lock (GetLock<TEntity>())
				{
					entity = Get(specification);

					if (entity == null)
					{
						using (IDbContextScope<StorageContext> scope = ContextScopeFactory.Create())
						{
							entity = GetRepository<TEntity>().Create(specification);

							scope.SaveChanges();
						}
					}
				}
			}

			return entity;
		}

		protected virtual TEntity UpdateProperties<TEntity>(
			TEntity                                    entity,
			params Expression<Func<TEntity, object>>[] properties
		) where TEntity : class, IEntity
		{
			if (this.IsReadOnly)
			{
				return null;
			}

			lock (GetLock<TEntity>())
			{
				using (IDbContextScope<StorageContext> scope = ContextScopeFactory.Create())
				{
					TEntity updatedEntity = GetRepository<TEntity>()
						.UpdateProperties(entity, properties);

					scope.SaveChanges();

					return updatedEntity;
				}
			}
		}

		protected virtual List<TEntity> UpdateProperties<TEntity>(
			IEnumerable<TEntity>                       entities,
			params Expression<Func<TEntity, object>>[] properties
		) where TEntity : class, IEntity
		{
			if (this.IsReadOnly)
			{
				return entities.ToList();
			}

			lock (GetLock<TEntity>())
			{
				using (IDbContextScope<StorageContext> scope = ContextScopeFactory.Create())
				{
					List<TEntity> updated = new List<TEntity>();

					foreach (TEntity entity in entities)
					{
						updated.Add(GetRepository<TEntity>().UpdateProperties(entity, properties));
					}

					scope.SaveChanges();

					return updated;
				}
			}
		}

		protected virtual TEntity InsertOrUpdate<TEntity>(TEntity entity)
			where TEntity : class, IEntity
		{
			if (this.IsReadOnly)
			{
				return null;
			}

			lock (GetLock<TEntity>())
			{
				using (IDbContextScope<StorageContext> scope = ContextScopeFactory.Create())
				{
					TEntity storedEntity = GetRepository<TEntity>()
						.InsertOrUpdate(entity);

					scope.SaveChanges();

					return storedEntity;
				}
			}
		}

		protected virtual TEntity Delete<TEntity>(long id)
			where TEntity : class, IEntity
		{
			if (this.IsReadOnly)
			{
				return null;
			}

			lock (GetLock<TEntity>())
			{
				using (IDbContextScope<StorageContext> scope = ContextScopeFactory.Create())
				{
					TEntity deleted = GetRepository<TEntity>()
						.Delete(entity => entity.Id == id)
						.FirstOrDefault();

					scope.SaveChanges();

					return deleted;
				}
			}
		}

		protected virtual TEntity Delete<TEntity>(TEntity entity)
			where TEntity : class, IEntity
		{
			if (this.IsReadOnly)
			{
				return null;
			}

			lock (GetLock<TEntity>())
			{
				using (IDbContextScope<StorageContext> scope = ContextScopeFactory.Create())
				{
					TEntity deleted = GetRepository<TEntity>().Delete(entity);

					scope.SaveChanges();

					return deleted;
				}
			}
		}

		protected virtual List<TEntity> Delete<TEntity>(Expression<Func<TEntity, bool>> filter = null)
			where TEntity : class, IEntity
		{
			if (this.IsReadOnly)
			{
				return new List<TEntity>();
			}

			lock (GetLock<TEntity>())
			{
				using (IDbContextScope<StorageContext> scope = ContextScopeFactory.Create())
				{
					List<TEntity> deleted = GetRepository<TEntity>()
						.Delete(filter);

					scope.SaveChanges();

					return deleted;
				}
			}
		}

		#endregion

		protected CrudRepository<TEntity> GetRepository<TEntity>()
			where TEntity : class, IEntity
		{
			return Repositories.Get<TEntity>();
		}

		protected object GetLock<TEntity>()
			where TEntity : class, IEntity
		{
			return Repositories.GetLock<TEntity>();
		}
	}
}
