using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using MSSQLServerAuditor.Core.Domain.Mapping;
using MSSQLServerAuditor.DataAccess.Entity;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Context;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;
using MSSQLServerAuditor.DataAccess.Scope.Interfaces;
using MSSQLServerAuditor.DataAccess.Utils;

namespace MSSQLServerAuditor.Core.Persistence.Services.Base
{
	public abstract class CachingService : AbstractService
	{
		protected ConcurrentDictionary<Type, EntityCache> SharedCache { get; private set; }

		protected CachingService(
			IDbContextScopeFactory<StorageContext>  scopeFactory,
			ConcurrentDictionary<Type, EntityCache> cache,
			RepositoryCatalog                       repositories,
			Mapper                                  mapper,
			bool                                    isReadOnly = false
		) : base(
			scopeFactory,
			repositories,
			mapper,
			isReadOnly
		)
		{
			this.SharedCache = cache;
		}

		protected override List<TEntity> UpdateProperties<TEntity>(
			IEnumerable<TEntity>                       entities,
			params Expression<Func<TEntity, object>>[] properties
		)
		{
			List<TEntity> updated = base.UpdateProperties(entities, properties);

			EntityCache cachedEntities = GetCache<TEntity>();

			foreach (TEntity updatedEntity in updated)
			{
				if (updatedEntity != null)
				{
					string key = GetCacheKey(updatedEntity);

					cachedEntities.AddOrUpdate(key, updatedEntity, (k, e) => e);
				}
			}

			return updated;
		}

		protected override TEntity UpdateProperties<TEntity>(
			TEntity entity,
			params Expression<Func<TEntity, object>>[] properties
		)
		{
			TEntity updated = base.UpdateProperties(entity, properties);

			if (updated != null)
			{
				string key = GetCacheKey(updated);

				EntityCache cachedEntities = GetCache<TEntity>();

				cachedEntities.AddOrUpdate(key, updated, (k, e) => e);
			}

			return updated;
		}

		protected override void Save<TEntity>(TEntity entity)
		{
			base.Save(entity);

			if (entity != null)
			{
				string key = GetCacheKey(entity);

				EntityCache cachedEntities = GetCache<TEntity>();

				cachedEntities.AddOrUpdate(key, entity, (k, e) => entity);
			}
		}

		protected override TEntity GetOrAdd<TEntity>(TEntity specification)
		{
			string key = GetCacheKey(specification);

			EntityCache cachedEntities = GetCache<TEntity>();

			return (TEntity) cachedEntities.GetOrAdd(
				key,
				k => base.GetOrAdd(specification)
			);
		}

		protected override TEntity InsertOrUpdate<TEntity>(TEntity entity)
		{
			TEntity savedEntity = base.InsertOrUpdate(entity);

			if (savedEntity != null)
			{
				string key = GetCacheKey(savedEntity);

				EntityCache cachedEntities = GetCache<TEntity>();

				cachedEntities.AddOrUpdate(
					key,
					savedEntity,
					(k, e) => savedEntity
				);
			}

			return savedEntity;
		}

		protected override TEntity Delete<TEntity>(TEntity entity)
		{
			TEntity deletedEntity = base.Delete(entity);

			if (deletedEntity != null)
			{
				string key = GetCacheKey(deletedEntity);

				EntityCache cachedEntities = GetCache<TEntity>();

				IEntity cached;

				cachedEntities.TryRemove(key, out cached);
			}

			return deletedEntity;
		}

		protected override TEntity Delete<TEntity>(long id)
		{
			TEntity deletedEntity = base.Delete<TEntity>(id);

			if (deletedEntity != null)
			{
				string key = GetCacheKey(deletedEntity);

				EntityCache cachedEntities = GetCache<TEntity>();

				IEntity cached;

				cachedEntities.TryRemove(key, out cached);
			}

			return deletedEntity;
		}

		protected override List<TEntity> Delete<TEntity>(Expression<Func<TEntity, bool>> filter = null)
		{
			List<TEntity> deleted = base.Delete(filter);

			EntityCache cachedEntities = GetCache<TEntity>();

			foreach (TEntity deletedEntity in deleted)
			{
				string key = GetCacheKey(deletedEntity);

				IEntity cached;

				cachedEntities.TryRemove(key, out cached);
			}

			return deleted;
		}

		private EntityCache GetCache<TEntity>()
		{
			return SharedCache.GetOrAdd(
				typeof(TEntity), tentity => new EntityCache());
		}

		private string GetCacheKey<TEntity>(TEntity entity)
			where TEntity : class, IEntity
		{
			PropertyInfo[] props = ReflectionUtils
				.GetPropertiesWithAttribute<TEntity, UniqueKeyAttribute>();

			if (props == null)
			{
				props = ReflectionUtils.GetPropertiesWithAttribute<TEntity, KeyAttribute>();
			}

			string key = string.Join("_", props
				.Select(p => p.GetValue(entity, null))
				.Select(v => v?.ToString() ?? string.Empty)
			);

			return key;
		}
	}
}
