﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using MSSQLServerAuditor.Core.Domain.Mapping;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Persistence.Services.Base;
using MSSQLServerAuditor.DataAccess.Entity;
using MSSQLServerAuditor.DataAccess.Entity.Context;
using MSSQLServerAuditor.DataAccess.Entity.Models;
using MSSQLServerAuditor.DataAccess.Scope.Interfaces;

namespace MSSQLServerAuditor.Core.Persistence.Services
{
	public class QueryService : CachingService
	{
		public QueryService(
			IDbContextScopeFactory<StorageContext>  scopeFactory,
			ConcurrentDictionary<Type, EntityCache> cache,
			RepositoryCatalog                       repositories,
			Mapper                                  mapper,
			bool                                    isReadOnly = false) 
			: base(scopeFactory, cache, repositories, mapper, isReadOnly)
		{
		}

		public DBQueryResults GetOrAddQueryResult(DBQueryResults qrSpec)
		{
			return GetOrAdd(qrSpec);
		}

		public DBQueryResultTable GetOrAddQueryResultTable(DBQueryResultTable qrtSpec)
		{
			return GetOrAdd(qrtSpec);
		}

		public void SaveQueryResult(DBQueryResults qrSpec)
		{
			Save(qrSpec);
		}

		public void SaveQueryResultTable(DBQueryResultTable qrtSpec)
		{
			Save(qrtSpec);
		}

		public List<Query> GetQueries(long nodeInstanceId, long serverInstanceId)
		{
			List<DBQuery> dbQueries = GetAll<DBQuery>(query => 
				query.NodeInstanceId   == nodeInstanceId &&
				query.ServerInstanceId == serverInstanceId
			);

			List<Query> queries = new List<Query>();
			foreach (DBQuery dbQuery in dbQueries)
			{
				queries.Add(Mapper.FromDb(dbQuery));
			}

			return queries;
		}
	}
}
