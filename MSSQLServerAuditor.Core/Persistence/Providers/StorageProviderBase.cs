﻿using System;
using System.Collections.Generic;
using System.Linq;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Querying;
using MSSQLServerAuditor.Core.Xml;
using MSSQLServerAuditor.DataAccess;
using MSSQLServerAuditor.DataAccess.Commands;
using MSSQLServerAuditor.DataAccess.Connections;
using MSSQLServerAuditor.DataAccess.Extensions;
using NLog;

namespace MSSQLServerAuditor.Core.Persistence.Providers
{
	public abstract class StorageProviderBase : IStorageProvider
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		protected StorageProviderBase(
			StorageConnectionInfo    connectionInfo,
			IDatabaseProviderFactory providerFactory,
			XmlLoader                xmlLoader
		)
		{
			Check.NotNull(connectionInfo, nameof(connectionInfo));
			Check.NotNull(xmlLoader,      nameof(xmlLoader));

			this.ConnectionInfo    = connectionInfo;
			this.ProviderFactory   = providerFactory;
			this.XmlLoader         = xmlLoader;
			this.ConnectionFactory = providerFactory.CreateConnectionFactory();
		}

		public    IDatabaseProviderFactory ProviderFactory    { get; }
		protected StorageConnectionInfo    ConnectionInfo     { get; }
		protected IConnectionFactory       ConnectionFactory  { get; private set; }
		protected XmlLoader                XmlLoader          { get; private set; }

		public abstract MainStorage               CreateCurrentStorage();
		public abstract IReportStorage            CreateReportStorage();
		public abstract List<HistoryStorage>      CreateExtraStorages();
		public abstract FsStorage                 CreateFsStorage();
		public abstract IStorageConnectionFactory CreateStorageConnectionFactory();

		public IStorageManager CreateStorages()
		{
			StorageManager storageManager = new StorageManager(ConnectionInfo)
			{
				CommandExecutor = ProviderFactory.CreateCommandExecutor(),
				MainStorage     = CreateCurrentStorage(),
				FsStorage       = CreateFsStorage(),
				ReportStorage   = CreateReportStorage()
			};

			if (ConnectionInfo.Databases != null)
			{
				storageManager.HistoryStorages = CreateExtraStorages();
			}

			storageManager.StorageConnectionFactory = CreateStorageConnectionFactory();
			storageManager.QueryFormatter           = CreateQueryFormatter(storageManager);

			return storageManager;
		}

		private QueryFormatter CreateQueryFormatter(StorageManager storageManager)
		{
			Dictionary<string, string> aliasMap = new Dictionary<string, string>();

			List<HistoryStorage> historyStorages = storageManager.HistoryStorages;
			if (historyStorages != null)
			{
				aliasMap = historyStorages
					.ToDictionary(
						hist => $"${{{hist.Alias}}}$",
						hist => hist.DbName
					);
			}

			aliasMap.Add("${current}$", storageManager.MainStorage.DbName);
			aliasMap.Add("${report}$", storageManager.ReportStorage.DbName);

			return new QueryFormatter(aliasMap);
		}

		protected void RunScripts(Storage storage, string scriptPath)
		{
			List<QueryInfo> queries = XmlLoader.GetQueries(scriptPath, ConnectionInfo.StorageType);

			foreach (QueryInfo query in queries)
			{
				foreach (QueryItemInfo queryItem in query.Items)
				{
					QueryItemInfo localQuery = queryItem;

					string queryInfo = $"Name: {query.Name}, Id: {query.Id}";

					if (string.IsNullOrWhiteSpace(localQuery.Text))
					{
						Log.Info($"Script '{queryInfo}' is empty. Skipping its execution.");

						continue;
					}

					try
					{
						storage.DbDatabase.Execute(context => storage.CommandExecutor.Execute(
							new ExecuteScriptCommand(context, localQuery.Text))
						);

						Log.Info($"Script executed successfully {queryInfo}");
					}
					catch (Exception exc)
					{
						Log.Error(exc, $"Unable to execute initialization script. {queryInfo}");
					}
				}
			}
		}
	}
}
