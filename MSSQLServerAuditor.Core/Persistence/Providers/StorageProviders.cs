﻿using System;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Persistence.Storages.Mssql;
using MSSQLServerAuditor.Core.Persistence.Storages.Sqlite;
using MSSQLServerAuditor.Core.Xml;

namespace MSSQLServerAuditor.Core.Persistence.Providers
{
	public static class StorageProviders
	{
		public static IStorageProvider CreateProvider(
			StorageConnectionInfo connectionInfo,
			XmlLoader             xmlLoader
		)
		{
			switch (connectionInfo.StorageType)
			{
				case StorageType.MSSQL:
					return new MssqlStorageProvider(connectionInfo, xmlLoader);
				case StorageType.SQLite:
					return new SqliteStorageProvider(connectionInfo, xmlLoader);
				default:
					throw new ArgumentException("Unknown storage type: " + connectionInfo.StorageType);
			}
		}
	}
}
