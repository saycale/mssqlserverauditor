﻿using MSSQLServerAuditor.Core.Persistence.Storages;

namespace MSSQLServerAuditor.Core.Persistence.Providers
{
	public interface IStorageProvider
	{
		IStorageManager CreateStorages();
	}
}
