﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using MSSQLServerAuditor.Common;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.CodeGuard;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Querying.Data;
using MSSQLServerAuditor.Core.Session;
using MSSQLServerAuditor.Core.Xml;
using MSSQLServerAuditor.DataAccess.Entity.Models;

namespace MSSQLServerAuditor.Core.Persistence
{
	public class QueryResultReader
	{
		private readonly IStorageManager  _storage;
		private readonly AuditCoordinator _auditCoordinator;
		private readonly IQueryLoader     _queryLoader;
		
		public QueryResultReader(
			IStorageManager  storage,
			AuditCoordinator auditCoordinator,
			IQueryLoader     queryLoader)
		{
			this._storage          = storage;
			this._auditCoordinator = auditCoordinator;
			this._queryLoader      = queryLoader;
		}

		public QueryResultSet LoadResults(
			AuditContext     context, 
			TemplateNodeInfo tnInfo)
		{
			QueryResultSet resultSet = new QueryResultSet();

			Dictionary<TemplateNodeQueryInfo, DataTable> histTables = ReadHistTable(
				context,
				tnInfo,
				CancellationToken.None
			);

			foreach (TemplateNodeQueryInfo tnQueryInfo in tnInfo.Queries)
			{
				DataTable histTable;
				histTables.TryGetValue(tnQueryInfo, out histTable);

				QueryResult tnResult = ReadQueryResult(
					context,
					tnQueryInfo,
					histTable
				);

				resultSet.Add(tnResult);
			}

			List<QueryResult> guardQueryResults = new List<QueryResult>();
			foreach (TemplateNodeSqlGuardQueryInfo guardQueryInfo in tnInfo.SqlCodeGuardQueries)
			{
				QueryResult tnResult = ReadGuardQueryResult(
					resultSet,
					guardQueryInfo
				);

				if (tnResult != null)
				{
					guardQueryResults.Add(tnResult);
				}
			}

			if (guardQueryResults.Any())
			{
				foreach (QueryResult guardQueryResult in guardQueryResults)
				{
					resultSet.Add(guardQueryResult);
				}
			}

			UpdateTiming timing = this._storage.MainStorage.Nodes.GetUpdateTiming(tnInfo);

			resultSet.UpdateTime     = timing.Date;
			resultSet.UpdateDuration = timing.Duration;

			return resultSet;
		}

		private QueryResult ReadQueryResult(
			AuditContext          context,
			TemplateNodeQueryInfo tnQueryInfo,
			DataTable             histTable
		)
		{
			QueryResult     queryResult = new QueryResult(tnQueryInfo);
			List<QueryInfo> queries     = this._queryLoader.LoadQueries(tnQueryInfo);

			List<AuditSession> sessions = context.CreateSessions();

			string connectionsSelectId = tnQueryInfo.ConnectionsSelectId;

			if (connectionsSelectId != null)
			{
				foreach (AuditSession session in sessions)
				{
					List<SessionResult> sessionResults = GetDynamicSessionResults(
						session,
						tnQueryInfo,
						queries,
						histTable
					);

					if (sessionResults != null && sessionResults.Any())
					{
						foreach (SessionResult sessionResult in sessionResults)
						{
							queryResult.AddSessionResult(sessionResult);
						}
					}
				}
			}
			else
			{
				foreach (AuditSession session in sessions)
				{
					SessionResult sessionResult = GetSessionResult(
						session,
						tnQueryInfo,
						queries,
						histTable
					);

					if (sessionResult != null)
					{
						queryResult.AddSessionResult(sessionResult);
					}
				}
			}

			return queryResult;
		}

		private QueryResult ReadGuardQueryResult(
			QueryResultSet                resultSet,
			TemplateNodeSqlGuardQueryInfo guardQueryInfo)
		{
			QueryResult queryResult = resultSet.Results.FirstOrDefault(qr => 
				qr.QueryInfo.Id == guardQueryInfo.SqlQueryId
			);

			if (queryResult == null)
			{
				return null;
			}

			TemplateNodeQueryInfo tnQueryInfo = queryResult.QueryInfo;
			List<ParameterValue>  userParams  = new List<ParameterValue>();

			QueryResult guardQueryResult = new QueryResult(guardQueryInfo);

			foreach (KeyValuePair<AuditSession, SessionResult> sessionResultPair in queryResult.SessionResults)
			{
				AuditSession  session       = sessionResultPair.Key;
				SessionResult sessionResult = sessionResultPair.Value;

				if (sessionResult.IsEmpty)
				{
					continue;
				}

				DataTable queryTable = sessionResult.DatabaseResults
					.First().Value
					.DataTables?.FirstOrDefault();

				if (queryTable == null || !queryTable.Columns.Contains(guardQueryInfo.QueryCodeColumn))
				{
					continue;
				}

				DBQuery query = this._storage.MainStorage.Nodes.GetQuery(
					session,
					tnQueryInfo,
					DefaultValues.Date.Minimum,
					true
				);

				if (query == null)
				{
					continue;
				}

				MetaResult meta = this._storage.MainStorage.MetaResults
					.ReadLastMeta(query.Id);

				if (meta == null)
				{
					continue;
				}

				SessionResult guardSessionResult;
				DateTime timestamp = meta.DateUpdated <= DefaultValues.Date.Minimum
					? meta.DateCreated
					: meta.DateUpdated;

				if (!string.IsNullOrEmpty(meta.ErrorMessage))
				{
					guardSessionResult = new SessionResult(
						new ErrorInfo(
							meta.ErrorId,
							meta.ErrorCode,
							meta.ErrorMessage,
							meta.DateCreated
						),
						session,
						timestamp
					);
				}
				else
				{
					guardSessionResult = new SessionResult(session, timestamp);
				}

				CodeGuardAnalyzer analyzer = new CodeGuardAnalyzer();
				DataTable[] dataTables = analyzer.ReadSqlCodeGuardResult(
					guardQueryInfo,
					queryTable,
					userParams
				);

				QueryItemInfo queryItemInfo = new QueryItemInfo
				{
					Parent = new QueryInfo { Name = guardQueryInfo.QueryName }
				};

				DatabaseResult databaseResult = new DatabaseResult(
					dataTables.ToList(),
					queryItemInfo
				);

				guardSessionResult.AddDatabaseResult(databaseResult);
				guardQueryResult.AddSessionResult(guardSessionResult);
			}

			return guardQueryResult;
		}

		private SessionResult GetSessionResult(
			AuditSession          session,
			TemplateNodeQueryInfo tnQueryInfo,
			long                  queryId,
			List<QueryInfo>       queries,
			DataTable             histTable

		)
		{
			SessionResult sessionResult  = null;
			long          recordSetCount = 0L;
			MetaResult    meta           = this._storage.MainStorage.MetaResults.ReadLastMeta(queryId);

			if (meta != null)
			{
				DateTime timestamp = meta.DateUpdated <= DefaultValues.Date.Minimum
					? meta.DateCreated
					: meta.DateUpdated;

				ServerInstance instance = session.Server;
				if (!string.IsNullOrEmpty(meta.ErrorMessage))
				{
					sessionResult = new SessionResult(
						new ErrorInfo(
							meta.ErrorId,
							meta.ErrorCode,
							meta.ErrorMessage,
							meta.DateCreated
						),
						session,
						timestamp
					);
				}
				else
				{
					sessionResult = new SessionResult(session, timestamp);
				}

				if (meta.RecordSets != null)
				{
					recordSetCount = meta.RecordSets.Value;
				}

				List<DataTable> dataTables = GetDataTables(
					session,
					tnQueryInfo,
					recordSetCount,
					queryId,
					histTable
				);

				ConnectionType type      = instance.ConnectionType;
				QueryInfo      queryInfo = queries.FirstOrDefault(x => x.ConnectionType == type || x.ConnectionType.IsInternal());

				if (queryInfo != null && queryInfo.SaveResults)
				{
					QueryItemInfo queryItem = instance.ResolveQuery(queryInfo.Items);

					DatabaseResult databaseResult = new DatabaseResult(
						dataTables,
						queryItem
					);

					sessionResult.AddDatabaseResult(databaseResult);
				}
			}

			return sessionResult;
		}

		private SessionResult GetSessionResult(
			AuditSession          session,
			TemplateNodeQueryInfo tnQueryInfo,
			List<QueryInfo>       queries,
			DataTable             histTable
		)
		{
			DBQuery query = this._storage.MainStorage.Nodes.GetQuery(
				session,
				tnQueryInfo,
				DateTime.Now,
				true
			);

			if (query == null)
			{
				return null;
			}

			return GetSessionResult(session, tnQueryInfo, query.Id, queries, histTable);
		}

		private List<SessionResult> GetDynamicSessionResults(
			AuditSession          session,
			TemplateNodeQueryInfo tnQueryInfo,
			List<QueryInfo>       queries,
			DataTable             histTable
		)
		{
			DBQuery query = this._storage.MainStorage.Nodes.GetQuery(
				session,
				tnQueryInfo,
				DateTime.Now,
				false
			);

			List<SessionResult> sessionResults = new List<SessionResult>();

			if (query != null)
			{
				List<DynamicConnectionInfo> connections = this._storage.MainStorage.Connections
					.GetDynamicConnections(query.Id);

				foreach (DynamicConnectionInfo connection in connections)
				{
					if (!connection.QueryId.HasValue)
					{
						continue;
					}

					long           dynamicQueryId  = connection.QueryId.Value;
					ServerInstance dynamicInstance = connection.ServerInstance;

					AuditSession dynamicSession = new AuditSession(
						session.Group,
						dynamicInstance,
						session.Template
					);

					SessionResult sessionResult = GetSessionResult(
						dynamicSession,
						tnQueryInfo,
						dynamicQueryId,
						queries,
						histTable
					);

					sessionResults.Add(sessionResult);
				}
			}

			return sessionResults;
		}

		private List<DataTable> GetDataTables(
			AuditSession          session,
			TemplateNodeQueryInfo tnQueryInfo,
			long                  recordSetCount,
			long                  queryId,
			DataTable             histTable)
		{
			List<DataTable> dataTables = new List<DataTable>((int) recordSetCount);

			for (long recordSet = 1L; recordSet <= recordSetCount; recordSet++)
			{
				if (histTable != null)
				{
					dataTables.Add(histTable);
				}
				else
				{
					long templateNodeId = this._storage.MainStorage.Templates.GetTemplateNodeId(
						session.Template,
						tnQueryInfo.TemplateNode.Template
					);

					string tableName = this._storage.ReportStorage.GetTableName(
						templateNodeId,
						session.Template.UId,
						tnQueryInfo,
						recordSet
					);

					NormalizeInfo structure = this._storage.ReportStorage.GetDbStucture(
						tableName,
						recordSet,
						false
					);

					dataTables.Add(this._storage.ReportStorage.ReadResult(structure, queryId));
				}
			}

			return dataTables;
		}

		public Dictionary<TemplateNodeQueryInfo, DataTable> ReadHistTable(
			AuditContext      context,
			TemplateNodeInfo  tnInfo,
			CancellationToken token
		)
		{
			Dictionary<TemplateNodeQueryInfo, DataTable> histTable = 
				new Dictionary<TemplateNodeQueryInfo, DataTable>();

			QueryResultSet resultSet = this._auditCoordinator.Execute(
				context,
				tnInfo,
				token,
				true
			);

			if (resultSet != null && !resultSet.Results.IsNullOrEmpty())
			{
				if (resultSet.Results.Count != 0)
				{
					long     requestId = this._storage.MainStorage.MetaResults.GetMaxRequestId() + 1L;
					DateTime timestamp = DateTime.Now;

					histTable = PrepareHistoryData(resultSet);

					this._storage.MainStorage.MetaResults.SaveMeta(
						tnInfo,
						resultSet,
						requestId,
						timestamp
					);
				}
			}

			return histTable;
		}

		private Dictionary<TemplateNodeQueryInfo, DataTable> PrepareHistoryData(QueryResultSet results)
		{
			Dictionary<TemplateNodeQueryInfo, DataTable> tablesMap =
				new Dictionary<TemplateNodeQueryInfo, DataTable>();

			foreach (QueryResult queryResult in results.Results)
			{
				TemplateNodeQueryInfo tnQueryInfo = queryResult.QueryInfo;

				foreach (SessionResult instanceResult in queryResult.SessionResults.Values)
				{
					if (instanceResult.ErrorInfo != null)
					{
						continue;
					}

					foreach (DatabaseResult dbResult in instanceResult.DatabaseResults.Values)
					{
						if (dbResult?.DataTables == null)
						{
							continue;
						}

						foreach (DataTable table in dbResult.DataTables)
						{
							if (!tablesMap.ContainsKey(tnQueryInfo))
							{
								tablesMap.Add(tnQueryInfo, table);
							}
						}
					}
				}
			}

			return tablesMap;
		}
	}
}
