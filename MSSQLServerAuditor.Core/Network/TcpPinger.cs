using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Common.Contracts;

namespace MSSQLServerAuditor.Core.Network
{
	public class TcpPinger : IPinger
	{
		public const int DefaultTimeout = 2000;

		private readonly string _machineName;
		private readonly int    _port;
		private readonly int    _timeout;
		
		public TcpPinger(
			string machineName,
			int    port,
			int    timeoutMillis = 0)
		{
			Check.Assert(timeoutMillis >= 0, "Timeout is less than zero.");

			this._timeout = timeoutMillis == 0
				? DefaultTimeout
				: timeoutMillis;

			this._machineName = machineName;
			this._port        = port;
		}

		public TcpPinger(string machineName, int port)
			: this(machineName, port, DefaultTimeout)
		{
		}

		public async Task<PingResult> PingAsync(CancellationToken cancellationToken)
		{
			return await Task.Run(
				() => PingPort(this._machineName, this._port, this._timeout),
				cancellationToken
			).ConfigureAwait(false);
		}

		private PingResult PingPort(string hostName, int port, int timeout)
		{
			Stopwatch stopwatch = new Stopwatch();

			using (TcpClient tcpClient = new TcpClient())
			{
				stopwatch.Start();

				IAsyncResult asyncResult = tcpClient.BeginConnect(hostName, port, null, null);
				WaitHandle   waitHandle  = asyncResult.AsyncWaitHandle;

				try
				{
					if (!waitHandle.WaitOne(timeout, false))
					{
						tcpClient.Close();
						throw new SocketException(10060); // timeout error
					}

					tcpClient.EndConnect(asyncResult);
				}
				catch (SocketException exc)
				{
					stopwatch.Stop();

					string errorStatus = exc.SocketErrorCode.ToString();

					return PingResult.Failed(
						errorStatus,
						exc.Message,
						-1
					);
				}
				finally
				{
					waitHandle.Close();
				}

				stopwatch.Stop();

				string status    = SocketError.Success.ToString();
				string ipAddress = ((IPEndPoint) tcpClient.Client.RemoteEndPoint).Address.ToString();

				return PingResult.Succeeded(
					status,
					ipAddress,
					stopwatch.ElapsedMilliseconds
				);
			}
		}
	}
}
