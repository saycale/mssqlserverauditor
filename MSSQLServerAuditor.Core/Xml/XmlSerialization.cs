﻿using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using MSSQLServerAuditor.Common.Utils;

namespace MSSQLServerAuditor.Core.Xml
{
	public static class XmlSerialization
	{
		public static T Deserialize<T>(XmlDocument xmlDocument, string root = null)
		{
			XmlSerializer xmlSerializer = CreateSerializer<T>(root);

			using (XmlReader xmlReader = new XmlNodeReader(xmlDocument))
			{
				return (T) xmlSerializer.Deserialize(xmlReader);
			}
		}

		public static T Deserialize<T>(string xmlFile, string root = null)
		{
			XmlSerializer xmlSerializer = CreateSerializer<T>(root);

			using (XmlReader xmlReader = XmlReader.Create(xmlFile, XmlDefaultSettings.Reader))
			{
				return (T) xmlSerializer.Deserialize(xmlReader);
			}
		}

		public static void Serialize<T>(string xmlFile, T serializable)
		{
			// serialize without any namespace
			XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
			ns.Add("", "");

			XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

			using (XmlWriter xmlWriter = XmlWriter.Create(xmlFile, XmlDefaultSettings.Writer))
			{
				xmlSerializer.Serialize(xmlWriter, serializable, ns);
			}
		}

		public static XmlDocument SerializeToXmlDocument<T>(T serializable)
		{
			// serialize without any namespace
			XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
			ns.Add("", "");

			XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));

			XmlDocument    xmlDoc    = new XmlDocument();
			XPathNavigator navigator = xmlDoc.CreateNavigator();

			using (XmlWriter xmlWriter = navigator.AppendChild())
			{
				xmlSerializer.Serialize(xmlWriter, serializable, ns);
			}

			return xmlDoc;
		}

		private static XmlSerializer CreateSerializer<T>(string root = null)
		{
			if (root == null)
			{
				return new XmlSerializer(typeof(T));
			}
			
			return new XmlSerializer(
				typeof(T),
				new XmlRootAttribute(root)
			);
		}
	}
}
