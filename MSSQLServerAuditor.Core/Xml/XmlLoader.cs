﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Security.Cryptography;
using System.Xml;
using MSSQLServerAuditor.Common.Collections;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Cryptography;
using MSSQLServerAuditor.Core.Cryptography.Xml;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Persistence.Storages;
using MSSQLServerAuditor.Core.Reporting;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Metadata;
using NLog;

namespace MSSQLServerAuditor.Core.Xml
{
	public class XmlLoader : ITemplateLoader, IQueryLoader, IPreprocessorLoader
	{
		private class XmlData
		{
			public XmlData(DateTime updateTime, XmlDocument content)
			{
				UpdateTime = updateTime;
				Content    = content;
			}

			public DateTime    UpdateTime { get; set; }
			public XmlDocument Content    { get; set; }
		}

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		public static XmlLoader Create(IAppSettings appSettings, StorageType storageType)
		{
			string scriptsDirectoryPattern = appSettings.System.ScriptsDirectory;

			if (string.IsNullOrWhiteSpace(scriptsDirectoryPattern))
			{
				throw new ApplicationException("<ScriptsDirectory> folder is not defined in system settings file.");
			}

			if (string.IsNullOrWhiteSpace(appSettings.System.TemplateDirectory))
			{
				throw new ApplicationException("<TemplateDirectory> folder is not defined in system settings file.");
			}

			string scriptsDirectory = string.Format(
				scriptsDirectoryPattern,
				storageType.ToString().ToLower()
			);

			XmlLoader loader = new XmlLoader(
				appSettings.System.TemplateDirectory,
				scriptsDirectory
			);

			// check if encryption key is defined
			if (!string.IsNullOrWhiteSpace(EnvironmentProperties.PrivateKey))
			{
				// configure xml decryption
				RSA rsa = new RSACryptoServiceProvider();
				rsa.FromXmlString(EnvironmentProperties.PrivateKey);

				loader.Encryptor = new XmlEncryptor(rsa);
			}

			// check if signature key is defined
			if (!string.IsNullOrWhiteSpace(EnvironmentProperties.PublicKey))
			{
				loader.SignatureVerifier = new SignatureVerifier(EnvironmentProperties.PublicKey);
			}

			return loader;
		}

		private readonly MemoryCache _xmlCache;

		public XmlLoader(string templateDirectory, string scriptsDirectory)
		{
			TemplateDirectory = templateDirectory;
			ScriptsDirectory  = scriptsDirectory;

			NameValueCollection cacheConfig = new NameValueCollection
			{
				{"PollingInterval",               "00:05:00"},
				{"PhysicalMemoryLimitPercentage", "0"},
				{"CacheMemoryLimitMegabytes",     "30"}
			};

			this._xmlCache = new MemoryCache("FilesCache", cacheConfig);

			EnableCaching = true;
		}

		public XmlEncryptor      Encryptor         { get; set; }
		public SignatureVerifier SignatureVerifier { get; set; }
		public string            TemplateDirectory { get; set; }
		public string            ScriptsDirectory  { get; set; }
		public bool              EnableCaching     { get; set; }

		public List<QueryInfo> LoadQueries(TemplateNodeQueryInfo tnQueryInfo)
		{
			return LoadQueries(tnQueryInfo.QueryFileName)
				.Where(q => q.Name == tnQueryInfo.QueryName)
				.ToList();
		}

		public List<QueryInfo> LoadQueries(string xmlQueryFile)
		{
			List<QueryInfo> queries = new List<QueryInfo>();

			Log.Info($"Resolving query file: '{xmlQueryFile}'");

			// load info about queries list from the file
			string    path      = Path.Combine(TemplateDirectory, xmlQueryFile);
			QueryRoot queryRoot = DeserializeFile<QueryRoot>(path);

			List<QueryListInfo> queryLists = queryRoot.Queries.ToList();
			
			foreach (QueryListInfo queryList in queryLists)
			{
				ConnectionType connectionType = queryList.ConnectionType;

				foreach (QueryInfoExt rawQuery in queryList.Queries)
				{
					QueryInfo queryInfo;
					if (rawQuery.IsExternal)
					{
						// query body is defined in external file
						string filePath = Path.Combine(ScriptsDirectory, rawQuery.File);

						if (!File.Exists(filePath))
						{
							throw new FileNotFoundException(
								$"External script file '{rawQuery.File}' does not exist"
							);
						}

						QueryListExt extQueryList = DeserializeFile<QueryListExt>(filePath);

						// find query by name
						queryInfo = extQueryList.Queries.Find(
							q => rawQuery.Query == q.Name
						);

						if (queryInfo == null)
						{
							throw new ApplicationException(
								$"External script file '{rawQuery.File} does not contain query '{rawQuery.Query}'"
							);
						}

						queryInfo.Id             = rawQuery.Id;
						queryInfo.Name           = rawQuery.Name;
						queryInfo.Namespace      = rawQuery.Namespace;
						queryInfo.SaveResults    = rawQuery.SaveResults;
						queryInfo.Scope          = rawQuery.Scope;
						queryInfo.ConnectionType = connectionType;
					}
					else
					{
						queryInfo                = rawQuery;
						queryInfo.ConnectionType = connectionType;
					}

					UpdateQueryItemsParent(queryInfo.DatabaseSelect, queryInfo);
					UpdateQueryItemsParent(queryInfo.Items,          queryInfo);
					UpdateQueryItemsParent(queryInfo.GroupSelect,    queryInfo);

					ResolveFillStatements(queryInfo);
					ResolveEtlStatements(queryInfo);

					queries.Add(queryInfo);
				}
			}

			if (SignatureVerifier != null)
			{
				QueryVerificationResult verificationResult = queries.Verify(SignatureVerifier);

				if (!verificationResult.IsValid)
				{
					throw new QueryVerificationFailedException(verificationResult);
				}
			}

			return queries;
		}

		public List<TemplateFile> LoadTemplates()
		{
			List<TemplateFile> templates = new List<TemplateFile>();

			DirectoryInfo directoryInfo = new DirectoryInfo(
				Path.Combine(AppDomain.CurrentDomain.BaseDirectory, TemplateDirectory)
			);

			if (directoryInfo.Exists)
			{
				try
				{
					FileInfo[] files = directoryInfo.GetFiles("*.xml");

					foreach (FileInfo file in files)
					{
						try
						{
							TemplateInfo content      = LoadTemplate(file.FullName);
							TemplateFile templateFile = new TemplateFile(
								content,
								file.FullName,
								false
							);

							templates.Add(templateFile);
						}
						catch (Exception ex)
						{
							Log.Error(ex, $"Failed to load template from the file '{file.FullName}'");
						}
					}
				}
				catch (Exception ex)
				{
					Log.Error(ex, $"Failed to load templates from the folder '{TemplateDirectory}'");
				}
			}
			else
			{
				Log.Error($"Folder '{TemplateDirectory}' does not exist");
			}

			return templates;
		}

		public TemplateInfo LoadTemplate(string xmlFile)
		{
			TemplateInfo template = DeserializeFile<TemplateInfo>(xmlFile);
			foreach (TemplateNodeInfo tnInfo in template.Nodes)
			{
				tnInfo.UpdateRelations();
			}

			return template;
		}

		public List<PreprocessorArea> LoadPreprocessors(string procFilePath)
		{
			Check.NotNullOrEmpty(procFilePath, nameof(procFilePath));

			if (File.Exists(procFilePath))
			{
				XmlDocument xslDoc = GetXml(procFilePath);

				try
				{
					List<PreprocessorArea> areas = PreprocessorParser.ParseXml(xslDoc);

					foreach (PreprocessorArea area in areas)
					{
						area.Validate();
					}

					return areas;
				}
				catch (Exception ex)
				{
					Log.Error(ex, $"Error when parsing preprocessor file '{procFilePath}'");
					throw;
				}
			}

			Log.Error($"File '{procFilePath}' does not exist");

			return Lists.Empty<PreprocessorArea>();
		}

		public List<QueryInfo> GetQueries(string scriptPath, StorageType storageType)
		{
			try
			{
				PostBuildScriptRoot buildScriptRoot = DeserializeFile<PostBuildScriptRoot>(scriptPath);

				List<PostBuildScriptListInfo> scriptLists = buildScriptRoot.BuildScripts
					.Where(scriptList => scriptList.StorageType == storageType)
					.ToList();

				List<QueryInfo> scripts = new List<QueryInfo>();
				foreach (PostBuildScriptListInfo scriptList in scriptLists)
				{
					scripts.AddRange(scriptList.Queries);
				}

				return scripts;
			}
			catch (Exception exc)
			{
				Log.Error(exc, $"Error loading queries from file: {scriptPath}");
				throw;
			}
		}

		public List<string> GetScripts(string scriptPath, StorageType storageType)
		{
			List<string> scripts = new List<string>();

			if (string.IsNullOrWhiteSpace(scriptPath))
			{
				return scripts;
			}

			List<QueryInfo> queries = GetQueries(scriptPath, storageType);

			foreach (QueryInfo query in queries)
			{
				foreach (QueryItemInfo queryItem in query.Items)
				{
					QueryItemInfo item = queryItem;

					if (!string.IsNullOrWhiteSpace(item.Text))
					{
						scripts.Add(item.Text);
					}
				}
			}

			return scripts;
		}

		private void ResolveFillStatements(QueryInfo queryInfo)
		{
			if (queryInfo.FillStatements != null)
			{
				bool updateStatements = false;
				List<FillStatement> resolvedStatements = new List<FillStatement>();

				foreach (FillStatement statement in queryInfo.FillStatements)
				{
					if (!string.IsNullOrEmpty(statement.File))
					{
						string scriptPath = Path.Combine(ScriptsDirectory, statement.File);

						if (!File.Exists(scriptPath))
						{
							throw new FileNotFoundException($"External script file '{scriptPath}' does not exist");
						}

						ScriptListInfo sqlScripts = DeserializeFile<ScriptListInfo>(scriptPath);
						ScriptInfo     script     = sqlScripts.Scripts.Find(s => s.Name.Equals(statement.Query));

						if (script == null)
						{
							throw new ApplicationException(
								$"External script file '{scriptPath} does not contain statement '{statement.Query}'");
						}

						statement.Text      = script.Text;
						statement.Signature = script.Signature;

						updateStatements = true;
					}

					resolvedStatements.Add(statement);
				}

				if (updateStatements)
				{
					queryInfo.FillStatements = resolvedStatements;
				}
			}
		}

		private void ResolveEtlStatements(QueryInfo queryInfo)
		{
			if (queryInfo.ETLStatements != null)
			{
				bool updateStatements = false;
				List<ETLStatement> resolvedStatements = new List<ETLStatement>();

				foreach (ETLStatement statement in queryInfo.ETLStatements)
				{
					ETLStatement statementCopy = statement.XmlClone();
					statementCopy.ETLScripts.Clear();

					if (!string.IsNullOrEmpty(statement.File))
					{
						// get script directory parent
						string scriptsDirPattern = Directory.GetParent(ScriptsDirectory).ToString();
						
						// iterate through supported ETL connections
						foreach (ConnectionType connectionType in new[] { ConnectionType.MSSQL, ConnectionType.SQLite })
						{
							string scriptPath = Path.Combine(scriptsDirPattern, connectionType.ToString().ToLower(), statement.File);

							if (!File.Exists(scriptPath))
							{
								Log.Warn($"External script file '{scriptPath}' does not exist");
								continue;
							}

							ETLScriptList etlScripts = DeserializeFile<ETLScriptList>(scriptPath);
							ETLScript     script     = etlScripts.ETLScripts.Find(s => s.Name.Equals(statement.Query));

							if (script == null)
							{
								throw new ApplicationException(
									$"External script file '{scriptPath} does not contain statement '{statement.Query}'");
							}

							script.ConnectionType = connectionType;
							statementCopy.ETLScripts.Add(script);
						}

						updateStatements = true;
						resolvedStatements.Add(statementCopy);
					}
					else
					{
						resolvedStatements.Add(statement);
					}
				}

				if (updateStatements)
				{
					queryInfo.ETLStatements = resolvedStatements;
				}
			}
		}

		private void UpdateQueryItemsParent(
			List<QueryItemInfo> queryItems,
			QueryInfo           parentQuery
		)
		{
			foreach (QueryItemInfo queryItem in queryItems)
			{
				queryItem.Parent = parentQuery;
				foreach (QueryItemInfo childGroup in queryItem.ChildGroups)
				{
					childGroup.Parent = parentQuery;
				}
			}
		}

		private T DeserializeFile<T>(string filePath)
		{
			XmlDocument xmlDocument = GetXml(filePath);

			return XmlSerialization.Deserialize<T>(xmlDocument);
		}

		private XmlDocument GetXml(string filePath)
		{
			if (EnableCaching)
			{
				FileInfo fileInfo   = new FileInfo(filePath);
				DateTime lastUpdate = fileInfo.LastWriteTimeUtc;

				XmlData xmlDoc = GetFromCache(
					filePath,
					lastUpdate,
					() => LoadXmlData(filePath, lastUpdate)
				);

				return xmlDoc.Content;
			}

			return LoadXmlDocument(filePath);
		}

		private XmlData LoadXmlData(string filePath, DateTime lastUpdate)
		{
			XmlDocument xmlDocument = LoadXmlDocument(filePath);

			XmlData xmlData = new XmlData(
				lastUpdate,
				xmlDocument
			);

			return xmlData;
		}

		private XmlDocument LoadXmlDocument(string filePath)
		{
			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.Load(filePath);

			// decrypt xml document (if decryptor is defined)
			Encryptor?.Decrypt(xmlDocument, "rsaKey");

			return xmlDocument;
		}

		private XmlData GetFromCache(string filename, DateTime lastUpdate, Func<XmlData> xmlLoadFactory)
		{
			Lazy<XmlData> xmlLazy = new Lazy<XmlData>(xmlLoadFactory);
			
			Lazy<XmlData> cachedLazy = (Lazy<XmlData>) this._xmlCache.AddOrGetExisting(
				filename,
				xmlLazy,
				ObjectCache.InfiniteAbsoluteExpiration
			);

			if (cachedLazy != null)
			{
				XmlData cachedXml = cachedLazy.Value;
				if (lastUpdate > cachedXml.UpdateTime)
				{
					// cached file was updated from outside
					// update cached value
					this._xmlCache.Set(
						filename,
						xmlLazy,
						ObjectCache.InfiniteAbsoluteExpiration
					);

					return xmlLazy.Value;
				}

				// return chached xml
				return cachedXml;
			}

			return xmlLazy.Value;
		}
	}
}
