﻿using System.Collections.Generic;
using MSSQLServerAuditor.Core.Cryptography.Xml;
using MSSQLServerAuditor.Core.Domain;

namespace MSSQLServerAuditor.Core.Xml
{
	public interface IQueryLoader
	{
		XmlEncryptor Encryptor { get; set; }

		List<QueryInfo> LoadQueries(TemplateNodeQueryInfo tnQueryInfo);
		List<QueryInfo> LoadQueries(string xmlQueryFile);
	}
}
