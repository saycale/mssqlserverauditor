﻿using System.Collections.Generic;
using MSSQLServerAuditor.Core.Cryptography.Xml;
using MSSQLServerAuditor.Core.Reporting;

namespace MSSQLServerAuditor.Core.Xml
{
	public interface IPreprocessorLoader
	{
		XmlEncryptor Encryptor { get; set; }

		List<PreprocessorArea> LoadPreprocessors(string procFilePath);
	}
}
