using System;
using System.Collections.Generic;
using SqlCodeGuardAPI;

namespace MSSQLServerAuditor.Core.CodeGuard
{
	public class CodeGuardCompleteEventArgs: EventArgs
	{
		public CodeGuardCompleteEventArgs(IEnumerable<Issue> issues)
		{
			this.Issues = issues;
		}

		public IEnumerable<Issue> Issues { get; set; }
	}
}
