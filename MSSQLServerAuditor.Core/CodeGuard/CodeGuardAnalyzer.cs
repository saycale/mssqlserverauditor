﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using MSSQLServerAuditor.Core.Domain;
using NLog;
using SqlCodeGuardAPI;

namespace MSSQLServerAuditor.Core.CodeGuard
{
	internal class CodeGuardAnalyzer
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly API _codeGuard;

		internal event EventHandler<CodeGuardProcessEventArgs>  OnReadObject;
		internal event EventHandler<CodeGuardProcessEventArgs>  OnModuleProcessed;
		internal event EventHandler<CodeGuardCompleteEventArgs> OnAnalyseComplete;
		internal event EventHandler<EventArgs>                  OnLoadComplete;

		public CodeGuardAnalyzer()
		{
			this._codeGuard = new API();
		}

		public DataTable[] ReadSqlCodeGuardResult(
			TemplateNodeSqlGuardQueryInfo guardQueryInfo,
			DataTable                     queryTable,
			List<ParameterValue>          userParams
		)
		{
			DataTable summaryTable = new DataTable();

			summaryTable.Columns.Add("SCGObject");
			summaryTable.Columns.Add("SCGErrorRows");

			foreach (ParameterValue parameterValue in guardQueryInfo.ParameterValues)
			{
				summaryTable.Columns.Add(parameterValue.Name);
			}

			foreach (ParameterValue parameterValue in userParams)
			{
				summaryTable.Columns.Add(parameterValue.Name);
			}

			DataTable table = new DataTable();

			table.Columns.Add("SCGErrorCode");
			table.Columns.Add("SCGRow");
			table.Columns.Add("SCGColumn");
			table.Columns.Add("SCGMessage");

			foreach (ParameterValue parameterValue in guardQueryInfo.ParameterValues)
			{
				table.Columns.Add(parameterValue.Name);
			}

			foreach (ParameterValue parameterValue in userParams)
			{
				table.Columns.Add(parameterValue.Name);
			}

			foreach (DataRow row in queryTable.Rows)
			{
				string sqlcode = row[guardQueryInfo.QueryCodeColumn].ToString();
				string result = ProcessSqlQuery(
					sqlcode,
					guardQueryInfo.IncludedIssue,
					guardQueryInfo.ExcludedIssue
				);

				StringBuilder errorRows = new StringBuilder();
				string[] statements = result.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

				foreach (string statement in statements)
				{
					DataRow newRow = table.NewRow();

					int start = statement.IndexOf("(", StringComparison.Ordinal);
					int end = start < 0 ? 0 : statement.IndexOf(")", start, StringComparison.Ordinal);

					if (start > -1 && end > 0)
					{
						newRow["SCGErrorCode"] = statement.Substring(start + 1, end - 1);
					}

					newRow["SCGMessage"] = statement.Substring(end + 1);

					string[] position   = Regex.Match(statement, "([0-9])+:([0-9])+").Value.Split(':');
					newRow["SCGRow"]    = position[1];
					newRow["SCGColumn"] = position[0];

					errorRows.AppendFormat(",{0}", position[1]);

					foreach (ParameterValue parameterValue in guardQueryInfo.ParameterValues)
					{
						newRow[parameterValue.Name] = row[parameterValue.UserValue ?? parameterValue.DefaultValue];
					}

					foreach (ParameterValue parameterValue in userParams)
					{
						newRow[parameterValue.Name] = row[parameterValue.UserValue ?? parameterValue.DefaultValue];
					}

					table.Rows.Add(newRow);
				}

				if (!guardQueryInfo.AddSummary)
				{
					continue;
				}

				DataRow newSummaryRow = summaryTable.NewRow();

				newSummaryRow["SCGObject"]    = row[guardQueryInfo.QueryObjectColumn];
				newSummaryRow["SCGErrorRows"] = errorRows.Length > 0 ? errorRows.Remove(0, 1).ToString() : String.Empty;

				summaryTable.Rows.Add(newSummaryRow);
			}

			if (!guardQueryInfo.AddSummary)
			{
				return new[] { table };
			}

			return new[] { table, summaryTable };
		}

		internal void ProcessAllDatabase(string server, string database)
		{
			try
			{
				this._codeGuard.Reset();

				this._codeGuard.TabSize = 4;

				this._codeGuard.Connect(server, database);

				this._codeGuard.OnReadObject      = InnerOnReadObject;
				this._codeGuard.OnModuleProcessed = InnerOnModuleProcessed;
				this._codeGuard.OnAnalyseComplete = InnerOnAnalyseComplete;
				this._codeGuard.OnLoadComplete    = InnerOnLoadComplete;

				this._codeGuard.LoadObjects();

				if (!this._codeGuard.AbortRequested)
				{
					this._codeGuard.Execute();
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex);

				throw;
			}
		}

		internal string ProcessSqlQuery(
			string sql,
			string include           = "ALL",
			string exclude           = "",
			bool   includeComplexity = false)
		{
			if (string.IsNullOrEmpty(include))
			{
				include = "ALL";
			}

			this._codeGuard.IncludeIssue(include);

			if (!string.IsNullOrEmpty(exclude))
			{
				this._codeGuard.ExcludeIssue(exclude);
			}

			bool          unparsed;
			StringBuilder result = new StringBuilder();
			List<Issue>   issues = this._codeGuard.GetIssues(sql, out unparsed);

			if (unparsed)
			{
				Log.Error(
					"SQLCodeGuard: Unparsed! Issue list may be incomplete or wrong. Sql starts with: {0}",
					sql.Take(30)
				);
			}

			foreach (Issue i in issues)
			{
				result.AppendLine(
					$"({i.ErrorCode}){i.ErrorText} at {i.Column}:{i.Line} ({i.ErrorMessage})"
				);
			}

			if (includeComplexity)
			{
				double complexity;
				int statementCount;

				this._codeGuard.GetComplexity(
					sql,
					out unparsed,
					out complexity,
					out statementCount
				);

				result.AppendLine(
					$"\nComplexity: {complexity}, Statement Count: {statementCount}"
				);
			}

			return result.ToString();
		}

		private void InnerOnReadObject(DatabaseObject obj, int total, int current)
		{
			OnReadObject?.Invoke(this._codeGuard, new CodeGuardProcessEventArgs(obj, total, current));
		}

		private void InnerOnModuleProcessed(DatabaseObject obj, int current, int total)
		{
			OnModuleProcessed?.Invoke(this._codeGuard, new CodeGuardProcessEventArgs(obj, total, current));
		}

		private void InnerOnAnalyseComplete(object sender, EventArgs e)
		{
			OnAnalyseComplete?.Invoke(this._codeGuard, new CodeGuardCompleteEventArgs(this._codeGuard.FoundIssues()));
		}

		private void InnerOnLoadComplete()
		{
			OnLoadComplete?.Invoke(this._codeGuard, new EventArgs());
		}
	}
}
