﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using MSSQLServerAuditor.Common.Collections;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Common.Utils;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Reporting;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Core.Xml;
using NLog;

namespace MSSQLServerAuditor.Core.Notification
{
	public class NotificationService
	{
		private string DefaultSubject = "MSSQLServerAuditor notification";

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly IAppSettings        _appSettings;
		private readonly IPreprocessorLoader _preprocessorLoader;
		private readonly IPreprocessManager  _preprocessManager;

		public NotificationService(
			IAppSettings        appSettings,
			IPreprocessorLoader preprocessorLoader,
			IPreprocessManager  preprocessManager
		)
		{
			this._appSettings        = appSettings;
			this._preprocessorLoader = preprocessorLoader;
			this._preprocessManager  = preprocessManager;
		}

		public void SendNotification(
			string     recipients,
			ReportData reportData
		)
		{
			if (NotificationSettings == null)
			{
				throw new ArgumentNullException(
					nameof(NotificationSettings), 
					@"Email notification settings are absent or empty"
				);
			}

			string subject;
			List<EmailContent> contents = new List<EmailContent>();

			try
			{
				TemplateNodeInfo tnInfo = reportData.TemplateNodeInfo;
				string procFile = tnInfo.ProcessorFile;

				string procPath = Path.Combine(
					this._appSettings.System.TemplateDirectory,
					"Templates",
					procFile
				);

				List<PreprocessorArea> preprocessorAreas = this._preprocessorLoader
					.LoadPreprocessors(procPath);

				bool         handled = false;
				List<string> titles  = new List<string>();

				if (preprocessorAreas != null)
				{
					foreach (PreprocessorArea preprocessorArea in preprocessorAreas)
					{
						IPreprocessor handler = this._preprocessManager
							.ResolvePreprocessor(preprocessorArea.Preprocessor);

						if (handler != null)
						{
							string title = handler.GetTitle(reportData, preprocessorArea);

							foreach (PreprocessorConfig preprocessor in preprocessorArea.PreprocessorsConfigs)
							{
								handler.Prepare(reportData, preprocessor);

								EmailContent mailContent = handler.CreateMailContent(reportData, preprocessor);

								contents.Add(mailContent);

								handled = true;
							}

							titles.Add(title);
						}
					}
				}

				subject = string.Join(", ", titles.Where(title => !string.IsNullOrEmpty(title)));

				if (!handled)
				{ 
					EmailContent defaultContent = new EmailContent
					{
						Subject   = DefaultSubject,
						Message   = reportData.XmlReport.XmlString,
						MediaType = MediaTypeNames.Text.Xml
					};

					contents.Add(defaultContent);
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Failed to send notification");
				throw;
			}

			if (string.IsNullOrWhiteSpace(subject))
			{
				subject = contents
					.Select(c => c.Subject)
					.Where(s => !string.IsNullOrWhiteSpace(s))
					.Join(", ");
			}

			SendMessage(subject, contents, recipients);
		}

		private void SendMessage(
			string             subject,
			List<EmailContent> contents,
			string             recipients
		)
		{
			SmtpSettings smtp = NotificationSettings.SmtpSettings;

			SmtpClient client = new SmtpClient(smtp.ServerAddress, smtp.Port)
			{
				DeliveryMethod = SmtpDeliveryMethod.Network
			};

			if (smtp.AuthenticationRequired)
			{
				client.EnableSsl             = smtp.SslEnabled;
				client.UseDefaultCredentials = false;

				client.Credentials = new NetworkCredential(
					smtp.SmtpCredentials != null ? smtp.SmtpCredentials.UserName : string.Empty,
					smtp.SmtpCredentials != null ? smtp.SmtpCredentials.Password : string.Empty
				);
			}
			else
			{
				client.UseDefaultCredentials = true;
			}

			MailMessage message = new MailMessage
			{
				From = new MailAddress(
					smtp.SenderEmail,
					"MSSQLServerAuditor"
				)
			};

			List<string> recipientList;

			if (!recipients.IsNullOrEmpty())
			{
				string[] recipientArray = recipients.Split(
					new[] { ";" },
					StringSplitOptions.RemoveEmptyEntries
				);

				recipientList = recipientArray.ToList();
			}
			else
			{
				string settingsRecipient = NotificationSettings.RecipientEmail;

				if (string.IsNullOrEmpty(settingsRecipient))
				{
					throw new ArgumentException("Recipient email address is not specified in notification settings");
				}

				recipientList = Lists.Of(settingsRecipient);
			}

			if (recipients.IsNullOrEmpty())
			{
				throw new ArgumentException("Recipients are not specified");
			}

			foreach (string recipient in recipientList)
			{
				message.To.Add(recipient);
			}

			message.BodyEncoding    = Encoding.UTF8;
			message.Subject         = StringUtils.FirstNonEmpty(subject, "MSSQLServerAuditor");
			message.SubjectEncoding = Encoding.UTF8;

			AlternateView htmlView = AlternateView.CreateAlternateViewFromString(
				CombineHtml(contents),
				Encoding.UTF8,
				MediaTypeNames.Text.Html
			);

			message.AlternateViews.Add(htmlView);

			try
			{
				client.Send(message);
			}
			catch (Exception ex)
			{
				string errMess = ex.Message;

				if (ex.InnerException != null)
				{
					errMess += " " + ex.InnerException.Message;
				}

				Log.Error($"[{message}] {errMess}");
				throw;
			}
		}

		private NotificationSettings NotificationSettings =>
			this._appSettings?.User?.NotificationSettings;

		private string CombineHtml(List<EmailContent> contents)
		{
			return contents
				.Select(content => content.ToHtml())
				.ToList()
				.Join("\n<br>\n");
		}
	}
}
