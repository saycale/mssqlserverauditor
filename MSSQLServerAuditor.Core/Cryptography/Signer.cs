﻿using System;
using System.Security.Cryptography;
using System.Text;
using MSSQLServerAuditor.Common.Extensions;

namespace MSSQLServerAuditor.Core.Cryptography
{
	public class Signer
	{
		private readonly RSACryptoServiceProvider _rsa;

		public Signer(string xmlPrivateKey)
		{
			this._rsa = new RSACryptoServiceProvider();
			this._rsa.FromXmlString(xmlPrivateKey);
		}

		/// <summary>
		/// Gets signature of a text.
		/// </summary>
		/// <param name="data">Text data.</param>
		/// <returns>Signature of data hash</returns>
		public string Sign(string data)
		{
			byte[] bytes = data != null
				? Encoding.UTF8.GetBytes(data.RemoveWhitespaces())
				: new byte[0];

			SHA1Managed sha = new SHA1Managed();

			byte[] shaHash = sha.ComputeHash(bytes);
			byte[] result  = this._rsa.SignHash(shaHash, null);

			return Convert.ToBase64String(result);
		}
	}
}
