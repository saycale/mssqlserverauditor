﻿namespace MSSQLServerAuditor.Core.Cryptography
{
	public interface ICryptoService
	{
		string Encrypt(string plainText);
		string Decrypt(string encryptedText);
	}
}
