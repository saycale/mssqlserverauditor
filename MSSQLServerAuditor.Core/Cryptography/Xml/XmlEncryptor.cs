﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Xml;
using MSSQLServerAuditor.Common.Contracts;

namespace MSSQLServerAuditor.Core.Cryptography.Xml
{
	public class XmlEncryptor
	{
		private readonly RSA _alg;

		public XmlEncryptor(RSA alg)
		{
			Check.NotNull(alg, nameof(alg));

			this._alg = alg;
		}

		/// <summary>
		/// Encrypt XML document's root element.
		/// </summary>
		/// <param name="xmlSourceFile">XML document source file.</param>
		/// <param name="xmlDestinationFile">XML document destination file.</param>
		/// <param name="keyName">Key name.</param>
		public void Encrypt(string xmlSourceFile, string xmlDestinationFile, string keyName)
		{
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load(xmlSourceFile);

			XmlElement root = xmlDoc.DocumentElement;
			if (root == null)
			{
				throw new XmlException($"XML document '{xmlSourceFile}' does not contain root element");
			}

			Encrypt(xmlDoc, root.Name, keyName);

			xmlDoc.Save(xmlDestinationFile);
		}

		/// <summary>
		/// Encrypt XML document's root element.
		/// </summary>
		/// <param name="xmlDoc">XML document source.</param>
		/// <param name="xmlDestinationFile">XML document destination file.</param>
		/// <param name="keyName">Key name.</param>
		public void EncryptToFile(XmlDocument xmlDoc, string xmlDestinationFile, string keyName)
		{
			XmlElement root = xmlDoc.DocumentElement;
			if (root == null)
			{
				throw new XmlException("XML document does not contain root element");
			}

			Encrypt(xmlDoc, root.Name, keyName);

			xmlDoc.Save(xmlDestinationFile);
		}

		/// <summary>
		/// Encrypt XML document.
		/// </summary>
		/// <param name="xmlDoc">XML document.</param>
		/// <param name="xmlElement">XML element to encrypt.</param>
		/// <param name="keyName">Key name.</param>
		public void Encrypt(XmlDocument xmlDoc, string xmlElement, string keyName)
		{
			Check.NotNull(xmlDoc,     nameof(xmlDoc));
			Check.NotNull(xmlElement, nameof(xmlElement));

			// Find the element by name
			XmlElement inputElement = xmlDoc.GetElementsByTagName(xmlElement)[0] as XmlElement;

			// If the element was not found, throw an exception.
			if (inputElement == null)
			{
				throw new Exception("The specified element was not found.");
			}

			// Create a 256 bit Rijndael session key (random symmetric key).
			using (RijndaelManaged sessionKey = new RijndaelManaged { KeySize = 256 })
			{
				// Encrypt the session key
				EncryptedKey encKey = new EncryptedKey
				{
					CipherData       = new CipherData(EncryptedXml.EncryptKey(sessionKey.Key, this._alg, false)),
					EncryptionMethod = new EncryptionMethod(EncryptedXml.XmlEncRSA15Url)
				};

				// specify key name in encrypted session key.
				encKey.KeyInfo.AddClause(new KeyInfoName { Value = keyName });

				// create EncryptedXml object and use it to encrypt xml element 
				EncryptedXml encXml = new EncryptedXml(xmlDoc);

				// Encrypt the element using the session key.
				byte[] encBytes = encXml.EncryptData(inputElement, sessionKey, false);

				// Create an EncryptedData object and populate it.
				EncryptedData encData = new EncryptedData
				{
					// Specify the namespace URI for XML encryption elements.
					Type = EncryptedXml.XmlEncElementUrl,

					// Specify the namespace URI for the AES algorithm.
					EncryptionMethod = new EncryptionMethod(EncryptedXml.XmlEncAES256Url),

					// Set the CipherData element to the value of the encrypted XML element.
					CipherData = new CipherData(encBytes)
				};

				encData.KeyInfo.AddClause(new KeyInfoEncryptedKey(encKey));

				// Replace the plaintext XML elemnt with an EncryptedData element.
				EncryptedXml.ReplaceElement(inputElement, encData, false);
			}
		}

		/// <summary>
		/// Decrypt XML document.
		/// </summary>
		/// <param name="xmlDoc">XML document to decrypt.</param>
		/// <param name="keyName">Key name.</param>
		public void Decrypt(XmlDocument xmlDoc, string keyName)
		{
			Check.NotNull(xmlDoc, nameof(xmlDoc));

			// Create a new EncryptedXml object.
			EncryptedXml encXml = new EncryptedXml(xmlDoc);

			// Add a key-name mapping.
			// This method can only decrypt documents that present the specified key name.
			encXml.AddKeyNameMapping(keyName, this._alg);

			encXml.DecryptDocument();
		}
	}
}
