﻿using System;
using System.Security.Cryptography;
using System.Text;
using MSSQLServerAuditor.Common.Extensions;

namespace MSSQLServerAuditor.Core.Cryptography
{
	public class SignatureVerifier
	{
		private readonly RSACryptoServiceProvider _rsa;

		public SignatureVerifier(string xmlPublicKey)
		{
			this._rsa = new RSACryptoServiceProvider();
			this._rsa.FromXmlString(xmlPublicKey);
		}

		/// <summary>
		/// Verifies data signature.
		/// </summary>
		/// <param name="data">Data.</param>
		/// <param name="signature">Data signature.</param>
		/// <returns>true, if the signature is correct.</returns>
		public bool Verify(string data, string signature)
		{
			byte[] bytes = data != null
				? Encoding.UTF8.GetBytes(data.RemoveWhitespaces())
				: new byte[0];

			SHA1Managed sha = new SHA1Managed();

			byte[] shaHash   = sha.ComputeHash(bytes);
			byte[] signBytes = Convert.FromBase64String(signature);

			return this._rsa.VerifyHash(shaHash, null, signBytes);
		}
	}
}
