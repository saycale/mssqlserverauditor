﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	/// <summary>
	/// Root for XML of post build scripts.
	/// </summary>
	[XmlRoot(ElementName = "root")]
	public class PostBuildScriptRoot
	{
		/// <summary>
		/// Post build scripts
		/// </summary>
		[XmlElement(ElementName = "sqlquery")]
		public List<PostBuildScriptListInfo> BuildScripts { get; set; }
	}
}
