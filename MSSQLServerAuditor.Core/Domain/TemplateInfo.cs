﻿using System.Collections.Generic;
using System.Xml.Serialization;
using MSSQLServerAuditor.Common.Extensions;

namespace MSSQLServerAuditor.Core.Domain
{
	/// <summary>
	/// Template definition
	/// </summary>
	[XmlRoot(ElementName = "MSSQLServerAuditorTemplate")]
	public class TemplateInfo
	{
		/// <summary>
		/// Template nodes
		/// </summary>
		[XmlElement(ElementName = "template")]
		public List<TemplateNodeInfo> Nodes { get; set; }

		/// <summary>
		/// Template identifier
		/// </summary>
		[XmlAttribute(AttributeName = "id")]
		public string Id { get; set; }

		/// <summary>
		/// Template startup node
		/// </summary>
		[XmlAttribute(AttributeName = "startuptemplateid")]
		public string StartupNodeId { get; set; }

		/// <summary>
		/// Template group queries
		/// </summary>
		[XmlElement(ElementName = "group-select")]
		public List<TemplateNodeQueryInfo> GroupQueries { get; set; }

		/// <summary>
		/// Template type (as string)
		/// </summary>
		[XmlAttribute(AttributeName = "type")]
		public string ModuleType { get; set; }

		/// <summary>
		/// Localization of the node
		/// </summary>
		[XmlElement(ElementName = "i18n")]
		public List<Translation> TemplateTitle { get; set; }

		[XmlArray(ElementName = "MainWindowTitle")]
		[XmlArrayItem(ElementName = "i18n")]
		public List<Translation> WindowTitle { get; set; }

		[XmlArray(ElementName = "TreeTitle")]
		[XmlArrayItem(ElementName = "i18n")]
		public List<Translation> TreeTitle { get; set; }

		public TemplateNodeInfo CreateRoot()
		{
			TemplateNodeInfo root = new TemplateNodeInfo
			{
				Title    = new List<Translation>(),
				Children = Nodes
			};

			foreach (Translation translation in TemplateTitle)
			{
				root.Title.Add(new Translation
				{
					Language     = translation.Language,
					Text         = translation.Text
				});
			}
			
			root.Queries             = new List<TemplateNodeQueryInfo>();
			root.GroupQueries        = GroupQueries ?? new List<TemplateNodeQueryInfo>();
			root.ConnectionQueries   = new List<TemplateNodeQueryInfo>();
			root.SqlCodeGuardQueries = new List<TemplateNodeSqlGuardQueryInfo>();

			NormalizeNode(root);
			root.Init();

			root.UpdateRelations();

			return root;
		}

		private static void NormalizeNode(TemplateNodeInfo node)
		{
			foreach (Translation title in node.Title)
			{
				title.Text = title.Text.RemoveWhitespaces();
			}

			foreach (TemplateNodeInfo child in node.Children)
			{
				NormalizeNode(child);
			}
		}
	}
}
