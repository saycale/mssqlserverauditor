﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using MSSQLServerAuditor.Core.Domain.Exceptions;

namespace MSSQLServerAuditor.Core.Domain
{
	public class TemplateNodeQueryInfo
	{
		/// <summary>
		/// Query file name
		/// </summary>
		[XmlAttribute(AttributeName = "file")]
		public string QueryFileName { get; set; }

		/// <summary>
		/// Query name in define filename
		/// </summary>
		[XmlAttribute(AttributeName = "name")]
		public string QueryName { get; set; }

		/// <summary>
		/// Query result hierarchy as "path" (chain if parent nodes) to node, containing result data, inside xml file.
		/// </summary>
		[XmlAttribute(AttributeName = "hierarchy")]
		public string Hierarchy { get; set; }

		[XmlAttribute(AttributeName = "defaultDataBaseField")]
		public string DatabaseForChildrenFieldName { get; set; }

		/// <summary>
		/// Parameter values
		/// </summary>
		[XmlElement(ElementName = "parameter")]
		public List<ParameterValue> ParameterValues { get; set; }

		/// <summary>
		/// Id for query (using as a part of table name)
		/// </summary>
		[XmlAttribute(AttributeName = "id")]
		public string Id { get; set; }

		[XmlAttribute(AttributeName = "connections-select-id")]
		public string ConnectionsSelectId { get; set; }

		[XmlAttribute(AttributeName = "IsHideTabs")]
		public bool HideTabs { get; set; }

		[XmlAttribute(AttributeName = "IsHideTypeColumn")]
		public bool HideTypeColumn { get; set; }

		[XmlElement(ElementName = "i18n")]
		public List<Translation> Title { get; set; }

		[XmlIgnore]
		public TemplateNodeInfo TemplateNode { get; set; }

		public TemplateNodeQueryInfo Clone()
		{
			TemplateNodeQueryInfo result = (TemplateNodeQueryInfo) MemberwiseClone();

			result.ParameterValues = ParameterValues.Select(pv => pv.Clone()).ToList();

			return result;
		}

		public TemplateNodeQueryInfo GetParentConnectionSelectQuery()
		{
			if (string.IsNullOrWhiteSpace(ConnectionsSelectId))
			{
				return null;
			}

			if (TemplateNode.Parent == null)
			{
				throw new InvalidTemplateException($"Query '{this}' has <connections-select-id> but has no parent node");
			}

			TemplateNodeQueryInfo result = TemplateNode.ConnectionQueries.FirstOrDefault(q => q.Id == ConnectionsSelectId);

			if (result == null)
			{
				throw new InvalidTemplateException($"Query '{this}' has <connections-select-id> = {ConnectionsSelectId} but there is no <connections-select> with such Id in parent node");
			}

			return result;
		}

		public override string ToString()
		{
			return $"QueryFileName = {QueryFileName} QueryName = {QueryName} [{string.Join(" ", ParameterValues.Select(pv => pv.ToString()))}]";
		}
	}
}
