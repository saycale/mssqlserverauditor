﻿using System;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.DataAccess.Entity.Models;

namespace MSSQLServerAuditor.Core.Domain.Mapping
{
	internal class ServerInstanceMapper
	{
		private readonly Mapper _mapper;

		internal ServerInstanceMapper(Mapper mapper)
		{
			Check.NotNull(mapper, nameof(mapper));

			this._mapper = mapper;
		}

		internal ServerInstance ToModel(DBServerInstance dbInstance)
		{
			Check.NotNull(dbInstance, nameof(dbInstance));

			// Do overload resolution at runtime
			dynamic        dynDbInstance = dbInstance;
			ServerInstance instance      = ToModelImpl(dynDbInstance);

			return instance;
		}

		internal DBServerInstance ToDb(ServerInstance instance)
		{
			Check.NotNull(instance, nameof(instance));

			// Do overload resolution at runtime
			dynamic          dynInstance = instance;
			DBServerInstance dbInstance  = ToDbImpl(dynInstance);

			return dbInstance;
		}

		private MssqlInstance ToModelImpl(DBServerInstanceMssql dbInstance)
		{
			MssqlInstance instance = new MssqlInstance
			{
				InstanceName     = dbInstance.InstanceName,
				Environment      = dbInstance.Environment,
				Host             = dbInstance.Host,
				Domain           = dbInstance.Domain,
				Version          = new InstanceVersion(dbInstance.Version),
				IsOdbc           = dbInstance.IsOdbc ?? false,
				ConnectionString = dbInstance.ConnectionString
			};

			CopyCommonDbProperties(instance, dbInstance);

			return instance;
		}

		private DBServerInstanceMssql ToDbImpl(MssqlInstance instance)
		{
			DBServerInstanceMssql dbInstance = new DBServerInstanceMssql
			{
				InstanceName     = instance.InstanceName,
				Environment      = instance.Environment,
				Host             = instance.Host,
				Domain           = instance.Domain,
				Version          = instance.Version.ToString(),
				IsOdbc           = instance.IsOdbc,
				ConnectionString = instance.ConnectionString
			};

			CopyCommonModelProperties(dbInstance, instance);

			return dbInstance;
		}

		private TeradataInstance ToModelImpl(DBServerInstanceTdsql dbInstance)
		{
			TeradataInstance instance = new TeradataInstance
			{
				InstanceName     = dbInstance.InstanceName,
				Environment      = dbInstance.Environment,
				Host             = dbInstance.Host,
				Domain           = dbInstance.Domain,
				Version          = new InstanceVersion(dbInstance.Version),
				IsOdbc           = dbInstance.IsOdbc ?? false,
				ConnectionString = dbInstance.ConnectionString
			};

			CopyCommonDbProperties(instance, dbInstance);

			return instance;
		}

		private DBServerInstanceTdsql ToDbImpl(TeradataInstance instance)
		{
			DBServerInstanceTdsql dbInstance = new DBServerInstanceTdsql
			{
				InstanceName     = instance.InstanceName,
				Environment      = instance.Environment,
				Host             = instance.Host,
				Domain           = instance.Domain,
				Version          = instance.Version.ToString(),
				IsOdbc           = instance.IsOdbc,
				ConnectionString = instance.ConnectionString
			};

			CopyCommonModelProperties(dbInstance, instance);

			return dbInstance;
		}

		private LdapInstance ToModelImpl(DBServerInstanceLdap dbInstance)
		{
			LdapInstance instance = new LdapInstance
			{
				ConnectionPath = dbInstance.ConnectionPath
			};

			CopyCommonDbProperties(instance, dbInstance);

			return instance;
		}

		private DBServerInstanceLdap ToDbImpl(LdapInstance instance)
		{
			DBServerInstanceLdap dbInstance = new DBServerInstanceLdap
			{
				ConnectionPath = instance.ConnectionPath
			};

			CopyCommonModelProperties(dbInstance, instance);

			return dbInstance;
		}

		private SystemConnectionInstance ToModelImpl(DBServerInstanceSystem dbInstance)
		{
			SystemConnectionInstance instance = new SystemConnectionInstance();

			CopyCommonDbProperties(instance, dbInstance);

			return instance;
		}

		private DBServerInstanceSystem ToDbImpl(SystemConnectionInstance instance)
		{
			DBServerInstanceSystem dbInstance = new DBServerInstanceSystem();

			CopyCommonModelProperties(dbInstance, instance);

			return dbInstance;
		}

		private EventLogInstance ToModelImpl(DBServerInstanceEventLog dbInstance)
		{
			EventLogInstance instance = new EventLogInstance
			{
				MachineName = dbInstance.MachineName
			};

			CopyCommonDbProperties(instance, dbInstance);

			return instance;
		}

		private DBServerInstanceEventLog ToDbImpl(EventLogInstance instance)
		{
			DBServerInstanceEventLog dbInstance = new DBServerInstanceEventLog
			{
				MachineName = instance.MachineName
			};

			CopyCommonModelProperties(dbInstance, instance);

			return dbInstance;
		}

		private InternalServerInstance ToModelImpl(DBServerInstanceInternal dbInstance)
		{
			if (dbInstance.LoginId == null)
			{
				throw new ArgumentException("Login id should not be null");
			}

			InternalServerInstance instance = new InternalServerInstance
			{
				ConnectionGroupSourceId = dbInstance.ConnectionGroupSourceId,
				ServerInstanceSourceId  = dbInstance.ServerInstanceSourceId,
				TemplateSourceId        = dbInstance.TemplateSourceId,
				LoginSourceId           = dbInstance.LoginId.Value
			};

			CopyCommonDbProperties(instance, dbInstance);

			return instance;
		}

		private DBServerInstanceInternal ToDbImpl(InternalServerInstance instance)
		{
			DBServerInstanceInternal dbInstance = new DBServerInstanceInternal
			{
				ConnectionGroupSourceId = instance.ConnectionGroupSourceId,
				ServerInstanceSourceId  = instance.ServerInstanceSourceId,
				TemplateSourceId        = instance.TemplateSourceId
			};

			CopyCommonModelProperties(dbInstance, instance);

			return dbInstance;
		}

		private NetworkInstance ToModelImpl(DBServerInstanceNetworkInformation dbInstance)
		{
			NetworkInstance instance = new NetworkInstance
			{
				Protocol = dbInstance.Protocol,
				Port     = dbInstance.Port,
				Timeout  = dbInstance.Timeout,
				Host     = dbInstance.Host
			};

			CopyCommonDbProperties(instance, dbInstance);

			return instance;
		}

		private DBServerInstanceNetworkInformation ToDbImpl(NetworkInstance instance)
		{
			DBServerInstanceNetworkInformation dbInstance = new DBServerInstanceNetworkInformation
			{
				Host     = instance.Host,
				Protocol = instance.Protocol,
				Port     = instance.Port,
				Timeout  = instance.Timeout
			};

			CopyCommonModelProperties(dbInstance, instance);

			return dbInstance;
		}

		private SqliteInstance ToModelImpl(DBServerInstanceSqlite dbInstance)
		{
			SqliteInstance instance = new SqliteInstance
			{
				DatabaseFilePath = dbInstance.DatabaseFilePath
			};

			CopyCommonDbProperties(instance, dbInstance);

			return instance;
		}

		private DBServerInstanceSqlite ToDbImpl(SqliteInstance instance)
		{
			DBServerInstanceSqlite dbInstance = new DBServerInstanceSqlite
			{
				DatabaseFilePath = instance.DatabaseFilePath
			};

			CopyCommonModelProperties(dbInstance, instance);

			return dbInstance;
		}

		private WmiInstance ToModelImpl(DBServerInstanceWmi dbInstance)
		{
			WmiInstance instance = new WmiInstance
			{
				Host           = dbInstance.Host,
				Authentication = dbInstance.Authentication,
				Authority      = dbInstance.Authority,
				Impersonation  = dbInstance.Impersonation
			};

			CopyCommonDbProperties(instance, dbInstance);

			return instance;
		}

		private DBServerInstanceWmi ToDbImpl(WmiInstance instance)
		{
			DBServerInstanceWmi dbInstance = new DBServerInstanceWmi
			{
				Authority      = instance.Authority,
				Impersonation  = instance.Impersonation,
				Authentication = instance.Authentication,
				Host           = instance.Host
			};

			CopyCommonModelProperties(dbInstance, instance);

			return dbInstance;
		}

		private void CopyCommonDbProperties(
			ServerInstance   instanceTarget,
			DBServerInstance dbInstanceSource
		)
		{
			instanceTarget.Id              = dbInstanceSource.Id;
			instanceTarget.ConnectionName  = dbInstanceSource.ConnectionName;
			instanceTarget.ConnectionAlias = dbInstanceSource.ConnectionAlias;
			instanceTarget.ConnectionType  = ConnectionTypeHelper.Parse(dbInstanceSource.ConnectionType.Name);
			instanceTarget.IsActive        = dbInstanceSource.IsActive;
			instanceTarget.IsDeleted       = dbInstanceSource.IsDeleted;
			instanceTarget.IsDynamic       = dbInstanceSource.IsDynamic ?? false;
			instanceTarget.Login           = this._mapper.FromDb(dbInstanceSource.Login);
		}

		private static void CopyCommonModelProperties(
			DBServerInstance dbInstanceTarget,
			ServerInstance   instanceSource
		)
		{
			dbInstanceTarget.Id              = instanceSource.Id;
			dbInstanceTarget.ConnectionName  = instanceSource.ConnectionName;
			dbInstanceTarget.ConnectionAlias = instanceSource.ConnectionAlias;
			dbInstanceTarget.IsActive        = instanceSource.IsActive;
			dbInstanceTarget.IsDeleted       = instanceSource.IsDeleted;
			dbInstanceTarget.IsDynamic       = instanceSource.IsDynamic;
			dbInstanceTarget.LoginId         = instanceSource.Login?.Id;
		}
	}
}
