﻿using System.Collections.Generic;
using MSSQLServerAuditor.Common;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Core.Cryptography;
using MSSQLServerAuditor.Core.Domain.Models;
using MSSQLServerAuditor.DataAccess.Entity.Models;

namespace MSSQLServerAuditor.Core.Domain.Mapping
{
	public class Mapper
	{
		private readonly ICryptoService       _cryptoService;
		private readonly ServerInstanceMapper _instanceMapper;

		public Mapper(ICryptoService cryptoService)
		{
			this._cryptoService  = cryptoService;
			this._instanceMapper = new ServerInstanceMapper(this);
		}

		public ConnectionGroup FromDb(DBConnectionGroup dbGroup)
		{
			Check.NotNull(dbGroup, nameof(dbGroup));

			ConnectionGroup group = new ConnectionGroup
			{
				Id             = dbGroup.Id,
				Name           = dbGroup.Name,
				ConnectionType = ConnectionTypeHelper.Parse(dbGroup.ConnectionType.Name),
				IsDirect       = dbGroup.IsDirectConnection ?? false
			};

			List<ServerInstance> instances = new List<ServerInstance>();
			foreach (DBGroupServer dbGroupServer in dbGroup.GroupServers)
			{
				DBServerInstance dbInstance = dbGroupServer.ServerInstance;
				if (dbInstance != null)
				{
					instances.Add(FromDb(dbInstance));
				}
			}

			group.ServerInstances = instances;

			return group;
		}

		public DBConnectionGroup ToDb(ConnectionGroup group)
		{
			Check.NotNull(group, nameof(group));

			DBConnectionGroup dbGroup = new DBConnectionGroup
			{
				Id                 = group.Id,
				Name               = group.Name,
				IsDirectConnection = group.IsDirect
			};

			return dbGroup;
		}

		
		public ServerInstance FromDb(DBServerInstance dbInstance)
		{
			Check.NotNull(dbInstance, nameof(dbInstance));

			return this._instanceMapper.ToModel(dbInstance);
		}

		public DBServerInstance ToDb(ServerInstance instance)
		{
			Check.NotNull(instance, nameof(instance));

			return this._instanceMapper.ToDb(instance);
		}

		public Login FromDb(DBLogin dbLogin)
		{
			Check.NotNull(dbLogin, nameof(dbLogin));

			string decryptedPwd = this._cryptoService.Decrypt(dbLogin.EncryptedPassword);
			Login login = new Login(
				dbLogin.Name,
				decryptedPwd,
				dbLogin.IsWinAuth
			)
			{
				Id = dbLogin.Id
			};

			return login;
		}

		public MetaResult FromDb(DBMetaResult dbMeta)
		{
			Check.NotNull(dbMeta, nameof(dbMeta));

			MetaResult meta = new MetaResult
			{
				Id           = dbMeta.Id,
				QueryId      = dbMeta.QueryId,
				QueryGroupId = dbMeta.QueryGroupId,
				SessionId    = dbMeta.SessionId,
				RequestId    = dbMeta.RequestId,
				RecordSets   = dbMeta.RecordSets,
				Rows         = dbMeta.Rows,
				ErrorCode    = dbMeta.ErrorCode,
				ErrorId      = dbMeta.ErrorId,
				ErrorMessage = dbMeta.ErrorMessage,
				DateCreated  = dbMeta.DateCreated,
				DateUpdated  = dbMeta.DateUpdated ?? DefaultValues.Date.Minimum
			};

			return meta;
		}

		public DBMetaResult ToDb(MetaResult meta)
		{
			Check.NotNull(meta, nameof(meta));

			DBMetaResult dbMeta = new DBMetaResult
			{
				Id           = meta.Id,
				QueryId      = meta.QueryId,
				QueryGroupId = meta.QueryGroupId,
				SessionId    = meta.SessionId,
				RequestId    = meta.RequestId,
				RecordSets   = meta.RecordSets,
				Rows         = meta.Rows,
				ErrorCode    = meta.ErrorCode,
				ErrorId      = meta.ErrorId,
				ErrorMessage = meta.ErrorMessage,
				DateCreated  = meta.DateCreated,
				DateUpdated  = meta.DateUpdated
			};

			return dbMeta;
		}

		public ETLResult FromDb(DBMetaEtlResult dbMeta)
		{
			Check.NotNull(dbMeta, nameof(dbMeta));

			ETLResult meta = new ETLResult
			{
				Id                     = dbMeta.Id,
				QueryId                = dbMeta.QueryId,
				TargetServerInstanceId = dbMeta.EtlServerInstanceId,
				Rows                   = dbMeta.Rows,
				ErrorCode              = dbMeta.ErrorCode,
				ErrorId                = dbMeta.ErrorId,
				ErrorMessage           = dbMeta.ErrorMessage,
				DateCreated            = dbMeta.DateCreated,
				DateUpdated            = dbMeta.DateUpdated ?? DefaultValues.Date.Minimum
			};

			return meta;
		}

		public DBMetaEtlResult ToDb(ETLResult meta)
		{
			Check.NotNull(meta, nameof(meta));

			DBMetaEtlResult dbMeta = new DBMetaEtlResult
			{
				Id                  = meta.Id,
				QueryId             = meta.QueryId,
				EtlServerInstanceId = meta.TargetServerInstanceId,
				Rows                = meta.Rows,
				ErrorCode           = meta.ErrorCode,
				ErrorId             = meta.ErrorId,
				ErrorMessage        = meta.ErrorMessage,
				DateCreated         = meta.DateCreated,
				DateUpdated         = meta.DateUpdated
			};

			return dbMeta;
		}

		public DBConnectionType ToDb(ConnectionType connectionType)
		{
			Check.NotNull(connectionType, nameof(connectionType));

			DBConnectionType dbConnectionType = new DBConnectionType
			{
				Name = connectionType.ToString()
			};

			return dbConnectionType;
		}

		public DBLogin ToDb(Login login)
		{
			Check.NotNull(login, nameof(login));

			DBLogin dbLogin = new DBLogin
			{
				Id                = login.Id,
				Name              = login.Name,
				EncryptedPassword = this._cryptoService.Encrypt(login.Password),
				IsWinAuth         = login.IsWinAuth
			};

			return dbLogin;
		}

		public DBTemplate ToDb(Template template)
		{
			Check.NotNull(template, nameof(template));

			DBTemplate dbTemplate = new DBTemplate
			{
				Id         = template.Id,
				Name       = template.Name,
				UId        = template.UId,
				Directory  = template.Directory,
				IsExternal = template.IsExternal
			};

			return dbTemplate;
		}

		public Template FromDb(DBTemplate dbTemplate)
		{
			Check.NotNull(dbTemplate, nameof(dbTemplate));

			Template template = new Template
			{
				Id         = dbTemplate.Id,
				Name       = dbTemplate.Name,
				UId        = dbTemplate.UId,
				Directory  = dbTemplate.Directory,
				IsExternal = dbTemplate.IsExternal
			};

			return template;
		}

		public NodeInstance FromDb(DBNodeInstance dbNode)
		{
			NodeInstance node = new NodeInstance
			{
				Id                   = dbNode.Id,
				ConnectionGroupId    = dbNode.ConnectionGroupId,
				TemplateNodeId       = dbNode.TemplateNodeId,
				ParentId             = dbNode.ParentId,
				UId                  = dbNode.UId,
				Name                 = dbNode.Name,
				Icon                 = dbNode.Icon,
				IsEnabled            = dbNode.IsEnabled,
				FontColor            = dbNode.FontColor,
				FontStyle            = dbNode.FontStyle,
				SequenceNumber       = dbNode.SequenceNumber,
				ChildrenNotProcessed = dbNode.ChildrenNotProcessed,
				ScheduledUpdate      = dbNode.ScheduledUpdate,
				Counter              = dbNode.Counter,
				LastUpdate           = dbNode.LastUpdate,
				LastUpdateDuration   = dbNode.LastUpdateDuration
			};

			return node;
		}

		public DBNodeInstance ToDb(NodeInstance node)
		{
			DBNodeInstance dbNode = new DBNodeInstance
			{
				Id                   = node.Id,
				ConnectionGroupId    = node.ConnectionGroupId,
				TemplateNodeId       = node.TemplateNodeId,
				ParentId             = node.ParentId,
				UId                  = node.UId,
				Name                 = node.Name,
				Icon                 = node.Icon,
				IsEnabled            = node.IsEnabled,
				FontColor            = node.FontColor,
				FontStyle            = node.FontStyle,
				SequenceNumber       = node.SequenceNumber,
				ChildrenNotProcessed = node.ChildrenNotProcessed,
				ScheduledUpdate      = node.ScheduledUpdate,
				Counter              = node.Counter,
				LastUpdate           = node.LastUpdate,
				LastUpdateDuration   = node.LastUpdateDuration
			};

			return dbNode;
		}

		public TemplateNode FromDb(DBTemplateNode dbTNode)
		{
			TemplateNode tnode = new TemplateNode
			{
				Id                 = dbTNode.Id,
				TemplateId         = dbTNode.TemplateId,
				ParentId           = dbTNode.ParentId,
				UId                = dbTNode.UId,
				QueryGroupParentId = dbTNode.TemplateQueryGroupParentId,
				Name               = dbTNode.Name,
				Icon               = dbTNode.Icon,
				ShowEmpty          = dbTNode.ShowEmpty,
				ShowRecordsNumber  = dbTNode.ShowRecordsNumber
			};

			return tnode;
		}

		public DBTemplateNode ToDb(TemplateNode tNode)
		{
			DBTemplateNode tnode = new DBTemplateNode
			{
				Id                         = tNode.Id,
				TemplateId                 = tNode.TemplateId,
				ParentId                   = tNode.ParentId,
				UId                        = tNode.UId,
				TemplateQueryGroupParentId = tNode.QueryGroupParentId,
				Name                       = tNode.Name,
				Icon                       = tNode.Icon,
				ShowEmpty                  = tNode.ShowEmpty,
				ShowRecordsNumber          = tNode.ShowRecordsNumber
			};

			return tnode;
		}

		public TemplateQuery FromDb(DBTemplateQuery dbTQuery)
		{
			TemplateQuery tQuery = new TemplateQuery
			{
				Id             = dbTQuery.Id,
				Name           = dbTQuery.Name,
				UId            = dbTQuery.UId,
				Hierarchy      = dbTQuery.Hierarchy,
				TemplateNodeId = dbTQuery.TemplateNodeId
			};

			return tQuery;
		}

		public DBTemplateQuery ToDb(TemplateQuery tQuery)
		{
			DBTemplateQuery dbTQuery = new DBTemplateQuery
			{
				Id             = tQuery.Id,
				Name           = tQuery.Name,
				UId            = tQuery.UId,
				Hierarchy      = tQuery.Hierarchy,
				TemplateNodeId = tQuery.TemplateNodeId
			};

			return dbTQuery;
		}

		public TemplateQueryGroup FromDb(DBTemplateQueryGroup dbTQueryGroup)
		{
			TemplateQueryGroup tQueryGroup = new TemplateQueryGroup
			{
				Id                   = dbTQueryGroup.Id,
				Name                 = dbTQueryGroup.Name,
				GroupId              = dbTQueryGroup.GroupId,
				TemplateNodeParentId = dbTQueryGroup.TemplateNodeParentId,
				DefaultDatabaseField = dbTQueryGroup.DefaultDatabaseField
			};

			return tQueryGroup;
		}

		public DBTemplateQueryGroup ToDb(TemplateQueryGroup tQueryGroup)
		{
			DBTemplateQueryGroup dbTQueryGroup = new DBTemplateQueryGroup
			{
				Id                   = tQueryGroup.Id,
				Name                 = tQueryGroup.Name,
				GroupId              = tQueryGroup.GroupId,
				TemplateNodeParentId = tQueryGroup.TemplateNodeParentId,
				DefaultDatabaseField = tQueryGroup.DefaultDatabaseField
			};

			return dbTQueryGroup;
		}

		public Query FromDb(DBQuery dbQuery)
		{
			Query query = new Query
			{
				Id                  = dbQuery.Id,
				NodeInstanceId      = dbQuery.NodeInstanceId,
				TemplateQueryId     = dbQuery.TemplateQueryId,
				ServerInstanceId    = dbQuery.ServerInstanceId,
				DefaultDatabaseName = dbQuery.DefaultDatabaseName
			};

			return query;
		}

		public DBQuery ToDb(Query query)
		{
			DBQuery dbQuery = new DBQuery
			{
				Id                  = query.Id,
				NodeInstanceId      = query.NodeInstanceId,
				TemplateQueryId     = query.TemplateQueryId,
				ServerInstanceId    = query.ServerInstanceId,
				DefaultDatabaseName = query.DefaultDatabaseName
			};

			return dbQuery;
		}

		public TemplateQueryParameter FromDb(DBTemplateQueryParameter dbParam)
		{
			TemplateQueryParameter param = new TemplateQueryParameter
			{
				Id              = dbParam.Id,
				TemplateQueryId = dbParam.TemplateQueryId,
				Name            = dbParam.Name
			};

			return param;
		}

		public DBTemplateQueryParameter ToDb(TemplateQueryParameter param)
		{
			DBTemplateQueryParameter dbParam = new DBTemplateQueryParameter
			{
				Id              = param.Id,
				TemplateQueryId = param.TemplateQueryId,
				Name            = param.Name
			};

			return dbParam;
		}

		public TemplateQueryGroupParameter FromDb(DBTemplateQueryGroupParameter dbParam)
		{
			TemplateQueryGroupParameter param = new TemplateQueryGroupParameter
			{
				Id                   = dbParam.Id,
				TemplateQueryGroupId = dbParam.TemplateQueryGroupId,
				Name                 = dbParam.Name
			};

			return param;
		}

		public DBTemplateQueryGroupParameter ToDb(TemplateQueryGroupParameter param)
		{
			DBTemplateQueryGroupParameter dbParam = new DBTemplateQueryGroupParameter
			{
				Id                   = param.Id,
				TemplateQueryGroupId = param.TemplateQueryGroupId,
				Name                 = param.Name
			};

			return dbParam;
		}

		public LastConnection FromDb(DBLastConnection dblc)
		{
			Check.NotNull(dblc.Template, nameof(dblc.Template));

			LastConnection lc = new LastConnection
			{
				Id                = dblc.Id,
				ConnectionGroupId = dblc.ConnectionGroupId,
				MachineName       = dblc.MachineName,
				ModuleType        = dblc.ModuleType,
				TemplateId        = dblc.TemplateId,
				ConnectionType    = ConnectionTypeHelper.Parse(dblc.ConnectionType.Name),
				Template          = FromDb(dblc.Template)
			};

			return lc;
		}

		public DynamicConnectionInfo FromDb(DBDynamicConnection dbDynConnection)
		{
			Check.NotNull(dbDynConnection, nameof(dbDynConnection));

			DynamicConnectionInfo dynConnection = new DynamicConnectionInfo
			{
				QueryId        = dbDynConnection.QueryId,
				ServerInstance = FromDb(dbDynConnection.ServerInstance)
			};

			return dynConnection;
		}

		public ScheduleSettings FromDb(DBNodeScheduleSettings dbSchedule)
		{
			ScheduleSettings schedule = new ScheduleSettings
			{
				Id                      = dbSchedule.Id,
				NodeInstanceId          = dbSchedule.NodeInstanceId,
				UId                     = dbSchedule.UId,
				Name                    = dbSchedule.Name,
				IsEnabled               = dbSchedule.IsEnabled,
				CronExpression          = dbSchedule.CronExpression,
				TargetMachine           = dbSchedule.TargetMachine,
				ServiceOnly             = dbSchedule.ServiceOnly,
				LastRan                 = dbSchedule.LastRan,
				SendMessage             = dbSchedule.SendMessage,
				SendMessageNonEmptyOnly = dbSchedule.SendMessageNonEmptyOnly,
				MessageRecipients       = dbSchedule.MessageRecipients,
				MessageLanguage         = dbSchedule.MessageLanguage,
				UpdateHierarchically    = dbSchedule.UpdateHierarchically,
				UpdateDeep              = dbSchedule.UpdateDeep
			};

			return schedule;
		}

		public DBNodeScheduleSettings ToDb(ScheduleSettings schedule)
		{
			DBNodeScheduleSettings dbSchedule = new DBNodeScheduleSettings
			{
				Id                      = schedule.Id,
				NodeInstanceId          = schedule.NodeInstanceId,
				UId                     = schedule.UId,
				Name                    = schedule.Name,
				IsEnabled               = schedule.IsEnabled,
				CronExpression          = schedule.CronExpression,
				TargetMachine           = schedule.TargetMachine,
				ServiceOnly             = schedule.ServiceOnly,
				LastRan                 = schedule.LastRan,
				SendMessage             = schedule.SendMessage,
				SendMessageNonEmptyOnly = schedule.SendMessageNonEmptyOnly,
				MessageRecipients       = schedule.MessageRecipients,
				MessageLanguage         = schedule.MessageLanguage,
				UpdateHierarchically    = schedule.UpdateHierarchically,
				UpdateDeep              = schedule.UpdateDeep
			};

			return dbSchedule;
		}

		public ScheduleDetails MapScheduleDetails(DBNodeScheduleSettings dbSchedule)
		{
			ScheduleDetails schedule = new ScheduleDetails
			{
				Id                      = dbSchedule.Id,
				NodeInstanceId          = dbSchedule.NodeInstanceId,
				UId                     = dbSchedule.UId,
				Name                    = dbSchedule.Name,
				IsEnabled               = dbSchedule.IsEnabled,
				CronExpression          = dbSchedule.CronExpression,
				TargetMachine           = dbSchedule.TargetMachine,
				ServiceOnly             = dbSchedule.ServiceOnly,
				LastRan                 = dbSchedule.LastRan,
				SendMessage             = dbSchedule.SendMessage,
				SendMessageNonEmptyOnly = dbSchedule.SendMessageNonEmptyOnly,
				MessageRecipients       = dbSchedule.MessageRecipients,
				MessageLanguage         = dbSchedule.MessageLanguage,
				UpdateHierarchically    = dbSchedule.UpdateHierarchically,
				UpdateDeep              = dbSchedule.UpdateDeep,
				DateUpdated             = dbSchedule.DateUpdated,
				DateCreated             = dbSchedule.DateCreated,
				ConnectionGroupId       = dbSchedule.NodeInstance.ConnectionGroupId ?? 0
			};

			DBTemplateNode dbTemplateNode = dbSchedule.NodeInstance.TemplateNode;
			schedule.TemplateNodeUId      = dbTemplateNode.UId;
			schedule.TemplateNodeName     = dbTemplateNode.Name;

			DBTemplate dbTemplate      = dbTemplateNode.Template;
			schedule.TemplateId        = dbTemplate.Id;
			schedule.TemplateDirectory = dbTemplate.Directory;
			schedule.TemplateName      = dbTemplate.Name;
			schedule.TemplateUId       = dbTemplate.UId;

			return schedule;
		}
	}
}
