﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	[XmlRoot(ElementName = "sql-select-content")]
	public class QueryListExt
	{
		[XmlElement(ElementName = "sql-select")]
		public List<QueryInfo> Queries { get; set; }
	}
}
