﻿using System.Data;
using System.Xml.Serialization;
using MSSQLServerAuditor.Common.Utils;

namespace MSSQLServerAuditor.Core.Domain
{
	/// <summary>
	/// Definition for query parameter
	/// </summary>
	public class QueryParameterInfo
	{
		/// <summary>
		/// Parameter name
		/// </summary>
		[XmlAttribute(AttributeName = "name")]
		public string Name { get; set; }

		/// <summary>
		/// Parameter type <see href="http://msdn.microsoft.com/en-us/library/system.data.sqldbtype.aspx"></see>
		/// </summary>
		[XmlAttribute(AttributeName = "type")]
		public SqlDbType Type { get; set; }

		/// <summary>
		/// Is null
		/// </summary>
		[XmlAttribute(AttributeName = "isnull")]
		public bool IsNull { get; set; }

		/// <summary>
		/// Default string value
		/// </summary>
		[XmlAttribute(AttributeName = "default")]
		public string DefaultValue { get; set; }

		public object GetDefaultValue()
		{
			return ConvertString.ToSqlDbType(DefaultValue, IsNull, Type);
		}
	}
}
