﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	[XmlRoot(ElementName = "sql-scripts")]
	public class ScriptListInfo
	{
		[XmlElement(ElementName = "sql-script")]
		public List<ScriptInfo> Scripts { get; set; }
	}
}