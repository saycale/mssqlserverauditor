﻿using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	public class QueryInfo : QueryContentInfo
	{
		[XmlAttribute(AttributeName = "id")]
		public string Id { get; set; }

		[XmlAttribute(AttributeName = "name")]
		public string Name { get; set; }

		[XmlAttribute(AttributeName = "namespace")]
		public string Namespace { get; set; }

		[XmlAttribute(AttributeName = "scope")]
		public QueryScope Scope { get; set; }

		[XmlAttribute(AttributeName = "save_results")]
		public bool SaveResults { get; set; } = true;

		[XmlIgnore]
		public ConnectionType ConnectionType { get; set; }

		public override string ToString()
		{
			return $"query: Id='{Id}', Name='{Name}', Scope={Scope}";
		}
	}
}
