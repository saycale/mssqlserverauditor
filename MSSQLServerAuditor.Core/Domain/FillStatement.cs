﻿using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	/// <summary>
	/// Statement to be executed for Historic DB
	/// </summary>
	public class FillStatement
	{
		/// <summary>
		/// Statement id (sorting field)
		/// </summary>
		[XmlAttribute("id")]
		public int Id { get; set; }

		/// <summary>
		/// Statement script file
		/// </summary>
		[XmlAttribute("file")]
		public string File { get; set; }

		/// <summary>
		/// Statement name
		/// </summary>
		[XmlAttribute("query")]
		public string Query { get; set; }

		/// <summary>
		/// Command text
		/// </summary>
		[XmlText]
		public string Text { get; set; }

		/// <summary>
		/// Command text signature
		/// </summary>
		[XmlAttribute("signature")]
		public string Signature { get; set; }
	}
}
