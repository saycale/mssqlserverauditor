﻿using System.Linq;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	public class TemplateNodeSqlGuardQueryInfo : TemplateNodeQueryInfo
	{
		/// <summary>
		/// Id of sql-select query which will be analyzed
		/// </summary>
		[XmlAttribute(AttributeName = "sql-select-id")]
		public string SqlQueryId { get; set; }

		/// <summary>
		/// Column in sql-select query result which contains code for analyzing
		/// </summary>
		[XmlAttribute(AttributeName = "code-column")]
		public string QueryCodeColumn { get; set; }

		/// <summary>
		/// Column in sql-select query result which contains name of analyzing object
		/// </summary>
		[XmlAttribute(AttributeName = "object-column")]
		public string QueryObjectColumn { get; set; }

		/// <summary>
		/// Column in sql-select query result which contains name of analyzing object
		/// </summary>
		[XmlAttribute(AttributeName = "add-summary")]
		public bool AddSummary { get; set; }

		/// <summary>
		/// Type of sqlcodeguard-select report
		/// </summary>
		[XmlAttribute(AttributeName = "include")]
		public string IncludedIssue { get; set; }

		/// <summary>
		/// Type of sqlcodeguard-select report
		/// </summary>
		[XmlAttribute(AttributeName = "exclude")]
		public string ExcludedIssue { get; set; }

		public new TemplateNodeSqlGuardQueryInfo Clone()
		{
			TemplateNodeSqlGuardQueryInfo result = (TemplateNodeSqlGuardQueryInfo) MemberwiseClone();

			result.ParameterValues = ParameterValues.Select(pv => pv.Clone()).ToList();

			return result;
		}
	}
}
