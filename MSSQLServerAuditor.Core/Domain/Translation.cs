﻿using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	// TODO: replace it with LangResource class, but change xml format before
	public class Translation
	{
		/// <summary>
		/// Language
		/// </summary>
		[XmlAttribute(AttributeName = "name")]
		public string Language { get; set; }

		/// <summary>
		/// Template name
		/// </summary>
		[XmlText]
		public string Text { get; set; }
	}
}
