﻿using System.Collections.Generic;
using System.Xml.Serialization;
using MSSQLServerAuditor.Core.Persistence.Storages;

namespace MSSQLServerAuditor.Core.Domain
{
	/// <summary>
	/// Build script root
	/// </summary>
	public class PostBuildScriptListInfo
	{
		/// <summary>
		/// Query type
		/// </summary>
		[XmlAttribute(AttributeName = "type")]
		public StorageType StorageType { get; set; }

		/// <summary>
		/// Informations.
		/// </summary>
		[XmlElement(ElementName = "sql-select")]
		public List<QueryInfoExt> Queries { get; set; }
	}
}
