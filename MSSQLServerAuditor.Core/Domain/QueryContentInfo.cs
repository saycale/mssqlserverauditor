﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	public abstract class QueryContentInfo
	{
		/// <summary>
		/// Items for defined versions
		/// </summary>
		[XmlElement(ElementName = "sql-select-text")]
		public List<QueryItemInfo> Items { get; set; }

		/// <summary>
		/// Queries for database selections
		/// </summary>
		[XmlElement(ElementName = "database-select-text")]
		public List<QueryItemInfo> DatabaseSelect { get; set; }

		/// <summary>
		/// Queries for Instance group selections
		/// </summary>
		[XmlElement(ElementName = "group-select-text")]
		public List<QueryItemInfo> GroupSelect { get; set; }

		/// <summary>
		/// Parameters for query
		/// </summary>
		[XmlArray(ElementName = "sql-select-parameters")]
		[XmlArrayItem(ElementName = "sql-select-parameter")]
		public List<QueryParameterInfo> Parameters { get; set; }

		/// <summary>
		/// Statements to fill historic DB
		/// </summary>
		[XmlArray(ElementName = "sqlite_statements")]
		[XmlArrayItem(ElementName = "sqlite_statement")]
		public List<FillStatement> FillStatements { get; set; }

		/// <summary>
		/// ETL statements
		/// </summary>
		[XmlArray(ElementName = "etl_statements")]
		[XmlArrayItem(ElementName = "etl_statement")]
		public List<ETLStatement> ETLStatements { get; set; }

		/// <summary>
		/// Aliases for columns
		/// </summary>
		[XmlArray(ElementName = "sql-select-aliases")]
		[XmlArrayItem(ElementName = "sql-select-alias")]
		public List<AliasInfo> Aliases { get; set; }
	}
}
