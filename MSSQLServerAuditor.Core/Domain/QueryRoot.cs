﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	/// <summary>
	/// Root for XML of queries.
	/// </summary>
	[XmlRoot(ElementName = "root")]
	public class QueryRoot
	{
		/// <summary>
		/// Queries
		/// </summary>
		[XmlElement(ElementName = "sqlquery")]
		public List<QueryListInfo> Queries { get; set; }
	}
}
