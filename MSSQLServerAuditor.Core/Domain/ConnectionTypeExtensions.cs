﻿using System;

namespace MSSQLServerAuditor.Core.Domain
{
	public static class ConnectionTypeExtensions
	{
		public static bool IsInternal(this ConnectionType type)
		{
			switch (type)
			{
				case ConnectionType.System:
				case ConnectionType.Internal:
					return true;
			}

			return false;
		}
	}

	public static class ConnectionTypeHelper
	{
		public static ConnectionType Parse(string s)
		{
			ConnectionType type;

			if (Enum.TryParse(s, true, out type))
			{
				return type;
			}

			return ConnectionType.MSSQL;
		}
	}
}