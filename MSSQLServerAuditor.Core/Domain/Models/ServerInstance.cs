using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Linq;
using System.Management;
using System.Text;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Querying.Connections;
using MSSQLServerAuditor.DataAccess.Connections;
using MSSQLServerAuditor.DataAccess.Entity.Models;
using Teradata.Client.Provider;

namespace MSSQLServerAuditor.Core.Domain.Models
{
	public abstract class ServerInstance : ModelBase
	{
		public string          ConnectionName  { get; set; }
		public string          ConnectionAlias { get; set; }
		public bool            IsDynamic       { get; set; }
		public bool            IsDeleted       { get; set; }
		public bool            IsActive        { get; set; }
		public Login           Login           { get; set; }
		public ConnectionType  ConnectionType  { get; set; } = ConnectionType.MSSQL;

		public override string ToString()
		{
			return ConnectionName;
		}

		public void ReadFrom(DataRow row)
		{
			string username        = row.GetValue(DBLogin.FieldName,                     string.Empty);
			string password        = row.GetValue(DBLogin.FieldPassword,                 string.Empty);
			bool   isWinAuth       = row.GetValue(DBLogin.FieldIsWinAuth,                false);
			string connectionName  = row.GetValue(DBServerInstance.FieldConnectionName,  string.Empty);
			string connectionAlias = row.GetValue(DBServerInstance.FieldConnectionAlias, string.Empty);
			string connectionType  = row.GetValue(DBConnectionType.FieldName,            ConnectionType.MSSQL.ToString());

			Login login = new Login(username, password, isWinAuth);

			ConnectionName  = connectionName;
			ConnectionAlias = connectionAlias;
			Login           = login;
			ConnectionType  = ConnectionTypeHelper.Parse(connectionType);

			ReadSpecificProperties(row);
		}

		protected abstract void ReadSpecificProperties(DataRow row);

		public virtual string Populate(string value)
		{
			return value;
		}

		public abstract IQueryConnection OpenNewConnection(IStorageConnectionFactory factory);

		public QueryItemInfo ResolveQuery(IEnumerable<QueryItemInfo> queryItems, ConnectionType connectionType)
		{
			IEnumerable<QueryItemInfo> matchQueries =
				from   queryItem in queryItems
				where  IsMatch(queryItem, connectionType)
				select queryItem;

			return matchQueries.FirstOrDefault();
		}

		public QueryItemInfo ResolveQuery(IEnumerable<QueryItemInfo> queryItems)
		{
			IEnumerable<QueryItemInfo> matchQueries =
				from   queryItem in queryItems
				where  IsMatch(queryItem, ConnectionType)
				select queryItem;

			return matchQueries.FirstOrDefault();
		}

		protected virtual bool IsMatchToVersions(InstanceVersion minVersion, InstanceVersion maxVersion)
		{
			return true;
		}

		private bool IsMatch(QueryItemInfo queryItem, ConnectionType connectionType)
		{
			if (connectionType.IsInternal())
			{
				return true;
			}

			InstanceVersion minVersion = InstanceVersion.GetMinVersion(queryItem.MinVersion);
			InstanceVersion maxVersion = InstanceVersion.GetMaxVersion(queryItem.MaxVersion);

			return IsMatchToVersions(minVersion, maxVersion);
		}
	}

	public abstract class SqlBaseInstance : ServerInstance
	{
		public static readonly Dictionary<string, Func<SqlBaseInstance, string>> PlaceholderDictionary;

		static SqlBaseInstance()
		{
			PlaceholderDictionary = new Dictionary<string, Func<SqlBaseInstance, string>>
			{
				{"${servername}$",         i => i.InstanceName},
				{"${serverenvironment}$",  i => i.Environment},
				{"${serverhostname}$",     i => i.Host},
				{"${serverdomainname}$",   i => i.Domain},
				{"${domainname}$",         i => i.Domain},
				{"${domain}$",             i => i.Domain},
				{"${serverinstancename}$", i => i.InstanceName},

				{"${loginname}$",          i => i.Login.Name},
				{"${loginpassword}$",      i => i.Login.Password}
			};
		}

		public string          InstanceName     { get; set; }
		public string          Environment      { get; set; }
		public string          Host             { get; set; }
		public string          Domain           { get; set; }
		public InstanceVersion Version          { get; set; }
		public bool            IsOdbc           { get; set; }
		public string          ConnectionString { get; set; }

		public string PopulateConnectionString()
		{
			return Populate(ConnectionString);
		}

		public override string Populate(string value)
		{
			StringBuilder output = new StringBuilder(value);

			foreach (KeyValuePair<string, Func<SqlBaseInstance, string>> func in PlaceholderDictionary)
			{
				output.Replace(func.Key, func.Value(this));
			}

			return output.ToString();
		}

		protected override bool IsMatchToVersions(InstanceVersion minVersion, InstanceVersion maxVersion)
		{
			return Version.CompareTo(minVersion) >= 0 && Version.CompareTo(maxVersion) <= 0;
		}

		public override string ToString()
		{
			return Populate(ConnectionString);
		}
	}

	public class MssqlInstance : SqlBaseInstance
	{
		public override IQueryConnection OpenNewConnection(IStorageConnectionFactory factory)
		{
			string connectionString = Populate(ConnectionString);

			IQueryConnection queryConnection;
			if (IsOdbc)
			{
				OdbcConnection odbcConnection = new OdbcConnection(connectionString);
				queryConnection = new OdbcQueryConnection(odbcConnection);
			}
			else
			{
				SqlConnection sqlConnection = new SqlConnection(connectionString);
				queryConnection = new MsSqlQueryConnection(sqlConnection);
			}

			queryConnection.Open();

			return queryConnection;
		}

		protected override void ReadSpecificProperties(DataRow row)
		{
			string connectionString  = row.GetValue(DBServerInstanceMssql.FieldConnectionString, string.Empty);
			string instanceName      = row.GetValue(DBServerInstanceMssql.FieldInstanceName,     string.Empty);
			string instanceVersion   = row.GetValue(DBServerInstanceMssql.FieldVersion,          string.Empty);
			string serverHostName    = row.GetValue(DBServerInstanceMssql.FieldHost,             string.Empty);
			string serverDomainName  = row.GetValue(DBServerInstanceMssql.FieldDomain,           string.Empty);
			string serverEnvironment = row.GetValue(DBServerInstanceMssql.FieldEnvironment,      string.Empty);
			bool?  isOdbc            = row.GetValue<bool?>(DBServerInstanceMssql.FieldIsOdbc,    null);

			ConnectionString = connectionString;
			IsOdbc           = isOdbc.HasValue && isOdbc.Value;
			InstanceName     = instanceName;
			Version          = new InstanceVersion(instanceVersion);
			Host             = serverHostName;
			Domain           = serverDomainName;
			Environment      = serverEnvironment;
		}
	}

	public class TeradataInstance : SqlBaseInstance
	{
		public override IQueryConnection OpenNewConnection(IStorageConnectionFactory factory)
		{
			string connectionString = Populate(ConnectionString);

			TdConnection               tdConnection    = new TdConnection(connectionString);
			TeradataSqlQueryConnection queryConnection = new TeradataSqlQueryConnection(tdConnection);

			queryConnection.Open();

			return queryConnection;
		}

		protected override void ReadSpecificProperties(DataRow row)
		{
			string connectionString  = row.GetValue(DBServerInstanceTdsql.FieldConnectionString, string.Empty);
			string instanceName      = row.GetValue(DBServerInstanceTdsql.FieldInstanceName,     string.Empty);
			string instanceVersion   = row.GetValue(DBServerInstanceTdsql.FieldVersion,          string.Empty);
			string serverHostName    = row.GetValue(DBServerInstanceTdsql.FieldHost,             string.Empty);
			string serverDomainName  = row.GetValue(DBServerInstanceTdsql.FieldDomain,           string.Empty);
			string serverEnvironment = row.GetValue(DBServerInstanceTdsql.FieldEnvironment,      string.Empty);
			bool?  isOdbc            = row.GetValue<bool?>(DBServerInstanceTdsql.FieldIsOdbc,    null);

			ConnectionString = connectionString;
			IsOdbc           = isOdbc.HasValue && isOdbc.Value;
			InstanceName     = instanceName;
			Version          = new InstanceVersion(instanceVersion);
			Host             = serverHostName;
			Domain           = serverDomainName;
			Environment      = serverEnvironment;
		}
	}

	public class SqliteInstance : ServerInstance
	{
		public string DatabaseFilePath { get; set; }

		public override IQueryConnection OpenNewConnection(IStorageConnectionFactory factory)
		{
			SQLiteConnection sqliteConnection = new SQLiteConnection(
				CreateSqliteConnectionString(DatabaseFilePath)
			);

			SqliteExternalQueryConnection queryConnection = new SqliteExternalQueryConnection(sqliteConnection);

			queryConnection.Open();

			return queryConnection;
		}

		private static string CreateSqliteConnectionString(string fileName, bool readOnly = true)
		{
			string connectionString;

			if (fileName.Equals("file::memory:"))
			{
				connectionString =
					"FullUri=file:memdb1?mode=memory&cache=shared; " +
					"PRAGMA journal_mode=off; "                      +
					"PRAGMA temp_store = MEMORY; "                   +
					"PRAGMA synchronous=off; "                       +
					"PRAGMA count_changes=off; "                     +
					"PRAGMA encoding = \"UTF-8\";";
			}
			else
			{
				connectionString =
					$"Data Source={fileName}; "       +
					$"PRAGMA locking_mode = NORMAL; " +
					$"Pooling=True; "                 +
					$"PRAGMA page_size = 4096; "      +
					$"PRAGMA cache_size=10000; "      +
					$"PRAGMA journal_mode=WAL; "      +
					$"PRAGMA synchronous=off; "       +
					$"PRAGMA count_changes=off; "     +
					$"PRAGMA temp_store=2; "          +
					$"PRAGMA encoding = \"UTF-8\"; "  +
					$"PRAGMA query_only={readOnly};{(readOnly ? "mode=ro" : string.Empty)}";
			}

			return connectionString;
		}

		protected override void ReadSpecificProperties(DataRow row)
		{
			DatabaseFilePath = row.GetValue(DBServerInstanceSqlite.FieldDatabaseFilePath, string.Empty);
		}

		public override string ToString()
		{
			return DatabaseFilePath;
		}
	}

	public class InternalServerInstance : ServerInstance
	{
		public long ServerInstanceSourceId  { get; set; }
		public long ConnectionGroupSourceId { get; set; }
		public long TemplateSourceId        { get; set; }
		public long LoginSourceId           { get; set; }

		public override IQueryConnection OpenNewConnection(IStorageConnectionFactory factory)
		{
			InternalConnectionParameters parameters = new InternalConnectionParameters(
				ConnectionGroupSourceId,
				ServerInstanceSourceId,
				TemplateSourceId,
				LoginSourceId
			);

			return new InternalQueryConnection(factory, parameters);
		}

		protected override void ReadSpecificProperties(DataRow row)
		{
			ConnectionGroupSourceId = row.GetValue<long>(DBServerInstanceInternal.FieldConnectionGroupSourceId, -1);
			ServerInstanceSourceId  = row.GetValue<long>(DBServerInstanceInternal.FieldServerInstanceSourceId,  -1);
			TemplateSourceId        = row.GetValue<long>(DBServerInstanceInternal.FieldTemplateSourceId,        -1);
		}
	}

	public class EventLogInstance : ServerInstance
	{
		public string MachineName { get; set; }

		public override IQueryConnection OpenNewConnection(IStorageConnectionFactory factory)
		{
			return new EventLogQueryConnection(MachineName);
		}

		protected override void ReadSpecificProperties(DataRow row)
		{
			MachineName = row.GetValue(DBServerInstanceEventLog.FieldMachineName, string.Empty);
		}

		public override string ToString()
		{
			return MachineName;
		}
	}

	public class WmiInstance : ServerInstance
	{
		public string Authentication { get; set; }
		public string Impersonation  { get; set; }
		public string Host           { get; set; }
		public string Authority      { get; set; }

		public override IQueryConnection OpenNewConnection(IStorageConnectionFactory factory)
		{
			WmiConnectionInfo info = ToWmiConnectionOptions();
			return new WmiQueryConnection(info);
		}

		public WmiConnectionInfo ToWmiConnectionOptions()
		{
			WmiConnectionInfo info = new WmiConnectionInfo
			{
				Authority    = Authority,
				ComputerName = Host,
				Username     = Login?.Name,
				Password     = Login?.Password
			};

			AuthenticationLevel auth;
			if (!string.IsNullOrWhiteSpace(Authentication) && Enum.TryParse(Authentication, true, out auth))
			{
				info.Authentication = auth;
			}

			ImpersonationLevel impersonation;
			if (!string.IsNullOrWhiteSpace(Impersonation) && Enum.TryParse(Impersonation, true, out impersonation))
			{
				info.Impersonation = impersonation;
			}

			return info;
		}

		protected override void ReadSpecificProperties(DataRow row)
		{
			Authentication = row.GetValue(DBServerInstanceWmi.FieldAuthentication, string.Empty);
			Authority      = row.GetValue(DBServerInstanceWmi.FieldAuthority,      string.Empty);
			Host           = row.GetValue(DBServerInstanceWmi.FieldHost,           string.Empty);
			Impersonation  = row.GetValue(DBServerInstanceWmi.FieldImpersonation,  string.Empty);
		}

		public override string ToString()
		{
			return ToWmiConnectionOptions().ToString();
		}
	}

	public class LdapInstance : ServerInstance
	{
		public string ConnectionPath { get; set; }
		
		public override IQueryConnection OpenNewConnection(IStorageConnectionFactory factory)
		{
			return new LdapQueryConnection(ConnectionPath);
		}

		protected override void ReadSpecificProperties(DataRow row)
		{
			ConnectionPath = row.GetValue(DBServerInstanceLdap.FieldConnectionPath, string.Empty);
		}

		public override string ToString()
		{
			return ConnectionPath;
		}
	}

	public class NetworkInstance : ServerInstance
	{
		public string Host     { get; set; }
		public int?   Port     { get; set; }
		public string Protocol { get; set; }
		public int?   Timeout  { get; set; }

		public override IQueryConnection OpenNewConnection(IStorageConnectionFactory factory)
		{
			NetworkConnectionInfo networkConnectionInfo = ToNetworkConnectionInfo();

			return new NetworkQueryConnection(networkConnectionInfo);
		}

		public NetworkConnectionInfo ToNetworkConnectionInfo()
		{
			ProtocolType protocol;
			if (!Enum.TryParse(Protocol, true, out protocol))
			{
				protocol = ProtocolType.Tcp;
			}

			int port;
			if (Port.HasValue)
			{
				port = Port.Value;
			}
			else
			{
				switch (protocol)
				{
					case ProtocolType.Tcp:
					case ProtocolType.Udp:
						port = 80; // assing default port for tcp/udp
						break;

					case ProtocolType.Icmp:
						port = 1;
						break;

					default:
						throw new ArgumentOutOfRangeException("Invalid network protocol");
				}
			}

			NetworkConnectionInfo networkConnectionInfo = new NetworkConnectionInfo(
				Host,
				port,
				protocol,
				Timeout ?? 0
			);

			return networkConnectionInfo;
		}

		protected override void ReadSpecificProperties(DataRow row)
		{
			Host     = row.GetValue(DBServerInstanceNetworkInformation.FieldHost,          string.Empty);
			Protocol = row.GetValue(DBServerInstanceNetworkInformation.FieldProtocol,      string.Empty);
			Timeout  = row.GetValue<int?>(DBServerInstanceNetworkInformation.FieldTimeout, null);
			Port     = row.GetValue<int?>(DBServerInstanceNetworkInformation.FieldPort,    null);
		}

		public override string ToString()
		{
			return ToNetworkConnectionInfo().ToString();
		}
	}

	public class SystemConnectionInstance : ServerInstance
	{
		public override IQueryConnection OpenNewConnection(IStorageConnectionFactory factory)
		{
			return new SystemQueryConnection(factory);
		}

		protected override void ReadSpecificProperties(DataRow row)
		{
			
		}
	}
}