﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain.Models
{
	[Serializable]
	public class ScheduleSettings : ModelBase
	{
		public ScheduleSettings()
		{
			this.IsEnabled      = true;
			this.CronExpression = "* * * * * ?";
		}

		[XmlIgnore]
		public long NodeInstanceId { get; set; }

		[XmlElement("Id")]
		public string UId { get; set; }

		[XmlElement("Name")]
		public string Name { get; set; }

		[XmlElement("Enabled")]
		public bool? IsEnabled { get; set; }

		[XmlElement("CronExpression")]
		public string CronExpression { get; set; }

		[XmlElement("TargetMachine")]
		public string TargetMachine { get; set; }

		[XmlElement("ServiceOnly")]
		public bool ServiceOnly { get; set; }

		[XmlIgnore]
		public DateTime? LastRan { get; set; }

		[XmlElement("SendMessage")]
		public bool? SendMessage { get; set; }

		[XmlElement("SendMessageNonEmptyOnly")]
		public bool SendMessageNonEmptyOnly { get; set; }

		[XmlElement("SendMessageRecipients")]
		public string MessageRecipients { get; set; }

		[XmlElement("SendMessageLanguage")]
		public string MessageLanguage { get; set; }

		[XmlElement("UpdateHierarchically")]
		public bool UpdateHierarchically { get; set; }

		[XmlElement("UpdateDeep")]
		public int UpdateDeep { get; set; }

		public ScheduleSettings Clone()
		{
			IFormatter formatter = new BinaryFormatter();
			Stream stream = new MemoryStream();

			using (stream)
			{
				formatter.Serialize(stream, this);

				stream.Seek(0, SeekOrigin.Begin);

				return (ScheduleSettings) formatter.Deserialize(stream);
			}
		}
	}
}
