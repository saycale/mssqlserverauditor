﻿using System;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain.Models
{
	[Serializable]
	public class ModelBase
	{
		[XmlIgnore]
		public long Id { get; set; } = 0;

		[XmlIgnore]
		public bool IsNew => Id == 0;
	}
}