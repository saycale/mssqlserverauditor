﻿namespace MSSQLServerAuditor.Core.Domain.Models
{
	public class LastConnection : ModelBase
	{
		public string         MachineName       { get; set; }
		public ConnectionType ConnectionType    { get; set; }
		public string         ModuleType        { get; set; }
		public long?          ConnectionGroupId { get; set; }
		public long?          TemplateId        { get; set; }
		public Template       Template          { get; set; }
	}
}
