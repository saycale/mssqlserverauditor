﻿using System;

namespace MSSQLServerAuditor.Core.Domain.Models
{
	public class NodeInstance : ModelBase
	{
		public long?     ConnectionGroupId    { get; set; }
		public long?     TemplateNodeId       { get; set; }
		public long?     ParentId             { get; set; }
		public string    UId                  { get; set; }
		public string    Name                 { get; set; }
		public string    Icon                 { get; set; }
		public bool?     IsEnabled            { get; set; }
		public string    FontColor            { get; set; }
		public string    FontStyle            { get; set; }
		public long?     SequenceNumber       { get; set; }
		public bool?     ChildrenNotProcessed { get; set; }
		public DateTime? ScheduledUpdate      { get; set; }
		public long?     Counter              { get; set; }
		public DateTime? LastUpdate           { get; set; }
		public long?     LastUpdateDuration   { get; set; }
	}
}
