using System;
using MSSQLServerAuditor.Common;

namespace MSSQLServerAuditor.Core.Domain.Models
{
	public class ETLResult : ModelBase
	{
		public long?    QueryId                { get; set; }
		public long     TargetServerInstanceId { get; set; }
		public long?    Rows                   { get; set; }
		public string   ErrorId                { get; set; }
		public string   ErrorCode              { get; set; }
		public string   ErrorMessage           { get; set; }
		public DateTime DateCreated            { get; set; }
		public DateTime DateUpdated            { get; set; } = DefaultValues.Date.Minimum;
	}
}