using System;

namespace MSSQLServerAuditor.Core.Domain.Models
{
	public static class ServerInstanceFactory
	{
		public static ServerInstance CreateInstance(ConnectionType connectionType)
		{
			switch (connectionType)
			{
				case ConnectionType.ActiveDirectory:
					return new LdapInstance();
				case ConnectionType.System:
					return new SystemConnectionInstance();
				case ConnectionType.EventLog:
					return new EventLogInstance();
				case ConnectionType.Internal:
					return new InternalServerInstance();
				case ConnectionType.MSSQL:
					return new MssqlInstance();
				case ConnectionType.NetworkInformation:
					return new NetworkInstance();
				case ConnectionType.SQLite:
					return new SqliteInstance();
				case ConnectionType.TDSQL:
					return new TeradataInstance();
				case ConnectionType.WMI:
					return new WmiInstance();
				default:
					throw new ArgumentOutOfRangeException($"Invalid connection type '{connectionType}'");
			}
		}
	}
}