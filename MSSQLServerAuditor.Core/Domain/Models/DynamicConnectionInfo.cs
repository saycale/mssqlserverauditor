﻿namespace MSSQLServerAuditor.Core.Domain.Models
{
	public class DynamicConnectionInfo
	{
		public long?          QueryId        { get; set; }
		public ServerInstance ServerInstance { get; set; }
	}
}
