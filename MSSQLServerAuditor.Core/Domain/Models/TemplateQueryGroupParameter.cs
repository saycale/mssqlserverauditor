﻿namespace MSSQLServerAuditor.Core.Domain.Models
{
	public class TemplateQueryGroupParameter : ModelBase
	{
		public long?  TemplateQueryGroupId { get; set; }
		public string Name                 { get; set; }
	}
}
