﻿using System.Xml.Serialization;

namespace MSSQLServerAuditor.Core.Domain
{
	public class QueryInfoExt : QueryInfo
	{
		[XmlAttribute(AttributeName = "file")]
		public string File { get; set; }

		[XmlAttribute(AttributeName = "query")]
		public string Query { get; set; }

		public bool IsExternal => !string.IsNullOrEmpty(File);
	}
}
