﻿using System.Collections.Generic;
using System.Data;
using MSSQLServerAuditor.Common.Utils;

namespace MSSQLServerAuditor.Core.Domain
{
	public class NodeAttributes
	{
		private const string AttUName      = "nodeuname";
		private const string AttIcon       = "nodeuicon";
		private const string AttUId        = "nodeuid";
		private const string AttIsEnabled  = "nodeisenabled";
		private const string AttIsDisabled = "nodeisdisabled";
		private const string AttFontColor  = "nodefontcolor";
		private const string AttFontStyle  = "nodefontstyle";

		private readonly Dictionary<string, string> _attributes;

		public NodeAttributes()
		{
			this._attributes    = new Dictionary<string, string>();
		}

		public NodeAttributes(DataRow row) : this()
		{
			for (int i = 0; i < row.Table.Columns.Count; i++)
			{
				string name  = row.Table.Columns[i].ColumnName;
				string value = row[i].ToString();

				if (!string.IsNullOrWhiteSpace(name))
				{
					this._attributes[name.ToLower()] = value;
				}
			}
		}

		public string UName
		{
			get { return Get(AttUName); }
			set { Set(AttUName, value); }
		}

		public string UId
		{
			get { return Get(AttUId); }
			set { Set(AttUId, value); }
		}

		public string Icon
		{
			get { return Get(AttIcon); }
			set { Set(AttIcon, value); }
		}

		public string FontColor
		{
			get { return Get(AttFontColor); }
			set { Set(AttFontColor, value); }
		}

		public string FontStyle
		{
			get { return Get(AttFontStyle); }
			set { Set(AttFontStyle, value); }
		}

		public bool IsEnabled
		{
			get
			{
				// check if it is enabled explicitly
				string sEnabled = Get(AttIsEnabled);
				if (sEnabled != null)
				{
					int enabled;
					if (int.TryParse(sEnabled, out enabled))
					{
						return enabled == 1;
					}
				}

				// check if it is disabled explicitly
				string sDisabled = Get(AttIsDisabled);
				int disabled;
				if (int.TryParse(sDisabled, out disabled))
				{
					return disabled != 1;
				}

				return true; // node is enabled by default
			}
			set { Set(AttIsEnabled, value ? "1" : "0"); }
		}

		public string Get(string name, string defaultValue = null)
		{
			if (name == null)
			{
				return defaultValue;
			}

			string value;
			if (this._attributes.TryGetValue(name.ToLower(), out value))
			{
				return value;
			}

			return defaultValue;
		}

		public void Set(string name, string value)
		{
			this._attributes[name] = value;
		}

		public T Get<T>(string name, T defaultValue = default(T))
		{
			if (name == null)
			{
				return defaultValue;
			}

			string value;
			if (this._attributes.TryGetValue(name.ToLower(), out value))
			{
				return ConvertString.ToType<T>(value);
			}

			return defaultValue;
		}

		public Dictionary<string, string> Values => this._attributes;
	}
}
