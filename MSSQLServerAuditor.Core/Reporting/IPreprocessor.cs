using MSSQLServerAuditor.Core.Settings;

namespace MSSQLServerAuditor.Core.Reporting
{
	public interface IPreprocessor
	{
		bool IsEnabled(IAppSettings appSettings);

		void Prepare(
			ReportData         reportData,
			PreprocessorConfig preprocessor
		);

		IVisualReport CreateControl(
			ReportData         reportData,
			PreprocessorConfig preprocessor
		);

		EmailContent CreateMailContent(
			ReportData         reportData,
			PreprocessorConfig preprocessor
		);

		string GetTitle(
			ReportData         reportData,
			PreprocessorConfig preprocessor
		);

		string GetTitle(
			ReportData       reportData,
			PreprocessorArea area
		);
	}
}
