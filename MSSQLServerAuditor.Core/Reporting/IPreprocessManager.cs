using System.Collections.Generic;

namespace MSSQLServerAuditor.Core.Reporting
{
	public interface IPreprocessManager
	{
		List<IPreprocessor> AllPreprocessors { get; }

		IPreprocessor ResolvePreprocessor(string type);
	}
}
