using System.IO;
using System.Xml;
using System.Xml.Xsl;

namespace MSSQLServerAuditor.Core.Reporting
{
	public static class XslTransformHelper
	{
		public static string Transform(ReportData data, string xslConfig)
		{
			XmlDocument xmlData    = data.XmlReport.ResultXml;
			XmlNode     docElement = xmlData.DocumentElement;

			return Transform(xslConfig, docElement);
		}

		private static string Transform(string xslConfig, XmlNode docElement)
		{
			using (StringReader xslStringReader = new StringReader(xslConfig))
			{
				using (XmlReader xslReader = XmlReader.Create(xslStringReader))
				{
					using (XmlReader inputReader = new XmlNodeReader(docElement))
					{
						XslCompiledTransform xslt = new XslCompiledTransform();

						xslt.Load(xslReader);

						using (StringWriter outputStringWriter = new StringWriter())
						{
							using (XmlWriter outputXmlWriter = XmlWriter.Create(outputStringWriter, xslt.OutputSettings))
							{
								xslt.Transform(inputReader, outputXmlWriter);

								return outputStringWriter.ToString();
							}
						}
					}
				}
			}
		}
	}
}
