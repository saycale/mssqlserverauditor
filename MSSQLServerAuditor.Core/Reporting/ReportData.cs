using System;
using MSSQLServerAuditor.Core.Domain;

namespace MSSQLServerAuditor.Core.Reporting
{
	public class ReportData
	{
		public XmlReport XmlReport { get; set; }

		public DateTime? DateTime { get; set; }
		public TimeSpan? Duration { get; set; }

		public TemplateNodeInfo TemplateNodeInfo  { get; set; }
		public long             ConnectionGroupId { get; set; }
	}
}
