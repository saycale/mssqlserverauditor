using System.Xml;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Querying.Data;

namespace MSSQLServerAuditor.Core.Reporting
{
	public class XmlReport
	{
		public XmlReport(QueryResultSet sourceData, XmlDocument resultXml)
		{
			this.SourceData = sourceData;
			this.ResultXml  = resultXml;

			if (resultXml?.DocumentElement != null)
			{
				this.XmlString = resultXml.FormatXml();
			}
		}

		public QueryResultSet SourceData { get; }
		public XmlDocument    ResultXml  { get; }
		public string         XmlString  { get; }
	}
}
