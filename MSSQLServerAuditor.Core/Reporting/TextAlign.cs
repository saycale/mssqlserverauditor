namespace MSSQLServerAuditor.Core.Reporting
{
	public enum TextAlign
	{
		Left,
		Right,
		Center,
	}
}
