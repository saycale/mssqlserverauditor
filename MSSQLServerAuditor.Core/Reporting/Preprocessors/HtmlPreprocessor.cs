using System.Net.Mime;
using MSSQLServerAuditor.Common.Utils;
using MSSQLServerAuditor.Core.Reporting.Attributes;
using MSSQLServerAuditor.Core.Settings;
using NLog;

namespace MSSQLServerAuditor.Core.Reporting.Preprocessors
{
	[Handles("HtmlPreprocessor")]
	public class HtmlPreprocessor : IPreprocessor
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		public void Prepare(ReportData reportData, PreprocessorConfig config)
		{
			// nothing to prepare
		}

		public IVisualReport CreateControl(ReportData data, PreprocessorConfig config)
		{
			string html = XslTransformHelper.Transform(data, config.Configuration);

			return new WebReport(html);
		}

		public EmailContent CreateMailContent(ReportData data, PreprocessorConfig config)
		{
			string html = XslTransformHelper.Transform(data, config.Configuration);

			return new EmailContent
			{
				Subject   = HtmlUtils.GetTitle(html, string.Empty),
				Message   = html,
				MediaType = MediaTypeNames.Text.Html
			};
		}

		public string GetTitle(ReportData data, PreprocessorConfig config)
		{
			string html = XslTransformHelper.Transform(data, config.Configuration);

			return HtmlUtils.GetTitle(html, config.Id);
		}

		public string GetTitle(ReportData data, PreprocessorArea area)
		{
			string html = XslTransformHelper.Transform(data, area.Configuration);

			return HtmlUtils.GetTitle(html, area.Id);
		}

		public bool IsEnabled(IAppSettings appSettings)
		{
			return appSettings.User.ShowHtml;
		}
	}
}
