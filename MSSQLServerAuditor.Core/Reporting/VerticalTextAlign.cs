namespace MSSQLServerAuditor.Core.Reporting
{
	public enum VerticalTextAlign
	{
		None,
		Top,
		Bottom,
	}
}
