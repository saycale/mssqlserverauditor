﻿using System.Xml.Serialization;

namespace MSSQLServerAuditor.Builder.Wix
{
	[XmlRoot("Component", Namespace = WixNamespace)]
	public class WixComponent : WixItem
	{
		public WixComponent()
		{
			
		}

		public WixComponent(string id, string guid) : base(id)
		{
			this.Guid = guid;
		}

		[XmlAttribute("Guid", Namespace = WixNamespace)]
		public string Guid { get; set; }
	}
}
