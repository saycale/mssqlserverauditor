﻿using System.Xml.Serialization;

namespace MSSQLServerAuditor.Builder.Wix
{
	[XmlRoot("File", Namespace = WixNamespace)]
	public class WixFile : WixItem
	{
		public WixFile()
		{

		}

		public WixFile(string id, string name, string source) : base(id)
		{
			this.Name   = name;
			this.Source = source;
		}

		[XmlAttribute("Name", Namespace = WixNamespace)]
		public string Name { get; set; }

		[XmlAttribute("Source", Namespace = WixNamespace)]
		public string Source { get; set; }
	}
}
