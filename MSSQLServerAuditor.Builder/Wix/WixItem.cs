﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace MSSQLServerAuditor.Builder.Wix
{
	public class WixItem
	{
		public const string WixNamespace = "http://schemas.microsoft.com/wix/2006/wi";

		public WixItem()
		{
			Children = new List<WixItem>();
		}

		public WixItem(string id) : this()
		{
			this.Id = id;
		}

		[XmlAttribute("Id", Namespace = WixNamespace)]
		public string Id { get; set; }

		[XmlElement("File",      typeof(WixFile),      Namespace = WixNamespace)]
		[XmlElement("Directory", typeof(WixDirectory), Namespace = WixNamespace)]
		[XmlElement("Component", typeof(WixComponent), Namespace = WixNamespace)]
		public List<WixItem> Children { get; set; }

		public string ToXml()
		{
			XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
			ns.Add("", WixNamespace);

			using (StringWriter writer = new StringWriter())
			{
				new XmlSerializer(GetType()).Serialize(writer, this, ns);
				return writer.ToString();
			}
		}

		public XElement ToXmlElement()
		{
			XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
			ns.Add("", WixNamespace);

			XDocument doc = new XDocument();
			using (XmlWriter writer = doc.CreateWriter())
			{
				XmlSerializer serializer = new XmlSerializer(GetType());
				serializer.Serialize(writer, this, ns);
			}

			return doc.Root;
		}

		public static T FromXmlElement<T>(XElement element)
		{
			XmlSerializer serializer = new XmlSerializer(typeof(T));

			using (XmlReader reader = element.CreateReader())
			{
				return (T) serializer.Deserialize(reader);
			}
		}
	}
}
