﻿using System;
using MSSQLServerAuditor.Builder.Model;

namespace MSSQLServerAuditor.Builder.Infrastructure
{
	public interface ILogProgress : IProgress<LogEntry>
	{
	}

	public interface IBuildProgress : ILogProgress
	{
		void Clear();

		bool IsBuilding { get; set; }
	}

	public static class ProgressExtension
	{
		public static void ReportError(this ILogProgress progress, string message)
		{
			progress.Report(LogEntry.Error(message));
		}

		public static void ReportError(this ILogProgress progress, Exception exc, string message)
		{
			progress.Report(LogEntry.Error(message));
			progress.Report(LogEntry.Error(exc.Message));
		}

		public static void ReportData(this ILogProgress progress, string message)
		{
			progress.Report(LogEntry.Data(message));
		}

		public static void ReportInfo(this ILogProgress progress, string message)
		{
			progress.Report(LogEntry.Info(message));
		}

		public static void ReportWarning(this ILogProgress progress, string message)
		{
			progress.Report(LogEntry.Warning(message));
		}

		public static void EmptyLine(this ILogProgress progress)
		{
			progress.Report(LogEntry.Empty);
		}
	}
}
