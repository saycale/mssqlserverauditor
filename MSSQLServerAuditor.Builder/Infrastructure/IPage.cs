﻿using System.ComponentModel;
using MSSQLServerAuditor.Builder.Model;
using MSSQLServerAuditor.Presentation.Validation;

namespace MSSQLServerAuditor.Builder.Infrastructure
{
	public interface IPage : IValidatable, INotifyDataErrorInfo
	{
		BuildConfig BuildConfig { get; set; }
		bool        IsEnabled   { get; set; }
		IShell      Shell       { get; }

		void Refresh();
	}
}