﻿using System;
using System.Windows.Media.Imaging;

namespace MSSQLServerAuditor.Builder.Images
{
	public static class StaticImages
	{
		static BitmapImage LoadBitmap(string name)
		{
			BitmapImage image = new BitmapImage(new Uri("pack://application:,,,/Images/" + name + ".png"));
			image.Freeze();
			return image;
		}

		public static readonly BitmapImage LangRussian = LoadBitmap("Russian");
		public static readonly BitmapImage LangEnglish = LoadBitmap("English");
	}
}
