﻿using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Builder.Infrastructure;
using MSSQLServerAuditor.Builder.Model;
using MSSQLServerAuditor.Common.Contracts;

namespace MSSQLServerAuditor.Builder.Tasks
{
	public abstract class BuilderTask
	{
		public BuilderTask(BuildConfig config)
		{
			Check.NotNull(config, nameof(config));

			this.BuildConfig = config;
		}

		public BuildConfig BuildConfig { get; }

		public abstract Task ExecuteAsync(CancellationToken cancelToken, ILogProgress progress);
	}
}
