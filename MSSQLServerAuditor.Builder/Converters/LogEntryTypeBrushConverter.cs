﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using MSSQLServerAuditor.Builder.Model;
using MSSQLServerAuditor.Presentation.Converters;

namespace MSSQLServerAuditor.Builder.Converters
{
	[ValueConversion(typeof(LogEntryType), typeof(Brush))]
	public class LogEntryTypeBrushConverter : ConverterBase
	{
		public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is LogEntryType))
			{
				throw new ArgumentException("value not of type LogEntryType");
			}

			LogEntryType type = (LogEntryType) value;

			switch (type)
			{
				case LogEntryType.Error:
					return Brushes.Red;
				case LogEntryType.Warning:
					return Brushes.Orange;
				case LogEntryType.Data:
					return Brushes.Black;
				default:
					return Brushes.Blue;
			}
		}
	}
}
