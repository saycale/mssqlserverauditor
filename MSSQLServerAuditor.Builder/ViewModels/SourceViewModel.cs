﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Caliburn.Micro;
using MSSQLServerAuditor.Builder.Model;
using MSSQLServerAuditor.Builder.Resources;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Common.Utils;
using MSSQLServerAuditor.Presentation.Services;
using MSSQLServerAuditor.Presentation.Validation;

namespace MSSQLServerAuditor.Builder.ViewModels
{
	public class SourceViewModel : PageViewModel
	{
		private readonly IFileDialogService _fileDialogService;

		public SourceViewModel(
			IShell             shell,
			IFileDialogService fileDialogService,
			BuildConfig        buildConfig
		) : base(shell, buildConfig)
		{
			Check.NotNull(fileDialogService, nameof(fileDialogService));

			this._fileDialogService = fileDialogService;
			
			SelectedTemplates = new BindableCollection<string>();

			ConfigureValidationRules();
		}

		public string BaseDirectory => Directory.GetCurrentDirectory();

		public string DataDirectory
		{
			get { return BuildConfig.DataDirectory; }
			set
			{
				if (BuildConfig.DataDirectory != value)
				{
					BuildConfig.DataDirectory = value;

					NotifyOfPropertyChange(() => DataDirectory);
					Validator.Validate(nameof(DataDirectory));
				}
			}
		}

		public string UserSettingsFile
		{
			get { return BuildConfig.UserSettingsFile; }
			set
			{
				if (BuildConfig.UserSettingsFile != value)
				{
					BuildConfig.UserSettingsFile = value;

					NotifyOfPropertyChange(() => UserSettingsFile);
					Validator.Validate(nameof(UserSettingsFile));
				}
			}
		}

		public string SystemSettingsFile
		{
			get { return BuildConfig.SystemSettingsFile; }
			set
			{
				if (BuildConfig.SystemSettingsFile != value)
				{
					BuildConfig.SystemSettingsFile = value;

					NotifyOfPropertyChange(() => SystemSettingsFile);
					Validator.Validate(nameof(SystemSettingsFile));
				}
			}
		}

		public IObservableCollection<string> Templates => BuildConfig.TemplateFiles;

		public IObservableCollection<string> SelectedTemplates { get; }

		public void RemoveSelectedTemplates()
		{
			List<string> selectedTemplates = new List<string>(SelectedTemplates);
			SelectedTemplates.Clear();
			foreach (string template in selectedTemplates)
			{
				Templates.Remove(template);
			}
		}

		public void OpenSelectTemplates()
		{
			string[] xmlTemplates;

			string filter = $"{Strings.TemplateFiles}| *.xml";
			if (this._fileDialogService.ShowOpenFilesDialog(out xmlTemplates, filter, BaseDirectory))
			{
				IEnumerable<string> newTemplateFiles = xmlTemplates
					.Select(file => Paths.GetRelativePath(BaseDirectory, file))
					.Where(file => Templates.All(tfile => tfile != file));

				foreach (string newTemplateFile in newTemplateFiles)
				{
					Templates.Add(newTemplateFile);
				}
			}
		}

		public override string Title => Strings.SourceData;

		private void ConfigureValidationRules()
		{
			Validator.AddRule(
				nameof(UserSettingsFile),
				() => ValidateSettingsFileExists(UserSettingsFile)
			);

			Validator.AddRule(
				nameof(SystemSettingsFile),
				() => ValidateSettingsFileExists(SystemSettingsFile)
			);

			Validator.AddRule(
				nameof(DataDirectory), () =>
				{
					if (string.IsNullOrWhiteSpace(DataDirectory))
					{
						return RuleResult.Invalid(Strings.DataDirectoryIsRequired);
					}

					if (!Directory.Exists(DataDirectory))
					{
						return RuleResult.Invalid(Strings.DataDirectoryDoesNotExist);
					}

					return RuleResult.Valid();
				}
			);
		}

		private RuleResult ValidateSettingsFileExists(string filepath)
		{
			if (string.IsNullOrWhiteSpace(filepath))
			{
				return RuleResult.Invalid(Strings.SettingsFileIsRequired);
			}

			if (!File.Exists(filepath))
			{
				return RuleResult.Invalid(Strings.SettingsFileDoesNotExist);
			}

			return RuleResult.Valid();
		}
	}
}
