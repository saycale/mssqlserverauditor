﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using Microsoft.Win32;
using MSSQLServerAuditor.Builder.Infrastructure;
using MSSQLServerAuditor.Builder.Localization;
using MSSQLServerAuditor.Builder.Model;
using MSSQLServerAuditor.Builder.Properties;
using MSSQLServerAuditor.Builder.Resources;
using MSSQLServerAuditor.Builder.Tasks;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.Core.Xml;
using MSSQLServerAuditor.Presentation.Services;
using NLog;
using LogManager = NLog.LogManager;

namespace MSSQLServerAuditor.Builder.ViewModels
{
	public class ShellViewModel : Screen, IShell
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly IWindowManager     _windowManager;
		private readonly IFileDialogService _fileDialogService;

		private bool         _isValid;
		private bool         _isEditEnabled = false;
		private bool         _showLanguages;
		private LanguageItem _language;

		private IPage _currentPage;

		private readonly BuildConfig            _buildConfig;
		private readonly BuildProgressViewModel _buildProgressPage;

		private CancellationTokenSource _canceller;

		public ShellViewModel(IWindowManager windowManager, IFileDialogService fileDialogService)
		{
			Check.NotNull(windowManager,     nameof(windowManager));
			Check.NotNull(fileDialogService, nameof(fileDialogService));

			this._windowManager     = windowManager;
			this._fileDialogService = fileDialogService;

			this._canceller = new CancellationTokenSource();

			Languages = new BindableCollection<LanguageItem>(LanguageProvider.AvailableLanguages);
			Language  = LanguageProvider.CurrentLanguage;

			this._buildConfig = BuildConfig.CreateDefault();

			this._buildProgressPage = new BuildProgressViewModel(this, this._buildConfig)
			{
				IsEnabled = false
			};

			Pages = new BindableCollection<IPage>
			{
				new ConfigViewModel(this, this._buildConfig),
				new SourceViewModel(this, fileDialogService, this._buildConfig) { IsEnabled = false },
				new SignAndEncryptViewModel(this, this._buildConfig)            { IsEnabled = false },
				new AppBuildViewModel(this, this._buildConfig)                  { IsEnabled = false },
				new SetupBuildViewModel(this, this._buildConfig)                { IsEnabled = false },
				this._buildProgressPage
			};

			IsValid = false;
			foreach (IPage page in Pages)
			{
				page.ErrorsChanged += PageOnErrorsChanged;
			}

			CurrentPage = Pages.First();
		}

		public IObservableCollection<LanguageItem> Languages { get; } 

		public IObservableCollection<IPage> Pages { get; }

		public IPage CurrentPage
		{
			get { return this._currentPage; }
			set
			{
				if (this._currentPage != value)
				{
					this._currentPage = value;

					NotifyOfPropertyChange(() => CanMoveNextPage);
					NotifyOfPropertyChange(() => CanMovePreviousPage);

					NotifyOfPropertyChange(() => CurrentPage);
				}
			}
		}

		public LanguageItem Language
		{
			get { return this._language; }
			set
			{
				this._language = value;
				NotifyOfPropertyChange(() => Language);
			}
		}

		public bool ShowLanguages
		{
			get { return this._showLanguages; }
			set
			{
				this._showLanguages = value;
				NotifyOfPropertyChange(() => ShowLanguages);
			}
		}

		public bool IsEditEnabled
		{
			get { return this._isEditEnabled; }
			set
			{
				this._isEditEnabled = value;
				NotifyOfPropertyChange(() => IsEditEnabled);
			}
		}

		public void SaveConfig()
		{
			string location = this._buildConfig.Location;
			if (location != null && File.Exists(location))
			{
				try
				{
					XmlSerialization.Serialize(location, this._buildConfig);
				}
				catch (Exception exc)
				{
					string message = string.Format(Strings.FailedSaveBuildConfig, location);
					Log.Error(exc, message);

					throw new FileLoadException(
						message,
						exc
					);
				}
			}
		}

		public void SaveConfigAs()
		{
			SaveFileDialog saveDialog = new SaveFileDialog
			{
				FileName   = "buildConfig.xml",
				Filter     = $"{Strings.ConfigurationFile} (*.xml)|*.xml",
				DefaultExt = ".xml"
			};

			if (saveDialog.ShowDialog() == true)
			{
				string location = saveDialog.FileName;

				try
				{
					this._buildConfig.Location = location;

					XmlSerialization.Serialize(location, this._buildConfig);

					EnableConfigEdit();
					Refresh();
				}
				catch (Exception exc)
				{
					string message = string.Format(Strings.FailedSaveBuildConfig, location);
					Log.Error(exc, message);

					throw new FileLoadException(
						message,
						exc
					);
				}
			}
		}

		public void Exit()
		{
			Application.Current.Shutdown();
		}

		public bool CanMovePreviousPage
		{
			get
			{
				int pageIndex = Pages.IndexOf(CurrentPage);
				if (pageIndex > 0)
				{
					return Pages[pageIndex - 1].IsEnabled;
				}

				return false;
			}
		}

		public void MovePreviousPage()
		{
			int pageIndex = Pages.IndexOf(CurrentPage);
			if (pageIndex > 0)
			{
				CurrentPage = Pages[pageIndex - 1];
			}
		}

		public void EnableConfigEdit()
		{
			foreach (IPage page in Pages)
			{
				page.Validate();
				page.IsEnabled = true;
			}

			CheckValidationErrors();

			this._buildProgressPage.IsEnabled = false;

			IsEditEnabled = true;
		}

		public bool CanMoveNextPage
		{
			get
			{
				int pageIndex = Pages.IndexOf(CurrentPage);

				if (pageIndex >= 0 && pageIndex < Pages.Count - 1)
				{
					return Pages[pageIndex + 1].IsEnabled;
				}

				return false;
			}
		} 

		public void MoveNextPage()
		{
			int pageIndex = Pages.IndexOf(CurrentPage);
			if (pageIndex < Pages.Count - 1)
			{
				CurrentPage = Pages[pageIndex + 1];
			}
		}

		public bool IsValid
		{
			get { return this._isValid; }
			set
			{
				if (this._isValid != value)
				{
					this._isValid = value;

					NotifyOfPropertyChange(() => IsValid);
				}
			}
		}

		public string ConfigLocation => this._buildConfig?.Location;

		public bool IsBuilding
		{
			get { return this._buildProgressPage.IsBuilding; }
			set
			{
				if (this._buildProgressPage.IsBuilding != value)
				{
					this._buildProgressPage.IsBuilding = value;
					NotifyOfPropertyChange(() => IsBuilding);
				}
			}
		}

		public async Task Build()
		{
			CheckValidationErrors(true);

			if (IsValid)
			{
				this._buildProgressPage.IsEnabled = true;
				this._buildProgressPage.Clear();

				CurrentPage = this._buildProgressPage;

				// save config before build
				XmlSerialization.Serialize(this._buildConfig.Location, this._buildConfig);

				CancellationToken cancelToken = this._canceller.Token;

				IsBuilding = true;
				try
				{
					if (this._buildConfig.Compile)
					{
						CompileAppTask compile = new CompileAppTask(this._buildConfig);
						await compile.ExecuteAsync(cancelToken, this._buildProgressPage);
					}

					cancelToken.ThrowIfCancellationRequested();

					BuildSetupTask setup = new BuildSetupTask(this._buildConfig);
					await setup.ExecuteAsync(cancelToken, this._buildProgressPage);

					this._buildProgressPage.ReportInfo("Done.");
				}
				catch (OperationCanceledException ocExc)
				{
					this._buildProgressPage.ReportWarning(ocExc.Message);
				}
				catch (Exception exc)
				{
					Log.Error(exc, "Error occurred during the build.");

					this._buildProgressPage.ReportError(exc.Message);
				}
				finally
				{
					IsBuilding = false;
				}
			}
		}

		public void StopBuild()
		{
			this._canceller.Cancel();
			this._canceller = new CancellationTokenSource();
		}

		public void HideLanguages()
		{
			ShowLanguages = false;
		}

		public void ChangeLanguage()
		{
			HideLanguages();

			string lang = Language.Name;
			if (!lang.IsNullOrEmpty())
			{
				Settings.Default.Language = lang;

				LocalizationManager.Localize(lang);

				foreach (IPage page in Pages)
				{
					page.Refresh();
				}
			}
		}

		protected override void OnInitialize()
		{
			base.OnInitialize();
			
			DisplayName = Strings.Builder;
		}

		public override void Refresh()
		{
			base.Refresh();
			foreach (IPage page in Pages)
			{
				page.Refresh();
			}
		}

		private void PageOnErrorsChanged(object sender, DataErrorsChangedEventArgs args)
		{
			CheckValidationErrors();
		}

		private void CheckValidationErrors(bool forceValidation = false)
		{
			foreach (IPage page in Pages)
			{
				if (forceValidation)
				{
					page.Validate();
				}

				if (page.HasErrors)
				{
					IsValid = false;
					return;
				}
			}

			IsValid = true;
		}
	}
}
