﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Threading;
using System.Threading.Tasks;
using MSSQLServerAuditor.Builder.Infrastructure;
using MSSQLServerAuditor.Common.Extensions;

namespace MSSQLServerAuditor.Builder.Utils
{
	public class PowerShellExecutor
	{
		private readonly ILogProgress _progress;

		public PowerShellExecutor(ILogProgress progress)
		{
			this._progress = progress;
		}

		public async Task ExecuteAsync(
			string                     scriptFile,
			Dictionary<string, string> parameters,
			CancellationToken          cancelToken)
		{
			await Task.Run(() =>
			{
				try
				{
					using (Runspace rs = RunspaceFactory.CreateRunspace())
					{
						string directory = Path.GetDirectoryName(scriptFile);
						string file      = Path.GetFileName(scriptFile);

						rs.Open();
						rs.SessionStateProxy.Path.SetLocation(directory);

						using (PowerShell powerShell = PowerShell.Create())
						{
							powerShell.Streams.Debug.DataAdded   += Debug_DataAdded;
							powerShell.Streams.Warning.DataAdded += Warning_DataAdded;
							powerShell.Streams.Error.DataAdded   += Error_DataAdded;

							powerShell.Runspace = rs;

							// allow script execution
							powerShell.AddScript("Set-ExecutionPolicy -Scope Process -ExecutionPolicy RemoteSigned");

							List<string> paramPairs = new List<string>();
							foreach (KeyValuePair<string, string> parameter in parameters)
							{
								string name  = parameter.Key;
								string value = parameter.Value;
								paramPairs.Add($"{name} {value}");
							}

							string args = paramPairs.Join(" ");
							powerShell.AddScript($@".\{file} {args}", true);

							PSDataCollection<PSObject> outputCollection = new PSDataCollection<PSObject>();
							outputCollection.DataAdded += Output_DataAdded;

							IAsyncResult psInvocation = powerShell.BeginInvoke<PSObject, PSObject>(null, outputCollection);

							WaitHandle.WaitAny(new[] { psInvocation.AsyncWaitHandle, cancelToken.WaitHandle });

							if (cancelToken.IsCancellationRequested)
							{
								powerShell.Stop();
							}

							cancelToken.ThrowIfCancellationRequested();

							powerShell.EndInvoke(psInvocation);
						}
					}
				}
				catch (OperationCanceledException)
				{
					throw;
				}
				catch (Exception exc)
				{
					this._progress.ReportError(exc.Message);
				}
			}, cancelToken);
		}

		private void Debug_DataAdded(object sender, DataAddedEventArgs e)
		{
			PSDataCollection<DebugRecord> collection = sender as PSDataCollection<DebugRecord>;
			if (collection != null)
			{
				DebugRecord dbgObj = collection[e.Index];

				this._progress.ReportData(dbgObj.ToString());
			}
		}

		private void Output_DataAdded(object sender, DataAddedEventArgs e)
		{
			PSDataCollection<PSObject> collection = sender as PSDataCollection<PSObject>;
			if (collection != null)
			{
				PSObject psObject = collection[e.Index];

				this._progress.ReportData(psObject.BaseObject.ToString());
			}
		}

		private void Error_DataAdded(object sender, DataAddedEventArgs e)
		{
			PSDataCollection<ErrorRecord> collection = sender as PSDataCollection<ErrorRecord>;
			if (collection != null)
			{
				ErrorRecord errObj = collection[e.Index];

				this._progress.ReportError(errObj.Exception.ToString());
			}
		}

		private void Warning_DataAdded(object sender, DataAddedEventArgs e)
		{
			PSDataCollection<WarningRecord> collection = sender as PSDataCollection<WarningRecord>;
			if (collection != null)
			{
				WarningRecord warnObj = collection[e.Index];

				this._progress.ReportWarning(warnObj.Message);
			}
		}
	}
}
