﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Caliburn.Micro;

namespace MSSQLServerAuditor.Builder.Model
{
	[XmlRoot("buildConfig")]
	public class BuildConfig
	{
		private const string DefaultBuildScriptFile = @"..\..\build.ps1";

		private static readonly List<string> TargetPlatformList     = new List<string> { "x86", "x64" };
		private static readonly List<string> BuildConfigurationList = new List<string> { "Release", "Debug" };

		public static ReadOnlyCollection<string> TargetPlatforms     => TargetPlatformList.AsReadOnly();
		public static ReadOnlyCollection<string> BuildConfigurations => BuildConfigurationList.AsReadOnly();

		public static BuildConfig CreateDefault()
		{
			BuildConfig config = new BuildConfig
			{
				TargetPlatform     = TargetPlatforms.First(),
				BuildConfiguration = BuildConfigurations.First()
			};

			if (File.Exists(DefaultBuildScriptFile))
			{
				config.BuildScriptFile = DefaultBuildScriptFile;
			}

			return config;
		}

		public BuildConfig()
		{
			TemplateFiles = new BindableCollection<string>();
		}

		[XmlIgnore]
		public string Location { get; set; }

		/// <summary>
		/// Target platform for both the application and the setup
		/// </summary>
		[XmlElement("targetPlatform")]
		public string TargetPlatform { get; set; } = "x64";

		/// <summary>
		/// Configuration to be used for compiling the app
		/// </summary>
		[XmlElement("configuration")]
		public string BuildConfiguration { get; set; } = "Release";

		/// <summary>
		/// Wix tool installation folder
		/// </summary>
		[XmlElement("wixDirectory")]
		public string WixDirectory { get; set; } = @"C:\Program Files (x86)\WiX Toolset v3.11\bin";

		/// <summary>
		/// Wix project template
		/// </summary>
		[XmlElement("wixProjectDirectory")]
		public string WixProjectDirectory { get; set; } = @"..\..\MSSQLServerAuditor.Setup";

		/// <summary>
		/// Directory to be used for temporary files
		/// </summary>
		[XmlElement("wixTempDirectory")]
		public string WixTempDirectory { get; set; } = @"..\..\temp";

		/// <summary>
		/// Directory that contains static files (Images, js, PostBuild) to be added to the setup
		/// </summary>
		[XmlElement("dataDirectory")]
		public string DataDirectory { get; set; } = @"..\..\data";

		/// <summary>
		/// The list of templates to be added to the setup
		/// </summary>
		[XmlArray("templates")]
		[XmlArrayItem("template")]
		public BindableCollection<string> TemplateFiles { get; set; }

		/// <summary>
		/// Path for system settings file
		/// </summary>
		[XmlElement("systemSettingsFile")]
		public string SystemSettingsFile { get; set; } = @"..\..\data\MSSQLServerAuditor.SystemSettings.xml";

		/// <summary>
		/// Path for user settings file
		/// </summary>
		[XmlElement("userSettingsFile")]
		public string UserSettingsFile { get; set; } = @"..\..\data\MSSQLServerAuditor.UserSettings.xml";

		/// <summary>
		/// Path for the cake build script 
		/// </summary>
		[XmlElement("buildScriptFile")]
		public string BuildScriptFile { get; set; }

		/// <summary>
		/// Specifies whether to compile the app
		/// </summary>
		[XmlElement("compile")]
		public bool Compile { get; set; } = true;

		/// <summary>
		/// Specifies whether to run tests after the app is compiled
		/// </summary>
		[XmlElement("runTests")]
		public bool RunTests { get; set; } = true;

		/// <summary>
		/// Specifies whether encryption and signing of xml files is required
		/// </summary>
		[XmlElement("protectData")]
		public bool ProtectData { get; set; } = false;

		/// <summary>
		/// Key to encrypt and decrypt xml files
		/// </summary>
		[XmlElement("encryptionKey")]
		public string EncryptionKey { get; set; }

		/// <summary>
		/// Private key to sign xml files (known for builder)
		/// </summary>
		[XmlElement("signPrivateKey")]
		public string SignPrivateKey { get; set; }

		/// <summary>
		/// Public key to validate signatures of xml files (used by client)
		/// </summary>
		[XmlElement("signPublicKey")]
		public string SignPublicKey { get; set; }

		public void CopyFrom(BuildConfig source)
		{
			TargetPlatform      = source.TargetPlatform;
			BuildConfiguration  = source.BuildConfiguration;

			WixDirectory        = source.WixDirectory;
			WixProjectDirectory = source.WixProjectDirectory;
			WixTempDirectory    = source.WixTempDirectory;

			DataDirectory       = source.DataDirectory;

			TemplateFiles.Clear();
			TemplateFiles.AddRange(source.TemplateFiles);

			SystemSettingsFile  = source.SystemSettingsFile;
			UserSettingsFile    = source.UserSettingsFile;

			BuildScriptFile     = source.BuildScriptFile;
			Compile             = source.Compile;
			RunTests            = source.RunTests;

			ProtectData         = source.ProtectData;
			EncryptionKey       = source.EncryptionKey;
			SignPrivateKey      = source.SignPrivateKey;
			SignPublicKey       = source.SignPublicKey;
		}
	}
}
