﻿using System.Windows.Media.Imaging;

namespace MSSQLServerAuditor.Builder.Localization
{
	public class LanguageItem
	{
		public string      Name    { get; set; }
		public string      Display { get; set; }
		public BitmapImage Image   { get; set; }
	}
}
