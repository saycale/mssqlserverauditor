﻿using System.Globalization;
using MSSQLServerAuditor.Builder.Resources;
using MSSQLServerAuditor.Presentation.Localization;

namespace MSSQLServerAuditor.Builder.Localization
{
	public static class LocalizationManager
	{
		public static void Localize(string lang)
		{
			CultureInfo culture = new CultureInfo(lang);

			AppLocalizer.ChangeCulture(culture);

			Strings.Culture = culture;
		}
	}
}
