﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using MSSQLServerAuditor.Builder.Images;
using MSSQLServerAuditor.Builder.Properties;

namespace MSSQLServerAuditor.Builder.Localization
{
	public static class LanguageProvider
	{
		static LanguageProvider()
		{
			AvailableLanguages = new List<LanguageItem>
			{
				new LanguageItem { Display = "ENG", Image = StaticImages.LangEnglish, Name = "en"},
				new LanguageItem { Display = "РУС", Image = StaticImages.LangRussian, Name = "ru"}
			};
		}

		public static List<LanguageItem> AvailableLanguages { get; }

		public static LanguageItem CurrentLanguage
		{
			get
			{
				string lang = Settings.Default.Language ?? CultureInfo.CurrentUICulture.TwoLetterISOLanguageName;

				LanguageItem langItem = AvailableLanguages.FirstOrDefault(
					l => string.Equals(l.Name, lang, StringComparison.OrdinalIgnoreCase)
				);

				return langItem ?? AvailableLanguages.First();
			}
		}
	}
}
