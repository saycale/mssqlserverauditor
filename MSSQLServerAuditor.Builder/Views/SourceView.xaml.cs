﻿using System.Windows.Controls;

namespace MSSQLServerAuditor.Builder.Views
{
	/// <summary>
	/// Interaction logic for SourceView.xaml
	/// </summary>
	public partial class SourceView : UserControl
	{
		public SourceView()
		{
			InitializeComponent();
		}
	}
}
