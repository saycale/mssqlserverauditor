<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="Monitoring.Instance.FixedDrives.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Instance Free Drive Size (MB)</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Свободное место на локальных дисках (МБ)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Instance Free Drive Size (MB)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="Monitoring.Instance.FixedDrives.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Instance Free Drive Size (MB)</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Свободное место на локальных дисках (МБ)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Instance Free Drive Size (MB)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
			<link rel="stylesheet" href="$JS_FOLDER$/tablesorter/css/theme.mssqlserverauditor.css" type="text/css"/>

			<script src="$JS_FOLDER$/json-js/json2.js"></script>
			<script src="$JS_FOLDER$/jquery-1.12.4.min.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.widgets.js"></script>

			<script type="text/javascript">
				$(document).ready(function() {
					$("#myErrorTable").tablesorter({
						theme : 'MSSQLServerAuditorError',

						widgets: [ "zebra", "resizable", "stickyHeaders" ],

						widgetOptions : {
							zebra : ["even", "odd"]
						}
					});

					$("#myTable").tablesorter({
						theme : 'MSSQLServerAuditor',

						widgets: [ "zebra", "resizable", "stickyHeaders" ],

						widgetOptions : {
							zebra : ["even", "odd"]
						}
					});
				});
			</script>
		</head>
		<body>
			<style>
				body { overflow: auto; padding:0; margin:0; }
			</style>
			<xsl:apply-templates select="child::node()"/>
		</body>
		</html>
		</xsl:template>

		<xsl:template match="MSSQLResult[@name='GetInstanceFixedDrive' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']">
			<table id="myTable">
			<caption>
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Instance Free Drive Size (MB)</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Свободное место на локальных дисках (МБ)</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Instance Free Drive Size (MB)</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</caption>
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Экземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Drive</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Диск</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Drive</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>FreeSpace (MB)</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Свободно (МБ)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>FreeSpace (MB)</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="Row">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="../../@instance != ''">
								<xsl:value-of select="../../@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="Drive != ''">
								<xsl:value-of select="Drive"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="FreeSpaceMB != ''">
								<xsl:value-of select="format-number(FreeSpaceMB, '###,###,##0')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
