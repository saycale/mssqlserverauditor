<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabaseUserAliases.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>User Aliased</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Ссылочные пользователи баз данных</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>User Aliased</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="DatabaseUserAliases.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>User Aliased</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Ссылочные пользователи баз данных</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>User Aliased</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Security issues when using aliased users in SQL Server</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Ссылочные пользователи баз данных</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Security issues when using aliased users in SQL Server</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>

						SQL Server has a lot of little features that are nice to use, but sometimes
						these things come back to get you. One such issue is the use of aliased
						users. This tip shows you how to find security holes when aliased users are
						setup in your databases and also that this feature will be deprecated in SQL
						Server 2008.

					</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>

						Использование ссылок для пользователей объявлено устаревшим и не
						рекомендуется к использованию, хотя и данная функциональность доступна в MS
						SQL 2005, главным образом для сохранения совместимости. Для того, чтобы
						избежать потенциальных проблем откажитесь от данной функциональности.

					</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>

						SQL Server has a lot of little features that are nice to use, but sometimes
						these things come back to get you. One such issue is the use of aliased
						users. This tip shows you how to find security holes when aliased users are
						setup in your databases and also that this feature will be deprecated in SQL
						Server 2008.

					</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Another interesting point to note is that, you cannot drop an aliased user using the sp_dropuser stored procedure, instead, use sp_dropalias.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Важное замечаний: нужно использовать хранимую процедуру sp_dropalias для удаления ссылок для пользователей.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Another interesting point to note is that, you cannot drop an aliased user using the sp_dropuser stored procedure, instead, use sp_dropalias.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> If there are aliased users that have "dbo" level rights look at removing these and using roles instead.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Постарайтесь отказаться от использования ссылок для пользователей.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> If there are aliased users that have "dbo" level rights look at removing these and using roles instead.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Moderate</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Moderate</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<a href="http://www.mssqltips.com/sqlservertip/1675/security-issues-when-using-aliased-users-in-sql-server/" target="_blank">Security issues when using aliased users in SQL Server</a>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
