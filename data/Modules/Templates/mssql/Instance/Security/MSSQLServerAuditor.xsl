<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="ServerSecurity.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Security and server access</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Безопасность и наличие доступа к серверу</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Security and server access</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="ServerSecurity.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Security and server access</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Безопасность и наличие доступа к серверу</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Security and server access</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
			<style>
				body { overflow: auto; padding:0; margin:0; }
			</style>

			<h2>
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Security and server access</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Безопасность и наличие доступа к серверу</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Security and server access</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h2>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
