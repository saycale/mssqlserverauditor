<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="AuditInstanceParameters.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Data on MS SQL server instance</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Данные об экземпляре MS SQL сервера</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Data on MS SQL server instance</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="AuditInstanceParameters.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Data on MS SQL server instance</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Данные об экземпляре MS SQL сервера</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Data on MS SQL server instance</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
			<link rel="stylesheet" href="$JS_FOLDER$/tablesorter/css/theme.mssqlserverauditor.css" type="text/css"/>

			<script src="$JS_FOLDER$/json-js/json2.js"></script>
			<script src="$JS_FOLDER$/jquery-1.12.4.min.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.widgets.js"></script>

			<script type="text/javascript">
				$(document).ready(function()
					{
						$("#myErrorTable").tablesorter({
							theme : 'MSSQLServerAuditorError',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});

						$("#myTable").tablesorter({
							theme : 'MSSQLServerAuditor',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});
					}
				);
			</script>
		</head>
		<body>
			<style>
				body { overflow: auto; padding:0; margin:0; }
			</style>
			<xsl:if test="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']/child::node()">
			<table id="myErrorTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Query</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Запрос</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Query</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Hierarchy</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Категория</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Hierarchy</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>RecordSets</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Наборов</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>RecordSets</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>#</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Код</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Номер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сообщение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="@instance != ''">
								<xsl:value-of select="@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@name != ''">
								<xsl:value-of select="@name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@hierarchy != ''">
								<xsl:value-of select="@hierarchy"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RecordSets != ''">
								<xsl:value-of select="@RecordSets"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RowCount != ''">
								<xsl:value-of select="@RowCount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorCode != ''">
								<xsl:value-of select="@SqlErrorCode"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorNumber != ''">
								<xsl:value-of select="@SqlErrorNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="SqlErrorMessage != ''">
								<xsl:value-of select="SqlErrorMessage"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
			<xsl:if test="MSSQLResult[@name='GetServerInfo' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/child::node()">
			<table id="myTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Name</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Название</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Name</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Installed version</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Установленнная версия</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Installed version</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>The latest released version</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Последняя выпущенная версия</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>The latest released version</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Type</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Тип</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Type</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Account</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Учётная запись</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Account</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Cluster unit</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Часть кластера</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Cluster unit</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Server</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сервер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Server</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Tcp/Ip port</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Tcp/Ip порт</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Tcp/Ip port</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Collation</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Тип сравнения строк</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Collation</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@name='GetServerInfo' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/Row">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="../../@instance != ''">
								<xsl:value-of select="../../@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="IsLatestVersion != 'True'">
								<xsl:attribute name="style">font-weight: bold; color: red;</xsl:attribute>
							</xsl:when>
							<xsl:otherwise>
								<xsl:attribute name="style">font-weight: bold; color: green;</xsl:attribute>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:choose>
							<xsl:when test="ProductVersion != ''">
								<xsl:value-of select="ProductVersion"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="LatestVersion != ''">
								<xsl:value-of select="LatestVersion"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="Edition != ''">
								<xsl:value-of select="Edition"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="ServiceAccount != ''">
								<xsl:value-of select="ServiceAccount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="IsClustered = '1'">
								<xsl:text>Да</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Нет</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="ComputerNamePhysicalNetBIOS != ''">
								<xsl:value-of select="ComputerNamePhysicalNetBIOS"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:choose>
									<xsl:when test="MachineName != ''">
										<xsl:value-of select="MachineName"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>&#160;</xsl:text>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="TcpPort != ''">
								<xsl:value-of select="TcpPort"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="Collation != ''">
								<xsl:value-of select="Collation"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>

<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="LatestMSSQLServerVersions.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>The Latest MS SQL server versions</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Последние версии MS SQL сервера</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>The Latest MS SQL server versions</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="LatestMSSQLServerVersions.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>The Latest MS SQL server versions</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Последние версии MS SQL сервера</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>The Latest MS SQL server versions</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
			<h2>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Summary table</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Итоговая таблица:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Summary table</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</h2>

			<table border="1" cellpadding="4" cellspacing="0">
				<tr>
					<th bgcolor="#f0f0f0">&#160;</th>
					<th bgcolor="#f0f0f0">RTM (Gold, no SP)</th>
					<th bgcolor="#f0f0f0">SP1</th>
					<th bgcolor="#f0f0f0">SP2</th>
					<th bgcolor="#f0f0f0">SP3</th>
					<th bgcolor="#f0f0f0">SP4</th>
					<th bgcolor="#f0f0f0">SP5</th>
					<th bgcolor="#f0f0f0">Last</th>
				</tr>
				<tr>
					<td><strong>SQL Server vNext</strong> (code name vNext)</td>
					<td bgcolor="#FFCCCC">&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td bgcolor="#3CB371">14.0.1.246</td>
				</tr>
				<tr>
					<td><strong>SQL Server 2016</strong> (code name SQL16)</td>
					<td bgcolor="#FFCCCC">13.0.1601.5</td>
					<td>13.0.4001.0</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td bgcolor="#3CB371">13.0.4199.0</td>
				</tr>
				<tr>
					<td><strong>SQL Server 2014</strong> (code name SQL14)</td>
					<td bgcolor="#FFCCCC">12.00.2000.8</td>
					<td>12.0.4100.1</td>
					<td>12.0.5000.0</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td bgcolor="#3CB371">12.00.5532.0</td>
				</tr>
				<tr>
					<td><strong>SQL Server 2012</strong> (code name Denali)</td>
					<td bgcolor="#FFCCCC">11.0.2100.60</td>
					<td>11.0.3000.0</td>
					<td>11.0.5058.0</td>
					<td>11.0.6020.0</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td bgcolor="#3CB371">11.00.6567.0</td>
				</tr>
				<tr>
					<td><strong>SQL Server 2008 R2</strong> (code name Kilimanjaro)</td>
					<td bgcolor="#FFCCCC">10.50.1600.1</td>
					<td>10.50.2500.0</td>
					<td>10.50.4000.0</td>
					<td>10.50.6000.34</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td bgcolor="#3CB371">10.50.6542.0</td>
				</tr>
				<tr>
					<td><strong>Azure SQL DB</strong> (code name CloudDB)</td>
					<td bgcolor="#FFCCCC">10.25</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td bgcolor="#3CB371">&#160;</td>
				</tr>
				<tr>
					<td><strong>SQL Server 2008</strong> (code name Katmai)</td>
					<td bgcolor="#FFCCCC">10.00.1600.22</td>
					<td>10.00.2531.0</td>
					<td>10.00.4000.0</td>
					<td>10.00.5500.0</td>
					<td>10.00.6000.29</td>
					<td>&#160;</td>
					<td bgcolor="#3CB371">10.00.6547.0</td>
				</tr>
				<tr>
					<td><strong>SQL Server 2005</strong> (code name Yukon)</td>
					<td bgcolor="#FFCCCC">9.00.1399.06</td>
					<td>9.00.2047</td>
					<td>9.00.3042</td>
					<td>9.00.4035</td>
					<td>9.00.5000</td>
					<td>&#160;</td>
					<td bgcolor="#3CB371">9.00.5324</td>
				</tr>
				<tr>
					<td><strong>SQL Server 2000</strong> (code name Shiloh)</td>
					<td bgcolor="#FFCCCC">8.00.194</td>
					<td>8.00.384</td>
					<td>8.00.532</td>
					<td>8.00.760</td>
					<td>8.00.2039</td>
					<td>&#160;</td>
					<td bgcolor="#3CB371">8.00.2305</td>
				</tr>
				<tr>
					<td><strong>SQL Server 7.0</strong> (code name Sphinx)</td>
					<td bgcolor="#FFCCCC">7.00.623</td>
					<td>7.00.699</td>
					<td>7.00.842</td>
					<td>7.00.961</td>
					<td>7.00.1063</td>
					<td>&#160;</td>
					<td bgcolor="#3CB371">7.00.1152</td>
				</tr>
				<tr>
					<td><strong>SQL Server 6.5</strong> (code name Hydra)</td>
					<td bgcolor="#FFCCCC">6.50.201</td>
					<td>6.50.213</td>
					<td>6.50.240</td>
					<td>6.50.252</td>
					<td>6.50.281</td>
					<td>6.50.415</td>
					<td bgcolor="#3CB371">6.50.480</td>
				</tr>
				<tr>
					<td><strong>SQL Server 6.0</strong> (code name SQL95)</td>
					<td bgcolor="#FFCCCC">6.00.121</td>
					<td>6.00.124</td>
					<td>6.00.139</td>
					<td>6.00.151</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td bgcolor="#3CB371">6.00.151</td>
				</tr>
				<tr>
					<td><strong>SQL Server 4.21 (WinNT)</strong> (code name SQLNT)</td>
					<td bgcolor="#FFCCCC">&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td bgcolor="#3CB371">&#160;</td>
				</tr>
				<tr>
					<td><strong>SQL Server 1.1 (OS/2)</strong> (code name ?)</td>
					<td bgcolor="#FFCCCC">&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td bgcolor="#3CB371">&#160;</td>
				</tr>
				<tr>
					<td><strong>SQL Server 1.0 (OS/2)</strong> (code name Ashton-Tate / MS SQL Server)</td>
					<td bgcolor="#FFCCCC">&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td>&#160;</td>
					<td bgcolor="#3CB371">&#160;</td>
				</tr>
			</table>

			<p><a href="http://sqlserverbuilds.blogspot.com.au/" target="_blank">What version of SQL Server do I have?</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>

