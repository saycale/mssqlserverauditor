<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="InstanceDynamicParameters.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Instance Dynamic Parameters</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Динамические счётчики</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Instance Dynamic Parameters</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="InstanceDynamicParameters.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Instance Dynamic Parameters</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Динамические счётчики</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Instance Dynamic Parameters</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Instance Dynamic Parameters</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Динамические счётчики</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Instance Dynamic Parameters</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Statistics based on the table dm_os_performance_counters</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Статистика основана на данных из виртуальной таблицы dm_os_performance_counters, которая собирается с момента последнего
			рестарта экземпляра сервера баз данных.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Statistics based on the table dm_os_performance_counters</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://technet.microsoft.com/en-us/library/ms187743.aspx" target="_blank">sys.dm_os_performance_counters (Transact-SQL)</a></p>

			<p><a href="http://olontsev.ru/2012/10/deprecated-features-usage-detection/" target="_blank">Deprecated features usage detection.</a></p>

			<p><a href="http://habrahabr.ru/post/70121/" target="_blank">SQL Server Dynamic Management Views and Functions</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
