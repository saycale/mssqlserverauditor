<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="InstanceMemoryBufferCacheHitRatio.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Memory Buffer Cache Hit Ratio</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Коэффициент попадания в кэш буфера</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Memory Buffer Cache Hit Ratio</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="InstanceMemoryBufferCacheHitRatio.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Memory Buffer Cache Hit Ratio</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Коэффициент попадания в кэш буфера</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Memory Buffer Cache Hit Ratio</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
			<link rel="stylesheet" href="$JS_FOLDER$/tablesorter/css/theme.mssqlserverauditor.css" type="text/css"/>

			<script src="$JS_FOLDER$/json-js/json2.js"></script>
			<script src="$JS_FOLDER$/jquery-1.12.4.min.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.widgets.js"></script>

			<script type="text/javascript">
				$(document).ready(function()
					{
						$("#myErrorTable").tablesorter({
							theme : 'MSSQLServerAuditorError',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});

						$("#myTable").tablesorter({
							theme : 'MSSQLServerAuditor',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});

						$("#myTable2").tablesorter({
							theme : 'MSSQLServerAuditor',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});
					}
				);
			</script>
		</head>
		<body>
			<style>
				body { overflow: auto; padding:0; margin:0; }
			</style>
			<xsl:if test="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']/child::node()">
			<table id="myErrorTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Query</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Запрос</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Query</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Hierarchy</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Категория</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Hierarchy</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>RecordSets</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Наборов</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>RecordSets</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>#</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Код</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Номер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сообщение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="@instance != ''">
								<xsl:value-of select="@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@name != ''">
								<xsl:value-of select="@name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@hierarchy != ''">
								<xsl:value-of select="@hierarchy"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RecordSets != ''">
								<xsl:value-of select="@RecordSets"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RowCount != ''">
								<xsl:value-of select="@RowCount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorCode != ''">
								<xsl:value-of select="@SqlErrorCode"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorNumber != ''">
								<xsl:value-of select="@SqlErrorNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="SqlErrorMessage != ''">
								<xsl:value-of select="SqlErrorMessage"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
			<xsl:if test="MSSQLResult[@name='GetInstanceMemoryBufferCacheHitRatio' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/child::node()">
			<table id="myTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Time</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Время</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Time</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Buffer Cache Hit Ratio</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Коэффициент попадания в кэш буфера</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Buffer Cache Hit Ratio</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Buffer Cache Hit Ratio Base</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Коэффициент попадания в кэш буфера (базовое значение)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Buffer Cache Hit Ratio Base</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Buffer Cache Hit Ratio (%)</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Коэффициент попадания в кэш буфера (%)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Buffer Cache Hit Ratio (%)</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@name='GetInstanceMemoryBufferCacheHitRatio' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/Row">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="../../@instance != ''">
								<xsl:value-of select="../../@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="EventTime != ''">
								<xsl:choose>
									<xsl:when test="lang('en')">
										<xsl:value-of select="ms:format-date(EventTime, 'dd/MM/yyyy')"/>
									</xsl:when>
									<xsl:when test="lang('ru')">
										<xsl:value-of select="ms:format-date(EventTime, 'dd.MM.yyyy')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="ms:format-date(EventTime, 'dd/MM/yyyy')"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(EventTime, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="BufferCacheHitRatio != ''">
								<xsl:value-of select="format-number(BufferCacheHitRatio, '###,###')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="BufferCacheHitRatioBase != ''">
								<xsl:value-of select="format-number(BufferCacheHitRatioBase, '###,###')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="BufferCacheHitRatioPercent != ''">
								<xsl:value-of select="format-number(BufferCacheHitRatioPercent, '###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
			<xsl:if test="MSSQLResult[@name='GetInstancePageLifeExpectancy' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/child::node()">
			<table id="myTable2">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Time</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Время</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Time</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Page Life Expectancy (seconds)</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Продолжительность жизни страницы (секунд)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Page Life Expectancy (seconds)</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@name='GetInstancePageLifeExpectancy' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/Row">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="../../@instance != ''">
								<xsl:value-of select="../../@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="EventTime != ''">
								<xsl:choose>
									<xsl:when test="lang('en')">
										<xsl:value-of select="ms:format-date(EventTime, 'dd/MM/yyyy')"/>
									</xsl:when>
									<xsl:when test="lang('ru')">
										<xsl:value-of select="ms:format-date(EventTime, 'dd.MM.yyyy')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="ms:format-date(EventTime, 'dd/MM/yyyy')"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(EventTime, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="PageLifeExpectancy != ''">
								<xsl:value-of select="format-number(PageLifeExpectancy, '###,##0')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>

<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="InstanceMemoryBufferCacheHitRatio.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Memory Buffer Cache Hit Ratio (description)</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Коэффициент попадания в кэш буфера (описание)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Memory Buffer Cache Hit Ratio (description)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="InstanceMemoryBufferCacheHitRatio.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Memory Buffer Cache Hit Ratio (description)</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Коэффициент попадания в кэш буфера (описание)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Memory Buffer Cache Hit Ratio (description)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Buffer cache hit ratio</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Коэффициент попадания в кэш буфера</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Buffer cache hit ratio</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>In order to figure out if you need more memory for a SQL Server you can start by
			taking a look at Buffer cache hit ratio and Page life expectancy. Here is what Books On
			Line has to say about Buffer cache hit ratio:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Чтобы узнать, не требуется ли SQL Server больше памяти, взгляните на
			Коэффициент попадания в кэш буфера и Время жизни страницы. Вот, что
			говорится о Коэффициенте попадания в кэш буфера в «Books On Line»:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>In order to figure out if you need more memory for a SQL Server you can start by
			taking a look at Buffer cache hit ratio and Page life expectancy. Here is what Books On
			Line has to say about Buffer cache hit ratio:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text><em>"Buffer cache hit ratio Percentage of pages found in the buffer cache without
			having to read from disk. The ratio is the total number of cache hits divided by the
			total number of cache lookups over the last few thousand page accesses. After a long
			period of time, the ratio moves very little. Because reading from the cache is much less
			expensive than reading from disk, you want this ratio to be high. Generally, you can
			increase the buffer cache hit ratio by increasing the amount of memory available to SQL
			Server."</em></xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text><em>"Коэффициент попадания в кэш буфера сообщает процент страниц,
			найденных в кэше буфера без необходимости чтения с диска. Данный коэффициент
			показывает общее количество попаданий к кэш, деленное на общее количество
			обращений в кэш за последние несколько тысяч доступов к странице. Данный
			коэффициент со временем изменяется незначительно. Поскольку чтение из КЭШе
			требует меньшего использования памяти, нежели чтение с диска, для Вас лучше,
			чтобы данный коэффициент был высоким. Как правило, вы можете увеличить
			коэффициент попадания в кэш буфера за счет увеличения объема памяти,
			доступной для SQL Server."</em></xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text><em>"Buffer cache hit ratio Percentage of pages found in the buffer cache without
			having to read from disk. The ratio is the total number of cache hits divided by the
			total number of cache lookups over the last few thousand page accesses. After a long
			period of time, the ratio moves very little. Because reading from the cache is much less
			expensive than reading from disk, you want this ratio to be high. Generally, you can
			increase the buffer cache hit ratio by increasing the amount of memory available to SQL
			Server."</em></xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text><em>"This is the pool of memory pages into which data pages are read. An important
			indicator of the performance of the buffer cache is the Buffer Cache Hit Ratio
			performance counter. It indicates the percentage of data pages found in the buffer cache
			as opposed to disk. A value of 95% indicates that pages were found in memory 95% of the
			time. The other 5% required physical disk access. A consistent value below 90% indicates
			that more physical memory is needed on the server.</em></xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text><em>"Это объём страниц памяти, в котором читаются данные страницы. Важным
			показателем производительности кэша буфера является процент попадания в кэш
			буфера. Он сообщает процент страниц с данными, найденных в кэше, а не на
			диске. Значение 95% означает, что страницы были найдены в памяти 95%
			времени. В оставшихся 5% необходим физический доступ к диску. Постоянное
			значение ниже 90% означает, что серверу необходимо больше физической памяти.
			</em></xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text><em>"This is the pool of memory pages into which data pages are read. An important
			indicator of the performance of the buffer cache is the Buffer Cache Hit Ratio
			performance counter. It indicates the percentage of data pages found in the buffer cache
			as opposed to disk. A value of 95% indicates that pages were found in memory 95% of the
			time. The other 5% required physical disk access. A consistent value below 90% indicates
			that more physical memory is needed on the server.</em></xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Basically what this means is what is the percentage that SQL Server had the data in
			cache and did not have to read the data from disk. Ideally you want this number to be as
			close to 100 as possible.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> По сути, он означает процент объема данных, которые SQL Server хранил в
			кэше без необходимости считывания с диска. Лучше, чтобы это число было как
			можно ближе к 100.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Basically what this means is what is the percentage that SQL Server had the data in
			cache and did not have to read the data from disk. Ideally you want this number to be as
			close to 100 as possible.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>In order to calculate the Buffer cache hit ratio we need to query the
			sys.dm_os_performance_counters dynamic management view. There are 2 counters we need in
			order to do our calculation, one counter is Buffer cache hit ratio and the other counter
			is Buffer cache hit ratio base. We divide Buffer cache hit ratio base by Buffer cache
			hit ratio and it will give us the Buffer cache hit ratio. Here is the query that will do
			that, this query will only work on SQL Server 2005 and up.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Чтобы вычислить коэффициент попадания в кэш буфера, необходимо сделать
			запрос sys.dm_os_performance_counters в режиме динамического управления. Для
			выполнения расчета необходимо 2 счетчика, один счетчик – это коэффициент
			попадания в кэш буфера, а другой – база коэффициента попадания в кэш буфера.
			База коэффициента попадания в кэш буфера делится на коэффициент попадания в
			кэш, что дает нам Коэффициент попадания в кэш буфера. Вот запрос, который
			нужно выполнить. Данный запрос будет работать только на SQL Server версии
			2005 и выше.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>In order to calculate the Buffer cache hit ratio we need to query the
			sys.dm_os_performance_counters dynamic management view. There are 2 counters we need in
			order to do our calculation, one counter is Buffer cache hit ratio and the other counter
			is Buffer cache hit ratio base. We divide Buffer cache hit ratio base by Buffer cache
			hit ratio and it will give us the Buffer cache hit ratio. Here is the query that will do
			that, this query will only work on SQL Server 2005 and up.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Page life expectancy</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Время жизни страницы</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Page life expectancy</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Now let's look at Page life expectancy. Page life expectancy is the number of seconds
			a page will stay in the buffer pool, ideally it should be above 300 seconds. If it is
			less than 300 seconds this could indicate memory pressure, a cache flush or missing
			indexes.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Теперь рассмотрим параметр "Продолжительность жизни страницы".
			Продолжительность жизни страницы - количество секунд, в течение которых
			страница находится в буфере. В идеале она должна быть выше 300 секунд. Если
			она составляет менее 300 секунд, это может означать вытеснение памяти,
			очистку кэша или отсутствующие индексы.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Now let's look at Page life expectancy. Page life expectancy is the number of seconds
			a page will stay in the buffer pool, ideally it should be above 300 seconds. If it is
			less than 300 seconds this could indicate memory pressure, a cache flush or missing
			indexes.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://blogs.lessthandot.com/index.php/DataMgmt/DBAdmin/MSSQLServerAdmin/use-sys-dm_os_performance_counters-to-ge" target="_blank">Use sys.dm_os_performance_counters to get your Buffer cache hit ratio and Page life expectancy counters</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
