<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabaseEnterpriseEditionFeatures.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Enterprise Edition Features In Use</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Использование Enterprise Edition Features</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Enterprise Edition Features In Use</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="DatabaseEnterpriseEditionFeatures.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Enterprise Edition Features In Use</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Использование Enterprise Edition Features</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Enterprise Edition Features In Use</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Enterprise Edition Features In Use</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Использование Enterprise Edition Features</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Enterprise Edition Features In Use</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>If you try to restore an Enterprise Edition database onto a Standard
				Edition server, the restore will chug right along until the very last step –
				bringing the database online. At that time, SQL Server checks to see whether
				any Enterprise Edition features are in use. If so, SQL Server just won't let
				you bring the database online. No, you can't even get it online just long
				enough to rip out those features.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>If you try to restore an Enterprise Edition database onto a Standard
			Edition server, the restore will chug right along until the very last step –
			bringing the database online. At that time, SQL Server checks to see whether
			any Enterprise Edition features are in use. If so, SQL Server just won't let
			you bring the database online. No, you can't even get it online just long
			enough to rip out those features.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>If you try to restore an Enterprise Edition database onto a Standard
				Edition server, the restore will chug right along until the very last step –
				bringing the database online. At that time, SQL Server checks to see whether
				any Enterprise Edition features are in use. If so, SQL Server just won't let
				you bring the database online. No, you can't even get it online just long
				enough to rip out those features.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>The script checks the </xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>The script checks the </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>The script checks the </xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>sys.dm_db_persisted_sku_features</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>sys.dm_db_persisted_sku_features</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>sys.dm_db_persisted_sku_features</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>
				DMV in each user database and reports back which EE features are in use –
				like compression, partitioning, or Transparent Data Encryption (TDE).</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>
			DMV in each user database and reports back which EE features are in use –
			like compression, partitioning, or Transparent Data Encryption (TDE).</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>
				DMV in each user database and reports back which EE features are in use –
				like compression, partitioning, or Transparent Data Encryption (TDE).</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> If you've got Standard Edition servers in
				your environment, be aware that you won't be able to restore these databases
				onto those Standard Edition servers. This is especially important in
				disaster recovery (DR) environments that might have been accidentally
				installed as Standard Edition.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> If you've got Standard Edition servers in
			your environment, be aware that you won't be able to restore these databases
			onto those Standard Edition servers. This is especially important in
			disaster recovery (DR) environments that might have been accidentally
			installed as Standard Edition.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> If you've got Standard Edition servers in
				your environment, be aware that you won't be able to restore these databases
				onto those Standard Edition servers. This is especially important in
				disaster recovery (DR) environments that might have been accidentally
				installed as Standard Edition.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Низкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Moderate</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Низкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Moderate</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://www.brentozar.com/blitz/enterprise-edition-features/" target="_blank">Blitz Result: Enterprise Edition Features in Use</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
