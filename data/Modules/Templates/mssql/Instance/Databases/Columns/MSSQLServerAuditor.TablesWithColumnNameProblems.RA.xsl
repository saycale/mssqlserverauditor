<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="TablesWithColumnNameProblems.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables with problems in column names</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с проблемами в именах колонок</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables with problems in column names</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="TablesWithColumnNameProblems.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables with problems in column names</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с проблемами в именах колонок</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables with problems in column names</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Do not use spaces or other invalid characters in your column names</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Не используйте пробелы и другие недопустимые символы в именах колонок</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Do not use spaces or other invalid characters in your column names</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Column names (and table names) should not have spaces or any other invalid characters
				in them. This is considered bad practice because it requires you to use square brackets
				around your names. Square brackets make the code harder to read and understand. The
				query (presented below) will also highlight columns and tables with numbers in the
				names. Most of the time, when there is a number in a column name, it represents a
				de-normalized database. There are exceptions to this rule, so not all occurrences of
				this problem need to be fixed.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Имена колонок и таблиц не должны содержать пробелы и другие недопустимые символы. Это
			считается плохой практикой, потому что вынуждает использовать вокруг имен квадратные
			скобки. Из-за квадратных скобок код сложнее прочесть и понять. Запрос, приведенный ниже,
			выделит колонки и таблицы, содержащие в загловке число. В большинстве случаев, если в
			имени колонки есть число, она представляет собой ненормированную базу данных. В этом
			правиле есть исключения, поэтому не все случаи нужно исправлять.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Column names (and table names) should not have spaces or any other invalid characters
				in them. This is considered bad practice because it requires you to use square brackets
				around your names. Square brackets make the code harder to read and understand. The
				query (presented below) will also highlight columns and tables with numbers in the
				names. Most of the time, when there is a number in a column name, it represents a
				de-normalized database. There are exceptions to this rule, so not all occurrences of
				this problem need to be fixed.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>It is recognized that some organizations allow (and may even encourage) the use of
				the underscore character. In the newly modified code below, you can include a list of
				acceptable symbols. The code below allows the underscore symbol and the $ symbol. You may ask the utility support to modify this local variable
				to include any symbol that is acceptable within your organization.
				</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Известно, что в некоторых компаниях разрешено (и даже рекомендовано) использовать
			символ черты снизу. Вы можете внести список допустимых символов в новый измененный коде
			ниже. Он позволяет использовать символ черты снизу и символ $. Вы
			можете попросить разработчиков программы изменить эту местную переменную и включить в
			нее символ, допустимый в Вашей организации.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>It is recognized that some organizations allow (and may even encourage) the use of
				the underscore character. In the newly modified code below, you can include a list of
				acceptable symbols. The code below allows the underscore symbol and the $ symbol. You may ask the utility support to modify this local variable
				to include any symbol that is acceptable within your organization.
				</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> If this is a number issue, you may need to
				redesign your database structure to include more tables. For example, if you have a
				StudentGrade table with (StudentId, Grade1, Grade2, Grade3, Grade4) you should change it
				to be StudentGrade with (StudentId, Grade, Identifier). Each student would have multiple
				rows in this table (one for each grade). You would need to add an identifier column to
				indicate what the grade is for (test on November 10, book report, etc).</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Если проблема вызвана количеством, возможно,
			следует изменить структуру базы данных, чтобы она включала больше таблиц. К примеру, у
			вас есть таблица StudentGrade (ОценкаУченика) с StudentId, Grade1, Grade2, Grade3,
			Grade4 (Ученик1, Оценка1, Оценка2, Оценка3, Оценка4). Следует изменить ее на
			StudentGrade (ОценкаУченика) с StudentId, Grade, Identifier (идентификатор Ученика, Оценка,
			идентификатор). У каждого учащегося в таблице будет несколько записей (один для каждой
			оценки). Вам нужно будет добавить колонку идентификации для указания, за что поставлена
			данная оценка (за контрольную 10-го ноября, доклад по книге и т.д.)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> If this is a number issue, you may need to
				redesign your database structure to include more tables. For example, if you have a
				StudentGrade table with (StudentId, Grade1, Grade2, Grade3, Grade4) you should change it
				to be StudentGrade with (StudentId, Grade, Identifier). Each student would have multiple
				rows in this table (one for each grade). You would need to add an identifier column to
				indicate what the grade is for (test on November 10, book report, etc).</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Moderate</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Moderate</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Mild</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Незначительный</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Mild</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://blogs.lessthandot.com/index.php/DataMgmt/DBProgramming/do-not-use-spaces-or-other-invalid-chara" target="_blank">Do not use spaces or other invalid characters in column name</a></p>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
