<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="TablesWithFloatTypes.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables with float type columns</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с колонками типа float</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables with float type columns</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="TablesWithFloatTypes.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables with float type columns</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с колонками типа float</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables with float type columns</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Do not use the float data type</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Не используйте тип данных float</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Do not use the float data type</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>That may seem a little harsh, and it's not always true. However, most of the time,
			the float data type should be avoided. Unfortunately, the float (and real) data types
			are approximate data types that can lead to significant rounding errors.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Возможно, это звучит жестко и не всегда верно. Тем не менее, в большинстве случаев
			следует избегать тип данных float. К сожалению, типы данных float и real являются
			приблизительными и могут привести к значительным ошибкам округления.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>That may seem a little harsh, and it's not always true. However, most of the time,
			the float data type should be avoided. Unfortunately, the float (and real) data types
			are approximate data types that can lead to significant rounding errors.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Query on Float DataType may return inconsistent result</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Запрос на тип данных float может дать несовместимый результат</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Query on Float DataType may return inconsistent result</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Floating point data is approximate; therefore, not all values in the data type range
			can be represented exactly. When you add a small number with a very big number, the
			small number might just lost in the end result.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Данные с плавающей запятой приблизительны. Поэтому не все значения в пределах данного
			типа данных могут быть точно отображены. При добавлении маленького и очень большого
			числа, маленькое число может быть потеряно в конечном результате.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Floating point data is approximate; therefore, not all values in the data type range
			can be represented exactly. When you add a small number with a very big number, the
			small number might just lost in the end result.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>As a end-user, we want consistent and correct result. However, float data type has no
			correct result since the value is not accurate. Query result on float data type is also
			not consistent as well. In the above example, we change the order of + and – operator,
			we will see different result. Suppose we want aggregate float values, such as
			sum(floatcolumn), SQL Server may choose to parallel scan the whole table with different
			thread, and sum up the result together. In this case, the order of data values are
			random, and the end-result will be random. In one of our customer's case, he run the
			same query multiple times, and each time he get a totally different result.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Будучи конечными пользователями, мы хотим получить непротиворечивый и правильный
			результат. А тип данных float не дает правильный результат, так как его значение не
			является точным. Результат запроса данных float дает несовместимый результат. На примере
			выше, мы меняем порядок операторов + и – и получаем другой результат. Предположим, что
			мы хотим суммировать значения типа float, например sum(floatcolumn). SQL Server может
			выполнить параллельное сканирование всей таблицы другим потоком и сложить полученные
			результаты. В этом случае значения имеют произвольный порядок и конечный результат тоже
			будет произвольным. В случае одного из наших клиентов, он выполнял один и тот же запрос
			несколько раз и каждый раз получал новый результат. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>As a end-user, we want consistent and correct result. However, float data type has no
			correct result since the value is not accurate. Query result on float data type is also
			not consistent as well. In the above example, we change the order of + and – operator,
			we will see different result. Suppose we want aggregate float values, such as
			sum(floatcolumn), SQL Server may choose to parallel scan the whole table with different
			thread, and sum up the result together. In this case, the order of data values are
			random, and the end-result will be random. In one of our customer's case, he run the
			same query multiple times, and each time he get a totally different result.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>So, please try to avoid using float data type, especially you want to do some
			aggregation on float types. A workaround is to covert the type to a numeric type before
			doing aggregate. For example, support X is float</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Поэтому мы советуем избегать тип данных float, особенно, если нужно сложить значения
			типа float. Перед сложением измените тип данных на numeric. Например, X имеет тип данных
			float.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>So, please try to avoid using float data type, especially you want to do some
			aggregation on float types. A workaround is to covert the type to a numeric type before
			doing aggregate. For example, support X is float</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>you can use sum(cast(x as decimal(30,4))) to get consistent result</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Вы можете использовать функцию sum(cast(x - десятичная дробь(30,4))) для получения
			непротиворечивого результата.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>you can use sum(cast(x as decimal(30,4))) to get consistent result</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Examine the data you are using and identify the
			precision and scale required. Change the data type (or code) to use a decimal with the
			precision and scale you require.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Проверьте используемые данные и оцените
			необходимые точность и диапазон. Измените тип данных (или код) для получения десятичной
			дроби с нужной точностью и диапазоном. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Examine the data you are using and identify the
			precision and scale required. Change the data type (or code) to use a decimal with the
			precision and scale you require.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Легкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Moderate</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Moderate</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://blogs.lessthandot.com/index.php/DataMgmt/DBProgramming/do-not-use-the-float-data-type" target="_blank">Do not use the float data type</a></p>

			<p><a href="http://blogs.msdn.com/b/qingsongyao/archive/2009/11/14/float-datatype-is-evil.aspx" target="_blank">Float datatype is evil</a></p>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
