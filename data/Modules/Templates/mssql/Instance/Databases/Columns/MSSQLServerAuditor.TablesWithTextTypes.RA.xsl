<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="TablesWithTextColumns.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables with columns text/ntext</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с колонками типа text/ntext</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables with columns text/ntext</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="TablesWithTextColumns.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables with columns text/ntext</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с колонками типа text/ntext</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables with columns text/ntext</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Don't use text datatype for SQL 2005 and up</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Не используйте текстовый тип данных для SQL 2005 и выше</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Don't use text datatype for SQL 2005 and up</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>With SQL Server versions prior to SQL2005, the only way to store large amounts of
				data was to use the text, ntext, or image data types. SQL2005 introduced new data types
				that replace these data type, while also allowing all of the useful string handling
				functions to work. Changing the data types to the new SQL2005+ equivalent should be
				relatively simple and quick to implement (depending on the size of your tables). So, why
				wait? Convert the data types now.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>В версиях SQL Server ниже SQL2005 единственным способом хранения большого объема данных были следующие типы данных: text, ntext и image.
			В SQL2005 представлены новые типы данных взамен выше перечисленных, а также поддерживаются все полезные функции работы со строками.
			Изменить тип данных в эквивалентный новым версиям SQL2005+ просто и быстро (в зависимости от размера таблицы). Итак, к чему ждать?
			Измените тип данных прямо сейчас.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>With SQL Server versions prior to SQL2005, the only way to store large amounts of
				data was to use the text, ntext, or image data types. SQL2005 introduced new data types
				that replace these data type, while also allowing all of the useful string handling
				functions to work. Changing the data types to the new SQL2005+ equivalent should be
				relatively simple and quick to implement (depending on the size of your tables). So, why
				wait? Convert the data types now.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Change the data type to a MS SQL 2005+ version.
				Text should be converted to VARCHAR(MAX), NTEXT should be converted to NVARCHAR(MAX) and
				IMAGE should be converted to VARBINARY(MAX).</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Измените тип данных на версию SQL2005+. Тип данных text будет изменен на varchar(max), ntext на nvarchar(max),
			image на varbinary(max).</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Change the data type to a MS SQL 2005+ version.
				Text should be converted to VARCHAR(MAX), NTEXT should be converted to NVARCHAR(MAX) and
				IMAGE should be converted to VARBINARY(MAX).</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Простой</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Low</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Низкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Low</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text><a href="http://blogs.lessthandot.com/index.php/DataMgmt/DBProgramming/don-t-use-text-datatype-for-sql-2005-and" target="_blank">Don't use text datatype for MS SQL 2005 and above</a></xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text><a href="http://blogs.lessthandot.com/index.php/DataMgmt/DBProgramming/don-t-use-text-datatype-for-sql-2005-and" target="_blank">Не используйте текстовый тип данных для SQL 2005 и выше</a></xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text><a href="http://blogs.lessthandot.com/index.php/DataMgmt/DBProgramming/don-t-use-text-datatype-for-sql-2005-and" target="_blank">Don't use text datatype for MS SQL 2005 and above</a></xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
