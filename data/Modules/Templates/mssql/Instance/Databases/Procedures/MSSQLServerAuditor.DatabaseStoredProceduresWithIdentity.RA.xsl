<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabaseProceduresWithIdentity.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Procedures With Identity</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Хранимые процедуры с @@Identity</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Procedures With Identity</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="DatabaseProceduresWithIdentity.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Procedures With Identity</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Хранимые процедуры с @@Identity</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Procedures With Identity</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>6 Different Ways To Get The Current Identity Value</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>6 способов получить значение текущей идентификации</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>6 Different Ways To Get The Current Identity Value</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Always use SCOPE_IDENTITY() unless you DO need the last identity value regradless of
				scope (for example you need to know the identity from the table insert inside the
				trigger).</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Всегда используйте SCOPE_IDENTITY() за исключением случаев, когда Вам НЕОБХОДИМО узнать последнюю идентификацию независимо от шкалы
			(например, вам нужно узнать идентификацию из вставки таблицы в тригерре).</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Always use SCOPE_IDENTITY() unless you DO need the last identity value regradless of
				scope (for example you need to know the identity from the table insert inside the
				trigger).</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> use SCOPE_IDENTITY().</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Используйте SCOPE_IDENTITY().</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> use SCOPE_IDENTITY().</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Low</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Низкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Low</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Low</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Низкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Low</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://wiki.lessthandot.com/index.php/6_Different_Ways_To_Get_The_Current_Identity_Value" target="_blank">6 Different Ways To Get The Current Identity Value</a></p>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
