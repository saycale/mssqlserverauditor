<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabaseProceduresWithDecimalIssues.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Procedures With Decimal Issues</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Хранимые процедуры без указания переменных с плавающей запятой</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Procedures With Decimal Issues</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="DatabaseProceduresWithDecimalIssues.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Procedures With Decimal Issues</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Хранимые процедуры без указания переменных с плавающей запятой</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Procedures With Decimal Issues</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Always include precision and scale with decimal and numeric</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Всегда указывайте точность и шкалу для данных типов NUMERIC и DECIMAL</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Always include precision and scale with decimal and numeric</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>When you use the decimal (or numeric) data type, you should always identity the
				precision and scale for it. If you do not, the precision defaults to 18, and the scale
				defaults to 0. When scale is 0, you cannot store fractional numbers. If you do not want
				to store fractional numbers, then you should use a different data type, like bigint,
				int, smallint, or tinyint.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Если Вы используете данные типов NUMERIC и DECIMAL, всегда указывайте точность и шкалу. В противном случае, точность по умолчанию
			будет установлена на 18, а шкала на 0. Когда шкала равна 0, невозможно хранить дробные числа. Если Вы не хотите хранить дробные числа,
			следует использовать другой тип данных, например bigint, int, smallint или tinyint.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>When you use the decimal (or numeric) data type, you should always identity the
				precision and scale for it. If you do not, the precision defaults to 18, and the scale
				defaults to 0. When scale is 0, you cannot store fractional numbers. If you do not want
				to store fractional numbers, then you should use a different data type, like bigint,
				int, smallint, or tinyint.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Use the query above to locate this problem with
				your code. Specify the precision and scale. This will often times require that you look
				up the proper precision and scale in a table definition.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Используйте запрос выше, чтобы найти проблему в коде. Укажите точность и шкалу.
			Часто требуется подбирать правильную точность и шкалу по таблице.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Use the query above to locate this problem with
				your code. Specify the precision and scale. This will often times require that you look
				up the proper precision and scale in a table definition.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Простой</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> High</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Высокий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> High</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://blogs.lessthandot.com/index.php/DataMgmt/DBProgramming/always-include-precision-and-scale-with" target="_blank">Always include precision and scale with decimal and numeric</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
