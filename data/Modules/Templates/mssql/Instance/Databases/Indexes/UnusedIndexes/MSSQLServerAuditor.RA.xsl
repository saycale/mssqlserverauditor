<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="UnusedIndexes.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Unused Indexes</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Неиспользуемые индексы</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Unused Indexes</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="UnusedIndexes.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Unused Indexes</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Неиспользуемые индексы</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Unused Indexes</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Discovering Unused Indexes</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Поиск неиспользуемых индексов</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Discovering Unused Indexes</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<h2 class="secondHeading">Обзор</h2>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>To ensure that data access can be as fast as possible, SQL Server like other
				relational database systems utilizes indexing to find data quickly. SQL Server has
				different types of indexes that can be created such as clustered indexes, non-clustered
				indexes, XML indexes and Full Text indexes.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Чтобы убедиться, что доступ к данным выполняется настолько быстро, как это возможно, SQL Server, как и другие
			реляционные базы данных, использует индексирование для быстрого поиска данных. В SQL Server можно создать различные типы индексов, например,
			кластерные индексы , некластерныеиндексы, XML индексы и полнотекстовые индексы. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>To ensure that data access can be as fast as possible, SQL Server like other
				relational database systems utilizes indexing to find data quickly. SQL Server has
				different types of indexes that can be created such as clustered indexes, non-clustered
				indexes, XML indexes and Full Text indexes.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>The benefit of having more indexes is that SQL Server can access the data quickly if
				an appropriate index exists. The downside to having too many indexes is that SQL Server
				has to maintain all of these indexes which can slow things down and indexes also require
				additional storage. So as you can see indexing can both help and hurt performance.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Преимущество наличия нескольких индексов в том, что SQL Server может быстро получить доступ к данным быстро, если есть соответствующий
			индекс. Недостатком большого количества индексов является то, что SQL Server должен хранить все эти индексы, что может замедлить
			производительность, для хранения индексов  также требуется дополнительное пространство. Итак, как Вы заметили, индексация может улучшить
			и нанести вред производительности. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>The benefit of having more indexes is that SQL Server can access the data quickly if
				an appropriate index exists. The downside to having too many indexes is that SQL Server
				has to maintain all of these indexes which can slow things down and indexes also require
				additional storage. So as you can see indexing can both help and hurt performance.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>In this section we will focus on how to identify indexes that exist, but are not
				being used and therefore can be dropped to improve performance and decrease storage
				requirements.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>В этом разделе я  расскажу о том, как определить индексы, которые существуют, но не используется , и потому могут быть удалены
			для улучшения производительности и уменьшения  объема хранилища. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>In this section we will focus on how to identify indexes that exist, but are not
				being used and therefore can be dropped to improve performance and decrease storage
				requirements.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<h2 class="secondHeading">Объяснение</h2>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>When SQL Server 2005 was introduced it added Dynamic Management Views (DMVs) that
				allow you to get additional insight as to what is going on within SQL Server. One of
				these areas is the ability to see how indexes are being used. There are two DMVs that we
				will discuss. Note that these views store cumulative data, so when SQL Server is
				restated the counters go back to zero, so be aware of this when monitoring your index
				usage.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>В SQL Server 2005 добавлена функция Dynamic Management Views (DMV) , которая позволяет взглянуть внутрь того, что происходит внутри SQL
			Server. Она дает возможность увидеть, как используются индексы. Мы рассмотрим два DMV. Обратите внимание, что они показывают накопленную
			информацию, поэтому, когда счетчики SQL Server настраиваются на 0, это следует учитывать при мониторинге индекса использования. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>When SQL Server 2005 was introduced it added Dynamic Management Views (DMVs) that
				allow you to get additional insight as to what is going on within SQL Server. One of
				these areas is the ability to see how indexes are being used. There are two DMVs that we
				will discuss. Note that these views store cumulative data, so when SQL Server is
				restated the counters go back to zero, so be aware of this when monitoring your index
				usage.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<h2 class="secondHeading">DMV - sys.dm_db_index_operational_stats</h2>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>This DMV allows you to see insert, update and delete information for various aspects
				for an index. Basically this shows how much effort was used in maintaining the index
				based on data changes.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Данный DMV позволяет увидеть вставки, обновления и удаления информации в различных аспектах индекса. Как правило, он показывает,
			сколько усилий было потрачено для сохранения индекса, основанного на изменениях данных. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>This DMV allows you to see insert, update and delete information for various aspects
				for an index. Basically this shows how much effort was used in maintaining the index
				based on data changes.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>If you query the table and return all columns, the output may be confusing. So the
				query below focuses on a few key columns. To learn more about the output for all columns
				you can check out Books Online.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Если создать запрос к таблице и вернуть все столбцы, на выходе может получится путаница. Запрос ниже акцентируется на нескольких ключевых
			столбцах. Чтобы узнать больше о выводе информации всех столбцов, прочтите «Books Online». </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>If you query the table and return all columns, the output may be confusing. So the
				query below focuses on a few key columns. To learn more about the output for all columns
				you can check out Books Online.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<h2 class="secondHeading">DMV - sys.dm_db_index_usage_stats</h2>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>This DMV shows you how many times the index was used for user queries. Again there
				are several other columns that are returned if you query all columns and you can refer
				to Books Online for more information.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Данный DMV показывает, сколько раз индекс был использован для запросов пользователя. Здесь также есть несколько других столбцов,
			возвращаемые при запросе всех столбцов. Вы можете обратиться к «Books Online» для получения дополнительной информации. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>This DMV shows you how many times the index was used for user queries. Again there
				are several other columns that are returned if you query all columns and you can refer
				to Books Online for more information.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<h2 class="secondHeading">Поиск неиспользуемых индексов</h2>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>So based on the output above you should focus on the output from the second query. If
				you see indexes where there are no seeks, scans or lookups, but there are updates this
				means that SQL Server has not used the index to satisfy a query but still needs to
				maintain the index. Remember that the data from these DMVs is reset when SQL Server is
				restarted, so make sure you have collected data for a long enough period of time to
				determine which indexes may be good candidates to be dropped.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Итак, основываясь на информации, выведенной выше, обратите внимание на вывод второго запроса. Если Вы видите индексы там, где их не выполняли
			сканирование или поиск, но есть обновления, это значит, что SQL Server не использовал индекс для удовлетворения запроса, но поддержания
			индекса все еще необходимо. Помните, что данные этих DMVs обнуляются при запуске SQL Server  поэтому убедитесь, что вы получиили данные
			за достаточно длительный периода времени, чтобы определить, какие индексы являются хорошими кандидатами для удаления. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>So based on the output above you should focus on the output from the second query. If
				you see indexes where there are no seeks, scans or lookups, but there are updates this
				means that SQL Server has not used the index to satisfy a query but still needs to
				maintain the index. Remember that the data from these DMVs is reset when SQL Server is
				restarted, so make sure you have collected data for a long enough period of time to
				determine which indexes may be good candidates to be dropped.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Moderate</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Moderate</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Moderate</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Moderate</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://www.mssqltips.com/sqlservertutorial/256/discovering-unused-indexes/" target="_blank">Discovering Unused Indexes </a></p>
			<p><a href="http://sequelserver.blogspot.com.au/search/label/sys.dm_db_index_physical_stats" target="_blank">Unused Index Script</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
