<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="MissingIndexes.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Missing Indexes</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Пропущенные индексы</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Missing Indexes</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="MissingIndexes.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Missing Indexes</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Пропущенные индексы</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Missing Indexes</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
			<link rel="stylesheet" href="$JS_FOLDER$/tablesorter/css/theme.mssqlserverauditor.css" type="text/css"/>

			<script src="$JS_FOLDER$/json-js/json2.js"></script>
			<script src="$JS_FOLDER$/jquery-1.12.4.min.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.widgets.js"></script>

			<script type="text/javascript">
				$(document).ready(function()
					{
						$("#myErrorTable").tablesorter({
							theme : 'MSSQLServerAuditorError',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});

						$("#myTable").tablesorter({
							theme : 'MSSQLServerAuditor',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});
					}
				);
			</script>
		</head>
		<body>
			<style>
				body { overflow: auto; padding:0; margin:0; }
			</style>
			<xsl:if test="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']/child::node()">
			<table id="myErrorTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Query</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Запрос</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Query</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Hierarchy</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Категория</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Hierarchy</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>RecordSets</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Наборов</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>RecordSets</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>#</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Код</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Номер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сообщение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="@instance != ''">
								<xsl:value-of select="@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@name != ''">
								<xsl:value-of select="@name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@hierarchy != ''">
								<xsl:value-of select="@hierarchy"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RecordSets != ''">
								<xsl:value-of select="@RecordSets"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RowCount != ''">
								<xsl:value-of select="@RowCount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorCode != ''">
								<xsl:value-of select="@SqlErrorCode"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorNumber != ''">
								<xsl:value-of select="@SqlErrorNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="SqlErrorMessage != ''">
								<xsl:value-of select="SqlErrorMessage"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
			<xsl:if test="MSSQLResult[@name='GetMissingIndexes' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/child::node()">
			<table id="myTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Schema</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Схема</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Schema</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('ru')">
								<xsl:text>Объект</xsl:text>
							</xsl:when>
							<xsl:when test="lang('en')">
								<xsl:text>Object</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Object</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Object type</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Тип объекта</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Object type</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Date created</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Дата создания</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Date created</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>EqualityColumns</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>EqualityColumns</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>EqualityColumns</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>InEqualityColumns</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>InEqualityColumns</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>InEqualityColumns</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>IncludedColumns</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>IncludedColumns</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>IncludedColumns</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>UniqueCompiles</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>UniqueCompiles</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>UniqueCompiles</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>UserSeeks</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>UserSeeks</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>UserSeeks</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>UserScans</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>UserScans</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>UserScans</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>LastUserSeekTime</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>LastUserSeekTime</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>LastUserSeekTime</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>LastUserScanTime</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>LastUserScanTime</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>LastUserScanTime</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>AvgTotalUserCost</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>AvgTotalUserCost</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>AvgTotalUserCost</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>AvgUserImpact</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>AvgUserImpact</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>AvgUserImpact</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SystemSeeks</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>SystemSeeks</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SystemSeeks</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SystemScans</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>SystemScans</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SystemScans</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>LastSystemSeekTime</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>LastSystemSeekTime</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>LastSystemSeekTime</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>LastSystemScanTime</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>LastSystemScanTime</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>LastSystemScanTime</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>AvgTotalSystemCost</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>AvgTotalSystemCost</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>AvgTotalSystemCost</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>AvgSystemImpact</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>AvgSystemImpact</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>AvgSystemImpact</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>IndexAdvantage</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>IndexAdvantage</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>IndexAdvantage</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>ProposedIndex</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>ProposedIndex</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>ProposedIndex</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@name='GetMissingIndexes' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/Row">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="../../@instance != ''">
								<xsl:value-of select="../../@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="SchemaName != ''">
								<xsl:value-of select="SchemaName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="ObjectName != ''">
								<xsl:value-of select="ObjectName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="ObjectType != ''">
								<xsl:value-of select="ObjectType"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="ObjectCreateDate != ''">
								<xsl:choose>
									<xsl:when test="lang('en')">
										<xsl:value-of select="ms:format-date(ObjectCreateDate, 'dd/MM/yyyy')"/>
									</xsl:when>
									<xsl:when test="lang('ru')">
										<xsl:value-of select="ms:format-date(ObjectCreateDate, 'dd.MM.yyyy')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="ms:format-date(ObjectCreateDate, 'dd/MM/yyyy')"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(ObjectCreateDate, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="EqualityColumns != ''">
								<xsl:value-of select="EqualityColumns"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="InEqualityColumns != ''">
								<xsl:value-of select="InEqualityColumns"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="IncludedColumns != ''">
								<xsl:value-of select="IncludedColumns"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="UniqueCompiles != ''">
								<xsl:value-of select="format-number(UniqueCompiles, '###,###,##0')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="UserSeeks != ''">
								<xsl:value-of select="format-number(UserSeeks, '###,###,##0')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="UserScans != ''">
								<xsl:value-of select="format-number(UserScans, '###,###,##0')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="LastUserSeekTime != ''">
								<xsl:choose>
									<xsl:when test="lang('ru')">
										<xsl:value-of select="ms:format-date(LastUserSeekTime, 'dd.MM.yyyy')"/>
									</xsl:when>
									<xsl:when test="lang('en')">
										<xsl:value-of select="ms:format-date(LastUserSeekTime, 'dd/MM/yyyy')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="ms:format-date(LastUserSeekTime, 'dd/MM/yyyy')"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(LastUserSeekTime, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="LastUserScanTime != ''">
								<xsl:choose>
									<xsl:when test="lang('ru')">
										<xsl:value-of select="ms:format-date(LastUserScanTime, 'dd.MM.yyyy')"/>
									</xsl:when>
									<xsl:when test="lang('en')">
										<xsl:value-of select="ms:format-date(LastUserScanTime, 'dd/MM/yyyy')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="ms:format-date(LastUserScanTime, 'dd/MM/yyyy')"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(LastUserScanTime, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="AvgTotalUserCost != ''">
								<xsl:value-of select="format-number(AvgTotalUserCost, '###,###,##0.##')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="AvgUserImpact != ''">
								<xsl:value-of select="format-number(AvgUserImpact, '###,###,##0.##')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="SystemSeeks != ''">
								<xsl:value-of select="format-number(SystemSeeks, '###,###,##0')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="SystemScans != ''">
								<xsl:value-of select="format-number(SystemScans, '###,###,##0')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="LastSystemSeekTime != ''">
								<xsl:choose>
									<xsl:when test="lang('ru')">
										<xsl:value-of select="ms:format-date(LastSystemSeekTime, 'dd.MM.yyyy')"/>
									</xsl:when>
									<xsl:when test="lang('en')">
										<xsl:value-of select="ms:format-date(LastSystemSeekTime, 'dd/MM/yyyy')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="ms:format-date(LastSystemSeekTime, 'dd/MM/yyyy')"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(LastSystemSeekTime, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="LastSystemScanTime != ''">
								<xsl:choose>
									<xsl:when test="lang('ru')">
										<xsl:value-of select="ms:format-date(LastSystemScanTime, 'dd.MM.yyyy')"/>
									</xsl:when>
									<xsl:when test="lang('en')">
										<xsl:value-of select="ms:format-date(LastSystemScanTime, 'dd/MM/yyyy')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="ms:format-date(LastSystemScanTime, 'dd/MM/yyyy')"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(LastSystemScanTime, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="AvgTotalSystemCost != ''">
								<xsl:value-of select="format-number(AvgTotalSystemCost, '###,###,##0')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="AvgSystemImpact != ''">
								<xsl:value-of select="format-number(AvgSystemImpact, '###,###,##0')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="IndexAdvantage != ''">
								<xsl:value-of select="format-number(IndexAdvantage, '###,###,##0.##')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="ProposedIndex != ''">
								<xsl:value-of select="ProposedIndex"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
