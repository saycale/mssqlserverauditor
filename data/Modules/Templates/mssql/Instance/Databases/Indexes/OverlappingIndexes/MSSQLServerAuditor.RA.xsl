<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="OverlappingIndexes.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Overlapping Indexes</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Перекрывающиеся индексы</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Overlapping Indexes</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="OverlappingIndexes.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Overlapping Indexes</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Перекрывающиеся индексы</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Overlapping Indexes</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Overlapping Indexes</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Поиск перекрывающиеся (лишние) индексы</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Overlapping Indexes</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>SQL Server allows the creation of multiple non-clustered indexes, with a
				maximum of 999 in the SQL 2008 release (compared to 249 in 2005 release);
				the only limitation is that the "Index Name" must be unique for the schema.
				This could mean that some indexes might actually be duplicates of each other
				in all but their name, also known as exact duplicate indexes. If this
				happens, it can waste precious SQL Server resources and generate unnecessary
				overhead, causing poor database performance.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>В некоторых приложениях из-за несогласованности работы программистов
			возникают ситуации, когда создаются индексы в которых нет необходимости.
			Т.е. запросы могут использовать уже существующие индексы. Из-за этого
			возникают дополнительные издержки на их обслуживание. Таким образом, если
			поля индекса перекрываются более широким индексом в том же порядке
			следования полей начиная с первого поля, то этот индекс считается лишним,
			так как запросы могут использовать более широкий индекс. Данный сценарий как
			раз ищет эти перекрывающиеся (лишние) индексы.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>SQL Server allows the creation of multiple non-clustered indexes, with a
				maximum of 999 in the SQL 2008 release (compared to 249 in 2005 release);
				the only limitation is that the "Index Name" must be unique for the schema.
				This could mean that some indexes might actually be duplicates of each other
				in all but their name, also known as exact duplicate indexes. If this
				happens, it can waste precious SQL Server resources and generate unnecessary
				overhead, causing poor database performance.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Remove unused indexes.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Удалите неиспользуемые индексы.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Remove unused indexes.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Легкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Незначительный</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text><a href="http://www.confio.com/logicalread/duplicate-indexes-and-sql-server-performance/" target="_blank">Duplicate Indexes and SQL Server Performance</a></xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text><a href="http://www.sql.ru/blogs/andraptor/1218" target="_blank">Поиск перекрывающихся (лишних) индексов в SQL Server 2005+</a></xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text><a href="http://www.confio.com/logicalread/duplicate-indexes-and-sql-server-performance/" target="_blank">Duplicate Indexes and SQL Server Performance</a></xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://www.confio.com/logicalread/duplicate-indexes-and-sql-server-performance/" target="_blank">Duplicate Indexes and SQL Server Performance</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
