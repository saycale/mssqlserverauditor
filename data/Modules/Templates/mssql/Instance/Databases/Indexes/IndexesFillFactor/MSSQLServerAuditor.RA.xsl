<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="IndexesFillFactorSummary.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Indexes Fill Factor Summary</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Заполнение некластерных индексов</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Indexes Fill Factor Summary</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="IndexesFillFactorSummary.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Indexes Fill Factor Summary</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Заполнение некластерных индексов</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Indexes Fill Factor Summary</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Specify Fill Factor for an Index</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Укажите фактор заполнения для индекса</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Specify Fill Factor for an Index</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>The fill-factor option is provided for fine-tuning index data storage and
				performance. When an index is created or rebuilt, the fill-factor value determines the
				percentage of space on each leaf-level page to be filled with data, reserving the
				remainder on each page as free space for future growth. For example, specifying a
				fill-factor value of 80 means that 20 percent of each leaf-level page will be left
				empty, providing space for index expansion as data is added to the underlying table. The
				empty space is reserved between the index rows rather than at the end of the index. The
				fill-factor value is a percentage from 1 to 100, and the server-wide default is 0 which
				means that the leaf-level pages are filled to capacity.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Опция "фактор заполнения" создана для точной настройки хранилища индекса
			с данными и его производительности. Когда индекс создан или перестроен,
			значение коэффициента заполнения определяет процент пространства на каждой
			странице нижнего уровня, которое будет заполнено данными, оставляя на каждой
			странице напоминание о свободном пространстве для расширения в будущем.
			Например, значение фактора заполнения 80 означает, что 20 процентов каждой
			страницы конечного уровня останется пустой, обеспечивая пространство для
			расширения индекса по мере добавления данных в базовую таблицу. Пустое
			пространство резервируется между индексами строк, а не в конце индекса.
			Коэффициент заполнения – это значение в процентах от 1 до 100. Значение
			сервера пол умолчанию - 0 (ноль), что означает, что страницы конечного
			уровня полностью заполнены.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>The fill-factor option is provided for fine-tuning index data storage and
				performance. When an index is created or rebuilt, the fill-factor value determines the
				percentage of space on each leaf-level page to be filled with data, reserving the
				remainder on each page as free space for future growth. For example, specifying a
				fill-factor value of 80 means that 20 percent of each leaf-level page will be left
				empty, providing space for index expansion as data is added to the underlying table. The
				empty space is reserved between the index rows rather than at the end of the index. The
				fill-factor value is a percentage from 1 to 100, and the server-wide default is 0 which
				means that the leaf-level pages are filled to capacity.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://technet.microsoft.com/en-us/library/ms177459.aspx" target="_blank">Specify Fill Factor for an Index</a></p>
			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text><a href="http://blog.sqlauthority.com/2009/12/16/sql-server-fillfactor-index-and-in-depth-look-at-effect-on-performance/" target="_blank">SQL SERVER – Fillfactor, Index and In-depth Look at Effect on Performance</a></xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text><a href="http://blog.sqlauthority.com/2009/12/16/sql-server-fillfactor-index-and-in-depth-look-at-effect-on-performance/" target="_blank">SQL SERVER – коэфициент заполнения, индекс и взгляд изнутри на производительность</a></xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text><a href="http://blog.sqlauthority.com/2009/12/16/sql-server-fillfactor-index-and-in-depth-look-at-effect-on-performance/" target="_blank">SQL SERVER – Fillfactor, Index and In-depth Look at Effect on Performance</a></xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
