<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="Indexes.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Database Tables Indexes Info</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Информация об индексах</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Database Tables Indexes Info</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="Indexes.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Database Tables Indexes Info</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Информация об индексах</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Database Tables Indexes Info</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Table indexes information</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Статистическая информация об индексах</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Table indexes information</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
