<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="IndexesWithForwardedRecords.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Finding Forwarded Records</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Индексы с перенаправленными записями</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Finding Forwarded Records</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="IndexesWithForwardedRecords.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Finding Forwarded Records</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Индексы с перенаправленными записями</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Finding Forwarded Records</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Finding Forwarded Records SQL Server 2008</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Поиск перенаправленных записей в SQL Server 2008</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Finding Forwarded Records SQL Server 2008</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>HEAP tables and forwarded records are a major and overlooked performance problem.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы HEAP и перенаправленные записи являются основной и не выявленной проблемой
			быстродействия программы.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>HEAP tables and forwarded records are a major and overlooked performance problem.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Imagine a customer using an ISV application that stores certain product information
				in a varchar(200) column in a SQL Server database table. When the system was first being
				used nobody ever entered a product description with more than 10 characters. However,
				over time new products were added to the portfolio which required the introduction of a
				prefix in the description. On the SQL level this was done by running an update which
				added the appropriate prefix. Additionally, the company merged with another one and the
				product description entries changed again and became on average 35 characters long.
				Again an update statement was used to implement the changes. Of course since the column
				was defined as a varchar(200) there was no problem storing the changed values.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Представьте себе клиента, использующего приложение ISV для хранения определенной
			информации о продукте в колонке varchar(200) в таблице базы данных SQL Server. В начале
			использования системы никто не вводил описания продукта длиннее 10 символов. Однако, со
			временем в папку добавлялись новые продукты, которые требовали введения префикса в
			описание. На уровне SQL это представляло собой запуск обновления, которое добавляло
			соответствующий префикс. Либо, компания сливалась с другой компанией, запись с описанием
			продукта снова менялась и состояла уже примерно из 35 символов. В этом случае для
			принятия изменений тоже использовался оператор обновления. Разумеется, если колонка была
			задана в типе varchar(200), проблем с хранением измененных значений не возникало.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Imagine a customer using an ISV application that stores certain product information
				in a varchar(200) column in a SQL Server database table. When the system was first being
				used nobody ever entered a product description with more than 10 characters. However,
				over time new products were added to the portfolio which required the introduction of a
				prefix in the description. On the SQL level this was done by running an update which
				added the appropriate prefix. Additionally, the company merged with another one and the
				product description entries changed again and became on average 35 characters long.
				Again an update statement was used to implement the changes. Of course since the column
				was defined as a varchar(200) there was no problem storing the changed values.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Unfortunately, the customer found a significant, unexplainable slowdown of the
				system. Why did this happen? </xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>К сожалению, у одного клиента возникло значительное и необъяснимое замедление работы
			системы. Чем оно было вызвано?</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Unfortunately, the customer found a significant, unexplainable slowdown of the
				system. Why did this happen? </xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<ul>

				<li>
					<xsl:choose>
						<xsl:when test="lang('en')">
							<xsl:text>The updates hadn't changed any of the indexes on the table.</xsl:text>
						</xsl:when>
						<xsl:when test="lang('ru')">
							<xsl:text>Обновления не изменили индексы таблицы.</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>The updates hadn't changed any of the indexes on the table.</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</li>

				<li>
					<xsl:choose>
						<xsl:when test="lang('en')">
							<xsl:text>The product table was used very heavily but an investigation of the user queries proved that
							the query plans were correct and hadn't changed over time.</xsl:text>
						</xsl:when>
						<xsl:when test="lang('ru')">
							<xsl:text>Таблица продукта активно использовалась, но исследование запросов пользователя
							показало, что планы выполнения запросов были верны и не изменились со временем.</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>The product table was used very heavily but an investigation of the user queries proved that
							the query plans were correct and hadn't changed over time.</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</li>

				<li>
					<xsl:choose>
						<xsl:when test="lang('en')">
							<xsl:text>There were no hardware issues and the number of concurrent users hadn't increased.</xsl:text>
						</xsl:when>
						<xsl:when test="lang('ru')">
							<xsl:text>Проблем с оборудованием тоже не было и количество одновременных пользователей не возросло.</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>There were no hardware issues and the number of concurrent users hadn't increased.</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</li>

				<li>
					<xsl:choose>
						<xsl:when test="lang('en')">
							<xsl:text>The table size difference due to new records shouldn't have had a huge impact because the queries
							always selected a limited number of rows using an index.</xsl:text>
						</xsl:when>
						<xsl:when test="lang('ru')">
							<xsl:text>Разница в размере таблицы из-за новых записей не должна была оказать сильное
							влияние, так как для записи всегда выделялось ограниченное количество рядов,
							использующих индекс.</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>The table size difference due to new records shouldn't have had a huge impact because the queries
							always selected a limited number of rows using an index.</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</li>

			</ul>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>So what was left as a potential root cause? A closer look at several performance
				counters indicated that there were an unusually high number of logical reads on the
				product table.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Итак, что же осталось в качестве причины? Пристальный обзор нескольких счетчиков
			производительности показал необычно высокое количество логических считываний в таблице
			продукта.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>So what was left as a potential root cause? A closer look at several performance
				counters indicated that there were an unusually high number of logical reads on the
				product table.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>And the reason for this:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Причина:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>And the reason for this:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Due to the new product descriptions, the
				row size increased. Updates of rows led to forward pointers because the
				rows didn't fit into their old page slots any more. This phenomena leaves the data row
				pointers in the indexes unchanged but adds forward pointers in the data table ( heap ).
				If the new row gets too big SQL Server will move it to a new page slot and leave a
				pointer at its original spot. Therefore, looking for a row will be more expensive
				afterwards. A lookup of the data row in a heap is no longer a direct access using the
				page and slot address. Instead of getting the data row the server might have to follow a
				forward pointer first.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Из-за новых описаний продукта размер ряда увеличился.
			Обновления рядов вызвали прямые указатели, так как ряды не помещались в
			свои прежние слоты. Этот феномен не изменяет указатели рядов данных, а добавляет прямые
			указатели в таблицу данных. Если новый ряд становится слишком большим, SQL Server
			переносит его на новую страницу и оставляет указатель на его первоначальную ячейку.
			Таким образом, поиск ряда впоследствии будет дороже. Поиск ряда с данными в таблице
			больше не происходит путем прямого доступа посредством адреса страницы и слота. Вместо
			того, чтобы получить ряд с данными, сервер сначала должен пройти за прямым
			указателем.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Due to the new product descriptions, the
				row size increased. Updates of rows led to forward pointers because the
				rows didn't fit into their old page slots any more. This phenomena leaves the data row
				pointers in the indexes unchanged but adds forward pointers in the data table ( heap ).
				If the new row gets too big SQL Server will move it to a new page slot and leave a
				pointer at its original spot. Therefore, looking for a row will be more expensive
				afterwards. A lookup of the data row in a heap is no longer a direct access using the
				page and slot address. Instead of getting the data row the server might have to follow a
				forward pointer first.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>With SQL Server 2000 I used the dbcc showcontig command to prove the theory. But you
				have to use the option "with tableresults" to get the info about "ForwardedRecords". In
				the default dbcc showcontig output you won't be albe to recognize the issue. </xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>В SQL Server 2000 я использовал команду dbcc showcontig для подтверждения этой
			теории. Но Вам придется использовать опцию "with tableresults" для получения информации
			о "ForwardedRecords" (перенаправленных записях). Вы не сможете распознать эту проблему
			при выводе dbcc showcontig по умолчанию.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>With SQL Server 2000 I used the dbcc showcontig command to prove the theory. But you
				have to use the option "with tableresults" to get the info about "ForwardedRecords". In
				the default dbcc showcontig output you won't be albe to recognize the issue. </xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>SQL Server 2005 on the other hand offers a DMV ( sys.dm_db_index_physical_stats() )
				which shows information regarding "ForwardedRecords" ( column "forwarded_record_count"
				in the result set ). In both cases the number of rows in the output can be a little bit
				misleading as it's the sum of the "real" number of rows and the number of the
				"Forwarded Records". A select count(*) on the table still returns the "real" number
				which you expect.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>С другой стороны, SQL Server 2005 предлагает команду DMV
			(sys.dm_db_index_physical_stats()), которая показывает информацию о "ForwardedRecords"
			(перенаправленных записях) (колонка "forwarded_record_count" в результатах). В обоих
			случаях количество рядов при выводе может ввести в заблуждение, так как является суммой
			«настоящего» количества рядов и числа перенаправленных записей. Запрос select count(*) в
			таблице возвращает "реальное" количество, которое Вы и ожидаете получить.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>SQL Server 2005 on the other hand offers a DMV ( sys.dm_db_index_physical_stats() )
				which shows information regarding "ForwardedRecords" ( column "forwarded_record_count"
				in the result set ). In both cases the number of rows in the output can be a little bit
				misleading as it's the sum of the "real" number of rows and the number of the
				"Forwarded Records". A select count(*) on the table still returns the "real" number
				which you expect.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Once a data table (heap) includes forward
				pointers there is only one way to get rid of them : table reorg. There are a few options
				to do this: The simplest one would be to create a clustered index on the data table and
				drop it again. But there is another option to avoid forward pointers entirely; by
				creating a clustered index from the beginning. A clustered index keeps the data rows in
				its leaf node level. Therefore the data is always sorted according to the index keys and
				forward pointers won't be used. It's like a continuous online reorg in this regard. The
				SQL script below shows the difference between using a data table ( heap ) and a
				clustered index as well as the difference between char() and varchar().</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Как только в таблице данных появляются прямые
			указатели, единственный способ избавиться от них – заново организовать таблицу. Есть
			несколько способов это сделать. Самый простой – создать кластерный индекс таблицы с
			данными и выгрузить ее из памяти. Но есть еще один способ полностью избежать прямых
			указателей – изначально создать кластерный индекс. В кластерном индексе ряды данных
			хранятся концевой вершине. Таким образом, данные всегда сортируются в соответствии с
			ключами индекса и нет необходимости в прямых указателях. Это похоже на постоянную
			реорганизацию через сеть. Приведенный ниже скрипт SQL показывает разницу между
			использованием таблицы данных и кластерного индекса, а также различие между char() and
			varchar().</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Once a data table (heap) includes forward
				pointers there is only one way to get rid of them : table reorg. There are a few options
				to do this: The simplest one would be to create a clustered index on the data table and
				drop it again. But there is another option to avoid forward pointers entirely; by
				creating a clustered index from the beginning. A clustered index keeps the data rows in
				its leaf node level. Therefore the data is always sorted according to the index keys and
				forward pointers won't be used. It's like a continuous online reorg in this regard. The
				SQL script below shows the difference between using a data table ( heap ) and a
				clustered index as well as the difference between char() and varchar().</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Легкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Moderate</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Moderate</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://wiki.lessthandot.com/index.php/Finding_Forwarded_Records_SQL_Server_2008" target="_blank">Finding Forwarded Records SQL Server 2008</a></p>

			<p><a href="http://blogs.msdn.com/b/mssqlisv/archive/2006/12/01/knowing-about-forwarded-records-can-help-diagnose-hard-to-find-performance-issues.aspx8" target="_blank">Knowing about forwarded records</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
