<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="UnnamedConstraints.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Unnamed Constraints</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Безымянные ограничения таблиц</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Unnamed Constraints</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="UnnamedConstraints.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Unnamed Constraints</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Безымянные ограничения таблиц</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Unnamed Constraints</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>How To Name Default Constraints And How To Drop Default Constraint Without A Name In SQL Server</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Как присвоить имя ограничению по умолчанию и удалить безымянные ограничения в SQL Server</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>How To Name Default Constraints And How To Drop Default Constraint Without A Name In SQL Server</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>A best practice is always to name your constraint because it will save you a lot of
				headaches down the road.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Лучше всего давать имена ограничениям, потому что это избавит Вас от множества проблем в дальнейшем. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>A best practice is always to name your constraint because it will save you a lot of
				headaches down the road.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Get the name for all your constraints.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Дайте имена всем ограничениям.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Get the name for all your constraints.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Легкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Легкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://blogs.lessthandot.com/index.php/DataMgmt/DBProgramming/how-to-name-default-constraints-and-how-" target="_blank">Unnamed Constraints</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
