<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="TablesWithForeignKeysNotTrusted.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Foreign Keys Constraints Not Trusted</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с ненадёжными внешними ключами</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Foreign Keys Constraints Not Trusted</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="TablesWithForeignKeysNotTrusted.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Foreign Keys Constraints Not Trusted</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с ненадёжными внешними ключами</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Foreign Keys Constraints Not Trusted</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Foreign Keys or Check Constraints Not Trusted</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Таблицы с ненадёжными внешними ключами</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Foreign Keys or Check Constraints Not Trusted</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>If you need to load a lot of data quickly, you can disable keys and
			constraints in order to improve performance. After the data load finishes,
			enable them again, and SQL Server will check them behind the scenes. This
			technique works best in large data warehouse environments where entire
			dimension tables might be reloaded from scratch every night. Disabling
			constraints is usually safer and easier than dropping and recreating
			them. As long as you remember to enable them correctly again – and most people
			don't. This report checks sys.foreign_keys looking for indexes with
			is_not_trusted = 1.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Достаточно распространённой практикой при загрузе большого объёма данных
			считается выключение внешних ключей (Foreign Keys), что действительно
			приводик улучшению производительности при загрузке. Но после загрузки
			данных, нужно не только включить внешние ключи, но и проверить корректность
			данных. Если этого не сделать, но внешние ключи будут считаться ненадёжными
			и не будут использоваться оптимизатором при запросах. Приведенный отчёт найдет все подобные внешние ключи, и остаётся только
			выполнить проверку для упомянутых таблиц.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>If you need to load a lot of data quickly, you can disable keys and
			constraints in order to improve performance. After the data load finishes,
			enable them again, and SQL Server will check them behind the scenes. This
			technique works best in large data warehouse environments where entire
			dimension tables might be reloaded from scratch every night. Disabling
			constraints is usually safer and easier than dropping and recreating
			them. As long as you remember to enable them correctly again – and most people
			don't. This report checks sys.foreign_keys looking for indexes with
			is_not_trusted = 1.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> You have to tell SQL Server to
			not just enable the constraint, but to recheck all of the data that's been
			loaded. The word CHECK appears twice on purpose – this tells SQL Server that
			it needs to check the data, not just enable the constraint. The checking of
			the existing data can take time and burn a lot of IO throughput, so you may
			want to do this during maintenance windows if the table is large. Also, it
			might turn out that some of the data violates your constraints – but that's
			a good thing to find out too. After this change, you may see improved query
			performance for tables with trusted keys and constraints.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Для всех таблиц выполните операцию
			alter table ИМЯ_ТАБЛИЦЫ with check check constraint ИМЯ_КЛЮЧА.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> You have to tell SQL Server to
			not just enable the constraint, but to recheck all of the data that's been
			loaded. The word CHECK appears twice on purpose – this tells SQL Server that
			it needs to check the data, not just enable the constraint. The checking of
			the existing data can take time and burn a lot of IO throughput, so you may
			want to do this during maintenance windows if the table is large. Also, it
			might turn out that some of the data violates your constraints – but that's
			a good thing to find out too. After this change, you may see improved query
			performance for tables with trusted keys and constraints.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Low</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Простой</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Low</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Low</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Низкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Low</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://www.brentozar.com/blitz/foreign-key-trusted/" target="_blank">Blitz Result: Foreign Keys or Check Constraints Not Trusted</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
