<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="TablesWithMissingForeignKeyIndexes.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables With Missing Foreign KeyIndexes</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с пропущенными индексами для ключей</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables With Missing Foreign KeyIndexes</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="TablesWithMissingForeignKeyIndexes.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables With Missing Foreign KeyIndexes</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с пропущенными индексами для ключей</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables With Missing Foreign KeyIndexes</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Missing Foreign Key Indexes</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Отсутствие индексов у внешних ключей</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Missing Foreign Key Indexes</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>One way to improve performance in SQL Server is simply to ensure that there﻿ are
				indexes on all foreign key. This is NOT done automatically, which is kind of surprising
				given how much of an impact such a change would have on the performance of a significant
				install base of SQL Server.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Один из способов повысить производительность в SQL Server – убедиться, что все внешние ключи имеют индексы.
			Это НЕ происходит автоматически, что отчасти удивительно, учитывая то, насколько это повысит производительность базы
			установленного оборудования SQL Server.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>One way to improve performance in SQL Server is simply to ensure that there﻿ are
				indexes on all foreign key. This is NOT done automatically, which is kind of surprising
				given how much of an impact such a change would have on the performance of a significant
				install base of SQL Server.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Review the tables and create the indexes</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Проверьте все таблицы и создайте индексы</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Review the tables and create the indexes</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Low</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Низкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Low</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Low</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Низкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Low</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://hazaa.com.au/blog/sql-server-generating-sql-for-missing-foreign-key-indexes/" target="_blank">Missing Foreign Key Indexes</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
