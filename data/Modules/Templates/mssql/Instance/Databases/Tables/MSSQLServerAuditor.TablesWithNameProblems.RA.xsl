<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="TablesWithNameProblems.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables With Name Problems</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с проблемными именами</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables With Name Problems</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="TablesWithNameProblems.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Tables With Name Problems</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Таблицы с проблемными именами</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Tables With Name Problems</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Do not use spaces or other invalid characters in your table names</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Не используйте пробелы или другие недопустимые символы в именах таблиц</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Do not use spaces or other invalid characters in your table names</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Column names (and table names) should not have spaces or any other invalid characters
				in them. This is considered bad practice because it requires you to use square brackets
				around your names. Square brackets make the code harder to read and understand. The
				query (presented below) will also highlight columns and tables with numbers in the
				names. Most of the time, when there is a number in a column name, it represents a
				de-normalized database. There are exceptions to this rule, so not all occurrences of
				this problem need to be fixed.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Имена колонок (и таблиц) не должны содержать пробелы и другие недопустимые символы. Это считается плохой практикой,
			так как часто вынуждает использовать вокруг имен квадратные скобки. Из-за квадратный скобок код труднее прочесть и понять.
			Приведенный ниже запрос выделит колонки и таблицы с числами в имени. Чаще всего, если в имени колонки есть цифра, она
			представляет собой неупорядоченную базу данных. В этом правиле есть исключения, поэтому не все элементы с данной проблемой
			нужно исправлять. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Column names (and table names) should not have spaces or any other invalid characters
				in them. This is considered bad practice because it requires you to use square brackets
				around your names. Square brackets make the code harder to read and understand. The
				query (presented below) will also highlight columns and tables with numbers in the
				names. Most of the time, when there is a number in a column name, it represents a
				de-normalized database. There are exceptions to this rule, so not all occurrences of
				this problem need to be fixed.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>

						If this is a number issue, you may need to redesign your database structure
						to include more tables. For example, if you have a StudentGrade table with
						(StudentId, Grade1, Grade2, Grade3, Grade4) you should change it to be
						StudentGrade with (StudentId, Grade, Identifier). Each student would have
						multiple rows in this table (one for each grade). You would need to add an
						identifier column to indicate what the grade is for (test on November 10,
						book report, etc).If this is a weird character issue, then you should change
						the name of the column so it is a simple word or phrase without any spaces,
						numbers, or symbols. When you do this, make sure you check all occurrences
						of where this is used from. This could include procedures, function, views,
						indexes, front end code, etc.

					</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>

						Если проблема в количестве, возможно, требуется изменить структуры базы
						данных так, чтобы она вмещала больше таблиц. Например, если у Вас есть
						таблица StudentGrade (Оценка учащегося) с StudentId, Grade1, Grade2, Grade3,
						Grade4 (Idученика, Оценка1, Оценка2, Оценка3). Переименуйте ее в
						StudentGrade (Оценка учащегося) с StudentId, Grade, Identifier (Id ученика,
						Оценка, Идентификатор). У каждого ученика в таблице будет несколько рядов
						(один для каждой оценки). Нужно будет добавить колонку с идентификатором,
						которая показывала бы, за что поставлена данная оценка (контрольную 10го
						ноября, доклад по книге и т.д.) Если возникнет путаница со знаками, следует
						переименовать колонку одним словом или фразой без пробелов, цифр и символов.
						Затем проверьте все связанные с ней элементы. Это могут быть процедуры,
						функции, виды, индексы, интерфейсный код и т.д.

					</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>

						If this is a number issue, you may need to redesign your database structure
						to include more tables. For example, if you have a StudentGrade table with
						(StudentId, Grade1, Grade2, Grade3, Grade4) you should change it to be
						StudentGrade with (StudentId, Grade, Identifier). Each student would have
						multiple rows in this table (one for each grade). You would need to add an
						identifier column to indicate what the grade is for (test on November 10,
						book report, etc).If this is a weird character issue, then you should change
						the name of the column so it is a simple word or phrase without any spaces,
						numbers, or symbols. When you do this, make sure you check all occurrences
						of where this is used from. This could include procedures, function, views,
						indexes, front end code, etc.

					</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Low</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Low</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Mild</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Mild</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://blogs.lessthandot.com/index.php/DataMgmt/DBProgramming/do-not-use-spaces-or-other-invalid-chara" target="_blank">Таблицы с проблемными именами</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
