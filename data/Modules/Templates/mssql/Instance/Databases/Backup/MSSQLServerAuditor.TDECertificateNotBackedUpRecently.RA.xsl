<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="TDECertificateNotBackedUpRecently.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>TDE Certificate Not Backed Up Recently</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Базы данных без архива для TDE сертификатов</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>TDE Certificate Not Backed Up Recently</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="TDECertificateNotBackedUpRecently.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>TDE Certificate Not Backed Up Recently</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Базы данных без архива для TDE сертификатов</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>TDE Certificate Not Backed Up Recently</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>TDE Certificate Not Backed Up Recently</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>TDE Certificate Not Backed Up Recently</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>TDE Certificate Not Backed Up Recently</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>The certificate is used to encrypt database, but there certificate was
			not backuped up recently or backup is not up to date.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>The certificate is used to encrypt database, but there certificate was
			not backuped up recently or backup is not up to date.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>The certificate is used to encrypt database, but there certificate was
			not backuped up recently or backup is not up to date.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>SQL Server 2008 introduced Transparent Data Encryption – a
			set-it-and-forget-it way to keep your databases protected on disk. If
			someone steals your backup tapes or your hard drives, they'll have a tougher
			time getting access to the data. Just set it up and you're done.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>SQL Server 2008 introduced Transparent Data Encryption – a
			set-it-and-forget-it way to keep your databases protected on disk. If
			someone steals your backup tapes or your hard drives, they'll have a tougher
			time getting access to the data. Just set it up and you're done.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>SQL Server 2008 introduced Transparent Data Encryption – a
			set-it-and-forget-it way to keep your databases protected on disk. If
			someone steals your backup tapes or your hard drives, they'll have a tougher
			time getting access to the data. Just set it up and you're done.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>However, if you don't back up your encryption certificate and password,
			you'll never be able to restore those databases!</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>However, if you don't back up your encryption certificate and password,
			you'll never be able to restore those databases!</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>However, if you don't back up your encryption certificate and password,
			you'll never be able to restore those databases!</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>This report checks sys.databases looking for databases that have been
			encrypted, and also checks to make sure that the certificates have been
			backed up recently.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>This report checks sys.databases looking for databases that have been
			encrypted, and also checks to make sure that the certificates have been
			backed up recently.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>This report checks sys.databases looking for databases that have been
			encrypted, and also checks to make sure that the certificates have been
			backed up recently.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>How to correct it:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Как это исправить:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>How to correct it:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Backup TDE certificates.</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Выполните архивное копирование для
			сертификатов.</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Backup TDE certificates.</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень сложности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of difficulty:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Easy</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Легкий</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Easy</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p>
			<strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Level of severity:</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Уровень опасности:</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Level of severity:</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</strong>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text> Mild</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text> Средний</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> Mild</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://BrentOzar.com/go/tde" target="_blank">TDE Certificate Not Backed Up Recently</a></p>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
