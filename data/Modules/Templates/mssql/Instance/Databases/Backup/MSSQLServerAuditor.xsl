<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabasesBackup.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Database backup (size in MBytes)</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Архивные копии баз данных (размер МБайт)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Database backup (size in MBytes)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="DatabasesBackup.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Database backup (size in MBytes)</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Архивные копии баз данных (размер МБайт)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Database backup (size in MBytes)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
			<link rel="stylesheet" href="$JS_FOLDER$/tablesorter/css/theme.mssqlserverauditor.css" type="text/css"/>

			<script src="$JS_FOLDER$/json-js/json2.js"></script>
			<script src="$JS_FOLDER$/jquery-1.12.4.min.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.widgets.js"></script>

			<script type="text/javascript">
				$(document).ready(function()
					{
						$("#myErrorTable").tablesorter({
							theme : 'MSSQLServerAuditorError',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});

						$("#myTable").tablesorter({
							theme : 'MSSQLServerAuditor',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});
					}
				);
			</script>
		</head>
		<body>
			<style>
				body { overflow: auto; padding:0; margin:0; }
			</style>
			<xsl:if test="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']/child::node()">
			<table id="myErrorTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Query</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Запрос</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Query</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Hierarchy</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Категория</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Hierarchy</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>RecordSets</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Наборов</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>RecordSets</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>#</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Код</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Номер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сообщение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="@instance != ''">
								<xsl:value-of select="@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@name != ''">
								<xsl:value-of select="@name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@hierarchy != ''">
								<xsl:value-of select="@hierarchy"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RecordSets != ''">
								<xsl:value-of select="@RecordSets"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RowCount != ''">
								<xsl:value-of select="@RowCount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorCode != ''">
								<xsl:value-of select="@SqlErrorCode"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorNumber != ''">
								<xsl:value-of select="@SqlErrorNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="SqlErrorMessage != ''">
								<xsl:value-of select="SqlErrorMessage"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
			<xsl:if test="MSSQLResult[@name='GetLastDatabaseBackup' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/child::node()">
			<table id="myTable">
			<thead>
				<tr>
					<th rowspan="2">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Экземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th rowspan="2">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Database</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>База данных</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Database</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th rowspan="2">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Status</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Статус</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Status</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th rowspan="2">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Updateability</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Обновляемость</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Updateability</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th colspan="3">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Dump</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Дамп</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Dump</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th colspan="3">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Full</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Полная</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Full</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th colspan="3">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Incremental</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Инкрементальная</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Incremental</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th colspan="3">
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Transaction Log</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Журнал транзакций</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Transaction Log</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Date</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Дата</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Date</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Standard</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Стандартная</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Standard</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Compressed</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сжатая</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Compressed</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Date</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Дата</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Date</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Standard</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Стандартная</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Standard</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Compressed</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сжатая</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Compressed</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Date</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Дата</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Date</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Standard</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Стандартная</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Standard</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Compressed</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сжатая</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Compressed</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Date</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Дата</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Date</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Standard</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Стандартная</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Standard</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Compressed</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сжатая</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Compressed</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@name='GetLastDatabaseBackup' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/Row">
				<tr>
					<xsl:if test="DatabaseStatus != 'ONLINE'">
						<xsl:attribute name="style">text-decoration: line-through;</xsl:attribute>
					</xsl:if>
					<td>
						<xsl:choose>
							<xsl:when test="../../@instance != ''">
								<xsl:value-of select="../../@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseName != ''">
								<xsl:value-of select="DatabaseName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseStatus != ''">
								<xsl:value-of select="DatabaseStatus"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseUpdateability != ''">
								<xsl:value-of select="DatabaseUpdateability"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:if test="S_DatabaseBackupStatus != 0">
							<xsl:attribute name="style">font-weight: bold; color: red;</xsl:attribute>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="S_DatabaseBackupDateTime != ''">
								<xsl:value-of select="ms:format-date(S_DatabaseBackupDateTime, 'dd.MM.yyyy')"/>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(S_DatabaseBackupDateTime, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="S_DatabaseBackupSize != ''">
								<xsl:value-of select="format-number(S_DatabaseBackupSize, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="S_DatabaseCompressedBackupSize != ''">
								<xsl:value-of select="format-number(S_DatabaseCompressedBackupSize, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:if test="D_DatabaseBackupStatus != 0">
							<xsl:attribute name="style">font-weight: bold; color: red;</xsl:attribute>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="D_DatabaseBackupDateTime != ''">
								<xsl:value-of select="ms:format-date(D_DatabaseBackupDateTime, 'dd.MM.yyyy')"/>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(D_DatabaseBackupDateTime, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="D_DatabaseBackupSize != ''">
								<xsl:value-of select="format-number(D_DatabaseBackupSize, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="D_DatabaseCompressedBackupSize != ''">
								<xsl:value-of select="format-number(D_DatabaseCompressedBackupSize, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:if test="I_DatabaseBackupStatus != 0">
							<xsl:attribute name="style">font-weight: bold; color: red;</xsl:attribute>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="I_DatabaseBackupDateTime != ''">
								<xsl:value-of select="ms:format-date(I_DatabaseBackupDateTime, 'dd.MM.yyyy')"/>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(I_DatabaseBackupDateTime, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="I_DatabaseBackupSize != ''">
								<xsl:value-of select="format-number(I_DatabaseBackupSize, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="I_DatabaseCompressedBackupSize != ''">
								<xsl:value-of select="format-number(I_DatabaseCompressedBackupSize, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:if test="L_DatabaseBackupStatus != 0">
							<xsl:attribute name="style">font-weight: bold; color: red;</xsl:attribute>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="L_DatabaseBackupDateTime != ''">
								<xsl:value-of select="ms:format-date(L_DatabaseBackupDateTime, 'dd.MM.yyyy')"/>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(L_DatabaseBackupDateTime, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="L_DatabaseBackupSize != ''">
								<xsl:value-of select="format-number(L_DatabaseBackupSize, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="L_DatabaseCompressedBackupSize != ''">
								<xsl:value-of select="format-number(L_DatabaseCompressedBackupSize, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
