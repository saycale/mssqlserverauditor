<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="DatabaseIOStatus.Description.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Disks IO Status</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Нагрузка дисковой системы</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Disks IO Status</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="DatabaseIOStatus.Description.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Disks IO Status</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Нагрузка дисковой системы</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Disks IO Status</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>

			<h1 class="firstHeading">
				<xsl:choose>
					<xsl:when test="lang('en')">
						<xsl:text>Databases disks IO Status</xsl:text>
					</xsl:when>
					<xsl:when test="lang('ru')">
						<xsl:text>Показатели нагрузки дисковой системы</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Databases disks IO Status</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</h1>

			<p>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>

						Statistics based on the virtual table dm_io_virtual_file_stats.

					</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>

						Статистика (число чтений и записей на каждый из файлов, составляющих базы данных)
						основана на данных из виртуальной таблицы dm_io_virtual_file_stats.
						Статистика собирается с момента последнего рестарта экземпляра сервера баз данных.

					</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>

						Statistics based on the virtual table dm_io_virtual_file_stats.

					</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</p>

			<p><a href="http://www.mssqltips.com/sqlservertip/2329/how-to-identify-io-bottlenecks-in-ms-sql-server/" target="_blank">How to Identify IO Bottlenecks in MS SQL Server</a></p>

			<p><a href="http://www.databasejournal.com/features/mssql/finding-the-source-of-your-sql-server-io.html" target="_blank">Finding the Source of Your SQL Server Database I/O</a></p>

			<p><a href="http://technet.microsoft.com/en-us/magazine/jj643251.aspx" target="_blank">SQL Server: Minimize Disk I/O</a></p>

		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
