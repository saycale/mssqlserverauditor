<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="UpMonitor.HTML" columns="100" rows="20;80;20" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Active processes (statistics for 15 sec)</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Активные работающие процессы (статистика за 15 секунд)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Active processes (statistics for 15 sec)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="UpMonitor.1x1.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Active processes (statistics for 15 sec)</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Активные работающие процессы (статистика за 15 секунд)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Active processes (statistics for 15 sec)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
			<link rel="stylesheet" href="$JS_FOLDER$/tablesorter/css/theme.mssqlserverauditor.css" type="text/css"/>

			<script src="$JS_FOLDER$/json-js/json2.js"></script>
			<script src="$JS_FOLDER$/jquery-1.12.4.min.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.widgets.js"></script>

			<script type="text/javascript">
				$(document).ready(function()
					{
						$("#myErrorTable").tablesorter({
							theme : 'MSSQLServerAuditorError',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});

						$("#myTable").tablesorter({
							theme : 'MSSQLServerAuditor',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});
					}
				);
			</script>
		</head>
		<body>
			<style>
				body { overflow: auto; padding:0; margin:0; }
			</style>
			<xsl:if test="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']/child::node()">
			<table id="myErrorTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Query</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Запрос</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Query</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Hierarchy</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Категория</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Hierarchy</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>RecordSets</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Наборов</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>RecordSets</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>#</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Код</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Номер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сообщение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="@instance != ''">
								<xsl:value-of select="@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@name != ''">
								<xsl:value-of select="@name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@hierarchy != ''">
								<xsl:value-of select="@hierarchy"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RecordSets != ''">
								<xsl:value-of select="@RecordSets"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RowCount != ''">
								<xsl:value-of select="@RowCount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorCode != ''">
								<xsl:value-of select="@SqlErrorCode"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorNumber != ''">
								<xsl:value-of select="@SqlErrorNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="SqlErrorMessage != ''">
								<xsl:value-of select="SqlErrorMessage"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
			<xsl:if test="MSSQLResult[@name='UpMonitor' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/child::node()">
			<table id="myTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Login</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Пользователь</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Login</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>CPU_Inc</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>CPU_Inc</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>CPU_Inc</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>CPU (%)</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>CPU (%)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>CPU (%)</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>IO_Inc</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>IO_Inc</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>IO_Inc</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>IO %</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>IO %</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>IO %</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Processes Count</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Число процессов</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Processes Count</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@name='UpMonitor' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/Row">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="../../@instance != ''">
								<xsl:value-of select="../../@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="nt_username != ''">
								<xsl:value-of select="nt_username"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="cpu_inc != ''">
								<xsl:value-of select="format-number(cpu_inc, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="cpu != ''">
								<xsl:value-of select="format-number(cpu, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="io_inc != ''">
								<xsl:value-of select="format-number(io_inc, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="io != ''">
								<xsl:value-of select="format-number(io, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="ProcessCount != ''">
								<xsl:value-of select="ProcessCount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
	<mssqlauditorpreprocessor id="UpMonitor.1x2.HTML" column="1" row="2" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Active processes (statistics for 15 sec)</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Активные работающие процессы (статистика за 15 секунд)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Active processes (statistics for 15 sec)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
			<link rel="stylesheet" href="$JS_FOLDER$/tablesorter/css/theme.mssqlserverauditor.css" type="text/css"/>

			<script src="$JS_FOLDER$/json-js/json2.js"></script>
			<script src="$JS_FOLDER$/jquery-1.12.4.min.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.widgets.js"></script>

			<script type="text/javascript">
				$(document).ready(function()
					{
						$("#myErrorTable").tablesorter({
							theme : 'MSSQLServerAuditorError',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});

						$("#myTable").tablesorter({
							theme : 'MSSQLServerAuditor',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});
					}
				);
			</script>
		</head>
		<body>
			<style>
				body { overflow: auto; padding:0; margin:0; }
			</style>
			<xsl:if test="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']/child::node()">
			<table id="myErrorTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Query</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Запрос</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Query</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Hierarchy</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Категория</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Hierarchy</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>RecordSets</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Наборов</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>RecordSets</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>#</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Код</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Номер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сообщение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="@instance != ''">
								<xsl:value-of select="@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@name != ''">
								<xsl:value-of select="@name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@hierarchy != ''">
								<xsl:value-of select="@hierarchy"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RecordSets != ''">
								<xsl:value-of select="@RecordSets"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RowCount != ''">
								<xsl:value-of select="@RowCount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorCode != ''">
								<xsl:value-of select="@SqlErrorCode"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorNumber != ''">
								<xsl:value-of select="@SqlErrorNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="SqlErrorMessage != ''">
								<xsl:value-of select="SqlErrorMessage"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
			<xsl:if test="MSSQLResult[@name='UpMonitor' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='2']/child::node()">
			<table id="myTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>User</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Пользователь</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>User</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>#</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Blocked</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Блокирован</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Blocked</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>CPU (%)</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>CPU (%)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>CPU (%)</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>IO (%)</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>IO (%)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>IO (%)</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('ru')">
								<xsl:text>Число</xsl:text>
							</xsl:when>
							<xsl:when test="lang('en')">
								<xsl:text>Number</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Number</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Commands</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Команды</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Commands</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Command time</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Время команды</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Command time</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Program</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Программа</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Program</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Server</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Компьютер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Server</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@name='UpMonitor' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='2']/Row">
				<tr>
					<xsl:choose>
						<xsl:when test="blocked != ''">
							<xsl:attribute name="style">color: red;</xsl:attribute>
						</xsl:when>
					</xsl:choose>
					<td>
						<xsl:choose>
							<xsl:when test="../../@instance != ''">
								<xsl:value-of select="../../@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="nt_username != ''">
								<xsl:value-of select="nt_username"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="spid != ''">
								<xsl:value-of select="spid"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="blocked != ''">
								<xsl:value-of select="blocked"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="cpu != ''">
								<xsl:value-of select="format-number(cpu, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="IO != ''">
								<xsl:value-of select="format-number(IO, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="Snapshots != ''">
								<xsl:value-of select="Snapshots"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="cmd_history != ''">
								<xsl:value-of select="cmd_history"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="last_batch != ''">
								<xsl:value-of select="ms:format-date(last_batch, 'dd.MM.yyyy')"/>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(last_batch, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="program_name != ''">
								<xsl:value-of select="program_name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="hostname != ''">
								<xsl:value-of select="hostname"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
	<mssqlauditorpreprocessor id="UpMonitor.1x3.HTML" column="1" row="3" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Active processes (statistics for 15 sec)</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Активные работающие процессы (статистика за 15 секунд)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Active processes (statistics for 15 sec)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
			<link rel="stylesheet" href="$JS_FOLDER$/tablesorter/css/theme.mssqlserverauditor.css" type="text/css"/>

			<script src="$JS_FOLDER$/json-js/json2.js"></script>
			<script src="$JS_FOLDER$/jquery-1.12.4.min.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.widgets.js"></script>

			<script type="text/javascript">
				$(document).ready(function()
					{
						$("#myErrorTable").tablesorter({
							theme : 'MSSQLServerAuditorError',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});

						$("#myTable").tablesorter({
							theme : 'MSSQLServerAuditor',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});
					}
				);
			</script>
		</head>
		<body>
			<style>
				body { overflow: auto; padding:0; margin:0; }
			</style>
			<xsl:if test="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']/child::node()">
			<table id="myErrorTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Query</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Запрос</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Query</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Hierarchy</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Категория</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Hierarchy</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>RecordSets</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Наборов</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>RecordSets</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>#</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Код</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Номер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сообщение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="@instance != ''">
								<xsl:value-of select="@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@name != ''">
								<xsl:value-of select="@name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@hierarchy != ''">
								<xsl:value-of select="@hierarchy"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RecordSets != ''">
								<xsl:value-of select="@RecordSets"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RowCount != ''">
								<xsl:value-of select="@RowCount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorCode != ''">
								<xsl:value-of select="@SqlErrorCode"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorNumber != ''">
								<xsl:value-of select="@SqlErrorNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="SqlErrorMessage != ''">
								<xsl:value-of select="SqlErrorMessage"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
			<xsl:if test="MSSQLResult[@name='UpMonitor' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='3']/child::node()">
			<table id="myTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Database</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>База данных</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Database</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Read (MB)</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Прочитано (МБ)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Read (MB)</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('ru')">
								<xsl:text>Записано (МБ)</xsl:text>
							</xsl:when>
							<xsl:when test="lang('en')">
								<xsl:text>Write (MB)</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Write (MB)</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@name='UpMonitor' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='3']/Row">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="../../@instance != ''">
								<xsl:value-of select="../../@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="Database != ''">
								<xsl:value-of select="Database"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="IO_Read_Mb != ''">
								<xsl:value-of select="format-number(IO_Read_Mb, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="IO_Written_Mb != ''">
								<xsl:value-of select="format-number(IO_Written_Mb, '###,###,##0.00')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
