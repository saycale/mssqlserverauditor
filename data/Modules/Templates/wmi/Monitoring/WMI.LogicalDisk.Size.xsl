<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="Monitoring.WMI.LogicalDisk.Size.HTML" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Logical Disk Size (b)</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Размеры локальных дисков (b)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Logical Disk Size (b)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>
	<mssqlauditorpreprocessor id="Monitoring.WMI.LogicalDisk.Size.HTML" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Logical Disk Size (b)</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Размеры локальных дисков (b)</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Logical Disk Size (b)</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
			<link rel="stylesheet" href="$JS_FOLDER$/tablesorter/css/theme.mssqlserverauditor.css" type="text/css"/>

			<script src="$JS_FOLDER$/json-js/json2.js"></script>
			<script src="$JS_FOLDER$/jquery-1.12.4.min.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.widgets.js"></script>

			<script type="text/javascript">
				$(document).ready(function() {
					$("#myErrorTable").tablesorter({
						theme : 'MSSQLServerAuditorError',

						widgets: [ "zebra", "resizable", "stickyHeaders" ],

						widgetOptions : {
							zebra : ["even", "odd"]
						}
					});

					$("#myTable").tablesorter({
						theme : 'MSSQLServerAuditor',

						widgets: [ "zebra", "resizable", "stickyHeaders" ],

						widgetOptions : {
							zebra : ["even", "odd"]
						}
					});
				});
			</script>
		</head>
		<body>
			<style>
				body { overflow: auto; padding:0; margin:0; }
			</style>
			<xsl:apply-templates select="child::node()"/>
		</body>
		</html>
		</xsl:template>

		<xsl:template match="MSSQLResult[@name='Get_Win32_LogicalDisk' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']">
			<table id="myTable">
			<caption>
			</caption>
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Экземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Drive</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Диск</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Drive</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>FileSystem</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Файловая система</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>FileSystem</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>DriveType</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Тип диска</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>DriveType</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>MediaType</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Тип хранилища</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>MediaType</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Size</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Размер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Size</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>FreeSpace</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Свободно</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>FreeSpace</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="Row">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="../../@instance != ''">
								<xsl:value-of select="../../@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="Name != ''">
								<xsl:value-of select="Name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="FileSystem != ''">
								<xsl:value-of select="FileSystem"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DriveType != ''">
								<xsl:value-of select="DriveType"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="MediaType != ''">
								<xsl:value-of select="MediaType"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="Size != ''">
								<xsl:value-of select="format-number(Size, '###,###,##0')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="FreeSpace != ''">
								<xsl:value-of select="format-number(FreeSpace, '###,###,##0')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
