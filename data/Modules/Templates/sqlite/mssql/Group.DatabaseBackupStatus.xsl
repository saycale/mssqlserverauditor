<?xml version="1.0" encoding="UTF-8"?>
<root>
<mssqlauditorpreprocessors preprocessor="WebPreprocessor" id="GroupDatabaseBackupStatus" columns="100" rows="100" splitter="yes">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Group Database Backup Status</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Статус архивного копирования баз данных группы</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Group Database Backup Status</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>

	<mssqlauditorpreprocessor id="GroupDatabaseBackupStatus" column="1" row="1" colspan="1" rowspan="1">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Group Database Backup Status</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Статус архивного копирования баз данных группы</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Group Database Backup Status</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
			<link rel="stylesheet" href="$JS_FOLDER$/tablesorter/css/theme.mssqlserverauditor.css" type="text/css"/>

			<script src="$JS_FOLDER$/json-js/json2.js"></script>
			<script src="$JS_FOLDER$/jquery-1.12.4.min.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.js"></script>
			<script src="$JS_FOLDER$/tablesorter/js/jquery.tablesorter.widgets.js"></script>

			<script type="text/javascript">
				$(document).ready(function()
					{
						$("#myErrorTable").tablesorter({
							theme : 'MSSQLServerAuditorError',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});

						$("#myTable").tablesorter({
							theme : 'MSSQLServerAuditor',

							widgets: [ "zebra", "resizable", "stickyHeaders" ],

							widgetOptions : {
								zebra : ["even", "odd"]
							}
						});
					}
				);
			</script>
		</head>
		<body>
			<style>
				body { overflow: auto; padding:0; margin:0; }
			</style>
			<xsl:if test="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']/child::node()">
			<table id="myErrorTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Query</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Запрос</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Query</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Hierarchy</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Категория</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Hierarchy</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>RecordSets</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Наборов</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>RecordSets</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>#</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Код</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Номер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сообщение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="@instance != ''">
								<xsl:value-of select="@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@name != ''">
								<xsl:value-of select="@name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@hierarchy != ''">
								<xsl:value-of select="@hierarchy"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RecordSets != ''">
								<xsl:value-of select="@RecordSets"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RowCount != ''">
								<xsl:value-of select="@RowCount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorCode != ''">
								<xsl:value-of select="@SqlErrorCode"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorNumber != ''">
								<xsl:value-of select="@SqlErrorNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="SqlErrorMessage != ''">
								<xsl:value-of select="SqlErrorMessage"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
			<xsl:if test="MSSQLResult[@name='GetGroupDatabaseBackupStatus' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/child::node()">
			<table id="myTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>EventTime</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Время</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>EventTime</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>ConnectionName</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сервер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>ConnectionName</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>DatabaseName</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>База данных</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>DatabaseName</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Status</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Статус</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Status</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Updateability</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Обновление</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Updateability</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Recovery</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Тип</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Recovery</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Snapshot</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Снимок</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Snapshot</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Full</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Полная</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Full</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Incremental</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Разностная</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Incremental</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>TransactionLog</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Журнал</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>TransactionLog</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@name='GetGroupDatabaseBackupStatus' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/Row">
				<tr>
					<td align="right">
						<xsl:choose>
							<xsl:when test="EventTime != ''">
								<xsl:choose>
									<xsl:when test="lang('en')">
										<xsl:value-of select="ms:format-date(EventTime, 'dd/MM/yyyy')"/>
									</xsl:when>
									<xsl:when test="lang('ru')">
										<xsl:value-of select="ms:format-date(EventTime, 'dd.MM.yyyy')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="ms:format-date(EventTime, 'dd/MM/yyyy')"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(EventTime, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="ConnectionName != ''">
								<xsl:value-of select="ConnectionName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseName != ''">
								<xsl:value-of select="DatabaseName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseStatus != ''">
								<xsl:value-of select="DatabaseStatus"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseUpdateability != ''">
								<xsl:value-of select="DatabaseUpdateability"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseRecovery != ''">
								<xsl:value-of select="DatabaseRecovery"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseBackupFinishDateTime_S != ''">
								<xsl:value-of select="ms:format-date(DatabaseBackupFinishDateTime_S, 'dd.MM.yyyy')"/>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(DatabaseBackupFinishDateTime_S, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseBackupFinishDateTime_D != ''">
								<xsl:value-of select="ms:format-date(DatabaseBackupFinishDateTime_D, 'dd.MM.yyyy')"/>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(DatabaseBackupFinishDateTime_D, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseBackupFinishDateTime_I != ''">
								<xsl:value-of select="ms:format-date(DatabaseBackupFinishDateTime_I, 'dd.MM.yyyy')"/>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(DatabaseBackupFinishDateTime_I, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseBackupFinishDateTime_L != ''">
								<xsl:value-of select="ms:format-date(DatabaseBackupFinishDateTime_L, 'dd.MM.yyyy')"/>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(DatabaseBackupFinishDateTime_L, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>

<mssqlauditorpreprocessors preprocessor="HtmlPreprocessor" id="GroupDatabaseBackupStatus.HTML">
	<configuration>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
			<title>
			<xsl:choose>
				<xsl:when test="lang('en')">
					<xsl:text>Group Database Backup Status</xsl:text>
				</xsl:when>
				<xsl:when test="lang('ru')">
					<xsl:text>Статус архивного копирования баз данных группы</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Group Database Backup Status</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			</title>
		</head>
		<body>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</configuration>

	<mssqlauditorpreprocessor id="GroupDatabaseBackupStatus.HTML">
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">

		<xsl:template match="/MSSQLResults">

		<html>
		<head>
		</head>
		<body>
			<style type="text/css">
				body
				{
					margin:           1px;
					overflow:         auto;
					padding:          1px;
					background:       #FFFFFF;
				}
				#myTable
				{
					background:       #FFFFFF;
					border-bottom:    1px solid #000000;
					border-collapse:  collapse;
					border-color:     #000000;
					border-left:      1px solid #000000;
					border-right:     1px solid #000000;
					border-top:       1px solid #000000;
					color:            #000000;
					font-family:      "Lucida Sans Unicode", "Lucida Grande", "Sans-Serif";
					font-size:        10px;
					font-weight:      normal;
					text-align:       left;
				}
				#myTable caption
				{
					background:       #FFFFFF;
					border-bottom:    0px solid #FFFFFF;
					border-collapse:  collapse;
					border-color:     #FFFFFF;
					border-left:      0px solid #FFFFFF;
					border-right:     0px solid #FFFFFF;
					border-top:       0px solid #FFFFFF;
					color:            #000000;
					font-weight:      bold;
					margin:           2px;
					padding:          2px;
					text-align:       center;
				}
				#myTable th
				{
					background:       #9ACD32;
					border-bottom:    1px solid #000000;
					border-color:     #9ACD32;
					border-left:      1px solid #000000;
					border-right:     1px solid #000000;
					border-top:       1px solid #000000;
					color:            #000000;
					font-weight:      bold;
					margin:           2px;
					padding:          2px;
					text-align:       center;
				}
				#myTable td
				{
					background:       #FFFFFF;
					border-bottom:    1px solid #000000;
					border-color:     #000000;
					border-left:      1px solid #000000;
					border-right:     1px solid #000000;
					border-top:       1px solid #000000;
					color:            #000000;
					margin:           1px;
					padding:          1px;
				}
				#myTable tr:nth-child(odd)
				{
					background-color: #FFFFFF;
				}
				#myTable tr:nth-child(even)
				{
					background-color: #FFFFFF;
				}
				#myErrorTable
				{
					font-family:      "Lucida Sans Unicode", "Lucida Grande", "Sans-Serif";
					font-size:        10px;
					text-align:       left;
					font-weight:      normal;
					background:       #FFFFFF;
					border-collapse:  collapse;
					border-right:     1px solid #000000;
					border-left:      1px solid #000000;
					border-top:       1px solid #000000;
					border-bottom:    1px solid #000000;
					color:            #000000;
				}
				#myErrorTable caption
				{
					text-align:       center;
					font-weight:      bold;
					background:       #FF0000;
					border-right:     1px solid #000000;
					border-left:      1px solid #000000;
					border-top:       1px solid #000000;
					border-bottom:    1px solid #000000;
					padding:          2px;
					margin:           2px;
				}
				#myErrorTable th
				{
					text-align:       center;
					font-weight:      bold;
					background:       #FF0000;
					border-right:     1px solid #000000;
					border-left:      1px solid #000000;
					border-top:       1px solid #000000;
					border-bottom:    1px solid #000000;
					padding:          2px;
					margin:           2px;
				}
				#myErrorTable td
				{
					border-right:     1px solid #000000;
					border-left:      1px solid #000000;
					border-top:       1px solid #000000;
					border-bottom:    1px solid #000000;
					padding:          1px;
					margin:           1px;
				}
				#myErrorTable tr:nth-child(odd)
				{
					background-color: #CCCCCC;
				}
				#myErrorTable tr:nth-child(even)
				{
					background-color: #FFFFFF;
				}
			</style>
			<xsl:if test="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']/child::node()">
			<table id="myErrorTable">
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Зкземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Query</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Запрос</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Query</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Hierarchy</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Категория</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Hierarchy</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>RecordSets</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Наборов</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>RecordSets</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>#</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>#</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Код</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorCode</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Номер</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorNumber</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Сообщение</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>SqlErrorMessage</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@SqlErrorNumber!='0' or @SqlErrorCode!='']">
				<tr>
					<td>
						<xsl:choose>
							<xsl:when test="@instance != ''">
								<xsl:value-of select="@instance"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@name != ''">
								<xsl:value-of select="@name"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="@hierarchy != ''">
								<xsl:value-of select="@hierarchy"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RecordSets != ''">
								<xsl:value-of select="@RecordSets"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@RowCount != ''">
								<xsl:value-of select="@RowCount"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorCode != ''">
								<xsl:value-of select="@SqlErrorCode"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="@SqlErrorNumber != ''">
								<xsl:value-of select="@SqlErrorNumber"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="SqlErrorMessage != ''">
								<xsl:value-of select="SqlErrorMessage"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
			<xsl:if test="MSSQLResult[@name='GetGroupDatabaseBackupStatus' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/child::node()">
			<table id="myTable">
			<caption>
				<xsl:choose>
					<xsl:when test="lang('ru')">
						<xsl:text>Статус архивного копирования баз данных</xsl:text>
					</xsl:when>
					<xsl:when test="lang('en')">
						<xsl:text>Databases backup status</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>Databases backup status</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</caption>
			<thead>
				<tr>
					<th>
						<xsl:choose>
							<xsl:when test="lang('ru')">
								<xsl:text>Окружение</xsl:text>
							</xsl:when>
							<xsl:when test="lang('en')">
								<xsl:text>Environment</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Environment</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Instance</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Экземпляр</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Instance</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Database</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>База данных</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Database</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Status</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Статус</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Status</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('ru')">
								<xsl:text>Обновление</xsl:text>
							</xsl:when>
							<xsl:when test="lang('en')">
								<xsl:text>Updateability</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Updateability</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Recovery</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Тип</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Recovery</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('ru')">
								<xsl:text>Снимок</xsl:text>
							</xsl:when>
							<xsl:when test="lang('en')">
								<xsl:text>Snapshot</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Snapshot</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('en')">
								<xsl:text>Full</xsl:text>
							</xsl:when>
							<xsl:when test="lang('ru')">
								<xsl:text>Полная</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Full</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('ru')">
								<xsl:text>Разностная</xsl:text>
							</xsl:when>
							<xsl:when test="lang('en')">
								<xsl:text>Incremental</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Incremental</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
					<th>
						<xsl:choose>
							<xsl:when test="lang('ru')">
								<xsl:text>Журнал</xsl:text>
							</xsl:when>
							<xsl:when test="lang('en')">
								<xsl:text>Transaction log</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>Transaction log</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="MSSQLResult[@name='GetGroupDatabaseBackupStatus' and @SqlErrorNumber='0' and @hierarchy='']/RecordSet[@id='1']/Row">
				<tr>
					<xsl:if test="ServerEnvironment != preceding-sibling::*[1]/ServerEnvironment or count(preceding-sibling::*[1]/ServerEnvironment) = 0">
						<td valign="top" rowspan="{count(..//*[ServerEnvironment=current()/ServerEnvironment])}">
							<xsl:choose>
								<xsl:when test="ServerEnvironment != ''">
									<xsl:value-of select="ServerEnvironment"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>&#160;</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:if>
					<xsl:if test="ServerEnvironment != preceding-sibling::*[1]/ServerEnvironment or ConnectionName != preceding-sibling::*[1]/ConnectionName or count(preceding-sibling::*[1]/ConnectionName) = 0">
						<td valign="top" rowspan="{count(..//*[ServerEnvironment=current()/ServerEnvironment and ConnectionName=current()/ConnectionName])}">
							<xsl:choose>
								<xsl:when test="ConnectionName != ''">
									<xsl:value-of select="ConnectionName"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>&#160;</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:if>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseName != ''">
								<xsl:value-of select="DatabaseName"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseStatus != ''">
								<xsl:value-of select="DatabaseStatus"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseUpdateability != ''">
								<xsl:value-of select="DatabaseUpdateability"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td>
						<xsl:choose>
							<xsl:when test="DatabaseRecovery != ''">
								<xsl:value-of select="DatabaseRecovery"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseBackupFinishDateTime_S != ''">
								<xsl:choose>
									<xsl:when test="lang('en')">
										<xsl:value-of select="ms:format-date(DatabaseBackupFinishDateTime_S, 'dd/MM/yyyy')"/>
									</xsl:when>
									<xsl:when test="lang('ru')">
										<xsl:value-of select="ms:format-date(DatabaseBackupFinishDateTime_S, 'dd.MM.yyyy')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="ms:format-date(DatabaseBackupFinishDateTime_S, 'dd/MM/yyyy')"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(DatabaseBackupFinishDateTime_S, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseBackupFinishDateTime_D != ''">
								<xsl:choose>
									<xsl:when test="lang('en')">
										<xsl:value-of select="ms:format-date(DatabaseBackupFinishDateTime_D, 'dd/MM/yyyy')"/>
									</xsl:when>
									<xsl:when test="lang('ru')">
										<xsl:value-of select="ms:format-date(DatabaseBackupFinishDateTime_D, 'dd.MM.yyyy')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="ms:format-date(DatabaseBackupFinishDateTime_D, 'dd/MM/yyyy')"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(DatabaseBackupFinishDateTime_D, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseBackupFinishDateTime_I != ''">
								<xsl:choose>
									<xsl:when test="lang('en')">
										<xsl:value-of select="ms:format-date(DatabaseBackupFinishDateTime_I, 'dd/MM/yyyy')"/>
									</xsl:when>
									<xsl:when test="lang('ru')">
										<xsl:value-of select="ms:format-date(DatabaseBackupFinishDateTime_I, 'dd.MM.yyyy')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="ms:format-date(DatabaseBackupFinishDateTime_I, 'dd/MM/yyyy')"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(DatabaseBackupFinishDateTime_I, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right">
						<xsl:choose>
							<xsl:when test="DatabaseBackupFinishDateTime_L != ''">
								<xsl:choose>
									<xsl:when test="lang('en')">
										<xsl:value-of select="ms:format-date(DatabaseBackupFinishDateTime_L, 'dd/MM/yyyy')"/>
									</xsl:when>
									<xsl:when test="lang('ru')">
										<xsl:value-of select="ms:format-date(DatabaseBackupFinishDateTime_L, 'dd.MM.yyyy')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="ms:format-date(DatabaseBackupFinishDateTime_L, 'dd/MM/yyyy')"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:text>&#160;</xsl:text>
								<xsl:value-of select="ms:format-time(DatabaseBackupFinishDateTime_L, 'HH:mm:ss')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>&#160;</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
				</xsl:for-each>
			</tbody>
			</table>
			</xsl:if>
		</body>
		</html>
		</xsl:template>
		</xsl:stylesheet>
	</mssqlauditorpreprocessor>
</mssqlauditorpreprocessors>
</root>
