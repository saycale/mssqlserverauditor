<?xml version="1.0" encoding="UTF-8"?>
<sql-select-content xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<sql-select name="GetInstanceSystemStatisticalFunctionsMonth">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			SELECT
				 LEFT(CONVERT([NVARCHAR](128), tM.[EventDayStartOfMonth], 112), 6)                                            AS [NodeUName]
				,LEFT(CONVERT([NVARCHAR](128), tM.[EventDayStartOfMonth], 112), 6)                                            AS [NodeUId]
				,1                                                                                                            AS [NodeEnabled]
				,N'NodeIcon'                                                                                                  AS [NodeUIcon]
				,N'#080808'                                                                                                   AS [NodeFontColor]
				,N'normal'                                                                                                    AS [NodeFontStyle]
				,CONVERT([NVARCHAR](128), tM.[EventDayStartOfMonth], 112)                                                     AS [EventDayMin]
				,CONVERT([NVARCHAR](128), DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, tM.[EventDayStartOfMonth]) + 1, 0)), 112) AS [EventDayMax]
			FROM (
				SELECT DISTINCT
					DATEADD(month, DATEDIFF(month, 0, hISSF.[EventDay]), 0) AS [EventDayStartOfMonth]
				FROM
					[${hist}$].[dbo].[h_InstanceSystemStatisticalFunctions] hISSF
					INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
						    hSI.[id]                  = hISSF.[h_ServerInstance_id]
						AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
						AND hSI.[d_Login_id]          = @d_Login_id
				WHERE
					hISSF.[EventDay] &gt; (
						SELECT
							DATEADD(mm, -12, MAX(hISSF.[EventDay]))
						FROM
							[${hist}$].[dbo].[h_InstanceSystemStatisticalFunctions] hISSF
							INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
								    hSI.[id]                  = hISSF.[h_ServerInstance_id]
								AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
								AND hSI.[d_Login_id]          = @d_Login_id
					)
				) tM
			ORDER BY
				tM.[EventDayStartOfMonth] DESC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@strDateTimeModifier" type="NVarChar" />
		</sql-select-parameters>
	</sql-select>

	<sql-select name="GetInstanceSystemStatisticalFunctionsMonthWeeks">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			SELECT
				 CONVERT([NVARCHAR](128), DATEPART(WEEK, tW.[EventDayMonday])) AS [NodeUName]
				,CONVERT([NVARCHAR](128), DATEPART(WEEK, tW.[EventDayMonday])) AS [NodeUId]
				,1                                                             AS [NodeEnabled]
				,N'NodeIcon'                                                   AS [NodeUIcon]
				,N'#080808'                                                    AS [NodeFontColor]
				,N'normal'                                                     AS [NodeFontStyle]
				,CASE
					WHEN (tW.[EventDayMonday] &lt; CONVERT([DATE], @EventDayMin, 112)) THEN
						CONVERT([NVARCHAR](128), @EventDayMin, 112)
					ELSE
						CONVERT([NVARCHAR](128), tW.[EventDayMonday], 112)
				END                                                            AS [EventDayMin]
				,CASE
					WHEN (tW.[EventDaySunday] &gt; CONVERT([DATE], @EventDayMax, 112)) THEN
						CONVERT([NVARCHAR](128), @EventDayMax, 112)
					ELSE
						CONVERT([NVARCHAR](128), tW.[EventDaySunday], 112)
				END                                                            AS [EventDayMax]
			FROM (
				SELECT DISTINCT
					 DATEADD(wk, DATEDIFF(d, 0, tD.[EventDay]) / 7, 0)                  AS [EventDayMonday]
					,DATEADD(day, 6, DATEADD(wk, DATEDIFF(d, 0, tD.[EventDay]) / 7, 0)) AS [EventDaySunday]
				FROM (
					SELECT DISTINCT
						hISSF.[EventDay] AS [EventDay]
					FROM
						[${hist}$].[dbo].[h_InstanceSystemStatisticalFunctions] hISSF
						INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
							    hSI.[id]                  = hISSF.[h_ServerInstance_id]
							AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
							AND hSI.[d_Login_id]          = @d_Login_id
					WHERE
						hISSF.[EventDay] BETWEEN CONVERT([DATE], @EventDayMin, 112) AND CONVERT([DATE], @EventDayMax, 112)
				) tD
			) tW
			ORDER BY
				DATEPART(WEEK, tW.[EventDayMonday]) DESC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@EventDayMin" type="NVarChar" />
			<sql-select-parameter name="@EventDayMax" type="NVarChar" />
		</sql-select-parameters>
	</sql-select>

	<sql-select name="GetInstanceSystemStatisticalFunctionsDays">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			SELECT
				 CONVERT([NVARCHAR](128), DATEPART(DAY, tD.[EventDay])) AS [NodeUName]
				,CONVERT([NVARCHAR](128), DATEPART(DAY, tD.[EventDay])) AS [NodeUId]
				,1                                                      AS [NodeEnabled]
				,N'NodeIcon'                                            AS [NodeUIcon]
				,CASE
					WHEN (DATENAME(weekday, tD.[EventDay]) = N'Sunday') THEN
						N'#CC0000'
					ELSE
						N'#080808'
				END                                                     AS [NodeFontColor]
				,N'normal'                                              AS [NodeFontStyle]
				,CONVERT([NVARCHAR](128), tD.[EventDay], 112)           AS [EventDayMin]
				,CONVERT([NVARCHAR](128), tD.[EventDay], 112)           AS [EventDayMax]
			FROM (
				SELECT DISTINCT
					hISSF.[EventDay] AS [EventDay]
				FROM
					[${hist}$].[dbo].[h_InstanceSystemStatisticalFunctions] hISSF
					INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
						    hSI.[id]                  = hISSF.[h_ServerInstance_id]
						AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
						AND hSI.[d_Login_id]          = @d_Login_id
				WHERE
					hISSF.[EventDay] BETWEEN CONVERT([DATE], @EventDayMin, 112) AND CONVERT([DATE], @EventDayMax, 112)
			) tD
			ORDER BY
				tD.[EventDay] DESC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@EventDayMin" type="NVarChar" />
			<sql-select-parameter name="@EventDayMax" type="NVarChar" />
		</sql-select-parameters>
	</sql-select>

	<sql-select name="GetInstanceSystemStatisticalFunctions">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			DECLARE
				 @EventTimeMin [DATETIME]
				,@EventTimeMax [DATETIME]
			;

			SET @EventTimeMin = GETDATE();
			SET @EventTimeMax = GETDATE();

			IF (@EventDayMin IS NOT NULL)
			BEGIN
				SET @EventTimeMin = CONVERT([DATETIME], @EventDayMin, 112);
			END
			IF (@EventDayMax IS NOT NULL)
			BEGIN
				SET @EventTimeMax = DATEADD(dd, 1, CONVERT([DATETIME], @EventDayMax, 112));
			END
			IF (@strDateTimeModifier IS NOT NULL)
			BEGIN
				SELECT
					@EventTimeMax = MAX(hISSF.[EventTime])
				FROM
					[${hist}$].[dbo].[h_InstanceSystemStatisticalFunctions] hISSF
					INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
						    hSI.[id]                  = hISSF.[h_ServerInstance_id]
						AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
						AND hSI.[d_Login_id]          = @d_Login_id;

				IF (@strDateTimeModifier = N'-1 day')
				BEGIN
					SET @EventTimeMin = DATEADD(dd, -1, @EventTimeMax);
				END
				ELSE
				BEGIN
					SET @EventTimeMin = @EventTimeMax;
				END
			END

			SELECT
				 hISSF.[EventTime]    AS [EventTime]
				,CASE
					WHEN ((hISSF.[Connections] &gt; hISSF.[ConnectionsLast]) AND (DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]) &gt; 0)) THEN
						CONVERT([FLOAT], hISSF.[Connections] - hISSF.[ConnectionsLast]) / CONVERT([FLOAT], DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]))
					ELSE
						0.0
				END                   AS [Connections]
				,CASE
					WHEN ((hISSF.[CPUBusy] &gt; hISSF.[CPUBusyLast]) AND (DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]) &gt; 0)) THEN
						CONVERT([FLOAT], hISSF.[CPUBusy] - hISSF.[CPUBusyLast]) / CONVERT([FLOAT], DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]))
					ELSE
						0.0
				END                   AS [CPUBusy]
				,CASE
					WHEN ((hISSF.[CPUIdle] &gt; hISSF.[CPUIdleLast]) AND (DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]) &gt; 0)) THEN
						CONVERT([FLOAT], hISSF.[CPUIdle] - hISSF.[CPUIdleLast]) / CONVERT([FLOAT], DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]))
					ELSE
						0.0
				END                   AS [CPUIdle]
				,CASE
					WHEN ((hISSF.[IOBusy] &gt; hISSF.[IOBusyLast]) AND (DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]) &gt; 0)) THEN
						CONVERT([FLOAT], hISSF.[IOBusy] - hISSF.[IOBusyLast]) / CONVERT([FLOAT], DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]))
					ELSE
						0.0
				END                   AS [IOBusy]
				,CASE
					WHEN ((hISSF.[PackReceived] &gt; hISSF.[PackReceivedLast]) AND (DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]) &gt; 0)) THEN
						CONVERT([FLOAT], hISSF.[PackReceived] - hISSF.[PackReceivedLast]) / CONVERT([FLOAT], DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]))
					ELSE
						0.0
				END                   AS [PackReceived]
				,CASE
					WHEN ((hISSF.[PackSent] &gt; hISSF.[PackSentLast]) AND (DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]) &gt; 0)) THEN
						CONVERT([FLOAT], hISSF.[PackSent] - hISSF.[PackSentLast]) / CONVERT([FLOAT], DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]))
					ELSE
						0.0
				END                   AS [PackSent]
				,CASE
					WHEN ((hISSF.[PacketErrors] &gt; hISSF.[PacketErrorsLast]) AND (DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]) &gt; 0)) THEN
						CONVERT([FLOAT], hISSF.[PacketErrors] - hISSF.[PacketErrorsLast]) / CONVERT([FLOAT], DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]))
					ELSE
						0.0
				END                   AS [PacketErrors]
				,CASE
					WHEN ((hISSF.[TimeTicks] &gt; hISSF.[TimeTicksLast]) AND (DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]) &gt; 0)) THEN
						CONVERT([FLOAT], hISSF.[TimeTicks] - hISSF.[TimeTicksLast]) / CONVERT([FLOAT], DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]))
					ELSE
						0.0
				END                   AS [TimeTicks]
				,CASE
					WHEN ((hISSF.[TotalErrors] &gt; hISSF.[TotalErrorsLast]) AND (DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]) &gt; 0)) THEN
						CONVERT([FLOAT], hISSF.[TotalErrors] - hISSF.[TotalErrorsLast]) / CONVERT([FLOAT], DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]))
					ELSE
						0.0
				END                   AS [TotalErrors]
				,CASE
					WHEN ((hISSF.[TotalRead] &gt; hISSF.[TotalReadLast]) AND (DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]) &gt; 0)) THEN
						CONVERT([FLOAT], hISSF.[TotalRead] - hISSF.[TotalReadLast]) / CONVERT([FLOAT], DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]))
					ELSE
						0.0
				END                   AS [TotalRead]
				,CASE
					WHEN ((hISSF.[TotalWrite] &gt; hISSF.[TotalWriteLast]) AND (DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]) &gt; 0)) THEN
						CONVERT([FLOAT], hISSF.[TotalWrite] - hISSF.[TotalWriteLast]) / CONVERT([FLOAT], DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]))
					ELSE
						0.0
				END                   AS [TotalWrite]
			FROM
				[${hist}$].[dbo].[h_InstanceSystemStatisticalFunctions] hISSF
				INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
					hSI.[id] = hISSF.[h_ServerInstance_id]
					AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
					AND hSI.[d_Login_id] = @d_Login_id
			WHERE
				DATEDIFF(ss, hISSF.[EventTimeLast], hISSF.[EventTime]) &gt; 0
				AND hISSF.[EventDay] BETWEEN CONVERT([DATE], @EventTimeMin) AND CONVERT([DATE], @EventTimeMax)
				AND hISSF.[EventTime] BETWEEN @EventTimeMin AND @EventTimeMax
			ORDER BY
				hISSF.[EventTime] ASC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@EventDayMin"         type="NVarChar" />
			<sql-select-parameter name="@EventDayMax"         type="NVarChar" />
			<sql-select-parameter name="@strDateTimeModifier" type="NVarChar" />
		</sql-select-parameters>
	</sql-select>
</sql-select-content>
