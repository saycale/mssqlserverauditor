<?xml version="1.0" encoding="UTF-8"?>
<sql-select-content xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<sql-select name="GetInstanceDatabaseBackupStatusMonth">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			SELECT
				 LEFT(CONVERT([NVARCHAR](128), tM.[EventDayStartOfMonth], 112), 6)                                            AS [NodeUName]
				,LEFT(CONVERT([NVARCHAR](128), tM.[EventDayStartOfMonth], 112), 6)                                            AS [NodeUId]
				,1                                                                                                            AS [NodeEnabled]
				,N'NodeIcon'                                                                                                  AS [NodeUIcon]
				,N'#080808'                                                                                                   AS [NodeFontColor]
				,N'normal'                                                                                                    AS [NodeFontStyle]
				,CONVERT([NVARCHAR](128), tM.[EventDayStartOfMonth], 112)                                                     AS [EventDayMin]
				,CONVERT([NVARCHAR](128), DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, tM.[EventDayStartOfMonth]) + 1, 0)), 112) AS [EventDayMax]
			FROM (
				SELECT DISTINCT
					DATEADD(month, DATEDIFF(month, 0, hIDB.[EventDay]), 0) AS [EventDayStartOfMonth]
				FROM
					[${hist}$].[dbo].[h_InstanceDatabaseBackup] hIDB
					INNER JOIN [${hist}$].[dbo].[h_InstanceDatabaseStatus] hIDS ON
						hIDS.[h_ServerInstance_id] = hIDB.[h_ServerInstance_id]
						AND hIDS.[h_ref_InstanceDatabase_id] = hIDB.[h_ref_InstanceDatabase_id]
						AND hIDS.[EventDay] = hIDB.[EventDay]
					INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
						    hSI.[id]                  = hIDB.[h_ServerInstance_id]
						AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
						AND hSI.[d_Login_id]          = @d_Login_id
				WHERE
					hIDB.[EventDay] &gt; (
						SELECT
							DATEADD(mm, -12, MAX(hIDB.[EventDay]))
						FROM
							[${hist}$].[dbo].[h_InstanceDatabaseBackup] hIDB
							INNER JOIN [${hist}$].[dbo].[h_InstanceDatabaseStatus] hIDS ON
								hIDS.[h_ServerInstance_id] = hIDB.[h_ServerInstance_id]
								AND hIDS.[h_ref_InstanceDatabase_id] = hIDB.[h_ref_InstanceDatabase_id]
								AND hIDS.[EventDay] = hIDB.[EventDay]
							INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
								    hSI.[id]                  = hIDB.[h_ServerInstance_id]
								AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
								AND hSI.[d_Login_id]          = @d_Login_id
					)
				) tM
			ORDER BY
				tM.[EventDayStartOfMonth] DESC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@strDateTimeModifier" type="NVarChar" />
		</sql-select-parameters>
	</sql-select>

	<sql-select name="GetInstanceDatabaseBackupStatusWeeks">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			SELECT
				 CONVERT([NVARCHAR](128), DATEPART(WEEK, tW.[EventDayMonday])) AS [NodeUName]
				,CONVERT([NVARCHAR](128), DATEPART(WEEK, tW.[EventDayMonday])) AS [NodeUId]
				,1                                                             AS [NodeEnabled]
				,N'NodeIcon'                                                   AS [NodeUIcon]
				,N'#080808'                                                    AS [NodeFontColor]
				,N'normal'                                                     AS [NodeFontStyle]
				,CASE
					WHEN (tW.[EventDayMonday] &lt; CONVERT([DATE], @EventDayMin, 112)) THEN
						CONVERT([NVARCHAR](128), @EventDayMin, 112)
					ELSE
						CONVERT([NVARCHAR](128), tW.[EventDayMonday], 112)
				END                                                            AS [EventDayMin]
				,CASE
					WHEN (tW.[EventDaySunday] &gt; CONVERT([DATE], @EventDayMax, 112)) THEN
						CONVERT([NVARCHAR](128), @EventDayMax, 112)
					ELSE
						CONVERT([NVARCHAR](128), tW.[EventDaySunday], 112)
				END                                                            AS [EventDayMax]
			FROM (
				SELECT DISTINCT
					 DATEADD(wk, DATEDIFF(d, 0, tD.[EventDay]) / 7, 0)                  AS [EventDayMonday]
					,DATEADD(day, 6, DATEADD(wk, DATEDIFF(d, 0, tD.[EventDay]) / 7, 0)) AS [EventDaySunday]
				FROM (
					SELECT DISTINCT
						hIDB.[EventDay] AS [EventDay]
					FROM
						[${hist}$].[dbo].[h_InstanceDatabaseBackup] hIDB
						INNER JOIN [${hist}$].[dbo].[h_InstanceDatabaseStatus] hIDS ON
							hIDS.[h_ServerInstance_id] = hIDB.[h_ServerInstance_id]
							AND hIDS.[h_ref_InstanceDatabase_id] = hIDB.[h_ref_InstanceDatabase_id]
							AND hIDS.[EventDay] = hIDB.[EventDay]
						INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
							    hSI.[id]                  = hIDB.[h_ServerInstance_id]
							AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
							AND hSI.[d_Login_id]          = @d_Login_id
					WHERE
						hIDB.[EventDay] BETWEEN CONVERT([DATE], @EventDayMin, 112) AND CONVERT([DATE], @EventDayMax, 112)
				) tD
			) tW
			ORDER BY
				DATEPART(WEEK, tW.[EventDayMonday]) DESC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@EventDayMin" type="NVarChar" />
			<sql-select-parameter name="@EventDayMax" type="NVarChar" />
		</sql-select-parameters>
	</sql-select>

	<sql-select name="GetInstanceDatabaseBackupStatusDays">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			SELECT
				 CONVERT([NVARCHAR](128), DATEPART(DAY, tD.[EventDay])) AS [NodeUName]
				,CONVERT([NVARCHAR](128), DATEPART(DAY, tD.[EventDay])) AS [NodeUId]
				,1                                                      AS [NodeEnabled]
				,N'NodeIcon'                                            AS [NodeUIcon]
				,CASE
					WHEN (DATENAME(weekday, tD.[EventDay]) = N'Sunday') THEN
						N'#CC0000'
					ELSE
						N'#080808'
				END                                                     AS [NodeFontColor]
				,N'normal'                                              AS [NodeFontStyle]
				,CONVERT([NVARCHAR](128), tD.[EventDay], 112)           AS [EventDayMin]
				,CONVERT([NVARCHAR](128), tD.[EventDay], 112)           AS [EventDayMax]
			FROM (
				SELECT DISTINCT
					hIDB.[EventDay] AS [EventDay]
				FROM
					[${hist}$].[dbo].[h_InstanceDatabaseBackup] hIDB
					INNER JOIN [${hist}$].[dbo].[h_InstanceDatabaseStatus] hIDS ON
						hIDS.[h_ServerInstance_id] = hIDB.[h_ServerInstance_id]
						AND hIDS.[h_ref_InstanceDatabase_id] = hIDB.[h_ref_InstanceDatabase_id]
						AND hIDS.[EventDay] = hIDB.[EventDay]
					INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
						    hSI.[id]                  = hIDB.[h_ServerInstance_id]
						AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
						AND hSI.[d_Login_id]          = @d_Login_id
				WHERE
					hIDB.[EventDay] BETWEEN CONVERT([DATE], @EventDayMin, 112) AND CONVERT([DATE], @EventDayMax, 112)
			) tD
			ORDER BY
				tD.[EventDay] DESC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@EventDayMin" type="NVarChar" />
			<sql-select-parameter name="@EventDayMax" type="NVarChar" />
		</sql-select-parameters>
	</sql-select>

	<sql-select name="GetInstanceDatabaseBackupStatus">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			DECLARE
				 @EventTimeMin [DATETIME]
				,@EventTimeMax [DATETIME]
			;

			SET @EventTimeMin = GETDATE();
			SET @EventTimeMax = GETDATE();

			IF (@EventDayMin IS NOT NULL)
			BEGIN
				SET @EventTimeMin = CONVERT([DATETIME], @EventDayMin, 112);
			END
			IF (@EventDayMax IS NOT NULL)
			BEGIN
				SET @EventTimeMax = DATEADD(dd, 1, CONVERT([DATETIME], @EventDayMax, 112));
			END
			IF (@strDateTimeModifier IS NOT NULL)
			BEGIN
				SELECT
					@EventTimeMax = MAX(hIDB.[EventTime])
				FROM
					[${hist}$].[dbo].[h_InstanceDatabaseBackup] hIDB
					INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
						    hSI.[id]                  = hIDB.[h_ServerInstance_id]
						AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
						AND hSI.[d_Login_id]          = @d_Login_id;

				IF (@strDateTimeModifier = N'-1 day')
				BEGIN
					SET @EventTimeMin = DATEADD(dd, -1, @EventTimeMax);
				END
				ELSE
				BEGIN
					SET @EventTimeMin = @EventTimeMax;
				END
			END

			SELECT
				 hSI.[ConnectionName]                  AS [ConnectionName]
				,hRID_Last.[DatabaseId]                AS [DatabaseId]
				,hRID_Last.[DatabaseName]              AS [DatabaseName]
				,hRIDS.[DatabaseStatus]                AS [DatabaseStatus]
				,hRIDU.[DatabaseUpdateability]         AS [DatabaseUpdateability]
				,hRIDR.[DatabaseRecovery]              AS [DatabaseRecovery]
				,hIDB_S.[DatabaseBackupFinishDateTime] AS [DatabaseBackupFinishDateTime_S]
				,hIDB_D.[DatabaseBackupFinishDateTime] AS [DatabaseBackupFinishDateTime_D]
				,hIDB_I.[DatabaseBackupFinishDateTime] AS [DatabaseBackupFinishDateTime_I]
				,hIDB_L.[DatabaseBackupFinishDateTime] AS [DatabaseBackupFinishDateTime_L]
			FROM
				[${ref}$].[dbo].[h_ServerInstance] hSI
				INNER JOIN (
					SELECT
						 hRID.[h_ServerInstance_id]       AS [h_ServerInstance_id]
						,hRID.[h_ref_InstanceDatabase_id] AS [h_ref_InstanceDatabase_id]
						,hRID.[DatabaseId]                AS [DatabaseId]
						,hRID.[DatabaseName]              AS [DatabaseName]
					FROM
						[${ref}$].[dbo].[h_ref_InstanceDatabase] hRID
						INNER JOIN (
							SELECT
								 hRID.[h_ServerInstance_id] AS [h_ServerInstance_id]
								,hRID.[DatabaseName]        AS [DatabaseName]
								,MAX(hRID.[EventTime])      AS [EventTime]
							FROM
								[${ref}$].[dbo].[h_ref_InstanceDatabase] hRID
							WHERE
								hRID.[EventTime] &lt; @EventTimeMax
							GROUP BY
								 hRID.[h_ServerInstance_id]
								,hRID.[DatabaseName]
						) hRID_Last ON
							    hRID_Last.[h_ServerInstance_id] = hRID.[h_ServerInstance_id]
							AND hRID_Last.[DatabaseName]        = hRID.[DatabaseName]
							AND hRID_Last.[EventTime]           = hRID.[EventTime]
				) hRID_Last ON
					hRID_Last.[h_ServerInstance_id] = hSI.[id]

				INNER JOIN (
					SELECT
						 hIDS.[h_ServerInstance_id]                    AS [h_ServerInstance_id]
						,hIDS.[h_ref_InstanceDatabase_id]              AS [h_ref_InstanceDatabase_id]
						,hIDS.[h_ref_InstanceSourceDatabase_id]        AS [h_ref_InstanceSourceDatabase_id]
						,hIDS.[h_ref_InstanceDatabaseStatus_id]        AS [h_ref_InstanceDatabaseStatus_id]
						,hIDS.[h_ref_InstanceDatabaseUpdateability_id] AS [h_ref_InstanceDatabaseUpdateability_id]
						,hIDS.[h_ref_InstanceDatabaseRecovery_id]      AS [h_ref_InstanceDatabaseRecovery_id]
					FROM
						[${hist}$].[dbo].[h_InstanceDatabaseStatus] hIDS
						INNER JOIN (
							SELECT
								 hIDS.[h_ServerInstance_id]       AS [h_ServerInstance_id]
								,hIDS.[h_ref_InstanceDatabase_id] AS [h_ref_InstanceDatabase_id]
								,MAX(hIDS.[EventTime])            AS [EventTime]
							FROM
								[${hist}$].[dbo].[h_InstanceDatabaseStatus] hIDS
							WHERE
								hIDS.[EventTime] &lt; @EventTimeMax
							GROUP BY
								 hIDS.[h_ServerInstance_id]
								,hIDS.[h_ref_InstanceDatabase_id]
						) hIDS_Last ON
							    hIDS_Last.[h_ServerInstance_id]       = hIDS.[h_ServerInstance_id]
							AND hIDS_Last.[h_ref_InstanceDatabase_id] = hIDS.[h_ref_InstanceDatabase_id]
							AND hIDS_Last.[EventTime]                 = hIDS.[EventTime]
				) hIDS_Last ON
					    hIDS_Last.[h_ServerInstance_id]       = hSI.[id]
					AND hIDS_Last.[h_ref_InstanceDatabase_id] = hRID_Last.[h_ref_InstanceDatabase_id]

				INNER JOIN [${ref}$].[dbo].[h_ref_InstanceDatabaseStatus] hRIDS ON
					    hRIDS.[h_ServerInstance_id]             = hSI.[id]
					AND hRIDS.[h_ref_InstanceDatabaseStatus_id] = hIDS_Last.[h_ref_InstanceDatabaseStatus_id]

				INNER JOIN [${ref}$].[dbo].[h_ref_InstanceDatabaseUpdateability] hRIDU ON
					    hRIDU.[h_ServerInstance_id]                    = hSI.[id]
					AND hRIDU.[h_ref_InstanceDatabaseUpdateability_id] = hIDS_Last.[h_ref_InstanceDatabaseUpdateability_id]

				INNER JOIN [${ref}$].[dbo].[h_ref_InstanceDatabaseRecovery] hRIDR ON
					    hRIDR.[h_ServerInstance_id]               = hSI.[id]
					AND hRIDR.[h_ref_InstanceDatabaseRecovery_id] = hIDS_Last.[h_ref_InstanceDatabaseRecovery_id]

				LEFT OUTER JOIN (
					SELECT
						 hIDB.[h_ServerInstance_id]               AS [h_ServerInstance_id]
						,hIDB.[h_ref_InstanceDatabase_id]         AS [h_ref_InstanceDatabase_id]
						,MAX(hIDB.[DatabaseBackupFinishDateTime]) AS [DatabaseBackupFinishDateTime]
					FROM
						[${hist}$].[dbo].[h_InstanceDatabaseBackup] hIDB
					WHERE
						hIDB.[DatabaseBackupFinishDateTime] &lt; @EventTimeMax
						AND hIDB.[DatabaseBackupIsSnapshot] = 1
					GROUP BY
						 hIDB.[h_ServerInstance_id]
						,hIDB.[h_ref_InstanceDatabase_id]
				) hIDB_S ON
					    hIDB_S.[h_ServerInstance_id]       = hSI.[id]
					AND hIDB_S.[h_ref_InstanceDatabase_id] = hRID_Last.[h_ref_InstanceDatabase_id]

				LEFT OUTER JOIN (
					SELECT
						 hIDB.[h_ServerInstance_id]               AS [h_ServerInstance_id]
						,hIDB.[h_ref_InstanceDatabase_id]         AS [h_ref_InstanceDatabase_id]
						,MAX(hIDB.[DatabaseBackupFinishDateTime]) AS [DatabaseBackupFinishDateTime]
					FROM
						[${hist}$].[dbo].[h_InstanceDatabaseBackup] hIDB
					WHERE
						hIDB.[DatabaseBackupFinishDateTime] &lt; @EventTimeMax
						AND hIDB.[DatabaseBackupIsSnapshot] = 0
						AND hIDB.[DatabaseBackupType]       = N'D'
					GROUP BY
						 hIDB.[h_ServerInstance_id]
						,hIDB.[h_ref_InstanceDatabase_id]
				) hIDB_D ON
					    hIDB_D.[h_ServerInstance_id]       = hSI.[id]
					AND hIDB_D.[h_ref_InstanceDatabase_id] = hRID_Last.[h_ref_InstanceDatabase_id]

				LEFT OUTER JOIN (
					SELECT
						 hIDB.[h_ServerInstance_id]               AS [h_ServerInstance_id]
						,hIDB.[h_ref_InstanceDatabase_id]         AS [h_ref_InstanceDatabase_id]
						,MAX(hIDB.[DatabaseBackupFinishDateTime]) AS [DatabaseBackupFinishDateTime]
					FROM
						[${hist}$].[dbo].[h_InstanceDatabaseBackup] hIDB
					WHERE
						hIDB.[DatabaseBackupFinishDateTime] &lt; @EventTimeMax
						AND hIDB.[DatabaseBackupIsSnapshot] = 0
						AND hIDB.[DatabaseBackupType]       = N'I'
					GROUP BY
						 hIDB.[h_ServerInstance_id]
						,hIDB.[h_ref_InstanceDatabase_id]
				) hIDB_I ON
					    hIDB_I.[h_ServerInstance_id]       = hSI.[id]
					AND hIDB_I.[h_ref_InstanceDatabase_id] = hRID_Last.[h_ref_InstanceDatabase_id]

				LEFT OUTER JOIN (
					SELECT
						 hIDB.[h_ServerInstance_id]               AS [h_ServerInstance_id]
						,hIDB.[h_ref_InstanceDatabase_id]         AS [h_ref_InstanceDatabase_id]
						,MAX(hIDB.[DatabaseBackupFinishDateTime]) AS [DatabaseBackupFinishDateTime]
					FROM
						[${hist}$].[dbo].[h_InstanceDatabaseBackup] hIDB
					WHERE
						hIDB.[DatabaseBackupFinishDateTime] &lt; @EventTimeMax
						AND hIDB.[DatabaseBackupIsSnapshot] = 0
						AND hIDB.[DatabaseBackupType]       = N'L'
					GROUP BY
						 hIDB.[h_ServerInstance_id]
						,hIDB.[h_ref_InstanceDatabase_id]
				) hIDB_L ON
					    hIDB_L.[h_ServerInstance_id]       = hSI.[id]
					AND hIDB_L.[h_ref_InstanceDatabase_id] = hRID_Last.[h_ref_InstanceDatabase_id]
			WHERE
				hSI.[d_ServerInstance_id] = @d_ServerInstance_id
				AND hSI.[d_Login_id] = @d_Login_id
				AND hRID_Last.[DatabaseName] IS NOT NULL
				AND LEN(LTRIM(RTRIM(hRID_Last.[DatabaseName]))) &gt; 0
				AND hRID_Last.[DatabaseName] NOT IN (
					N'tempdb'
				)
				AND hRID_Last.[DatabaseId] NOT IN (
					32767 -- ResourceDB
				)
				AND hIDS_Last.[h_ref_InstanceSourceDatabase_id] IS NULL
				AND hRIDS.[DatabaseStatus] NOT IN (
					N'OFFLINE'
				)
			ORDER BY
				 hSI.[ConnectionName] ASC
				,hRID_Last.[DatabaseName] ASC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@EventDayMin"         type="NVarChar" />
			<sql-select-parameter name="@EventDayMax"         type="NVarChar" />
			<sql-select-parameter name="@strDateTimeModifier" type="NVarChar" />
		</sql-select-parameters>
	</sql-select>
</sql-select-content>
