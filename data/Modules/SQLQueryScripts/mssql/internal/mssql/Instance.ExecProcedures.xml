<?xml version="1.0" encoding="UTF-8"?>
<sql-select-content xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<sql-select name="GetInstanceProceduresExecutionMonth">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			SELECT
				 LEFT(CONVERT([NVARCHAR](128), tM.[EventDayStartOfMonth], 112), 6)                                            AS [NodeUName]
				,LEFT(CONVERT([NVARCHAR](128), tM.[EventDayStartOfMonth], 112), 6)                                            AS [NodeUId]
				,1                                                                                                            AS [NodeEnabled]
				,N'NodeIcon'                                                                                                  AS [NodeUIcon]
				,N'#080808'                                                                                                   AS [NodeFontColor]
				,N'normal'                                                                                                    AS [NodeFontStyle]
				,CONVERT([NVARCHAR](128), tM.[EventDayStartOfMonth], 112)                                                     AS [EventDayMin]
				,CONVERT([NVARCHAR](128), DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, tM.[EventDayStartOfMonth]) + 1, 0)), 112) AS [EventDayMax]
			FROM (
				SELECT DISTINCT
					DATEADD(month, DATEDIFF(month, 0, hIEP.[EventDay]), 0) AS [EventDayStartOfMonth]
				FROM
					[${hist}$].[dbo].[h_InstanceExecProcedure] hIEP
					INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
						    hSI.[id]                  = hIEP.[h_ServerInstance_id]
						AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
						AND hSI.[d_Login_id]          = @d_Login_id
				WHERE
					hIEP.[EventDay] &gt; (
						SELECT
							DATEADD(mm, -12, MAX(hIEP.[EventDay]))
						FROM
							[${hist}$].[dbo].[h_InstanceExecProcedure] hIEP
							INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
								    hSI.[id]                  = hIEP.[h_ServerInstance_id]
								AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
								AND hSI.[d_Login_id]          = @d_Login_id
					)
				) tM
			ORDER BY
				tM.[EventDayStartOfMonth] DESC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@strDateTimeModifier" type="NVarChar" />
		</sql-select-parameters>
	</sql-select>

	<sql-select name="GetInstanceProceduresExecutionWeeks">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			SELECT
				 CONVERT([NVARCHAR](128), DATEPART(WEEK, tW.[EventDayMonday])) AS [NodeUName]
				,CONVERT([NVARCHAR](128), DATEPART(WEEK, tW.[EventDayMonday])) AS [NodeUId]
				,1                                                             AS [NodeEnabled]
				,N'NodeIcon'                                                   AS [NodeUIcon]
				,N'#080808'                                                    AS [NodeFontColor]
				,N'normal'                                                     AS [NodeFontStyle]
				,CASE
					WHEN (tW.[EventDayMonday] &lt; CONVERT([DATE], @EventDayMin, 112)) THEN
						CONVERT([NVARCHAR](128), @EventDayMin, 112)
					ELSE
						CONVERT([NVARCHAR](128), tW.[EventDayMonday], 112)
				END                                                            AS [EventDayMin]
				,CASE
					WHEN (tW.[EventDaySunday] &gt; CONVERT([DATE], @EventDayMax, 112)) THEN
						CONVERT([NVARCHAR](128), @EventDayMax, 112)
					ELSE
						CONVERT([NVARCHAR](128), tW.[EventDaySunday], 112)
				END                                                            AS [EventDayMax]
			FROM (
				SELECT DISTINCT
					 DATEADD(wk, DATEDIFF(d, 0, tD.[EventDay]) / 7, 0)                  AS [EventDayMonday]
					,DATEADD(day, 6, DATEADD(wk, DATEDIFF(d, 0, tD.[EventDay]) / 7, 0)) AS [EventDaySunday]
				FROM (
					SELECT DISTINCT
						hIEP.[EventDay] AS [EventDay]
					FROM
						[${hist}$].[dbo].[h_InstanceExecProcedure] hIEP
						INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
							    hSI.[id]                  = hIEP.[h_ServerInstance_id]
							AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
							AND hSI.[d_Login_id]          = @d_Login_id
					WHERE
						hIEP.[EventDay] BETWEEN CONVERT([DATE], @EventDayMin, 112) AND CONVERT([DATE], @EventDayMax, 112)
				) tD
			) tW
			ORDER BY
				DATEPART(WEEK, tW.[EventDayMonday]) DESC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@EventDayMin" type="NVarChar" />
			<sql-select-parameter name="@EventDayMax" type="NVarChar" />
		</sql-select-parameters>
	</sql-select>

	<sql-select name="GetInstanceProceduresExecutionDays">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			SELECT
				 CONVERT([NVARCHAR](128), DATEPART(DAY, tD.[EventDay])) AS [NodeUName]
				,CONVERT([NVARCHAR](128), DATEPART(DAY, tD.[EventDay])) AS [NodeUId]
				,1                                                      AS [NodeEnabled]
				,N'NodeIcon'                                            AS [NodeUIcon]
				,CASE
					WHEN (DATENAME(weekday, tD.[EventDay]) = N'Sunday') THEN
						N'#CC0000'
					ELSE
						N'#080808'
				END                                                     AS [NodeFontColor]
				,N'normal'                                              AS [NodeFontStyle]
				,CONVERT([NVARCHAR](128), tD.[EventDay], 112)           AS [EventDayMin]
				,CONVERT([NVARCHAR](128), tD.[EventDay], 112)           AS [EventDayMax]
			FROM (
				SELECT DISTINCT
					hIEP.[EventDay] AS [EventDay]
				FROM
					[${hist}$].[dbo].[h_InstanceExecProcedure] hIEP
					INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
						    hSI.[id]                  = hIEP.[h_ServerInstance_id]
						AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
						AND hSI.[d_Login_id]          = @d_Login_id
				WHERE
					hIEP.[EventDay] BETWEEN CONVERT([DATE], @EventDayMin, 112) AND CONVERT([DATE], @EventDayMax, 112)
			) tD
			ORDER BY
				tD.[EventDay] DESC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@EventDayMin" type="NVarChar" />
			<sql-select-parameter name="@EventDayMax" type="NVarChar" />
		</sql-select-parameters>
	</sql-select>

	<sql-select name="GetInstanceProceduresExecution">
		<sql-select-text MinSupportedVersion="*" MaxSupportedVersion="*" signature="">
			SET NOCOUNT ON;
			SET DEADLOCK_PRIORITY LOW;

			DECLARE
				 @EventTimeMin [DATETIME]
				,@EventTimeMax [DATETIME]
			;

			SET @EventTimeMin = GETDATE();
			SET @EventTimeMax = GETDATE();

			IF (@EventDayMin IS NOT NULL)
			BEGIN
				SET @EventTimeMin = CONVERT([DATETIME], @EventDayMin, 112);
			END
			IF (@EventDayMax IS NOT NULL)
			BEGIN
				SET @EventTimeMax = DATEADD(dd, 1, CONVERT([DATETIME], @EventDayMax, 112));
			END
			IF (@strDateTimeModifier IS NOT NULL)
			BEGIN
				SELECT
					@EventTimeMax = MAX(hIEP.[EventTime])
				FROM
					[${hist}$].[dbo].[h_InstanceExecProcedure] hIEP
					INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
						    hSI.[id]                  = hIEP.[h_ServerInstance_id]
						AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
						AND hSI.[d_Login_id]          = @d_Login_id;

				IF (@strDateTimeModifier = N'-1 day')
				BEGIN
					SET @EventTimeMin = DATEADD(dd, -1, @EventTimeMax);
				END
				ELSE
				BEGIN
					SET @EventTimeMin = @EventTimeMax;
				END
			END

			SELECT
				CASE
					WHEN ((MAX(hIEPBase.[execution_count]) IS NOT NULL) AND (MAX(hIEPBase.[execution_count]) &lt;= MAX(hIEP.[execution_count]))) THEN
						MAX(hIEP.[execution_count]) - MAX(hIEPBase.[execution_count])
					ELSE
						MAX(hIEP.[execution_count])
				END                               AS [execution_count]
				,CASE
					WHEN ((MAX(hIEPBase.[total_worker_time]) IS NOT NULL) AND (MAX(hIEPBase.[total_worker_time]) &lt;= MAX(hIEP.[total_worker_time]))) THEN
						MAX(hIEP.[total_worker_time]) - MAX(hIEPBase.[total_worker_time])
					ELSE
						MAX(hIEP.[total_worker_time])
				END                               AS [total_worker_time]
				,CASE
					WHEN ((MAX(hIEPBase.[total_elapsed_time]) IS NOT NULL) AND (MAX(hIEPBase.[total_elapsed_time]) &lt;= MAX(hIEP.[total_elapsed_time]))) THEN
						MAX(hIEP.[total_elapsed_time]) - MAX(hIEPBase.[total_elapsed_time])
					ELSE
						MAX(hIEP.[total_elapsed_time])
				END                               AS [total_elapsed_time]
				,CASE
					WHEN ((MAX(hIEPBase.[total_logical_reads]) IS NOT NULL) AND (MAX(hIEPBase.[total_logical_reads]) &lt;= MAX(hIEP.[total_logical_reads]))) THEN
						MAX(hIEP.[total_logical_reads]) - MAX(hIEPBase.[total_logical_reads])
					ELSE
						MAX(hIEP.[total_logical_reads])
				END                               AS [total_logical_reads]
				,CASE
					WHEN ((MAX(hIEPBase.[total_logical_writes]) IS NOT NULL) AND (MAX(hIEPBase.[total_logical_writes]) &lt;= MAX(hIEP.[total_logical_writes]))) THEN
						MAX(hIEP.[total_logical_writes]) - MAX(hIEPBase.[total_logical_writes])
					ELSE
						MAX(hIEP.[total_logical_writes])
				END                               AS [total_logical_writes]
				,hRID.[DatabaseName]              AS [DatabaseName]
				,hRIEPN.[ProcedureType]           AS [ProcedureType]
				,hRIEPN.[ProcedureName]           AS [ProcedureName]
				,MIN(hIEP.[EventTime])            AS [EventTimeMin]
				,MAX(hIEP.[EventTime])            AS [EventTimeMax]
			FROM
				[${hist}$].[dbo].[h_InstanceExecProcedure] hIEP
				LEFT OUTER JOIN [${hist}$].[dbo].[h_InstanceExecProcedure] hIEPBase ON
					hIEPBase.[h_ServerInstance_id] = hIEP.[h_ServerInstance_id]
					AND hIEPBase.[h_ref_InstanceDatabase_id] = hIEP.[h_ref_InstanceDatabase_id]
					AND hIEPBase.[ProcedureId] = hIEP.[ProcedureId]
					AND hIEPBase.[EventDay] = CASE
						WHEN (@EventDayMin IS NOT NULL) THEN
							DATEADD(dd, -1, @EventDayMin)
						ELSE
							CONVERT([DATE], N'19000101', 120)
					END
				LEFT OUTER JOIN [${ref}$].[dbo].[h_ref_InstanceDatabase] hRID ON
					hRID.[h_ServerInstance_id] = hIEP.[h_ServerInstance_id]
					AND hRID.[h_ref_InstanceDatabase_id] = hIEP.[h_ref_InstanceDatabase_id]
				INNER JOIN [${ref}$].[dbo].[h_ref_InstanceExecProcedureName] hRIEPN ON
					hRIEPN.[h_ServerInstance_id] = hIEP.[h_ServerInstance_id]
					AND hRIEPN.[h_ref_InstanceDatabase_id] = hIEP.[h_ref_InstanceDatabase_id]
					AND hRIEPN.[ProcedureId] = hIEP.[ProcedureId]
				INNER JOIN [${ref}$].[dbo].[h_ServerInstance] hSI ON
					hSI.[id] = hIEP.[h_ServerInstance_id]
					AND hSI.[d_ServerInstance_id] = @d_ServerInstance_id
					AND hSI.[d_Login_id] = @d_Login_id
			WHERE
				hIEP.[EventDay] BETWEEN CONVERT([DATE], @EventTimeMin) AND CONVERT([DATE], @EventTimeMax)
				AND hIEP.[EventTime] BETWEEN @EventTimeMin AND @EventTimeMax
			GROUP BY
				 hRID.[DatabaseName]
				,hRIEPN.[ProcedureType]
				,hRIEPN.[ProcedureName]
			HAVING
				CASE
					WHEN ((MAX(hIEPBase.[execution_count]) IS NOT NULL) AND (MAX(hIEPBase.[execution_count]) &lt;= MAX(hIEP.[execution_count]))) THEN
						MAX(hIEP.[execution_count]) - MAX(hIEPBase.[execution_count])
					ELSE
						MAX(hIEP.[execution_count])
				END &gt; 0
			ORDER BY
				1 DESC
			;
		</sql-select-text>
		<sql-select-parameters>
			<sql-select-parameter name="@EventDayMin"         type="NVarChar" />
			<sql-select-parameter name="@EventDayMax"         type="NVarChar" />
			<sql-select-parameter name="@strDateTimeModifier" type="NVarChar" />
		</sql-select-parameters>
	</sql-select>
</sql-select-content>
