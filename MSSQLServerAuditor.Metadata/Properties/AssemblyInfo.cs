﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyConfiguration("Beta")]
[assembly: AssemblyCompany("MSSQLServerAuditor")]
[assembly: AssemblyProduct("MSSQLServerAuditor")]
[assembly: AssemblyCopyright("Copyright © City24 Pty. Ltd., 2013-2017")]
[assembly: AssemblyTrademark("City24 Pty. Ltd.")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion("0.7.0.1")]
[assembly: AssemblyFileVersion("0.7.0.1")]

[assembly: AssemblyTitle("MSSQLServerAuditor.Metadata")]

[assembly: ComVisible(false)]

[assembly: Guid("996d0547-d16a-4bb4-9d95-e3a6bfd95144")]
