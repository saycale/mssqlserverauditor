﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.Win32;

namespace MSSQLServerAuditor.Common
{
	public class FrameworkInfo
	{
		public FrameworkInfo(
			string name,
			string version,
			string install,
			string profile,
			string servicePack
		)
		{
			this.Name        = name;
			this.Version     = version;
			this.Install     = install;
			this.Profile     = profile;
			this.ServicePack = servicePack;
		}

		public string Install     { get; private set; }
		public string Name        { get; private set; }
		public string Version     { get; private set; }
		public string Profile     { get; private set; }
		public string ServicePack { get; private set; }

		public override string ToString()
		{
			string result = $"v.{Version}";

			if (!string.IsNullOrWhiteSpace(Profile))
			{
				result += $" {Profile}";
			}

			if (!string.IsNullOrWhiteSpace(ServicePack))
			{
				result += $" SP {ServicePack}";
			}

			return result;
		}
	}

	public static class RuntimeEnvironment
	{
		/// <summary>
		/// Get the list of installed .NET Framework versions from the registry
		/// </summary>
		/// <returns>List of installed .NET Frameworks</returns>
		public static List<FrameworkInfo> GetInstalledFrameworks()
		{
			List<FrameworkInfo> frameworks = new List<FrameworkInfo>();

			using (RegistryKey lmKey = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, ""))
			{
				using (RegistryKey ndpKey = lmKey.OpenSubKey(@"SOFTWARE\Microsoft\NET Framework Setup\NDP\"))
				{
					if (ndpKey != null)
					{
						foreach (string versionKeyName in ndpKey.GetSubKeyNames())
						{
							if (versionKeyName.StartsWith("v"))
							{
								RegistryKey versionKey = ndpKey.OpenSubKey(versionKeyName);
								if (versionKey != null)
								{
									string version = versionKey.GetValue("Version", string.Empty).ToString();
									string sp      = versionKey.GetValue("SP", string.Empty).ToString();
									string install = versionKey.GetValue("Install", string.Empty).ToString();

									if (!string.IsNullOrWhiteSpace(version))
									{
										FrameworkInfo framework = new FrameworkInfo(
											versionKeyName,
											version,
											install,
											string.Empty,
											sp
										);

										frameworks.Add(framework);
									}
									else
									{
										foreach (string subKeyName in versionKey.GetSubKeyNames())
										{
											RegistryKey subKey = versionKey.OpenSubKey(subKeyName);
											if (subKey != null)
											{
												version = subKey.GetValue("Version", string.Empty).ToString();
												if (!string.IsNullOrWhiteSpace(version))
												{
													sp = subKey.GetValue("SP", string.Empty).ToString();
												}

												install = subKey.GetValue("Install", string.Empty).ToString();

												FrameworkInfo framework = new FrameworkInfo(
													versionKeyName,
													version,
													install,
													subKeyName,
													sp
												);

												frameworks.Add(framework);
											}
										}
									}
								}
							}
						}
					}
				}
			}
			return frameworks;
		}

		/// <summary>
		/// Get the list of installed .NET Framework 4.5+ versions from the registry
		/// </summary>
		/// <returns>List of installed .NET Frameworks 4.5+</returns>
		public static List<FrameworkInfo> GetInstalledFrameworks45Plus()
		{
			List<FrameworkInfo> frameworks45Plus = new List<FrameworkInfo>();
			const string subkey = @"SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\";
			using (RegistryKey ndpKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey(subkey))
			{
				if (ndpKey?.GetValue("Release") != null)
				{
					string version = CheckFor45PlusVersion((int?)ndpKey.GetValue("Release"));

					FrameworkInfo framework = new FrameworkInfo(
						"v" + version,
						version,
						"1",
						string.Empty,
						string.Empty
					);

					frameworks45Plus.Add(framework);
				}
			}

			return frameworks45Plus;
		}

		/// <summary>
		/// Get loaded assemblies in app domain.
		/// </summary>
		/// <returns></returns>
		public static List<AssemblyName> LoadedAssemblies
		{
			get
			{
				List<AssemblyName> assemblies = new List<AssemblyName>();

				foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
				{
					AssemblyName assemblyName = assembly.GetName();

					assemblies.Add(assemblyName);
				}

				return assemblies;
			}
		}

		public static string WinVersion =>
			Environment.OSVersion + (Environment.Is64BitOperatingSystem ? " x64" : string.Empty);

		/// <summary>
		/// Get version ADO.
		/// </summary>
		/// <returns></returns>
		public static string AdoVersion => Registry
			.GetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\DataAccess", "FullInstallVer", string.Empty)
			.ToString();

		/// <summary>
		/// Get version IE.
		/// </summary>
		/// <returns>Return version IE.</returns>
		public static string IeVersion => Registry
			.GetValue("HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Internet Explorer", "Version", string.Empty)
			.ToString();

		// Checking the version using >= will enable forward compatibility.
		private static string CheckFor45PlusVersion(int? releaseKey)
		{
			if (releaseKey >= 460798)
			{
				return "4.7+";
			}
			if (releaseKey >= 394802)
			{
				return "4.6.2";
			}
			if (releaseKey >= 394254)
			{
				return "4.6.1";
			}
			if (releaseKey >= 393295)
			{
				return "4.6";
			}
			if (releaseKey >= 379893)
			{
				return "4.5.2";
			}
			if (releaseKey >= 378675)
			{
				return "4.5.1";
			}
			if (releaseKey >= 378389)
			{
				return "4.5";
			}

			// This code should never execute.
			return string.Empty;
		}
	}
}
