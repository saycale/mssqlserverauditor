﻿using System;

namespace MSSQLServerAuditor.Common
{
	public static class DefaultValues
	{
		public static class Date
		{
			public static readonly DateTime Minimum = new DateTime(1900, 1, 1);
		}
	}
}
