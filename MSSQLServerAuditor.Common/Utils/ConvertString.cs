﻿using System;
using System.ComponentModel;
using System.Data;
using System.Globalization;

namespace MSSQLServerAuditor.Common.Utils
{
	public static class ConvertString
	{
		public static T ToType<T>(string value, T defaultValue = default(T))
		{
			TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));

			try
			{
				return (T) converter.ConvertFromString(value);
			}
			catch (Exception)
			{
				return defaultValue;
			}
		}

		public static object ToDbType(string value, bool isNull, DbType type)
		{
			if (isNull || string.IsNullOrEmpty(value))
			{
				return DBNull.Value;
			}

			switch (type)
			{
				case DbType.AnsiString:
				case DbType.AnsiStringFixedLength:
				case DbType.StringFixedLength:
				case DbType.String:
				case DbType.Xml:
				case DbType.VarNumeric:
					return value;

				case DbType.Boolean:
					return bool.Parse(value);

				case DbType.Byte:
					return byte.Parse(value);

				case DbType.SByte:
					return sbyte.Parse(value);

				case DbType.Decimal:
				case DbType.Currency:
					return decimal.Parse(value);

				case DbType.Date:
				case DbType.DateTime:
				case DbType.DateTime2:
					return DateTime.Parse(value);

				case DbType.DateTimeOffset:
					return DateTimeOffset.Parse(value);

				case DbType.Double:
					return double.Parse(value);

				case DbType.Guid:
					return Guid.Parse(value);

				case DbType.Int16:
					return short.Parse(value);

				case DbType.Int32:
					return int.Parse(value);

				case DbType.Int64:
					return long.Parse(value);

				case DbType.UInt16:
					return ushort.Parse(value);

				case DbType.UInt32:
					return uint.Parse(value);

				case DbType.UInt64:
					return ulong.Parse(value);

				case DbType.Single:
					return float.Parse(value);

				default:
					return value;
			}
		}

		public static object ToSqlDbType(string value, bool isNull, SqlDbType type)
		{
			if (isNull || string.IsNullOrEmpty(value))
			{
				return DBNull.Value;
			}

			switch (type)
			{
				case SqlDbType.Char:
				case SqlDbType.NChar:
				case SqlDbType.NText:
				case SqlDbType.NVarChar:
				case SqlDbType.Xml:
				case SqlDbType.Text:
				case SqlDbType.VarBinary:
				case SqlDbType.VarChar:
					return value;

				case SqlDbType.Bit:
					return bool.Parse(value);

				case SqlDbType.TinyInt:
					return byte.Parse(value);

				case SqlDbType.Decimal:
				case SqlDbType.Money:
				case SqlDbType.SmallMoney:
					return decimal.Parse(value);

				case SqlDbType.Date:
				case SqlDbType.DateTime:
				case SqlDbType.DateTime2:
				case SqlDbType.SmallDateTime:
					return DateTime.Parse(value);

				case SqlDbType.DateTimeOffset:
					return DateTimeOffset.Parse(value);

				case SqlDbType.Float:
					return double.Parse(value, CultureInfo.InvariantCulture.NumberFormat);

				case SqlDbType.UniqueIdentifier:
					return Guid.Parse(value);

				case SqlDbType.SmallInt:
					return short.Parse(value);

				case SqlDbType.Int:
					return int.Parse(value);

				case SqlDbType.BigInt:
					return long.Parse(value);

				case SqlDbType.Real:
					return float.Parse(value);

				default:
					return value;
			}
		}
	}
}
