﻿using System.Xml;

namespace MSSQLServerAuditor.Common.Utils
{
	/// <summary>
	/// Default settings to work with XML.
	/// </summary>
	public static class XmlDefaultSettings
	{
		public static readonly XmlWriterSettings Writer;
		public static readonly XmlReaderSettings Reader;

		static XmlDefaultSettings()
		{
			Writer = new XmlWriterSettings
			{
				Indent      = true,
				IndentChars = "\t"
			};

			Reader = new XmlReaderSettings
			{
				IgnoreWhitespace = true
			};
		}
	}
}
