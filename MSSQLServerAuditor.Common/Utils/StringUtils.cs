using System.Linq;

namespace MSSQLServerAuditor.Common.Utils
{
	public static class StringUtils
	{
		public static string FirstNonEmpty(string first, params string[] other)
		{
			return !string.IsNullOrWhiteSpace(first)
				? first
				: other.FirstOrDefault(s => !string.IsNullOrWhiteSpace(s));
		}
	}
}
