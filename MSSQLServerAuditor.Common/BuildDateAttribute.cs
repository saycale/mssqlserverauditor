using System;
using System.Globalization;

namespace MSSQLServerAuditor.Common
{
	public class BuildDateAttribute : Attribute
	{
		public DateTime BuildDate { get; private set; }

		public BuildDateAttribute(string buildDate)
		{
			if (!string.IsNullOrEmpty(buildDate))
			{
				DateTime datetime;

				if (DateTime.TryParseExact(
					buildDate,
					"yyyy/MM/dd",
					CultureInfo.InvariantCulture,
					DateTimeStyles.None,
					out datetime
				))
				{
					datetime = datetime.Date.AddSeconds(-1).AddDays(1);
				}

				BuildDate = datetime;
			}
		}
	}
}
