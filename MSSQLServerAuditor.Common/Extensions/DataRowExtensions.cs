﻿using System;
using System.ComponentModel;
using System.Data;

namespace MSSQLServerAuditor.Common.Extensions
{
	public static class DataRowExtensions
	{
		public static T GetValue<T>(
			this DataRow row,
			     string  column,
			     T       defaultValue
		)
		{
			if (!row.Table.Columns.Contains(column))
			{
				return defaultValue;
			}

			TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));

			try
			{
				return (T) converter.ConvertFromString(row[column].ToString());
			}
			catch (Exception)
			{
				return defaultValue;
			}
		}
	}
}
