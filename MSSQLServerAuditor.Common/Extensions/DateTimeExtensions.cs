﻿using System;

namespace MSSQLServerAuditor.Common.Extensions
{
	public static class DateTimeExtensions
	{
		public static string ToSortableDateString(this DateTime dt)
		{
			return dt.ToString("s", System.Globalization.CultureInfo.InvariantCulture);
		} 
	}
}
