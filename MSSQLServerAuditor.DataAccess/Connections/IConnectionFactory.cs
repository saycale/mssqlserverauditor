﻿using System.Data.Common;

namespace MSSQLServerAuditor.DataAccess.Connections
{
	public interface IConnectionFactory
	{
		DbConnection CreateConnection(string connectionString);
		DbConnection CreateOpenedConnection(string connectionString);
	}
}
