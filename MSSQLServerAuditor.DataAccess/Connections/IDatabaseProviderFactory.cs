﻿using MSSQLServerAuditor.DataAccess.Commands.Interfaces;
using Perceiveit.Data;

namespace MSSQLServerAuditor.DataAccess.Connections
{
	public interface IDatabaseProviderFactory
	{
		IConnectionFactory CreateConnectionFactory();
		DBDatabase         CreateDatabase(string connectionString);
		DBDatabase         GetDatabase(string connectionString);
		ICommandExecutor   CreateCommandExecutor();
	}
}