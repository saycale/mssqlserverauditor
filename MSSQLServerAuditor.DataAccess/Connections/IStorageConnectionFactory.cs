﻿using System.Data.Common;

namespace MSSQLServerAuditor.DataAccess.Connections
{
	public interface IStorageConnectionFactory
	{
		DbConnection CreateOpenedConnection();
	}
}
