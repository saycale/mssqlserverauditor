using System;
using System.Data.Common;
using Perceiveit.Data;
using Perceiveit.Data.Query;

namespace MSSQLServerAuditor.DataAccess.Extensions
{
	public static class DBDatabaseExtension
	{
		public static T Execute<T>(
			this DBDatabase                      database,
			Func<DbConnection, DbTransaction, T> action
		)
		{
			using (DbConnection connection = database.CreateConnection())
			{
				connection.Open();

				using (DbTransaction transaction = connection.BeginTransaction())
				{
					T result = action(connection, transaction);
					transaction.Commit();
					return result;
				}
			}
		}

		public static T ExecuteTransactionally<T>(
			this DBDatabase           database,
			Func<ExecutionContext, T> action
		)
		{
			using (DbConnection connection = database.CreateConnection())
			{
				connection.Open();

				using (DbTransaction transaction = connection.BeginTransaction())
				{
					ExecutionContext context = new ExecutionContext(
						database,
						connection,
						transaction
					);

					T result = action(context);
					transaction.Commit();

					return result;
				}
			}
		}

		public static T Execute<T>(
			this DBDatabase           database,
			Func<ExecutionContext, T> action
		)
		{
			using (DbConnection connection = database.CreateConnection())
			{
				connection.Open();

				ExecutionContext context = new ExecutionContext(database, connection);

				return action(context);
			}
		}

		public static T ExecuteOnConnection<T>(
			this DBDatabase           database,
			DbConnection              connection,
			Func<ExecutionContext, T> action
		)
		{
			ExecutionContext context = new ExecutionContext(database, connection);

			return action(context);
		}

		public static DBSelectQuery InnerJoinOn(
			this DBSelectQuery selectQuery,
			string             joinTable,
			string             joinTableField,
			string             parentTable,
			string             parentTableField)
		{
			return selectQuery.InnerJoin(
				DBTable.Table(joinTable),
				DBField.Field(joinTable, joinTableField),
				Compare.Equals,
				DBField.Field(parentTable, parentTableField));
		}
	}
}
