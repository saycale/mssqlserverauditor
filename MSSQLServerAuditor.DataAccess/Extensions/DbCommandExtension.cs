﻿using System;
using System.Data.Common;
using MSSQLServerAuditor.DataAccess.Structure;

namespace MSSQLServerAuditor.DataAccess.Extensions
{
	public static class DbCommandExtensions
	{
		public static void AddParameter(this DbCommand command, object value, FieldInfo field)
		{
			DbParameter parameter   = command.CreateParameter();
			parameter.ParameterName = field.ParameterName;
			parameter.DbType        = field.DbType;
			parameter.IsNullable    = !field.IsNotNull;
			parameter.Value         = !field.IsNotNull && value == null ? DBNull.Value : value;

			command.Parameters.Add(parameter);
		}
	}
}
