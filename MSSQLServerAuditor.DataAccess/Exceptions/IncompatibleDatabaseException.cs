﻿using System;

namespace MSSQLServerAuditor.DataAccess.Exceptions
{
	public class IncompatibleDatabaseException : Exception
	{
		public IncompatibleDatabaseException(string database)
		{
			this.Database = database;
		}

		public IncompatibleDatabaseException(string database, string message) : base(message)
		{
			this.Database = database;
		}

		public IncompatibleDatabaseException(string database, string message, Exception inner) : base(message, inner)
		{
			this.Database = database;
		}
		
		public string Database { get; }
	}
}
