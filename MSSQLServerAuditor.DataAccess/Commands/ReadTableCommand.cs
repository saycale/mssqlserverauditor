﻿using System.Collections.Generic;
using System.Data;
using Perceiveit.Data.Query;

namespace MSSQLServerAuditor.DataAccess.Commands
{
	public class ReadTableCommand : DatabaseCommand<DataTable>
	{
		private readonly DBSelectQuery            _selectQuery;
		private readonly List<DbCommandParameter> _parameters;

		public ReadTableCommand(
			ExecutionContext         context,
			DBSelectQuery            selectQuery, 
			List<DbCommandParameter> parameters) : base(context)
		{
			this._selectQuery = selectQuery;
			this._parameters  = parameters;
		}

		public override DataTable Execute()
		{
			string query = ExecutionContext.GenerateSql(this._selectQuery);

			return ExecuteDataTable(query, this._parameters.ToArray());
		}
	}
}
