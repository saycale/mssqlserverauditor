using System.Collections.Generic;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.DataAccess.Structure;
using MSSQLServerAuditor.DataAccess.Tables;
using Perceiveit.Data.Query;

namespace MSSQLServerAuditor.DataAccess.Commands
{
	public class InsertRowCommand : DatabaseCommand<long>
	{
		private readonly TableInfo       _tableInfo;
		private readonly List<ITableRow> _rows;

		public InsertRowCommand(ExecutionContext context, TableInfo tableInfo) : base(context)
		{
			Check.NotNull(tableInfo, nameof(tableInfo));

			this._tableInfo = tableInfo;
			this._rows      = new List<ITableRow>();
		}

		public void AddRowToInsert(ITableRow row)
		{
			this._rows.Add(row);
		}

		public override long Execute()
		{
			long totalRows = 0;

			foreach (ITableRow row in this._rows)
			{
				DBInsertQuery insertQuery = string.IsNullOrWhiteSpace(this._tableInfo.Schema)
					? DBQuery.InsertInto(this._tableInfo.Name)
					: DBQuery.InsertInto(this._tableInfo.Schema, this._tableInfo.Name);

				List<string>             fieldList = new List<string>();
				List<DbCommandParameter> cmdParams = new List<DbCommandParameter>();
				List<DBClause>           paramList = new List<DBClause>();

				foreach (FieldInfo field in this._tableInfo.Fields)
				{
					if (field.IsIdentity)
					{
						continue;
					}

					object value;

					if (field.DefaultValue == null | row.Values.TryGetValue(field.Name, out value))
					{
						fieldList.Add(field.Name);
						paramList.Add(DBParam.Param(field.Name));

						DbCommandParameter cmdParam = new DbCommandParameter(value, field);
						cmdParams.Add(cmdParam);
					}
				}

				insertQuery
					.Fields(fieldList.ToArray())
					.Values(paramList.ToArray());

				totalRows += this.ExecuteNonQuery(
					ExecutionContext.GenerateSql(insertQuery),
					cmdParams.ToArray()
				);
			}

			this._rows.Clear();

			return totalRows;
		}
	}
}