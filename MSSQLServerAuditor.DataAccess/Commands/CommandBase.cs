using System;
using MSSQLServerAuditor.DataAccess.Commands.Interfaces;
using NLog;

namespace MSSQLServerAuditor.DataAccess.Commands
{
	public abstract class CommandBase<TResult> : ICommand<TResult>
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		public abstract TResult Execute();

		public TResult Execute(ICommandExecutor cmdExecutor)
		{
			try
			{
				return cmdExecutor.Execute(this);
			}
			catch (Exception exc)
			{
				Log.Error(exc, "Unable to execute database command");

				throw;
			}
		}
	}
}
