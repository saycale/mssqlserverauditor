using System;
using NLog;
using Perceiveit.Data.Query;

namespace MSSQLServerAuditor.DataAccess.Commands
{
	internal class DropTableCommand : DatabaseCommand<int>
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		public DropTableCommand(
			ExecutionContext context,
			string           tableName
		) : base(context)
		{
			this.TableName = tableName;
		}

		public string TableName { get; private set; }

		public override int Execute()
		{
			DBDropTableQuery dropTableQuery = DBQuery
				.Drop
				.Table(TableName)
				.IfExists();

			try
			{
				string sql = ExecutionContext.GenerateSql(dropTableQuery);

				return ExecuteNonQuery(sql);
			}
			catch (Exception exc)
			{
				Log.Error(exc, $"Unable to drop table {TableName}");

				throw;
			}
		}
	}
}
