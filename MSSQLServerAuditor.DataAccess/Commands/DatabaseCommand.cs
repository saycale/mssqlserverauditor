﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using MSSQLServerAuditor.Common.Contracts;
using MSSQLServerAuditor.DataAccess.Structure;
using MSSQLServerAuditor.DataAccess.Tables;

namespace MSSQLServerAuditor.DataAccess.Commands
{
	public abstract class DatabaseCommand<TResult> : CommandBase<TResult>
	{
		protected DatabaseCommand(ExecutionContext context)
		{
			Check.NotNull(context, nameof(context));

			this.ExecutionContext = context;
		}

		protected ExecutionContext ExecutionContext { get; private set; }

		protected int ExecuteNonQuery(
			string                      query, 
			params DbCommandParameter[] parameters)
		{
			using (DbCommand command = CreateCommand(query, parameters))
			{
				return command.ExecuteNonQuery();
			}
		}

		protected object ExecuteScalar(
			string                      query,
			params DbCommandParameter[] parameters)
		{
			using (DbCommand command = CreateCommand(query, parameters))
			{
				return command.ExecuteScalar();
			}
		}

		protected DataTable ExecuteDataTable(
			string                      query, 
			params DbCommandParameter[] parameters)
		{
			using (DbCommand command = CreateCommand(query, parameters))
			{
				using (DbDataReader reader = command.ExecuteReader())
				{
					DataTable dataTable = new DataTable();
					dataTable.Load(reader);
					return dataTable;
				}
			}
		}

		protected List<ITableRow> ExecuteReadRows(
			TableInfo                   tableInfo,
			string                      query,
			params DbCommandParameter[] parameters)
		{
			using (DbCommand command = CreateCommand(query, parameters))
			{
				List<ITableRow> tableRows = new List<ITableRow>();
				using (DbDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						ITableRow row = TableRow.Read(tableInfo, reader);
						tableRows.Add(row);
					}
				}
				return tableRows;
			}
		}

		protected static int GetInt(DataRow row, FieldInfo field)
		{
			return Convert.ToInt32(row[field.Name]);
		}

		protected static T GetValue<T>(DataRow row, FieldInfo field)
		{
			return GetValue<T>(row, field.Name);
		}

		protected static T GetValue<T>(DataRow row, string fieldName)
		{
			object value = row[fieldName];
			return value == DBNull.Value ? default(T) : (T)value;
		}

		private DbCommand CreateCommand(
			string                      query,
			params DbCommandParameter[] parameters)
		{
			return ExecutionContext.CreateCommand(query, parameters);
		}
	}
}
