using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace MSSQLServerAuditor.DataAccess.Utils
{
	public static class ReflectionUtils
	{
		private class PropertyDictionary : ConcurrentDictionary<Type, PropertyInfo[]>
		{
		}

		private static readonly ConcurrentDictionary<Type, PropertyDictionary> Cache;

		static ReflectionUtils()
		{
			Cache = new ConcurrentDictionary<Type, PropertyDictionary>();
		}

		public static PropertyInfo[] GetPropertiesWithAttribute<TClass, TAttr>()
			where TClass : class
			where TAttr : Attribute
		{
			Type classType = typeof(TClass);

			PropertyDictionary typeCache = Cache.GetOrAdd(classType, ct =>
				new PropertyDictionary());

			Type attrType = typeof(TAttr);

			PropertyInfo[] properties = typeCache.GetOrAdd(attrType, at =>
			{
				return classType.GetProperties()
					.Where(prop => Attribute.IsDefined(prop, at))
					.ToArray();
			});

			return properties;
		}

		public static PropertyInfo[] GetProperties<TEntity>(Expression<Func<TEntity, object>> identifiers)
			where TEntity : class
		{
			// e => e.SomeValue
			MemberExpression direct = identifiers.Body as MemberExpression;

			if (direct != null)
			{
				return new[]
				{
					(PropertyInfo) direct.Member
				};
			}

			// e => (object)e.SomeValue
			UnaryExpression convert = identifiers.Body as UnaryExpression;

			if (convert != null)
			{
				return new[]
				{
					(PropertyInfo) ((MemberExpression) convert.Operand).Member
				};
			}

			// e => new { e.SomeValue, e.OtherValue }
			NewExpression multiple = identifiers.Body as NewExpression;

			if (multiple != null)
			{
				return multiple.Arguments
					.Cast<MemberExpression>()
					.Select(a => (PropertyInfo)a.Member)
					.ToArray();
			}

			throw new NotSupportedException();
		}
	}
}
