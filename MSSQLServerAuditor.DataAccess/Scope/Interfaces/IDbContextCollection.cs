﻿/* 
 * Copyright (C) 2014 Mehdi El Gueddari
 * http://mehdi.me
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

using System;
using System.Data.Entity;

namespace MSSQLServerAuditor.DataAccess.Scope.Interfaces
{
	/// <summary>
	/// Maintains a list of lazily-created DbContext instances.
	/// </summary>
	public interface IDbContextCollection<out TDbContext> : IDisposable where TDbContext : DbContext
	{
		/// <summary>
		/// Get or create a DbContext instance of the specified type. 
		/// </summary>
		TDbContext Get();
	}
}