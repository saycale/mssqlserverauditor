/*
 * Copyright (C) 2014 Mehdi El Gueddari
 * http://mehdi.me
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

using System.Data;
using System.Data.Entity;
using MSSQLServerAuditor.DataAccess.Scope.Enums;
using MSSQLServerAuditor.DataAccess.Scope.Interfaces;

namespace MSSQLServerAuditor.DataAccess.Scope.Implementations
{
	public class DbContextReadOnlyScope<TDbContext> : IDbContextReadOnlyScope<TDbContext> where TDbContext : DbContext
	{
		private DbContextScope<TDbContext> _internalScope;

		public IDbContextCollection<TDbContext> DbContexts { get { return _internalScope.DbContexts; } }

		public DbContextReadOnlyScope(IDbContextFactory<TDbContext> dbContextFactory = null)
			: this(joiningOption: DbContextScopeOption.JoinExisting, isolationLevel: null, dbContextFactory: dbContextFactory)
		{
		}

		public DbContextReadOnlyScope(IsolationLevel isolationLevel, IDbContextFactory<TDbContext> dbContextFactory = null)
			: this(joiningOption: DbContextScopeOption.ForceCreateNew, isolationLevel: isolationLevel, dbContextFactory: dbContextFactory)
		{
		}

		public DbContextReadOnlyScope(DbContextScopeOption joiningOption, IsolationLevel? isolationLevel, IDbContextFactory<TDbContext> dbContextFactory = null)
		{
			this._internalScope = new DbContextScope<TDbContext>(joiningOption: joiningOption, readOnly: true, isolationLevel: isolationLevel, dbContextFactory: dbContextFactory);
		}

		public void Dispose()
		{
			this._internalScope.Dispose();
		}
	}
}
