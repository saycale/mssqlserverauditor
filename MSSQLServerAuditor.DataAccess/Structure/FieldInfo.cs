﻿using System;
using System.Data;

namespace MSSQLServerAuditor.DataAccess.Structure
{
	[Flags]
	public enum FieldInfoFlag
	{
		None      = 0,
		Identity  = 1,
		Unique    = 2,
		IsNotNull = 4
	}

	/// <summary>
	/// Definition for database field
	/// </summary>
	public class FieldInfo : IEquatable<FieldInfo>
	{
		public FieldInfo(
			string name,
			DbType dbType,
			object defaultValue = null) : this(name, dbType, FieldInfoFlag.None, defaultValue)
		{
			
		}

		public FieldInfo(
			string        name,
			DbType        dbType,
			FieldInfoFlag flags,
			object        defaultValue = null) : this(name, dbType, flags, null, defaultValue)
		{
			
		}

		public FieldInfo(
			string        name,
			DbType        dbType,
			FieldInfoFlag flags,
			int?          maxLength,
			object        defaultValue = null)
		{
			this.Name         = name;
			this.DbType       = dbType;
			this.Flags        = flags;
			this.MaxLength    = maxLength;
			this.DefaultValue = defaultValue;
		}

		public string        Name         { get; private set; }
		public DbType        DbType       { get; private set; }
		public FieldInfoFlag Flags        { get; private set; }
		public int?          MaxLength    { get; private set; }
		public object        DefaultValue { get; private set; }

		public bool IsNotNull  { get { return Flags.HasFlag(FieldInfoFlag.IsNotNull); } }
		public bool IsUnique   { get { return Flags.HasFlag(FieldInfoFlag.Unique);    } }
		public bool IsIdentity { get { return Flags.HasFlag(FieldInfoFlag.Identity);  } }

		public string ParameterName { get { return "@" + Name; } }
		
		public bool Equals(FieldInfo other)
		{
			char[] trimChars = { ' ', '(', ')' };

			string def1 = (this.DefaultValue  == null) ? string.Empty : this.DefaultValue.ToString();
			string def2 = (other.DefaultValue == null) ? string.Empty : other.DefaultValue.ToString();

			def1 = def1.Trim(trimChars);
			def2 = def2.Trim(trimChars);

			return string.Equals(this.Name, other.Name) 
				&& this.DbType    == other.DbType
				&& this.Flags     == other.Flags
				&& this.MaxLength == other.MaxLength
				&& def1.Equals(def2);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = (this.Name != null ? this.Name.GetHashCode() : 0);

				hashCode = (hashCode * 397) ^ (int)this.DbType;
				hashCode = (hashCode * 397) ^ this.Flags.GetHashCode();
				hashCode = (hashCode * 397) ^ this.MaxLength ?? 0;
				
				if (DefaultValue != null)
				{
					hashCode = (hashCode * 397) ^ this.DefaultValue.GetHashCode();
				}

				return hashCode;
			}
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
			{
				return false;
			}

			return obj is FieldInfo && this.Equals((FieldInfo)obj);
		}

		public static bool operator ==(FieldInfo left, FieldInfo right)
		{
			if (ReferenceEquals(left, right))
			{
				return true;
			}

			if (ReferenceEquals(left, null) || ReferenceEquals(right, null))
			{
				return false;
			}
			
			return left.Equals(right);
		}

		public static bool operator !=(FieldInfo left, FieldInfo right)
		{
			return !(left == right);
		}
	}
}
