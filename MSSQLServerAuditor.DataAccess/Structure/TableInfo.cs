﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace MSSQLServerAuditor.DataAccess.Structure
{
	/// <summary>
	/// Definition for database table
	/// </summary>
	public class TableInfo
	{
		public const string FieldDateCreated = "date_created";
		public const string FieldDateUpdated = "date_updated";

		public TableInfo(string schema, string name)
		{
			this.Schema = schema;
			this.Name   = name;

			this.Fields   = new List<FieldInfo>();
			this.Indexes  = new List<IndexInfo>();
			this.Triggers = new List<TriggerInfo>();

			this.PrimaryKey = IndexInfo.Empty;
		}

		public TableInfo(string name) : this(null, name)
		{
			
		}

		public string            Schema     { get; private set; }
		public string            Name       { get; private set; }
		public List<FieldInfo>   Fields     { get; private set; }
		public List<TriggerInfo> Triggers   { get; private set; }
		public List<IndexInfo>   Indexes    { get; private set; }
		public IndexInfo         PrimaryKey { get; set; } 

		public Dictionary<string, FieldInfo> GetFieldDictionary()
		{
			return Fields.ToDictionary(f => f.Name, f => f);
		}

		public TableInfo AddField(FieldInfo fieldInfo)
		{
			if (!this.Fields.Exists(info => info.Name == fieldInfo.Name))
			{
				this.Fields.Add(fieldInfo);
			}

			return this;
		}

		public TableInfo AddTrigger(TriggerInfo trigger)
		{
			this.Triggers.Add(trigger);

			return this;
		}

		public TableInfo AddIndex(IndexInfo index)
		{
			this.Indexes.Add(index);

			return this;
		}

		public TableInfo SetPrimaryKey(IndexInfo primaryKey)
		{
			this.PrimaryKey = primaryKey;

			return this;
		}

		public TableInfo AddField(
			string        name,
			DbType        dbType,
			FieldInfoFlag flags,
			object        defaultValue = null)
		{
			FieldInfo fieldInfo = new FieldInfo(
				name,
				dbType,
				flags,
				defaultValue);

			AddField(fieldInfo);

			return this;
		}

		public TableInfo AddStringField(string name, FieldInfoFlag flags = FieldInfoFlag.None)
		{
			return this.AddField(
				name,
				DbType.String, 
				flags
			);
		}

		public TableInfo AddBigIntField(string name, FieldInfoFlag flags = FieldInfoFlag.None)
		{
			return this.AddField(
				name,
				DbType.Int64, 
				flags
			);
		}

		public TableInfo AddIntField(string name, FieldInfoFlag flags = FieldInfoFlag.None)
		{
			return this.AddField(
				name,
				DbType.Int32, 
				flags
			);
		}

		public TableInfo AddBinaryField(string name, FieldInfoFlag flags = FieldInfoFlag.None)
		{
			return this.AddField(
				name,
				DbType.Binary,
				flags
			);
		}

		public TableInfo AddDateCreatedField()
		{
			return this.AddField(
				FieldDateCreated,
				DbType.DateTime, 
				FieldInfoFlag.None
			);
		}

		public TableInfo AddDateUpdatedField()
		{
			return this.AddField(
				FieldDateUpdated,
				DbType.DateTime,
				FieldInfoFlag.None
			);
		}

		public TableInfo AddIndex(string name, bool isUnique, string[] indexFields)
		{
			return this.AddIndex(new IndexInfo(name, isUnique, indexFields));
		}

		public TableInfo SetPrimaryKey(string name, params string[] indexFields)
		{
			return this.SetPrimaryKey(new IndexInfo(name, true, indexFields));
		}
	}
}
