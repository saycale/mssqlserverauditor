namespace MSSQLServerAuditor.DataAccess.Entity
{
	internal static class Defaults
	{
		public static class Size
		{
			public const int TextSmall  = 128;
			public const int TextMedium = 256;
			public const int TextLarge  = 512;
			public const int TextLong   = 1024;
			public const int TextXLarge = 2048;
		}
	}
}
