using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBTemplateQueryParameter : AuditableEntity
	{
		public const string TableName            = "d_TemplateNodeQueryParameter";

		public const string FieldId              = "d_TemplateNodeQueryParameter_id";
		public const string FieldTemplateQueryId = "d_TemplateNodeQuery_id";
		public const string FieldName            = "ParameterName";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldTemplateQueryId)]
		public long? TemplateQueryId { get; set; }

		[UniqueKey]
		[Column(FieldName)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Name { get; set; }

		[ForeignKey(nameof(TemplateQueryId))]
		public virtual DBTemplateQuery TemplateQuery { get; set; }

		public virtual ICollection<DBQueryParameter> QueryParameters { get; set; }
	}
}
