using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBTemplateNode : AuditableEntity
	{
		public const string TableName                       = "d_TemplateNode";

		public const string FieldId                         = "d_TemplateNode_id";
		public const string FieldTemplateId                 = "d_Template_id";
		public const string FieldParentId                   = "d_TemplateNodeParent_id";
		public const string FieldUId                        = "TemplateNodeUserId";
		public const string FieldTemplateQueryGroupParentId = "d_TemplateNodeQueryGroupParent_Id";
		public const string FieldName                       = "TemplateNodeUserName";
		public const string FieldIcon                       = "TemplateNodeIcon";
		public const string FieldShowEmpty                  = "TemplateNodeShowIfEmpty";
		public const string FieldShowRecordsNumber          = "TemplateNodeShowNumberOfRecords";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldTemplateId)]
		public long? TemplateId { get; set; }

		[UniqueKey]
		[Column(FieldParentId)]
		public long? ParentId { get; set; }

		[UniqueKey]
		[Column(FieldUId)]
		[StringLength(Defaults.Size.TextSmall)]
		public string UId { get; set; }

		[Column(FieldTemplateQueryGroupParentId)]
		public long? TemplateQueryGroupParentId { get; set; }

		[Column(FieldName)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Name { get; set; }

		[Column(FieldIcon)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Icon { get; set; }

		[Column(FieldShowEmpty)]
		public bool? ShowEmpty { get; set; }

		[Column(FieldShowRecordsNumber)]
		public bool? ShowRecordsNumber { get; set; }

		[ForeignKey(nameof(TemplateId))]
		public virtual DBTemplate Template { get; set; }

		[ForeignKey(nameof(ParentId))]
		public virtual DBTemplateNode Parent { get; set; }

		[ForeignKey(nameof(TemplateQueryGroupParentId))]
		public virtual DBTemplateQueryGroup TemplateQueryGroupParent { get; set; }

		// added to support one to zero-or-one relationship
		// need to help EF figure out that a template node 'has' multiple query groups
		// instead of being random entities that point to each other.
		[InverseProperty(nameof(DBTemplateQueryGroup.TemplateNodeParent))]
		public virtual ICollection<DBTemplateQueryGroup> TemplateQueryGroups { get; set; }

		public virtual ICollection<DBTemplateQuery> TemplateQueries { get; set; }

		public virtual ICollection<DBNodeInstance> NodeInstances { get; set; }
	}
}
