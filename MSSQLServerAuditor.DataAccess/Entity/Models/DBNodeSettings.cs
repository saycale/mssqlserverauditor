using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBNodeSettings : AuditableEntity
	{
		public const string TableName           = "d_NodeInstance_UserSettings";

		public const string FieldId             = "d_NodeInstance_UserSettings_id";
		public const string FieldNodeInstanceId = "d_NodeInstance_id";
		public const string FieldLanguage       = "Language";
		public const string FieldName           = "NodeUName";
		public const string FieldIcon           = "NodeUIcon";
		public const string FieldIsEnabled      = "NodeEnabled";
		public const string FieldFontStyle      = "NodeFontStyle";
		public const string FieldFontColor      = "NodeFontColor";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldNodeInstanceId)]
		public long? NodeInstanceId { get; set; }

		[UniqueKey]
		[Column(FieldLanguage)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Language { get; set; }

		[Column(FieldName)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Name { get; set; }

		[Column(FieldIcon)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Icon { get; set; }

		[Column(FieldIsEnabled)]
		public bool? IsEnabled { get; set; }

		[Column(FieldFontStyle)]
		[StringLength(Defaults.Size.TextSmall)]
		public string FontStyle { get; set; }

		[Column(FieldFontColor)]
		[StringLength(Defaults.Size.TextSmall)]
		public string FontColor { get; set; }

		[ForeignKey(nameof(NodeInstanceId))]
		public virtual DBNodeInstance NodeInstance { get; set; }
	}
}
