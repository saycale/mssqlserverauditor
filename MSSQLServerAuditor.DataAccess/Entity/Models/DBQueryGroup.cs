using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBQueryGroup : AuditableEntity
	{
		public const string TableName                 = "d_Query_Group";

		public const string FieldId                   = "d_Query_Group_id";
		public const string FieldNodeInstanceId       = "d_NodeInstance_id";
		public const string FieldTemplateQueryGroupId = "d_TemplateNodeQueryGroup_id";
		public const string FieldServerInstanceId     = "d_ServerInstance_id";
		public const string FieldDefaultDatabaseName  = "DefaultDatabaseName";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldNodeInstanceId)]
		public long? NodeInstanceId { get; set; }

		[UniqueKey]
		[Column(FieldTemplateQueryGroupId)]
		public long? TemplateQueryGroupId { get; set; }

		[UniqueKey]
		[Column(FieldServerInstanceId)]
		public long? ServerInstanceId { get; set; }

		[Column(FieldDefaultDatabaseName)]
		[StringLength(Defaults.Size.TextMedium)]
		public string DefaultDatabaseName { get; set; }

		[ForeignKey(nameof(NodeInstanceId))]
		public virtual DBNodeInstance NodeInstance { get; set; }

		[ForeignKey(nameof(TemplateQueryGroupId))]
		public virtual DBTemplateQueryGroup TemplateQueryGroup { get; set; }

		[ForeignKey(nameof(ServerInstanceId))]
		public virtual DBServerInstance ServerInstance { get; set; }

		public virtual ICollection<DBQueryGroupParameter> QueryGroupParameters { get; set; }

		public virtual ICollection<DBMetaResult> MetaResults { get; set; }
	}
}
