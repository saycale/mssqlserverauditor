using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBQueryParameter : AuditableEntity
	{
		public const string TableName                     = "d_Query_Parameter";

		public const string FieldId                       = "d_Query_Parameter_id";
		public const string FieldQueryId                  = "d_Query_id";
		public const string FieldTemplateQueryParameterId = "d_TemplateNodeQueryParameter_id";
		public const string FieldValue                    = "ParameterValue";
		public const string FieldIsEnabled                = "IsParameterEnabled";

		public DBQueryParameter()
		{
			IsEnabled = true;
		}

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldQueryId)]
		public long? QueryId { get; set; }

		[UniqueKey]
		[Column(FieldTemplateQueryParameterId)]
		public long? TemplateQueryParameterId { get; set; }

		[Column(FieldValue)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Value { get; set; }

		[Column(FieldIsEnabled)]
		public bool IsEnabled { get; set; }

		[ForeignKey(nameof(QueryId))]
		public virtual DBQuery Query { get; set; }

		[ForeignKey(nameof(TemplateQueryParameterId))]
		public virtual DBTemplateQueryParameter TemplateQueryParameter { get; set; }
	}
}
