using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBQueryResults : AuditableEntity
	{
		public const string TableName          = "d_Query_Results";

		public const string FieldId            = "d_Query_Results_id";
		public const string FieldQueryId       = "d_Query_id";
		public const string FieldResultTableId = "d_Query_ResultTable_id";
		public const string FieldRecordSet     = "RecordSet";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldQueryId)]
		public long QueryId { get; set; }

		[UniqueKey]
		[Column(FieldRecordSet)]
		public long RecordSet { get; set; }

		[Column(FieldResultTableId)]
		public long ResultTableId { get; set; }

		[ForeignKey(nameof(ResultTableId))]
		public virtual DBQueryResultTable ResultTable { get; set; }

		[ForeignKey(nameof(QueryId))]
		public virtual DBQuery Query { get; set; }
	}
}
