﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBMetaSubResult : AuditableEntity
	{
		public const string TableName         = "d_MetaSubResult";

		public const string FieldId           = "d_MetaSubResult_id";
		public const string FieldQueryId      = "d_Query_id";
		public const string FieldSubQueryId   = "d_SubQuery_id";
		public const string FieldRows         = "rows";
		public const string FieldErrorId      = "error_id";
		public const string FieldErrorCode    = "error_code";
		public const string FieldErrorMessage = "error_message";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldQueryId)]
		public long? QueryId { get; set; }

		[UniqueKey]
		[Column(FieldSubQueryId)]
		public long SubQueryId { get; set; }

		[Column(FieldRows)]
		public long? Rows { get; set; }

		[Column(FieldErrorId)]
		[StringLength(Defaults.Size.TextSmall)]
		public string ErrorId { get; set; }

		[Column(FieldErrorCode)]
		[StringLength(Defaults.Size.TextSmall)]
		public string ErrorCode { get; set; }

		[Column(FieldErrorMessage)]
		[StringLength(Defaults.Size.TextXLarge)]
		public string ErrorMessage { get; set; }

		[ForeignKey(nameof(QueryId))]
		public virtual DBQuery Query { get; set; }
	}
}
