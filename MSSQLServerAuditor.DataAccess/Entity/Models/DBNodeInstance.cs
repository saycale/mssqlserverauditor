using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBNodeInstance : AuditableEntity
	{
		public const string TableName                 = "d_NodeInstance";

		public const string FieldId                   = "d_NodeInstance_id";
		public const string FieldConnectionGroupId    = "d_ConnectionGroup_id";
		public const string FieldTemplateNodeId       = "d_TemplateNode_id";
		public const string FieldParentId             = "d_NodeInstanceParent_Id";
		public const string FieldUId                  = "NodeUId";
		public const string FieldName                 = "NodeUName";
		public const string FieldIcon                 = "NodeUIcon";
		public const string FieldIsEnabled            = "NodeEnabled";
		public const string FieldFontColor            = "NodeFontColor";
		public const string FieldFontStyle            = "NodeFontStyle";
		public const string FieldSequenceNumber       = "NodeSequenceNumber";
		public const string FieldChildrenNotProcessed = "ChildrenNotYetProcessed";
		public const string FieldScheduledUpdate      = "NodeScheduledUpdate";
		public const string FieldCounter              = "NodeCounterValue";
		public const string FieldLastUpdate           = "NodeLastUpdated";
		public const string FieldLastUpdateDuration   = "NodeLastUpdateDuration";

		public DBNodeInstance()
		{
			IsEnabled            = true;
			ChildrenNotProcessed = true;
		}

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldConnectionGroupId)]
		public long? ConnectionGroupId { get; set; }

		[UniqueKey]
		[Column(FieldTemplateNodeId)]
		public long? TemplateNodeId { get; set; }

		[UniqueKey]
		[Column(FieldParentId)]
		public long? ParentId { get; set; }

		[Column(FieldUId)]
		[StringLength(Defaults.Size.TextSmall)]
		public string UId { get; set; }

		[UniqueKey]
		[Column(FieldName)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Name { get; set; }

		[Column(FieldIcon)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Icon { get; set; }

		[Column(FieldIsEnabled)]
		public bool? IsEnabled { get; set; }

		[Column(FieldFontColor)]
		[StringLength(Defaults.Size.TextSmall)]
		public string FontColor { get; set; }

		[Column(FieldFontStyle)]
		[StringLength(Defaults.Size.TextSmall)]
		public string FontStyle { get; set; }

		[UniqueKey]
		[Column(FieldSequenceNumber)]
		public long? SequenceNumber { get; set; }

		[Column(FieldChildrenNotProcessed)]
		public bool? ChildrenNotProcessed { get; set; }

		[Column(FieldScheduledUpdate)]
		public DateTime? ScheduledUpdate { get; set; }

		[Column(FieldCounter)]
		public long? Counter { get; set; }

		[Column(FieldLastUpdate)]
		public DateTime? LastUpdate { get; set; }

		[Column(FieldLastUpdateDuration)]
		public long? LastUpdateDuration { get; set; }

		[ForeignKey(nameof(ConnectionGroupId))]
		public virtual DBConnectionGroup ConnectionGroup { get; set; }

		[ForeignKey(nameof(TemplateNodeId))]
		public virtual DBTemplateNode TemplateNode { get; set; }

		[ForeignKey(nameof(ParentId))]
		public virtual DBNodeInstance NodeInstanceParent { get; set; }

		public virtual ICollection<DBNodeAttribute> Attributes { get; set; }

		public virtual ICollection<DBQuery> Queries { get; set; }

		public virtual ICollection<DBQueryGroup> QueryGroups { get; set; }

		public virtual ICollection<DBNodeSettings> UserSettings { get; set; }

		public virtual ICollection<DBNodeScheduleSettings> ScheduleUserSettings { get; set; }
	}
}
