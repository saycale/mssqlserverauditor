using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBTemplateQueryGroupParameter : AuditableEntity
	{
		public const string TableName                 = "d_TemplateNodeQueryGroupParameter";

		public const string FieldId                   = "d_TemplateNodeQueryGroupParameter_id";
		public const string FieldTemplateQueryGroupId = "d_TemplateNodeQueryGroup_id";
		public const string FieldName                 = "ParameterName";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldTemplateQueryGroupId)]
		public long? TemplateQueryGroupId { get; set; }

		[UniqueKey]
		[Column(FieldName)]
		[StringLength(Defaults.Size.TextSmall)]
		public string Name { get; set; }

		[ForeignKey(nameof(TemplateQueryGroupId))]
		public virtual DBTemplateQueryGroup TemplateQueryGroup { get; set; }

		public virtual ICollection<DBQueryGroupParameter> QueryGroupParameters { get; set; }
	}
}
