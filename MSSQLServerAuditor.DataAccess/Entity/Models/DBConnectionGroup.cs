using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBConnectionGroup : AuditableEntity
	{
		public const string TableName               = "d_ConnectionGroup";

		public const string FieldId                 = "d_ConnectionGroup_id";
		public const string FieldConnectionTypeId   = "d_ConnectionType_id";
		public const string FieldName               = "ConnectionGroupName";
		public const string FieldIsDirectConnection = "IsDirectConnection";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldName)]
		[StringLength(Defaults.Size.TextLarge)]
		public string Name { get; set; }

		[UniqueKey]
		[Column(FieldConnectionTypeId)]
		public long ConnectionTypeId { get; set; }

		[UniqueKey]
		[Column(FieldIsDirectConnection)]
		public bool? IsDirectConnection { get; set; } = true;

		[ForeignKey(nameof(ConnectionTypeId))]
		public virtual DBConnectionType ConnectionType { get; set; }

		public virtual ICollection<DBNodeInstance> NodeInstances { get; set; }

		public virtual ICollection<DBGroupServer> GroupServers { get; set; }
	}
}
