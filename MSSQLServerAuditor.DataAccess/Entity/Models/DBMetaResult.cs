using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBMetaResult : AuditableEntity
	{
		public const string TableName         = "d_MetaResult";

		public const string FieldId           = "d_MetaResult_id";
		public const string FieldQueryId      = "d_Query_id";
		public const string FieldQueryGroupId = "d_Query_Group_id";
		public const string FieldRequestId    = "request_id";
		public const string FieldSessionId    = "session_id";
		public const string FieldRecordSets   = "record_sets";
		public const string FieldRows         = "rows";
		public const string FieldErrorId      = "error_id";
		public const string FieldErrorCode    = "error_code";
		public const string FieldErrorMessage = "error_message";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldQueryId)]
		public long? QueryId { get; set; }

		[UniqueKey]
		[Column(FieldQueryGroupId)]
		public long? QueryGroupId { get; set; }

		[Column(FieldRequestId)]
		public long? RequestId { get; set; }

		[UniqueKey]
		[Column(FieldSessionId)]
		public long SessionId { get; set; }

		[Column(FieldRecordSets)]
		public long? RecordSets { get; set; }

		[Column(FieldRows)]
		public long? Rows { get; set; }

		[Column(FieldErrorId)]
		[StringLength(Defaults.Size.TextSmall)]
		public string ErrorId { get; set; }

		[Column(FieldErrorCode)]
		[StringLength(Defaults.Size.TextSmall)]
		public string ErrorCode { get; set; }

		[Column(FieldErrorMessage)]
		[StringLength(Defaults.Size.TextXLarge)]
		public string ErrorMessage { get; set; }

		[ForeignKey(nameof(QueryId))]
		public virtual DBQuery Query { get; set; }

		[ForeignKey(nameof(QueryGroupId))]
		public virtual DBQueryGroup QueryGroup { get; set; }
	}
}
