using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBLastConnection : AuditableEntity
	{
		public const string TableName              = "d_LastConnection";

		public const string FieldId                = "d_LastConnection_id";
		public const string FieldMachineName       = "MachineName";
		public const string FieldConnectionTypeId  = "d_ConnectionType_id";
		public const string FieldModuleType        = "ModuleType";
		public const string FieldConnectionGroupId = "d_ConnectionGroup_id";
		public const string FieldTemplateId        = "d_Template_id";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldMachineName)]
		[StringLength(Defaults.Size.TextSmall)]
		public string MachineName { get; set; }

		[UniqueKey]
		[Column(FieldConnectionTypeId)]
		[Required]
		public long ConnectionTypeId { get; set; }

		[UniqueKey]
		[Column(FieldModuleType)]
		[Required]
		[StringLength(Defaults.Size.TextSmall)]
		public string ModuleType { get; set; }

		[Column(FieldConnectionGroupId)]
		public long? ConnectionGroupId { get; set; }

		[Column(FieldTemplateId)]
		public long? TemplateId { get; set; }

		[ForeignKey(nameof(ConnectionGroupId))]
		public virtual DBConnectionGroup ConnectionGroup { get; set; }

		[ForeignKey(nameof(TemplateId))]
		public virtual DBTemplate Template { get; set; }

		[ForeignKey(nameof(ConnectionTypeId))]
		public virtual DBConnectionType ConnectionType { get; set; }
	}
}
