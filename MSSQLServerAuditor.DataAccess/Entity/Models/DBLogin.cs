using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MSSQLServerAuditor.DataAccess.Entity.Attributes;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Models
{
	[Table(TableName)]
	public class DBLogin : AuditableEntity
	{
		public const string TableName      = "d_Login";

		public const string FieldId        = "d_Login_id";
		public const string FieldName      = "Login";
		public const string FieldPassword  = "Password";
		public const string FieldIsWinAuth = "IsWinAuth";

		[Key]
		[Column(FieldId)]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public override long Id { get; set; }

		[UniqueKey]
		[Column(FieldName)]
		[StringLength(Defaults.Size.TextSmall)]
		[Required]
		public string Name { get; set; }

		// password in encrypted form
		[UniqueKey]
		[Column(FieldPassword)]
		[StringLength(Defaults.Size.TextMedium)]
		public string EncryptedPassword { get; set; }

		[Column(FieldIsWinAuth)]
		[Required]
		[DefaultValue(false)]
		public bool IsWinAuth { get; set; }

		public virtual ICollection<DBServerInstance> ServerInstances { get; set; }
	}
}
