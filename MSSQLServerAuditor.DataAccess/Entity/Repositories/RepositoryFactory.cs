using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;
using MSSQLServerAuditor.DataAccess.Scope.Interfaces;

namespace MSSQLServerAuditor.DataAccess.Entity.Repositories
{
	public class RepositoryFactory
	{
		private readonly IAmbientDbContextLocator _contextLocator;

		public RepositoryFactory(IAmbientDbContextLocator contextLocator)
		{
			this._contextLocator = contextLocator;
		}

		public CruRepository<TEntity> CreateCruRepository<TEntity>()
			where TEntity : class, IEntity
		{
			return new CruRepository<TEntity>(this._contextLocator);
		}

		public CrudRepository<TEntity> CreateCrudRepository<TEntity>()
			where TEntity : class, IEntity
		{
			return new CrudRepository<TEntity>(this._contextLocator);
		}
	}
}
