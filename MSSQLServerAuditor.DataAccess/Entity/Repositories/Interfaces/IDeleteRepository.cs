﻿using System;
using System.Linq.Expressions;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Repositories.Interfaces
{
	public interface IDeleteRepository<T> where T : class, IEntity
	{
		void Delete(long id);

		void Delete(T entity);

		void Delete(Expression<Func<T, bool>> predicate); 
	}
}
