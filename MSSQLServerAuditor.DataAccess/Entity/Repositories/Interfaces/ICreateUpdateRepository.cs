﻿using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity.Repositories.Interfaces
{
	public interface ICreateUpdateRepository<T> where T : class, IEntity
	{
		T Create(T entity);

		void Update(T entity);

		T InsertOrUpdate(T entity);
	}
}
