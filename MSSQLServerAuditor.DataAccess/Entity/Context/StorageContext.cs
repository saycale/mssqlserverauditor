using System.Data.Common;
using System.Data.Entity;
using MSSQLServerAuditor.DataAccess.Entity.Interfaces;
using MSSQLServerAuditor.DataAccess.Entity.Models;

namespace MSSQLServerAuditor.DataAccess.Entity.Context
{
	public class StorageContext : DbContext
	{
		private readonly IStorageInitializerFactory _initializerFactory;

		public StorageContext(
			IStorageInitializerFactory initializerFactory,
			DbConnection               connection,
			bool                       contextOwnsConnection = true
		) : base(
			connection,
			contextOwnsConnection
		)
		{
			this._initializerFactory = initializerFactory;

			Configuration.ValidateOnSaveEnabled = false;
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			ConfigureTables(modelBuilder);

			IDatabaseInitializer<StorageContext> initializer = this._initializerFactory
				.CreateInitializer(modelBuilder);

			Database.SetInitializer(initializer);

			base.OnModelCreating(modelBuilder);
		}

		private void ConfigureTables(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<DBConnectionGroup>();

			modelBuilder.Entity<DBDynamicConnection>();

			modelBuilder.Entity<DBLastConnection>();

			modelBuilder.Entity<DBLogin>();
			modelBuilder.Entity<DBMetaResult>();
			modelBuilder.Entity<DBMetaSubResult>();
			modelBuilder.Entity<DBMetaEtlResult>();

			modelBuilder.Entity<DBNodeInstance>();
			modelBuilder.Entity<DBNodeAttribute>();

			modelBuilder.Entity<DBQuery>();
			modelBuilder.Entity<DBQueryParameter>();
			modelBuilder.Entity<DBQueryGroup>();
			modelBuilder.Entity<DBQueryGroupParameter>();
			modelBuilder.Entity<DBQueryResults>();
			modelBuilder.Entity<DBQueryResultTable>();

			modelBuilder.Entity<DBConnectionType>();
			modelBuilder.Entity<DBServerInstance>();

			modelBuilder.Entity<DBTemplate>();
			modelBuilder.Entity<DBTemplateNode>();
			modelBuilder.Entity<DBTemplateQuery>();
			modelBuilder.Entity<DBTemplateQueryGroup>();
			modelBuilder.Entity<DBTemplateQueryGroupParameter>();
			modelBuilder.Entity<DBNodeSettings>();
			modelBuilder.Entity<DBNodeScheduleSettings>();
		}
	}
}
