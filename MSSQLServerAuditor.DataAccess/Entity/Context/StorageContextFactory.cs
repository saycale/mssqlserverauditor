using System.Data.Common;
using MSSQLServerAuditor.DataAccess.Connections;
using MSSQLServerAuditor.DataAccess.Entity.Interfaces;
using MSSQLServerAuditor.DataAccess.Scope.Interfaces;

namespace MSSQLServerAuditor.DataAccess.Entity.Context
{
	public class StorageContextFactory : IDbContextFactory<StorageContext>
	{
		private readonly IConnectionFactory         _connectionFactory;
		private readonly IStorageInitializerFactory _initializerFactory;
		private readonly string                     _connectionString;

		public StorageContextFactory(
			IConnectionFactory         connectionFactory,
			IStorageInitializerFactory initializerFactory,
			string                     connectionString
		)
		{
			this._connectionFactory  = connectionFactory;
			this._initializerFactory = initializerFactory;
			this._connectionString   = connectionString;
		}

		public StorageContext CreateDbContext()
		{
			DbConnection connection = CreateDbContextConnection();

			return new StorageContext(
				this._initializerFactory,
				connection
			);
		}

		public DbConnection CreateDbContextConnection()
		{
			return this._connectionFactory.CreateConnection(this._connectionString);
		}
	}
}
