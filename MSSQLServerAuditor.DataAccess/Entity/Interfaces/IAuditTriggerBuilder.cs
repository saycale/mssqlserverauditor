﻿using System.Data.Entity;

namespace MSSQLServerAuditor.DataAccess.Entity.Interfaces
{
	public interface IAuditTriggerBuilder
	{
		void CreateTriggers(Database database);
	}
}