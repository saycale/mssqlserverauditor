using System.Data.Entity;
using MSSQLServerAuditor.DataAccess.Entity.Context;

namespace MSSQLServerAuditor.DataAccess.Entity.Interfaces
{
	public interface IStorageInitializerFactory
	{
		IDatabaseInitializer<StorageContext> CreateInitializer(DbModelBuilder modelBuilder);
	}
}