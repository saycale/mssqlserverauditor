using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using MSSQLServerAuditor.DataAccess.Entity.Interfaces;
using MSSQLServerAuditor.DataAccess.Entity.Models;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity
{
	public abstract class AuditTriggerBuilder : IAuditTriggerBuilder
	{
		protected abstract string GetDateUpdatedTriggerSql(string tableName, string keyColumnName);
		protected abstract string GetDateCreatedTriggerSql(string tableName, string keyColumnName);

		public void CreateTriggers(Database db)
		{
			CreateTrigger<DBConnectionGroup>(db);
			CreateTrigger<DBDynamicConnection>(db);
			CreateTrigger<DBGroupServer>(db);
			CreateTrigger<DBConnectionType>(db);
			CreateTrigger<DBServerInstance>(db);
			CreateTrigger<DBLastConnection>(db);
			CreateTrigger<DBLogin>(db);
			CreateTrigger<DBMetaResult>(db);
			CreateTrigger<DBMetaSubResult>(db);
			CreateTrigger<DBMetaEtlResult>(db);
			CreateTrigger<DBNodeInstance>(db);
			CreateTrigger<DBNodeAttribute>(db);

			CreateTrigger<DBQuery>(db);
			CreateTrigger<DBQueryGroup>(db);
			CreateTrigger<DBQueryGroupParameter>(db);
			CreateTrigger<DBQueryParameter>(db);
			CreateTrigger<DBQueryResults>(db);
			CreateTrigger<DBQueryResultTable>(db);

			CreateTrigger<DBTemplate>(db);
			CreateTrigger<DBTemplateNode>(db);
			CreateTrigger<DBTemplateQuery>(db);
			CreateTrigger<DBTemplateQueryGroup>(db);
			CreateTrigger<DBTemplateQueryGroupParameter>(db);
			CreateTrigger<DBTemplateQueryParameter>(db);
			CreateTrigger<DBNodeScheduleSettings>(db);
			CreateTrigger<DBNodeSettings>(db);
		}

		public void CreateTrigger<TEntity>(Database database) where TEntity : AuditableEntity
		{
			Type modelType = typeof(TEntity);

			// get table name for EF model from Table attribute
			TableAttribute tableAttribute = modelType
				.GetCustomAttributes(typeof(TableAttribute), true)
				.FirstOrDefault() as TableAttribute;

			if (tableAttribute != null)
			{
				string tableName = tableAttribute.Name;

				// get property with Key attribute
				PropertyInfo keyProperty = modelType.GetProperties().FirstOrDefault(
					prop => Attribute.IsDefined(prop, typeof(KeyAttribute)));

				if (keyProperty != null)
				{
					// get column name of Key property
					ColumnAttribute columnAttribute = keyProperty
						.GetCustomAttributes(typeof(ColumnAttribute), false)
						.FirstOrDefault() as ColumnAttribute;

					string keyColumnName = keyProperty.Name + "_id";

					if (columnAttribute != null)
					{
						keyColumnName = columnAttribute.Name;
					}

					string sqlUpdateTrigger = GetDateUpdatedTriggerSql(
						tableName,
						keyColumnName
					);

					database.ExecuteSqlCommand(sqlUpdateTrigger);

					string sqlInsertTrigger = GetDateCreatedTriggerSql(
						tableName,
						keyColumnName
					);

					database.ExecuteSqlCommand(sqlInsertTrigger);
				}
			}
		}
	}
}
