using System.Collections.Concurrent;
using MSSQLServerAuditor.DataAccess.Entity.Models.Contract;

namespace MSSQLServerAuditor.DataAccess.Entity
{
	public class EntityCache : ConcurrentDictionary<string, IEntity>
	{
	}
}
