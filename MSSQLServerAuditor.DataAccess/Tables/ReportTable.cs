using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using MSSQLServerAuditor.DataAccess.Commands;
using MSSQLServerAuditor.DataAccess.Extensions;
using MSSQLServerAuditor.DataAccess.Structure;
using Perceiveit.Data.Query;

namespace MSSQLServerAuditor.DataAccess.Tables
{
	public class ReportTable : Table
	{
		public ReportTable(Storage storage, TableInfo tableInfo) : base(storage, tableInfo)
		{
		}

		public long? InsertOrUpdateRow(
			DbConnection connection,
			ITableRow    row
		)
		{
			List<DbCommandParameter> parameters;
			DBSelectQuery            selectQuery = GetSelectRowQuery(row, out parameters).TopN(1);
			List<ITableRow>          result      = GetRows(connection, selectQuery, parameters);
			long?                    identity;

			if (result != null && result.Count > 0)
			{
				ITableRow existRow = result.First();

				if (row.CopyValues(existRow))
				{
					UpdateRow(connection, existRow);
				}

				identity = (long?) existRow.Values[IdentityField];
			}
			else
			{
				lock (GetTableLock())
				{
					result = this.GetRows(connection, selectQuery, parameters);

					if (result != null && result.Count > 0)
					{
						ITableRow existRow = result.First();
						identity = (long?) existRow.Values[IdentityField];
					}
					else
					{
						identity = AddRow(connection, row);
					}
				}
			}

			return identity;
		}

		private void UpdateRow(DbConnection connection, ITableRow row)
		{
			Storage.DbDatabase.ExecuteOnConnection(connection, context =>
			{
				UpdateRowCommand updateCommand = new UpdateRowCommand(
					context,
					this.TableInfo,
					this.IdentityField
				);

				updateCommand.AddRowToUpdate(row);

				return updateCommand.Execute(Storage.CommandExecutor);
			});
		}

		private long? AddRow(DbConnection connection, ITableRow row)
		{
			long? rowId = null;

			Storage.DbDatabase.Execute(context =>
			{
				InsertRowCommand command = new InsertRowCommand(context, TableInfo);
				command.AddRowToInsert(row);

				return command.Execute(Storage.CommandExecutor);
			});

			if (row != null)
			{
				rowId = this.GetRow(connection, row);
			}

			return rowId;
		}

		private long? GetRow(DbConnection connection, ITableRow row)
		{
			List<DbCommandParameter> parameters;
			DBSelectQuery            selectQuery = GetSelectRowQuery(row, out parameters).TopN(1);
			List<ITableRow>          rows      = GetRows(connection, selectQuery, parameters);

			if (rows.Count != 0)
			{
				return (long?) rows[0].Values[IdentityField];
			}

			return null;
		}

		private List<ITableRow> GetRows(
			DbConnection             connection,
			DBSelectQuery            selectQuery,
			List<DbCommandParameter> parameters
		)
		{
			return Storage.DbDatabase.ExecuteOnConnection(connection, context =>
			{
				SelectRowsCommand commandSelect = new SelectRowsCommand(
					context,
					selectQuery,
					this.TableInfo,
					parameters
				);

				return commandSelect.Execute(Storage.CommandExecutor);
			});
		}
	}
}
