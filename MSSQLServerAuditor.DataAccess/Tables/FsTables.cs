namespace MSSQLServerAuditor.DataAccess.Tables
{
	public class FsTables
	{
		public FsFileTable   FileTable   { get; private set; }
		public FsFolderTable FolderTable { get; private set; }

		public static FsTables CreateTables(Storage dbfs, long connectionId)
		{
			string folderTableName = string.Format("tFolder_{0}", connectionId);
			string fileTableName   = string.Format("tFile_{0}",   connectionId);

			return new FsTables(dbfs, fileTableName, folderTableName);
		}

		private FsTables(Storage dbfs, string fileTableName, string folderTableName)
		{
			FsFolderTable folderTable = new FsFolderTable(
				dbfs,
				folderTableName
			);

			FsFileTable fileTable = new FsFileTable(
				dbfs,
				fileTableName,
				folderTableName
			);

			folderTable.CreateIfNotExists();
			fileTable.CreateIfNotExists();

			FolderTable = folderTable;
			FileTable   = fileTable;
		}
	}
}
