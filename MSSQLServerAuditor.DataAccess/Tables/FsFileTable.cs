using System.Collections.Generic;
using MSSQLServerAuditor.Common.Extensions;
using MSSQLServerAuditor.DataAccess.Commands;
using MSSQLServerAuditor.DataAccess.Extensions;
using MSSQLServerAuditor.DataAccess.Structure;

namespace MSSQLServerAuditor.DataAccess.Tables
{
	public class FsFileTable : Table
	{
		public const string FieldId       = "Id";
		public const string FieldFileName = "FileName";
		public const string FieldContent  = "Content";

		private readonly string _folderTableName;

		public FsFileTable(
			Storage fsStorage,
			string  fileTableName,
			string  folderTableName
		) : base(
			fsStorage,
			CreateTableInfo(fileTableName, folderTableName)
		)
		{
			this._folderTableName = folderTableName;
		}

		protected override string IdentityField
		{
			get { return FieldId; }
		}

		private static TableInfo CreateTableInfo(string fileTableName, string folderTableName)
		{
			TableInfo tableInfo = new TableInfo(fileTableName)
				.AddBigIntField(FieldId,                FieldInfoFlag.Identity | FieldInfoFlag.IsNotNull)
				.AddBigIntField(folderTableName.AsFk(), FieldInfoFlag.Unique)
				.AddStringField(FieldFileName,          FieldInfoFlag.Unique | FieldInfoFlag.IsNotNull)
				.AddBinaryField(FieldContent)
				.SetPrimaryKey(fileTableName + "_key", FieldId);

			return tableInfo;
		}

		public void WriteFile(byte[] raw, long? folderId, string file)
		{
			ITableRow row = this.NewRow();

			row.Values.Add(this._folderTableName.AsFk(), folderId);
			row.Values.Add(FieldFileName, file);
			row.Values.Add(FieldContent, raw);

			InsertOrUpdateRow(row, true);
		}

		public byte[] ReadFile(long? folderId, string file)
		{
			List<DbCommandParameter> cmdParams = new List<DbCommandParameter>
				{
					new DbCommandParameter(folderId, GetField(this._folderTableName.AsFk())),
					new DbCommandParameter(file, GetField(FieldFileName))
				};

			return Storage.DbDatabase.Execute(context =>
			{
				GetValueCommand<byte[]> getFileCommand = new GetValueCommand<byte[]>(
					context, TableInfo, FieldContent, cmdParams);

				return getFileCommand.Execute(Storage.CommandExecutor);
			});
		}
	}
}