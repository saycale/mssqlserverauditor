﻿using System.IO;
using MSSQLServerAuditor.DataAccess.Structure;

namespace MSSQLServerAuditor.DataAccess.Tables
{
	public class FsFolderTable : Table
	{
		public const string FieldId             = "Id";
		public const string FieldFolderName     = "FolderName";
		public const string FieldParentFolderId = "ParentFolderId";

		public FsFolderTable(
			Storage fsStorage, 
			string  folderTableName)
			: base(fsStorage, CreateTableInfo(folderTableName))
		{
		}

		protected override string IdentityField
		{
			get { return FieldId; }
		}

		private static TableInfo CreateTableInfo(string folderTableName)
		{
			TableInfo tableInfo = new TableInfo(folderTableName)
				.AddBigIntField(FieldId,             FieldInfoFlag.Identity | FieldInfoFlag.IsNotNull)
				.AddStringField(FieldFolderName,     FieldInfoFlag.Unique)
				.AddBigIntField(FieldParentFolderId, FieldInfoFlag.Unique)
				.SetPrimaryKey(folderTableName + "_key", FieldId);

			return tableInfo;
		}		

		public long? GetFolderId(string path)
		{
			string[] folders = path.Split(Path.DirectorySeparatorChar);
			long? result     = null;
			ITableRow row    = this.NewRow();

			row.Values.Add(FieldParentFolderId, null);
			row.Values.Add(FieldFolderName,     null);

			foreach (string folder in folders)
			{
				row.Values[FieldParentFolderId] = result;
				row.Values[FieldFolderName]     = folder;

				result = InsertOrUpdateRow(row);
			}

			return result;
		}
	}
}