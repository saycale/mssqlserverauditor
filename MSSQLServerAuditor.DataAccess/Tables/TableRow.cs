using System;
using System.Collections.Generic;
using System.Data.Common;
using MSSQLServerAuditor.DataAccess.Structure;

namespace MSSQLServerAuditor.DataAccess.Tables
{
	public class TableRow : ITableRow
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="tableInfo">The definition of the table</param>
		public TableRow(TableInfo tableInfo)
		{
			this.TableInfo = tableInfo;
			this.Values    = new Dictionary<string, object>();
		}

		public TableInfo TableInfo { get; private set; }

		/// <summary>
		/// Values of row
		/// </summary>
		public Dictionary<string, object> Values { get; private set; }

		/// <summary>
		/// Read rows
		/// </summary>
		/// <param name="tableInfo">Table definition</param>
		/// <param name="reader">Data reader</param>
		/// <returns></returns>
		public static ITableRow Read(TableInfo tableInfo, DbDataReader reader)
		{
			ITableRow    result = new TableRow(tableInfo);
			List<string> fields = new List<string>();

			for (int i = 0; i < reader.FieldCount; i++)
			{
				fields.Add(reader.GetName(i));
			}

			foreach (FieldInfo fieldInfo in result.TableInfo.Fields)
			{
				string fieldName = fieldInfo.Name;

				if (fields.Contains(fieldName))
				{
					result.Values.Add(fieldName, reader[fieldName]);
				}
			}

			return result;
		}

		/// <summary>
		/// Copy values to new row
		/// </summary>
		/// <param name="dest">Destination row</param>
		public bool CopyValues(ITableRow dest)
		{
			bool hasChanges = false;

			foreach (KeyValuePair<string, object> pair in this.Values)
			{
				object existingValue;

				if (dest.Values.TryGetValue(pair.Key, out existingValue))
				{
					hasChanges = hasChanges || !this.AreEqual(existingValue, pair.Value);

					dest.Values[pair.Key] = pair.Value;
				}
				else
				{
					dest.Values.Add(pair.Key, pair.Value);
				}
			}

			return hasChanges;
		}

		private bool AreEqual(object value1, object value2)
		{
			if ((value1 is DBNull && value2 == null) || (value2 is DBNull && value1 == null))
			{
				return true;
			}

			return Equals(value1, value2);
		}

		public T GetValue<T>(string name, T defaultValue = default(T))
		{
			if (this.Values.ContainsKey(name))
			{
				object value = this.Values[name];

				if (value is T)
				{
					return (T) value;
				}
			}

			return defaultValue;
		}

		public void SetValue(string name, object value)
		{
			this.Values[name] = value;
		}
	}
}
