using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using MSSQLServerAuditor.DataAccess.Commands;
using MSSQLServerAuditor.DataAccess.Extensions;
using MSSQLServerAuditor.DataAccess.Structure;
using Perceiveit.Data;
using Perceiveit.Data.Query;

namespace MSSQLServerAuditor.DataAccess.Tables
{
	public class Table
	{
		protected const string DefaultIdentityField = "rowid";

		private static readonly ConcurrentDictionary<string, object> TableLocks;

		static Table()
		{
			TableLocks = new ConcurrentDictionary<string, object>();
		}

		protected readonly object _globalLock = new object();

		public Table(Storage storage, TableInfo tableInfo)
		{
			this.Storage   = storage;
			this.TableInfo = tableInfo;
		}

		public Storage   Storage   { get; private set; }
		public TableInfo TableInfo { get; private set; }

		protected virtual string IdentityField
		{
			get { return DefaultIdentityField; }
		}

		/// <summary>
		/// Table lock
		/// </summary>
		/// <returns></returns>
		protected object GetTableLock()
		{
			return TableLocks.GetOrAdd(TableInfo.Name, name => name);
		}

		/// <summary>
		/// Upgrade table according to table definition
		/// </summary>
		public void DropAndCreate()
		{
			lock (this._globalLock)
			{
				DropTable();
				CreateTable();
			}
		}

		/// <summary>
		/// Create table according to table definition
		/// </summary>
		public void CreateIfNotExists()
		{
			lock (this._globalLock)
			{
				CreateTable();
			}
		}

		/// <summary>
		/// Find or create row according values
		/// </summary>
		/// <param name="row">Row to search or create</param>		
		/// <param name="updateOnConcurrentInsert">True means that the method should execute update on concurrent insert</param>
		/// <returns>row id</returns>
		public long? InsertOrUpdateRow(
			ITableRow row,
			bool      updateOnConcurrentInsert = false
		)
		{
			List<DbCommandParameter> parameters;
			DBSelectQuery            selectQuery = GetSelectRowQuery(row, out parameters).TopN(1);
			List<ITableRow>          result      = this.GetRows(selectQuery, parameters);
			long?                    identity;

			if (result != null && result.Count > 0)
			{
				ITableRow existRow = result.First();

				if (row.CopyValues(existRow))
				{
					UpdateRow(existRow);
				}

				identity = (long?) existRow.Values[IdentityField];
			}
			else
			{
				lock (GetTableLock())
				{
					result = this.GetRows(selectQuery, parameters);

					if (result != null && result.Count > 0)
					{
						ITableRow existRow = result.First();

						if (updateOnConcurrentInsert)
						{
							if (row.CopyValues(existRow))
							{
								UpdateRow(existRow);
							}
						}

						identity = (long?) existRow.Values[IdentityField];
					}
					else
					{
						identity = AddRow(row);
					}
				}
			}

			return identity;
		}

		/// <summary>
		/// Add rows to table
		/// </summary>
		/// <param name="rows">Row collection</param>
		/// <returns></returns>
		public long? AddRows(IEnumerable<ITableRow> rows)
		{
			long?     lastRowId = null;
			ITableRow lastRow   = null;

			Storage.DbDatabase.Execute(context =>
			{
				InsertRowCommand command = new InsertRowCommand(context, TableInfo);

				foreach (ITableRow row in rows)
				{
					command.AddRowToInsert(row);

					lastRow = row;
				}

				return command.Execute(Storage.CommandExecutor);
			});

			if (lastRow != null)
			{
				lastRowId = this.GetRow(lastRow);
			}

			return lastRowId;
		}

		/// <summary>
		/// Get id of a row
		/// </summary>
		/// <param name="row">Row to find</param>
		/// <returns>Row id</returns>
		protected long? GetRow(ITableRow row)
		{
			List<DbCommandParameter> parameters;
			DBSelectQuery            selectQuery = this.GetSelectRowQuery(row, out parameters).TopN(1);
			List<ITableRow>          rows = GetRows(selectQuery, parameters);

			if (rows.Count != 0)
			{
				return (long?) rows[0].Values[IdentityField];
			}

			return null;
		}

		/// <summary>
		/// Add row to table
		/// </summary>
		/// <param name="row">Row to add</param>
		/// <returns></returns>
		public long? AddRow(ITableRow row)
		{
			return AddRows(new[] { row });
		}

		/// <summary>
		/// Update one row by row identity
		/// </summary>
		/// <param name="row">Row to update</param>
		public void UpdateRow(ITableRow row)
		{
			UpdateRows(new[] { row });
		}

		/// <summary>
		/// Update table: row by row ids
		/// </summary>
		/// <param name="rows">Rows to update</param>
		public void UpdateRows(IEnumerable<ITableRow> rows)
		{
			Storage.DbDatabase.Execute(context =>
			{
				UpdateRowCommand updateCommand = new UpdateRowCommand(
					context,
					this.TableInfo,
					this.IdentityField
				);

				foreach (ITableRow row in rows)
				{
					updateCommand.AddRowToUpdate(row);
				}

				return updateCommand.Execute(Storage.CommandExecutor);
			});
		}

		/// <summary>
		/// Prepare the query to the table data
		/// </summary>
		/// <param name="row">Table row</param>
		/// <param name="parameters">Select query parameters</param>
		/// <returns></returns>
		protected DBSelectQuery GetSelectRowQuery(ITableRow row, out List<DbCommandParameter> parameters)
		{
			DBSelectQuery      selectQuery  = DBQuery.SelectAll().From(TableInfo.Name);
			List<DBComparison> whereClauses = new List<DBComparison>();

			parameters = new List<DbCommandParameter>();

			foreach (FieldInfo field in this.TableInfo.Fields)
			{
				if (field.IsUnique)
				{
					string fieldName = field.Name;
					object value     = row.Values[fieldName];

					whereClauses.Add(GetWhereClause(fieldName, value));
					parameters.Add(new DbCommandParameter(value, field));
				}
			}

			if (whereClauses.Count == 0)
			{
				foreach (IndexInfo index in this.TableInfo.Indexes)
				{
					if (index.IsUnique)
					{
						foreach (string fieldName in index.FieldNames)
						{
							FieldInfo field = this.TableInfo.Fields.Find(fi => fi.Name == fieldName);

							if (field != null)
							{
								object value = row.Values[fieldName];

								whereClauses.Add(GetWhereClause(fieldName, value));
								parameters.Add(new DbCommandParameter(value, field));
							}
						}
					}
				}
			}

			if (whereClauses.Count > 0)
			{
				selectQuery.WhereAll(whereClauses.ToArray());
			}

			return selectQuery;
		}

		private DBComparison GetWhereClause(string fieldName, object value)
		{
			if (value != null)
			{
				return DBComparison.Compare(
					DBField.Field(fieldName),
					Compare.Equals,
					DBParam.Param(fieldName)
				);
			}

			return DBComparison.Compare(
				DBField.Field(fieldName),
				Compare.Is,
				DBConst.Null()
			);
		}

		/// <summary>
		/// Get rows from table
		/// </summary>		
		/// <param name="selectQuery">SQL clause</param>
		/// <param name="parameters">SQL parameters</param>		
		/// <returns></returns>
		public List<ITableRow> GetRows(
			DBSelectQuery            selectQuery,
			List<DbCommandParameter> parameters = null
		)
		{
			return Storage.DbDatabase.Execute(context =>
			{
				SelectRowsCommand commandSelect = new SelectRowsCommand(
					context,
					selectQuery,
					this.TableInfo,
					parameters
				);

				return commandSelect.Execute(Storage.CommandExecutor);
			});
		}

		protected bool TableExists(DBDatabase database)
		{
			return Storage.DbDatabase.Execute(context =>
				new CheckTableExistsCommand(context, TableInfo.Name).Execute(Storage.CommandExecutor));
		}

		protected void CreateTable()
		{
			Storage.DbDatabase.Execute(context =>
			{
				CreateTableCommand command = new CreateTableCommand(
					context,
					this.TableInfo
				);

				return command.Execute(Storage.CommandExecutor);
			});
		}

		protected void DropTable()
		{
			Storage.DbDatabase.Execute(context =>
			{
				DropTableCommand command = new DropTableCommand(
					context,
					this.TableInfo.Name
				);

				return command.Execute(Storage.CommandExecutor);
			});
		}

		/// <summary>
		/// Create new row
		/// </summary>
		protected ITableRow NewRow()
		{
			return new TableRow(this.TableInfo);
		}

		protected FieldInfo GetField(string name)
		{
			return TableInfo.Fields.Find(fi => fi.Name == name);
		}
	}
}
