using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using MSSQLServerAuditor.DataAccess.Entity.Context;
using MSSQLServerAuditor.DataAccess.Exceptions;
using NLog;
using SQLite.CodeFirst;

namespace MSSQLServerAuditor.DataAccess.Vendors.Sqlite
{
	public class SqliteStorageInitializer : SqliteInitializerBase<StorageContext>
	{
		private readonly Type _historyEntityType = typeof(History);

		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly List<string> _initScripts;

		public SqliteStorageInitializer(
			DbModelBuilder modelBuilder,
			List<string>   initScripts
		) : base(modelBuilder)
		{
			this._initScripts = initScripts;

			ModelBuilder.RegisterEntityType(this._historyEntityType);
		}

		public override void InitializeDatabase(StorageContext context)
		{
			string databaseFilePath = GetDatabasePathFromContext(context);

			bool dbExists = File.Exists(databaseFilePath);
			if (dbExists)
			{
				if (IsSameModel(context))
				{
					return;
				}

				string dbName = context.Database.Connection.Database;
				throw new IncompatibleDatabaseException(
					dbName,
					string.Format(Errors.IncompatibleDatabase, dbName)
				);
			}

			base.InitializeDatabase(context);
			SaveHistory(context);
		}

		protected override void Seed(StorageContext context)
		{
			SqliteAuditTriggerBuilder triggerBuilder = new SqliteAuditTriggerBuilder();

			triggerBuilder.CreateTriggers(context.Database);

			foreach (string script in this._initScripts)
			{
				try
				{
					context.Database.ExecuteSqlCommand(script);

					Log.Info("Current storage initialization script executed successfully");
				}
				catch (Exception exc)
				{
					string errorMessage = "Failed to execute current storage initialization script";

					Log.Error(exc, errorMessage + ": " + script);
				}
			}
		}

		private void SaveHistory(StorageContext context)
		{
			string   hash    = GetHashFromModel(context.Database.Connection);
			IHistory history = GetHistoryRecord(context);

			EntityState entityState;

			if (history == null)
			{
				history = (IHistory) Activator.CreateInstance(this._historyEntityType);
				entityState = EntityState.Added;
			}
			else
			{
				entityState = EntityState.Modified;
			}

			history.Context    = context.GetType().FullName;
			history.Hash       = hash;
			history.CreateDate = DateTime.UtcNow;

			context.Set(this._historyEntityType).Attach(history);
			context.Entry(history).State = entityState;
			context.SaveChanges();
		}

		private bool IsSameModel(StorageContext context)
		{
			string hash = GetHashFromModel(context.Database.Connection);

			try
			{
				IHistory history = GetHistoryRecord(context);
				return history?.Hash == hash;
			}
			catch (Exception)
			{
				// This happens if the history table does not exist.
				// So it covers also the case with a null byte file (see SqliteCreateDatabaseIfNotExists).
				return false;
			}
		}

		private IHistory GetHistoryRecord(StorageContext context)
		{
			DbQuery dbQuery = context.Set(this._historyEntityType).AsNoTracking();
			IEnumerable<IHistory> records = Enumerable.Cast<IHistory>(dbQuery);
			return records.SingleOrDefault();
		}

		private string GetHashFromModel(DbConnection connection)
		{
			string sql = GetSqlFromModel(connection);
			return CreateHash(sql);
		}

		private string GetSqlFromModel(DbConnection connection)
		{
			DbModel model = ModelBuilder.Build(connection);
			SqliteSqlGenerator sqliteSqlGenerator = new SqliteSqlGenerator();

			return sqliteSqlGenerator.Generate(model.StoreModel);
		}

		private static string CreateHash(string data)
		{
			byte[] dataBytes = Encoding.ASCII.GetBytes(data);
			using (SHA512 sha512 = new SHA512Managed())
			{
				byte[] hashBytes = sha512.ComputeHash(dataBytes);
				return Convert.ToBase64String(hashBytes);
			}
		}
	}
}
