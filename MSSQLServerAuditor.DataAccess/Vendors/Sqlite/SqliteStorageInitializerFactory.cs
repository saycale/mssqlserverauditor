using System.Collections.Generic;
using System.Data.Entity;
using MSSQLServerAuditor.DataAccess.Entity.Context;
using MSSQLServerAuditor.DataAccess.Entity.Interfaces;

namespace MSSQLServerAuditor.DataAccess.Vendors.Sqlite
{
	public class SqliteStorageInitializerFactory : IStorageInitializerFactory
	{
		public SqliteStorageInitializerFactory(List<string> initScripts)
		{
			this.InitScripts = initScripts;
		}

		public List<string> InitScripts { get; private set; }

		public IDatabaseInitializer<StorageContext> CreateInitializer(DbModelBuilder modelBuilder)
		{
			return new SqliteStorageInitializer(modelBuilder, InitScripts);
		}
	}
}
