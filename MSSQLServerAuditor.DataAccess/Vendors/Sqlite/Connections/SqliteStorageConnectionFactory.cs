using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using System.Linq;
using MSSQLServerAuditor.DataAccess.Commands;
using MSSQLServerAuditor.DataAccess.Commands.Interfaces;
using MSSQLServerAuditor.DataAccess.Connections;
using MSSQLServerAuditor.DataAccess.Vendors.Sqlite.Commands;
using NLog;

namespace MSSQLServerAuditor.DataAccess.Vendors.Sqlite.Connections
{
	public class SqliteStorageConnectionFactory : IStorageConnectionFactory
	{
		private static readonly Logger Log = LogManager.GetCurrentClassLogger();

		private readonly string                     _connectionString;
		private readonly SqliteConnectionFactory    _factory;
		private readonly Dictionary<string, string> _databasesToAttach;
		private readonly ICommandExecutor           _commandExecutor;

		public SqliteStorageConnectionFactory(
			string                     connectionString,
			Dictionary<string, string> databasesToAttach
		)
		{
			this._connectionString  = connectionString;
			this._factory           = new SqliteConnectionFactory();
			this._databasesToAttach = databasesToAttach;
			this._commandExecutor   = new SqliteCommandExecutor();
		}

		public DbConnection CreateOpenedConnection()
		{
			DbConnection connection = this._factory.CreateOpenedConnection(
				this._connectionString
			);

			if (this._databasesToAttach.Any())
			{
				AttachDatabases(connection, this._databasesToAttach);
			}

			return connection;
		}

		private void AttachDatabases(DbConnection connection, Dictionary<string, string> databasesToAttach)
		{
			List<string> attachedDbs = GetAttachedDatabases(connection);

			foreach (KeyValuePair<string, string> attachDatabase in databasesToAttach)
			{
				if (!attachedDbs.Contains(attachDatabase.Value, StringComparer.OrdinalIgnoreCase))
				{
					AttachDatabase(connection, attachDatabase.Key, attachDatabase.Value);
				}
			}
		}

		private List<string> GetAttachedDatabases(DbConnection connection)
		{
			const string sqlAttachedDatabases = "PRAGMA database_list;";
			List<string> attachedDbs          = new List<string>();

			SelectReaderCommand command = new SelectReaderCommand(
				connection,
				sqlAttachedDatabases,
				reader =>
				{
					while (reader.Read())
					{
						string attachedDb = (reader.GetValue(1)).ToString();
						attachedDbs.Add(attachedDb);
					}
				},
				null
			);

			command.Execute(this._commandExecutor);

			return attachedDbs;
		}

		private void AttachDatabase(DbConnection connection, string databaseFileName, string databaseAlias)
		{
			string sql = $"ATTACH '{databaseFileName}' AS '{databaseAlias}';";

			try
			{
				DbCommand attachCommand = connection.CreateCommand();

				attachCommand.CommandText = sql;

				attachCommand.ExecuteNonQuery();
			}
			catch (SQLiteException exc)
			{
				Log.Error($"AttachDb: FileName'{databaseFileName}', dbName:'{databaseAlias}', sql:'{sql}', Error:'{exc}'");
			}
		}
	}
}
