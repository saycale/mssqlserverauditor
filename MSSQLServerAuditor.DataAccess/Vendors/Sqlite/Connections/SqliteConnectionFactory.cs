using System;
using System.Data.Common;
using MSSQLServerAuditor.DataAccess.Connections;
using NLog;

namespace MSSQLServerAuditor.DataAccess.Vendors.Sqlite.Connections
{
	public class SqliteConnectionFactory : IConnectionFactory
	{
		private static readonly Logger            Log = LogManager.GetCurrentClassLogger();
		private const           string            SqliteProviderName = "System.Data.SQLite";
		private readonly        DbProviderFactory _factory;

		public SqliteConnectionFactory()
		{
			this._factory = DbProviderFactories.GetFactory(SqliteProviderName);
		}

		public DbConnection CreateConnection(string connectionString)
		{
			DbConnection connection = this._factory.CreateConnection();

			if (connection != null)
			{
				connection.ConnectionString = connectionString;

				return connection;
			}

			throw new ApplicationException(Errors.FailedCreateSqliteConnection);
		}

		public DbConnection CreateOpenedConnection(string connectionString)
		{
			DbConnection connection = CreateConnection(connectionString);

			connection.Open();

			return connection;
		}
	}
}
