using MSSQLServerAuditor.DataAccess.Commands;
using MSSQLServerAuditor.DataAccess.Commands.Interfaces;

namespace MSSQLServerAuditor.DataAccess.Vendors.Mssql.Commands
{
	internal class MssqlCommandExecutor : ICommandExecutor
	{
		public TResult Execute<TResult>(CommandBase<TResult> command)
		{
			return command.Execute();
		}
	}
}
