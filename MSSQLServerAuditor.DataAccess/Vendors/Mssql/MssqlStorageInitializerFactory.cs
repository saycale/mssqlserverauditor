using System.Collections.Generic;
using System.Data.Entity;
using MSSQLServerAuditor.DataAccess.Entity.Context;
using MSSQLServerAuditor.DataAccess.Entity.Interfaces;

namespace MSSQLServerAuditor.DataAccess.Vendors.Mssql
{
	public class MssqlStorageInitializerFactory : IStorageInitializerFactory
	{
		public MssqlStorageInitializerFactory(List<string> initScripts)
		{
			this.InitScripts = initScripts;
		}

		public List<string> InitScripts { get; private set; }

		public IDatabaseInitializer<StorageContext> CreateInitializer(DbModelBuilder modelBuilder)
		{
			return new MssqlStorageInitializer(InitScripts);
		}
	}
}
