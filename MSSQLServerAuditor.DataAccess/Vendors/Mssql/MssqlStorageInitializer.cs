using System;
using System.Collections.Generic;
using System.Data.Entity;
using MSSQLServerAuditor.DataAccess.Entity.Context;
using MSSQLServerAuditor.DataAccess.Exceptions;
using NLog;

namespace MSSQLServerAuditor.DataAccess.Vendors.Mssql
{
	public class MssqlStorageInitializer : CreateDatabaseIfNotExists<StorageContext>
	{
		private static readonly Logger       Log = LogManager.GetCurrentClassLogger();
		private readonly        List<string> _initScripts;

		public MssqlStorageInitializer(List<string> initScripts)
		{
			this._initScripts = initScripts;
		}

		public override void InitializeDatabase(StorageContext context)
		{
			Database database = context.Database;
			if (database.Exists())
			{
				if (!database.CompatibleWithModel(false))
				{
					string dbName = database.Connection.Database;
					throw new IncompatibleDatabaseException(
						dbName,
						string.Format(Errors.IncompatibleDatabase, dbName)
					);
				}
			}

			base.InitializeDatabase(context);
		}

		protected override void Seed(StorageContext context)
		{
			MssqlAuditTriggerBuilder triggerBuilder = new MssqlAuditTriggerBuilder();

			triggerBuilder.CreateTriggers(context.Database);

			foreach (string script in this._initScripts)
			{
				try
				{
					context.Database.ExecuteSqlCommand(script);

					Log.Info("Current storage initialization script executed successfully");
				}
				catch (Exception exc)
				{
					string errorMessage = "Failed to execute current storage initialization script";

					Log.Error(exc, errorMessage + ": " + script);
				}
			}
		}
	}
}
