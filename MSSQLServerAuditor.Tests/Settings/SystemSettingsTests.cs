﻿using System.Collections.Generic;
using System.IO;
using MSSQLServerAuditor.Core.Domain;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Core.Xml;
using MSSQLServerAuditor.Tests.Properties;
using NUnit.Framework;

namespace MSSQLServerAuditor.Tests.Settings
{
	[TestFixture]
	public class SystemSettingsTests
	{
		private readonly string _systemSettingsPath = Path.Combine(
			Path.GetTempPath(), "SystemSettings.xml");

		[SetUp]
		public void Prepare()
		{
			using (StreamWriter writer = File.CreateText(this._systemSettingsPath))
			{
				writer.Write(Resources.SystemSettings);
			}
		}

		[Test]
		public void DeserializeSystemSettings()
		{
			SystemSettings systemSettings = XmlSerialization.Deserialize<SystemSettings>(
				this._systemSettingsPath
			);

			Assert.NotNull(systemSettings);

			Assert.AreEqual(2, systemSettings.UiLanguages.Count);
			Assert.AreEqual("en", systemSettings.UiLanguages[0]);
			Assert.AreEqual("ru", systemSettings.UiLanguages[1]);

			Assert.AreEqual(2, systemSettings.ReportLanguages.Count);
			Assert.AreEqual("en", systemSettings.ReportLanguages[0]);
			Assert.AreEqual("ru", systemSettings.ReportLanguages[1]);

			Assert.AreEqual(2, systemSettings.WindowTitles.Count);
			Assert.IsNotEmpty(systemSettings.WindowTitles[0].Text);
			Assert.IsNotEmpty(systemSettings.WindowTitles[1].Text);

			List<ConnectionTypeInfo> types = systemSettings.ConnectionTypes;
			Assert.AreEqual(8, types.Count);

			ConnectionTypeInfo eventLogConnection = types.Find(info => info.Id.Equals("EventLog"));
			Assert.AreEqual("EventLog",       eventLogConnection.Id);
			Assert.AreEqual("Event Log",      eventLogConnection.Title[0].Text);
			Assert.AreEqual("Журнал событий", eventLogConnection.Title[1].Text);
			Assert.AreEqual(1,                eventLogConnection.ModuleTypes.Count);
			Assert.AreEqual("EventLog",       eventLogConnection.ModuleTypes[0].Id);
			Assert.AreEqual("Event Log",      eventLogConnection.ModuleTypes[0].Title.Find(r => r.Language.Equals("en")).Text);

			Assert.AreEqual("support@mssqlserverauditor.com", systemSettings.SupportEmail);
			Assert.AreEqual("http://www.mssqlserverauditor.com/", systemSettings.SupportUrl);

			Assert.AreEqual("Modules", systemSettings.TemplateDirectory);
			Assert.IsNotEmpty(systemSettings.ScriptsDirectory);
			Assert.IsNotEmpty(systemSettings.PostBuildCurrentDbScript);

			Assert.AreEqual(4, systemSettings.PostBuildScripts.Count);
			Assert.AreEqual("ref", systemSettings.PostBuildScripts[0].Alias);
			Assert.IsNotEmpty(systemSettings.PostBuildScripts[0].File);

			Assert.AreEqual(120, systemSettings.ServiceDataUpdateTimeout);
		}

		[TearDown]
		public void CleanUp()
		{
			FileUtils.DeleteIfExists(this._systemSettingsPath);
		}
	}
}
