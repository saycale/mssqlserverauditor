﻿using System.IO;
using MSSQLServerAuditor.Core.Settings;
using MSSQLServerAuditor.Core.Xml;
using MSSQLServerAuditor.Tests.Properties;
using NUnit.Framework;

namespace MSSQLServerAuditor.Tests.Settings
{
	[TestFixture]
	public class UserSettingsTests
	{
		private readonly string _userDefaultSettingsPath = Path.Combine(
			Path.GetTempPath(), "UserDefaultSettings.xml");

		[SetUp]
		public void Prepare()
		{
			using (StreamWriter writer = File.CreateText(this._userDefaultSettingsPath))
			{
				writer.Write(Resources.UserDefaultSettings);
			}
		}

		[Test]
		public void DeserializeUserSettings()
		{
			UserSettings userSettings = XmlSerialization.Deserialize<UserSettings>(
				this._userDefaultSettingsPath
			);

			Assert.NotNull(userSettings);

			Assert.AreEqual("en", userSettings.UiLanguage);
			Assert.AreEqual("ru", userSettings.ReportLanguage);
			Assert.AreEqual(90, userSettings.Timeout);
			Assert.AreEqual(10, userSettings.MaxThreads);

			NotificationSettings notifSettings = userSettings.NotificationSettings;
			Assert.NotNull(notifSettings);

			Assert.AreEqual("recipient@test.com", notifSettings.RecipientEmail);
			Assert.NotNull(notifSettings.SmtpSettings);

			Assert.AreEqual("smtp.server.com", notifSettings.SmtpSettings.ServerAddress);
			Assert.AreEqual(25, notifSettings.SmtpSettings.Port);
			Assert.AreEqual(true, notifSettings.SmtpSettings.AuthenticationRequired);
			Assert.AreEqual(false, notifSettings.SmtpSettings.SslEnabled);
			Assert.AreEqual("sender@test.com", notifSettings.SmtpSettings.SenderEmail);

			Assert.NotNull(notifSettings.SmtpSettings.SmtpCredentials);
			Assert.AreEqual("admin", notifSettings.SmtpSettings.SmtpCredentials.UserName);
			Assert.AreEqual("1", notifSettings.SmtpSettings.SmtpCredentials.Password);
		}

		[TearDown]
		public void CleanUp()
		{
			FileUtils.DeleteIfExists(this._userDefaultSettingsPath);
		}
	}
}
