﻿using System.Security.Cryptography;
using System.Xml;
using MSSQLServerAuditor.Core.Cryptography.Xml;
using NUnit.Framework;

namespace MSSQLServerAuditor.Tests.Cryptography.Xml
{
	public class EncryptingAndDecryptingTests
	{
		private static XmlDocument LoadXmlFromString(string xml)
		{
			XmlDocument doc = new XmlDocument { PreserveWhitespace = true };
			doc.LoadXml(xml);
			return doc;
		}

		[Test]
		public void TestAsymmetricEncryptionRoundtrip()
		{
			const string xmlInnerText = "some text node";
			const string xmlRoot      = "example";
			const string xmlDocument  = @"<?xml version=""1.0""?>
<example>
<test>some text node</test>
</example>";

			string key = "rsaKey";

			using (RSA rsa = RSA.Create())
			{
				XmlEncryptor encryptor = new XmlEncryptor(rsa);

				XmlDocument xmlDocToEncrypt = LoadXmlFromString(xmlDocument);

				StringAssert.Contains(xmlInnerText, xmlDocToEncrypt.OuterXml);
				encryptor.Encrypt(xmlDocToEncrypt, xmlRoot, key);

				StringAssert.DoesNotContain(xmlInnerText, xmlDocToEncrypt.OuterXml);

				XmlDocument xmlDocToDecrypt = LoadXmlFromString(xmlDocToEncrypt.OuterXml);
				encryptor.Decrypt(xmlDocToDecrypt, key);

				Assert.AreEqual(xmlDocument.Replace("\r\n", "\n"), xmlDocToDecrypt.OuterXml.Replace("\r\n", "\n"));
			}
		}
	}
}
