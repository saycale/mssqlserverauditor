﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;

namespace MSSQLServerAuditor.Controls
{
	/// <summary>
	/// Extends ListBox by making the SelectedItems property read/write.
	/// ListBox does have a SelectedItems property but it is read-only. 
	/// MultiSelectListBox hides the base class's SelectedItems property.
	/// The SelectedItems property of MultiSelectListBox can participate in two way data binding 
	/// and Can be set in Xaml, whereas in the base class it can only participate in one way binding.
	/// SelectionMode should not be Single when using the SelectedItems property.
	/// By default, selection mode is Multiple.
	/// </summary>
	public class MultiselectListBox : ListBox
	{
		public MultiselectListBox()
		{
			// Add handler for when the listbox internal selection changes.
			base.SelectionChanged += UpdateNewClassSelectedItems;
		}

		#region Public Properties

		/// <summary>
		/// The SelectedItems dependency property. Access to the values of the items that are 
		/// selected in the selectedItems box. If SelectionMode is Single, this property returns an array
		/// of length one.
		/// </summary>
		public new static readonly DependencyProperty SelectedItemsProperty = DependencyProperty.Register(
			nameof(SelectedItems),
			typeof(IList),
			typeof(MultiselectListBox),
			new FrameworkPropertyMetadata((sender, args) =>
				{
					// When the property changes, update the selected values in the selectedItems box.
					MultiselectListBox listBox = sender as MultiselectListBox;
					listBox?.SetSelectedItemsNew(args.NewValue as IList);
				}
			)
		);

		/// <summary>
		/// Get or set the selected items.
		/// </summary>
		public new IList SelectedItems
		{
			get { return GetValue(SelectedItemsProperty) as IList; }
			set { SetValue(SelectedItemsProperty, value); }
		}

		#endregion

		#region Protected Methods

		/// <summary>
		/// Call this method to programmatically add an item to the selected items collection.
		/// Because this class hides ListBox.SelectedItems, it is necessary add the value directly
		/// to the base class's collection for all the related events to properly propagate.
		/// </summary>
		protected int SelectItem(object item)
		{
			return base.SelectedItems.Add(item);
		}

		/// <summary>
		/// Call this method to programmatically add an item to the selectedItems that is displayed in 
		/// the listbox. NOTE: For the selectedItems box display to update, ItemsSource must impliment INotifyCollectionChanged
		/// such as ObservableCollection.
		/// </summary>
		protected int AddItem(object item)
		{
			IList list = ItemsSource as IList;
			if (list != null)
			{
				return list.Add(item);
			}

			return base.Items.Add(item);
		}

		#endregion

		#region Private

		/// <summary>
		/// Synchronizes the selected items of this class with the selected items of the base class.
		/// </summary>
		public void UpdateNewClassSelectedItems(object sender, SelectionChangedEventArgs e)
		{
			// If null, then we aren't bound to.
			if (SelectedItems == null)
			{
				return;
			}

			foreach (object item in e.AddedItems)
			{
				SelectedItems.Add(item);
			}

			foreach (object item in e.RemovedItems)
			{
				SelectedItems.Remove(item);
			}
		}

		/// <summary>
		/// Synchronizes the selected items with the selected values. 
		/// </summary>
		private void SetSelectedItemsNew(IList newSelectedItems)
		{
			if (newSelectedItems == null)
			{
				throw new InvalidOperationException("Collection cannot be null");
			}

			// Remove the event handler to prevent recursion.
			base.SelectionChanged -= UpdateNewClassSelectedItems;

			base.SetSelectedItems(SelectedItems);

			// Reestablish the event handler.
			base.SelectionChanged += UpdateNewClassSelectedItems;

			// Add a collection changed handler to the new list, if it supports the interface.
			AddSelectedItemsChangedHandler(newSelectedItems as INotifyCollectionChanged);
		}

		private void AddSelectedItemsChangedHandler(INotifyCollectionChanged collection)
		{
			if (collection != null)
			{
				collection.CollectionChanged += SelectedItems_CollectionChanged;
			}
		}

		private void RemoveSelectedItemsChangedHandler(INotifyCollectionChanged collection)
		{
			if (collection != null)
			{
				collection.CollectionChanged -= SelectedItems_CollectionChanged;
			}
		}

		private void SelectedItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			base.SelectionChanged -= UpdateNewClassSelectedItems;
			base.SetSelectedItems(SelectedItems);
			base.SelectionChanged += UpdateNewClassSelectedItems;
		}

		#endregion
	}
}
