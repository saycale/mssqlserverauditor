﻿using System.Windows;
using System.Windows.Controls;

namespace MSSQLServerAuditor.Controls
{
	/// <summary>
	/// Interaction logic for TitleFrame.xaml
	/// </summary>
	public partial class TitleFrame : UserControl
	{
		public static readonly DependencyProperty HeaderProperty =
			DependencyProperty.Register(nameof(Header), typeof(string), typeof(TitleFrame), new UIPropertyMetadata(null));

		public static readonly DependencyProperty FooterProperty =
			DependencyProperty.Register(nameof(Footer), typeof(string), typeof(TitleFrame), new UIPropertyMetadata(null));

		public static readonly DependencyProperty HeaderAlignmentProperty =
			DependencyProperty.Register(nameof(HeaderAlignment), typeof(TextAlignment), typeof(TitleFrame), new UIPropertyMetadata(TextAlignment.Left));

		public static readonly DependencyProperty FooterAlignmentProperty =
			DependencyProperty.Register(nameof(FooterAlignment), typeof(TextAlignment), typeof(TitleFrame), new UIPropertyMetadata(TextAlignment.Left));

		public static readonly DependencyProperty HeaderVisibilityProperty =
			DependencyProperty.Register(nameof(HeaderVisibility), typeof(Visibility), typeof(TitleFrame), new UIPropertyMetadata(Visibility.Visible));

		public static readonly DependencyProperty FooterVisibilityProperty =
			DependencyProperty.Register(nameof(FooterVisibility), typeof(Visibility), typeof(TitleFrame), new UIPropertyMetadata(Visibility.Collapsed));

		public static readonly DependencyProperty ContentControlProperty =
			DependencyProperty.Register(nameof(ContentControl), typeof(FrameworkElement), typeof(TitleFrame), new UIPropertyMetadata(null));

		public TitleFrame()
		{
			InitializeComponent();
		}

		public string Header
		{
			get { return (string) GetValue(HeaderProperty); }
			set { SetValue(HeaderProperty, value); }
		}

		public Visibility HeaderVisibility
		{
			get { return (Visibility) GetValue(HeaderVisibilityProperty); }
			set { SetValue(HeaderVisibilityProperty, value); }
		}

		public string Footer
		{
			get { return (string) GetValue(FooterProperty); }
			set { SetValue(FooterProperty, value); }
		}

		public Visibility FooterVisibility
		{
			get { return (Visibility) GetValue(FooterVisibilityProperty); }
			set { SetValue(FooterVisibilityProperty, value); }
		}

		public TextAlignment HeaderAlignment
		{
			get { return (TextAlignment) GetValue(HeaderAlignmentProperty); }
			set { SetValue(HeaderAlignmentProperty, value); }
		}

		public TextAlignment FooterAlignment
		{
			get { return (TextAlignment) GetValue(FooterAlignmentProperty); }
			set { SetValue(FooterAlignmentProperty, value); }
		}

		public FrameworkElement ContentControl
		{
			get { return (FrameworkElement) GetValue(ContentControlProperty); }
			set { SetValue(ContentControlProperty, value); }
		}
	}
}
