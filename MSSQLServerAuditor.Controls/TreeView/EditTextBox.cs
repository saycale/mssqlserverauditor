﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MSSQLServerAuditor.Controls.TreeView
{
	public class EditTextBox : TextBox
	{
		private bool _commiting;

		static EditTextBox()
		{
			DefaultStyleKeyProperty.OverrideMetadata(
				typeof(EditTextBox),
				new FrameworkPropertyMetadata(typeof(EditTextBox))
			);
		}

		public EditTextBox()
		{
			Loaded += delegate { Init(); };
		}

		public AdvTreeViewItem Item { get; set; }

		public AdvTreeNode Node => Item.Node;

		private void Init()
		{
			Text = Node.LoadEditText();
			Focus();
			SelectAll();
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			switch (e.Key)
			{
				case Key.Enter:
					Commit();
					break;
				case Key.Escape:
					Node.IsEditing = false;
					break;
			}
		}

		protected override void OnLostKeyboardFocus(KeyboardFocusChangedEventArgs e)
		{
			if (Node.IsEditing)
			{
				Commit();
			}
		}

		private void Commit()
		{
			if (this._commiting)
			{
				return;
			}

			this._commiting = true;
			Node.IsEditing = false;

			if (!Node.SaveEditText(Text))
			{
				Item.Focus();
			}

			Node.NotifyOfPropertyChange(() => Text);
			this._commiting = false;
		}
	}
}
