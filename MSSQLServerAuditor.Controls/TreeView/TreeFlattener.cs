﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;

namespace MSSQLServerAuditor.Controls.TreeView
{
	public sealed class TreeFlattener : IList, INotifyCollectionChanged
	{
		/// <summary>
		/// The root node of the flat list tree.
		/// Tjis is not necessarily the root of the model!
		/// </summary>
		internal AdvTreeNode Root;

		private readonly bool _includeRoot;

		public TreeFlattener(AdvTreeNode modelRoot, bool includeRoot)
		{
			this.Root = modelRoot;
			while (Root.ListParent != null)
			{
				Root = Root.ListParent;
			}

			Root.TreeFlattener = this;
			this._includeRoot = includeRoot;
		}

		public event NotifyCollectionChangedEventHandler CollectionChanged;
		
		public void RaiseCollectionChanged(NotifyCollectionChangedEventArgs e)
		{
			CollectionChanged?.Invoke(this, e);
		}

		public void NodesInserted(int index, IEnumerable<AdvTreeNode> nodes)
		{
			if (!this._includeRoot)
			{
				index--;
			}
			foreach (AdvTreeNode node in nodes)
			{
				RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, node, index++));
			}
		}
		
		public void NodesRemoved(int index, IEnumerable<AdvTreeNode> nodes)
		{
			if (!this._includeRoot)
			{
				index--;
			}
			foreach (AdvTreeNode node in nodes)
			{
				RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, node, index));
			}
		}
		
		public void Stop()
		{
			Debug.Assert(Root.TreeFlattener == this);
			Root.TreeFlattener = null;
		}
		
		public object this[int index]
		{
			get
			{
				if (index < 0 || index >= this.Count)
				{
					throw new ArgumentOutOfRangeException();
				}

				return AdvTreeNode.GetNodeByVisibleIndex(Root, this._includeRoot ? index : index + 1);
			}
			set
			{
				throw new NotSupportedException();
			}
		}
		
		public int Count => this._includeRoot
			? Root.GetTotalListLength()
			: Root.GetTotalListLength() - 1;

		public int IndexOf(object item)
		{
			AdvTreeNode node = item as AdvTreeNode;
			if (node != null && node.IsVisible && node.GetListRoot() == Root)
			{
				if (this._includeRoot)
				{
					return AdvTreeNode.GetVisibleIndexForNode(node);
				}
				return AdvTreeNode.GetVisibleIndexForNode(node) - 1;
			}

			return -1;
		}
		
		bool IList.IsReadOnly => true;

		bool IList.IsFixedSize => false;

		bool ICollection.IsSynchronized => false;

		object ICollection.SyncRoot { get; } = new object();

		void IList.Insert(int index, object item)
		{
			throw new NotSupportedException();
		}
		
		void IList.RemoveAt(int index)
		{
			throw new NotSupportedException();
		}
		
		int IList.Add(object item)
		{
			throw new NotSupportedException();
		}
		
		void IList.Clear()
		{
			throw new NotSupportedException();
		}
		
		public bool Contains(object item)
		{
			return IndexOf(item) >= 0;
		}
		
		public void CopyTo(Array array, int arrayIndex)
		{
			foreach (object item in this)
			{
				array.SetValue(item, arrayIndex++);
			}
		}
		
		void IList.Remove(object item)
		{
			throw new NotSupportedException();
		}
		
		public IEnumerator GetEnumerator()
		{
			for (int i = 0; i < this.Count; i++)
			{
				yield return this[i];
			}
		}
	}
}
