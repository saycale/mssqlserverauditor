﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Threading;

namespace MSSQLServerAuditor.Controls.TreeView
{
	public class AdvTreeView : ListView
	{
		static AdvTreeView()
		{
			DefaultStyleKeyProperty.OverrideMetadata(
				typeof(AdvTreeView),
				new FrameworkPropertyMetadata(typeof(AdvTreeView))
			);

			SelectionModeProperty.OverrideMetadata(
				typeof(AdvTreeView),
				new FrameworkPropertyMetadata(SelectionMode.Extended)
			);

			AlternationCountProperty.OverrideMetadata(
				typeof(AdvTreeView), new FrameworkPropertyMetadata(2)
			);

			DefaultItemContainerStyleKey = new ComponentResourceKey(
				typeof(AdvTreeView),
				"DefaultItemContainerStyleKey"
			);

			VirtualizingStackPanel.VirtualizationModeProperty.OverrideMetadata(
				typeof(AdvTreeView),
				new FrameworkPropertyMetadata(VirtualizationMode.Recycling)
			);
		}

		public static ResourceKey DefaultItemContainerStyleKey { get; private set; }

		private TreeFlattener _flattener;
		private bool _doNotScrollOnExpanding;

		public AdvTreeView()
		{
			SetResourceReference(ItemContainerStyleProperty, DefaultItemContainerStyleKey);
		}

		public static readonly DependencyProperty RootProperty = DependencyProperty.Register(
			nameof(Root),
			typeof(AdvTreeNode),
			typeof(AdvTreeView)
		);

		public AdvTreeNode Root
		{
			get { return (AdvTreeNode)GetValue(RootProperty); }
			set { SetValue(RootProperty, value); }
		}

		public static readonly DependencyProperty ShowRootProperty = DependencyProperty.Register(
			nameof(ShowRoot),
			typeof(bool),
			typeof(AdvTreeView),
			new FrameworkPropertyMetadata(true)
		);

		public bool ShowRoot
		{
			get { return (bool)GetValue(ShowRootProperty); }
			set { SetValue(ShowRootProperty, value); }
		}

		public static readonly DependencyProperty ShowRootExpanderProperty = DependencyProperty.Register(
			nameof(ShowRootExpander),
			typeof(bool),
			typeof(AdvTreeView),
			new FrameworkPropertyMetadata(false)
		);

		public bool ShowRootExpander
		{
			get { return (bool) GetValue(ShowRootExpanderProperty); }
			set { SetValue(ShowRootExpanderProperty, value); }
		}

		public static readonly DependencyProperty ShowLinesProperty = DependencyProperty.Register(
			nameof(ShowLines),
			typeof(bool),
			typeof(AdvTreeView),
			new FrameworkPropertyMetadata(true)
		);

		public bool ShowLines
		{
			get { return (bool)GetValue(ShowLinesProperty); }
			set { SetValue(ShowLinesProperty, value); }
		}

		public static bool GetShowAlternation(DependencyObject obj)
		{
			return (bool)obj.GetValue(ShowAlternationProperty);
		}

		public static void SetShowAlternation(DependencyObject obj, bool value)
		{
			obj.SetValue(ShowAlternationProperty, value);
		}

		public static readonly DependencyProperty ShowAlternationProperty = DependencyProperty.RegisterAttached(
			"ShowAlternation",
			typeof(bool),
			typeof(AdvTreeView),
			new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.Inherits)
		);
		
		protected override async void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
		{
			base.OnPropertyChanged(e);
			if (e.Property == RootProperty ||
			    e.Property == ShowRootProperty ||
			    e.Property == ShowRootExpanderProperty) {
				await Reload();
			}
		}

		private async Task Reload()
		{
			this._flattener?.Stop();
			if (Root != null)
			{
				if (!(ShowRoot && ShowRootExpander))
				{
					await Root.Expand(CancellationToken.None);
				}

				this._flattener = new TreeFlattener(Root, ShowRoot);
				this._flattener.CollectionChanged += flattener_CollectionChanged;
				this.ItemsSource = this._flattener;
			}
		}

		private void flattener_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			// Deselect nodes that are being hidden, if any remain in the tree
			if (e.Action == NotifyCollectionChangedAction.Remove && Items.Count > 0)
			{
				List<AdvTreeNode> selectedOldItems = null;
				foreach (AdvTreeNode node in e.OldItems)
				{
					if (node.IsSelected)
					{
						if (selectedOldItems == null)
						{
							selectedOldItems = new List<AdvTreeNode>();
						}

						selectedOldItems.Add(node);
					}
				}
				if (selectedOldItems != null)
				{
					var list = SelectedItems.Cast<AdvTreeNode>().Except(selectedOldItems).ToList();
					SetSelectedItems(list);
					if (SelectedItem == null && this.IsKeyboardFocusWithin)
					{
						// if we removed all selected nodes, then move the focus to the node
						// preceding the first of the old selected nodes
						SelectedIndex = Math.Max(0, e.OldStartingIndex - 1);
						if (SelectedIndex >= 0)
						{
							FocusNode((AdvTreeNode) SelectedItem);
						}
					}
				}
			}
		}

		protected override DependencyObject GetContainerForItemOverride()
		{
			return new AdvTreeViewItem();
		}

		protected override bool IsItemItsOwnContainerOverride(object item)
		{
			return item is AdvTreeViewItem;
		}

		protected override void PrepareContainerForItemOverride(DependencyObject element, object item)
		{
			base.PrepareContainerForItemOverride(element, item);
			AdvTreeViewItem container = element as AdvTreeViewItem;
			if (container != null)
			{
				container.ParentTreeView = this;

				// Make sure that the line renderer takes into account the new bound data
				container.NodeView?.LinesRenderer.InvalidateVisual();
			}
		}

		/// <summary>
		/// Handles the node expanding event in the tree view.
		/// This method gets called only if the node is in the visible region (a AdvTreeNodeView exists).
		/// </summary>
		internal void HandleExpanding(AdvTreeNode node)
		{
			if (this._doNotScrollOnExpanding)
			{
				return;
			}

			AdvTreeNode lastVisibleChild = node;
			while (true)
			{
				AdvTreeNode tmp = lastVisibleChild.Children.LastOrDefault(c => c.IsVisible);
				if (tmp != null)
				{
					lastVisibleChild = tmp;
				}
				else
				{
					break;
				}
			}

			if (lastVisibleChild != node)
			{
				// Make the the expanded children are visible; but don't scroll down
				// to much (keep node itself visible)
				base.ScrollIntoView(lastVisibleChild);
				// For some reason, this only works properly when delaying it...
				Dispatcher.BeginInvoke(DispatcherPriority.Loaded, new Action(
					delegate {
						base.ScrollIntoView(node);
					}));
			}
		}
		
		protected override async void OnKeyDown(KeyEventArgs e)
		{
			AdvTreeViewItem container = e.OriginalSource as AdvTreeViewItem;
			switch (e.Key)
			{
				case Key.Left:
					if (container != null && ItemsControlFromItemContainer(container) == this)
					{
						if (container.Node.IsExpanded)
						{
							await container.Node.Collapse();
						}
						else if (container.Node.Parent != null)
						{
							this.FocusNode(container.Node.Parent);
						}
						e.Handled = true;
					}
					break;
				case Key.Right:
					if (container != null && ItemsControlFromItemContainer(container) == this)
					{
						if (!container.Node.IsExpanded && container.Node.ShowExpander)
						{
							await container.Node.Expand();
						}
						else if (container.Node.Children.Count > 0)
						{
							// jump to first child:
							container.MoveFocus(new TraversalRequest(FocusNavigationDirection.Down));
						}
						e.Handled = true;
					}
					break;
				case Key.Return:
					if (container != null && Keyboard.Modifiers == ModifierKeys.None && this.SelectedItems.Count == 1 && this.SelectedItem == container.Node)
					{
						e.Handled = true;
						container.Node.ActivateItem(e);
					}
					break;
				case Key.Space:
					if (container != null && Keyboard.Modifiers == ModifierKeys.None && this.SelectedItems.Count == 1 && this.SelectedItem == container.Node)
					{
						e.Handled = true;
						if (container.Node.IsCheckable)
						{
							if (container.Node.IsChecked == null) // If partially selected, we want to select everything
								container.Node.IsChecked = true;
							else
								container.Node.IsChecked = !container.Node.IsChecked;
						}
						else
						{
							container.Node.ActivateItem(e);
						}
					}
					break;
				case Key.Add:
					if (container != null && ItemsControl.ItemsControlFromItemContainer(container) == this)
					{
						await container.Node.Expand();
						e.Handled = true;
					}
					break;
				case Key.Subtract:
					if (container != null && ItemsControl.ItemsControlFromItemContainer(container) == this)
					{
						await container.Node.Collapse();
						e.Handled = true;
					}
					break;
				case Key.Multiply:
					if (container != null && ItemsControl.ItemsControlFromItemContainer(container) == this)
					{
						await container.Node.Expand();
						await ExpandRecursively(container.Node);
						e.Handled = true;
					}
					break;
			}

			if (!e.Handled)
			{
				base.OnKeyDown(e);
			}
		}
		
		private async Task ExpandRecursively(AdvTreeNode node)
		{
			if (node.CanExpandRecursively)
			{
				await node.Expand();
				foreach (AdvTreeNode child in node.Children)
				{
					await ExpandRecursively(child);
				}
			}
		}
		
		/// <summary>
		/// Scrolls the specified node in view and sets keyboard focus on it.
		/// </summary>
		public void FocusNode(AdvTreeNode node)
		{
			if (node == null)
			{
				throw new ArgumentNullException(nameof(node));
			}

			ScrollIntoView(node);
			// WPF's ScrollIntoView() uses the same if/dispatcher construct, so we call OnFocusItem() after the item was brought into view.
			if (this.ItemContainerGenerator.Status == GeneratorStatus.ContainersGenerated)
			{
				OnFocusItem(node);
			}
			else
			{
				this.Dispatcher.BeginInvoke(
					DispatcherPriority.Loaded,
					new DispatcherOperationCallback(this.OnFocusItem),
					node
				);
			}
		}
		
		public async void ScrollIntoView(AdvTreeNode node)
		{
			if (node == null)
			{
				throw new ArgumentNullException(nameof(node));
			}

			this._doNotScrollOnExpanding = true;

			foreach (AdvTreeNode ancestor in node.Ancestors())
			{
				await ancestor.Expand();
			}

			this._doNotScrollOnExpanding = false;
			base.ScrollIntoView(node);
		}
		
		private object OnFocusItem(object item)
		{
			FrameworkElement element = this.ItemContainerGenerator.ContainerFromItem(item) as FrameworkElement;
			element?.Focus();
			return null;
		}

		protected override System.Windows.Automation.Peers.AutomationPeer OnCreateAutomationPeer()
		{
			return new AdvTreeViewAutomationPeer(this);
		}
		#region Track selection

		protected override void OnSelectionChanged(SelectionChangedEventArgs e)
		{
			foreach (AdvTreeNode node in e.RemovedItems)
			{
				node.IsSelected = false;
			}

			foreach (AdvTreeNode node in e.AddedItems)
			{
				node.IsSelected = true;
			}

			base.OnSelectionChanged(e);
		}
		
		#endregion

		public void SetSelectedNodes(IEnumerable<AdvTreeNode> nodes)
		{
			this.SetSelectedItems(nodes.ToList());
		}
	}
}
