﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace MSSQLServerAuditor.Controls.TreeView
{
	// This part of TreeNode controls the 'flat list' data structure, which emulates
	// a big flat list containing the whole tree; allowing access by visible index.
	partial class AdvTreeNode
	{
		/// <summary>The parent in the flat list</summary>
		private AdvTreeNode _listParent;

		/// <summary>Left/right nodes in the flat list</summary>
		private AdvTreeNode _left;
		private AdvTreeNode _right;

		internal TreeFlattener TreeFlattener;
		
		/// <summary>Subtree height in the flat list tree</summary>
		private byte _height = 1;

		/// <summary>Length in the flat list, including children (children within the flat list). -1 = invalidated</summary>
		private int _totalListLength = -1;
		
		public int Balance => Height(_right) - Height(_left);

		static int Height(AdvTreeNode node)
		{
			return node?._height ?? 0;
		}
		
		internal AdvTreeNode GetListRoot()
		{
			AdvTreeNode node = this;
			while (node._listParent != null)
			{
				node = node._listParent;
			}
			return node;
		}
		
		#region Debugging
		[Conditional("DEBUG")]
		void CheckRootInvariants()
		{
			GetListRoot().CheckInvariants();
		}
		
		[Conditional("DATACONSISTENCYCHECK")]
		void CheckInvariants()
		{
			Debug.Assert(_left == null || _left._listParent == this);
			Debug.Assert(_right == null || _right._listParent == this);
			Debug.Assert(_height == 1 + Math.Max(Height(_left), Height(_right)));
			Debug.Assert(Math.Abs(this.Balance) <= 1);
			Debug.Assert(_totalListLength == -1 || _totalListLength == (_left != null ? _left._totalListLength : 0) + (_isVisible ? 1 : 0) + (_right != null ? _right._totalListLength : 0));
			if (_left != null) _left.CheckInvariants();
			if (_right != null) _right.CheckInvariants();
		}
		
		[Conditional("DEBUG")]
		static void DumpTree(AdvTreeNode node)
		{
			node.GetListRoot().DumpTree();
		}
		
		[Conditional("DEBUG")]
		void DumpTree()
		{
			Debug.Indent();
			if (_left != null)
				_left.DumpTree();
			Debug.Unindent();
			Debug.WriteLine("{0}, totalListLength={1}, height={2}, Balance={3}, isVisible={4}", ToString(), _totalListLength, _height, Balance, _isVisible);
			Debug.Indent();
			if (_right != null)
				_right.DumpTree();
			Debug.Unindent();
		}
		#endregion

		public AdvTreeNode ListParent => this._listParent;

		#region GetNodeByVisibleIndex / GetVisibleIndexForNode
		internal static AdvTreeNode GetNodeByVisibleIndex(AdvTreeNode root, int index)
		{
			root.GetTotalListLength(); // ensure all list lengths are calculated

			Debug.Assert(index >= 0);
			Debug.Assert(index < root._totalListLength);

			AdvTreeNode node = root;
			while (true)
			{
				if (node._left != null && index < node._left._totalListLength)
				{
					node = node._left;
				}
				else
				{
					if (node._left != null)
					{
						index -= node._left._totalListLength;
					}
					if (node._isVisible)
					{
						if (index == 0)
						{
							return node;
						}

						index--;
					}

					node = node._right;
				}
			}
		}
		
		internal static int GetVisibleIndexForNode(AdvTreeNode node)
		{
			int index = node._left?.GetTotalListLength() ?? 0;
			while (node._listParent != null)
			{
				if (node == node._listParent._right)
				{
					if (node._listParent._left != null)
					{
						index += node._listParent._left.GetTotalListLength();
					}

					if (node._listParent._isVisible)
					{
						index++;
					}
				}

				node = node._listParent;
			}
			return index;
		}
		#endregion
		
		#region Balancing
		/// <summary>
		/// Balances the subtree rooted in <paramref name="node"/> and recomputes the 'height' field.
		/// This method assumes that the children of this node are already balanced and have an up-to-date 'height' value.
		/// </summary>
		/// <returns>The new root node</returns>
		static AdvTreeNode Rebalance(AdvTreeNode node)
		{
			Debug.Assert(node._left == null || Math.Abs(node._left.Balance) <= 1);
			Debug.Assert(node._right == null || Math.Abs(node._right.Balance) <= 1);
			// Keep looping until it's balanced. Not sure if this is stricly required; this is based on
			// the Rope code where node merging made this necessary.
			while (Math.Abs(node.Balance) > 1)
			{
				// AVL balancing
				// note: because we don't care about the identity of concat nodes, this works a little different than usual
				// tree rotations: in our implementation, the "this" node will stay at the top, only its children are rearranged
				if (node.Balance > 1)
				{
					if (node._right != null && node._right.Balance < 0)
					{
						node._right = node._right.RotateRight();
					}
					node = node.RotateLeft();
					// If 'node' was unbalanced by more than 2, we've shifted some of the inbalance to the left node; so rebalance that.
					node._left = Rebalance(node._left);
				}
				else if (node.Balance < -1)
				{
					if (node._left != null && node._left.Balance > 0)
					{
						node._left = node._left.RotateLeft();
					}
					node = node.RotateRight();
					// If 'node' was unbalanced by more than 2, we've shifted some of the inbalance to the right node; so rebalance that.
					node._right = Rebalance(node._right);
				}
			}

			Debug.Assert(Math.Abs(node.Balance) <= 1);

			node._height = (byte)(1 + Math.Max(Height(node._left), Height(node._right)));
			node._totalListLength = -1; // mark for recalculation
			// since balancing checks the whole tree up to the root, the whole path will get marked as invalid
			return node;
		}
		
		internal int GetTotalListLength()
		{
			if (this._totalListLength >= 0)
			{
				return this._totalListLength;
			}

			int length = (this._isVisible ? 1 : 0);

			if (this._left != null)
			{
				length += this._left.GetTotalListLength();
			}

			if (this._right != null)
			{
				length += this._right.GetTotalListLength();
			}

			return this._totalListLength = length;
		}

		private AdvTreeNode RotateLeft()
		{
			/* Rotate tree to the left
			 * 
			 *       this               right
			 *       /  \               /  \
			 *      A   right   ===>  this  C
			 *           / \          / \
			 *          B   C        A   B
			 */
			AdvTreeNode b = this._right._left;
			AdvTreeNode newTop = this._right;

			if (b != null)
			{
				b._listParent = this;
			}

			this._right        = b;
			newTop._left       = this;
			newTop._listParent = this._listParent;
			this._listParent   = newTop;

			// rebalance the 'this' node - this is necessary in some bulk insertion cases:
			newTop._left = Rebalance(this);

			return newTop;
		}
		
		private AdvTreeNode RotateRight()
		{
			/* Rotate tree to the right
			 * 
			 *       this             left
			 *       /  \             /  \
			 *     left  C   ===>    A   this
			 *     / \                   /  \
			 *    A   B                 B    C
			 */
			AdvTreeNode b = _left._right;
			AdvTreeNode newTop = _left;

			if (b != null)
			{
				b._listParent = this;
			}

			this._left         = b;
			newTop._right      = this;
			newTop._listParent = this._listParent;
			this._listParent   = newTop;

			newTop._right = Rebalance(this);

			return newTop;
		}
		
		static void RebalanceUntilRoot(AdvTreeNode pos)
		{
			while (pos._listParent != null)
			{
				if (pos == pos._listParent._left)
				{
					pos = pos._listParent._left = Rebalance(pos);
				}
				else
				{
					Debug.Assert(pos == pos._listParent._right);
					pos = pos._listParent._right = Rebalance(pos);
				}

				pos = pos._listParent;
			}

			AdvTreeNode newRoot = Rebalance(pos);

			if (newRoot != pos && pos.TreeFlattener != null)
			{
				Debug.Assert(newRoot.TreeFlattener == null);

				newRoot.TreeFlattener       = pos.TreeFlattener;
				pos.TreeFlattener          = null;
				newRoot.TreeFlattener.Root = newRoot;
			}

			Debug.Assert(newRoot._listParent == null);
			newRoot.CheckInvariants();
		}
		#endregion
		
		#region Insertion
		static void InsertNodeAfter(AdvTreeNode pos, AdvTreeNode newNode)
		{
			// newNode might be the model root of a whole subtree, so go to the list root of that subtree:
			newNode = newNode.GetListRoot();
			if (pos._right == null)
			{
				pos._right          = newNode;
				newNode._listParent = pos;
			}
			else
			{
				// insert before pos.right's leftmost:
				pos = pos._right;
				while (pos._left != null)
				{
					pos = pos._left;
				}

				Debug.Assert(pos._left == null);

				pos._left           = newNode;
				newNode._listParent = pos;
			}
			RebalanceUntilRoot(pos);
		}
		#endregion
		
		#region Removal
		private void RemoveNodes(AdvTreeNode start, AdvTreeNode end)
		{
			// Removes all nodes from start to end (inclusive)
			// All removed nodes will be reorganized in a separate tree, do not delete
			// regions that don't belong together in the tree model!
			
			List<AdvTreeNode> removedSubtrees = new List<AdvTreeNode>();
			AdvTreeNode oldPos;
			AdvTreeNode pos = start;
			do
			{
				// recalculate the endAncestors every time, because the tree might have been rebalanced
				HashSet<AdvTreeNode> endAncestors = new HashSet<AdvTreeNode>();
				for (AdvTreeNode tmp = end; tmp != null; tmp = tmp._listParent)
				{ 
					endAncestors.Add(tmp);
				}
				
				removedSubtrees.Add(pos);
				if (!endAncestors.Contains(pos))
				{
					// we can remove pos' right subtree in a single step:
					if (pos._right != null)
					{
						removedSubtrees.Add(pos._right);

						pos._right._listParent = null;
						pos._right             = null;
					}
				}
				AdvTreeNode succ = pos.Successor();
				DeleteNode(pos); // this will also rebalance out the deletion of the right subtree
				
				oldPos = pos;
				pos    = succ;
			} while (oldPos != end);
			
			// merge back together the removed subtrees:
			AdvTreeNode removed = removedSubtrees[0];
			for (int i = 1; i < removedSubtrees.Count; i++)
			{
				removed = ConcatTrees(removed, removedSubtrees[i]);
			}
		}
		
		static AdvTreeNode ConcatTrees(AdvTreeNode first, AdvTreeNode second)
		{
			AdvTreeNode tmp = first;
			while (tmp._right != null)
			{
				tmp = tmp._right;
			}

			InsertNodeAfter(tmp, second);
			return tmp.GetListRoot();
		}
		
		AdvTreeNode Successor()
		{
			if (this._right != null)
			{
				AdvTreeNode node = _right;
				while (node._left != null)
				{
					node = node._left;
				}

				return node;
			}
			else
			{
				AdvTreeNode node = this;
				AdvTreeNode oldNode;

				do
				{
					oldNode = node;
					node    = node._listParent;
					// loop while we are on the way up from the right part
				} while (node != null && node._right == oldNode);
				return node;
			}
		}
		
		private static void DeleteNode(AdvTreeNode node)
		{
			AdvTreeNode balancingNode;
			if (node._left == null)
			{
				balancingNode = node._listParent;
				node.ReplaceWith(node._right);
				node._right = null;
			}
			else if (node._right == null)
			{
				balancingNode = node._listParent;
				node.ReplaceWith(node._left);
				node._left = null;
			}
			else
			{
				AdvTreeNode tmp = node._right;
				while (tmp._left != null)
				{
					tmp = tmp._left;
				}

				// First replace tmp with tmp.right
				balancingNode = tmp._listParent;
				tmp.ReplaceWith(tmp._right);

				tmp._right = null;

				Debug.Assert(tmp._left == null);
				Debug.Assert(tmp._listParent == null);

				// Now move node's children to tmp:
				tmp._left = node._left; node._left = null;
				tmp._right = node._right; node._right = null;

				if (tmp._left != null) tmp._left._listParent = tmp;
				if (tmp._right != null) tmp._right._listParent = tmp;

				// Then replace node with tmp
				node.ReplaceWith(tmp);
				if (balancingNode == node)
				{
					balancingNode = tmp;
				}
			}

			Debug.Assert(node._listParent == null);
			Debug.Assert(node._left == null);
			Debug.Assert(node._right == null);

			node._height = 1;
			node._totalListLength = -1;
			if (balancingNode != null)
			{
				RebalanceUntilRoot(balancingNode);
			}
		}
		
		private void ReplaceWith(AdvTreeNode node)
		{
			if (this._listParent != null)
			{
				if (this._listParent._left == this)
				{
					this._listParent._left = node;
				}
				else
				{
					Debug.Assert(this._listParent._right == this);
					this._listParent._right = node;
				}

				if (node != null)
				{
					node._listParent = this._listParent;
				}

				this._listParent = null;
			}
			else
			{
				// this was a root node
				Debug.Assert(node != null); // cannot delete the only node in the tree
				node._listParent = null;

				if (TreeFlattener != null)
				{
					Debug.Assert(node.TreeFlattener == null);
					node.TreeFlattener = this.TreeFlattener;
					this.TreeFlattener = null;
					node.TreeFlattener.Root = node;
				}
			}
		}
		#endregion
	}
}
