﻿using System.Windows;
using System.Windows.Controls;

namespace MSSQLServerAuditor.Controls.TreeView
{
	public class InsertMarker : Control
	{
		static InsertMarker()
		{
			DefaultStyleKeyProperty.OverrideMetadata(
				typeof(InsertMarker),
				new FrameworkPropertyMetadata(typeof(InsertMarker))
			);
		}
	}
}
