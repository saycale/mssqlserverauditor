﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;

namespace MSSQLServerAuditor.Controls.TreeView
{
	public class AdvTreeViewItemAutomationPeer  : FrameworkElementAutomationPeer, IExpandCollapseProvider
	{
		internal AdvTreeViewItemAutomationPeer(AdvTreeViewItem owner)
			: base(owner)
		{
			AdvTreeViewItem.DataContextChanged += OnDataContextChanged;
			AdvTreeNode node = AdvTreeViewItem.DataContext as AdvTreeNode;

			if (node == null)
			{
				return;
			}
			
			node.PropertyChanged += OnPropertyChanged;
		}

		private AdvTreeViewItem AdvTreeViewItem => (AdvTreeViewItem) base.Owner;

		protected override AutomationControlType GetAutomationControlTypeCore()
		{
			return AutomationControlType.TreeItem;
		}

		public override object GetPattern(PatternInterface patternInterface)
		{
			if (patternInterface == PatternInterface.ExpandCollapse)
			{
				return this;
			}
			return base.GetPattern(patternInterface);
		}
		
		public void Collapse()
		{
		}

		public void Expand()
		{
		}

		public ExpandCollapseState ExpandCollapseState
		{
			get
			{
				AdvTreeNode node = AdvTreeViewItem.DataContext as AdvTreeNode;
				if (node == null || !node.ShowExpander)
				{
					return ExpandCollapseState.LeafNode;
				}
				return node.IsExpanded 
					? ExpandCollapseState.Expanded
					: ExpandCollapseState.Collapsed;
			}
		}
		
		private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName != nameof(AdvTreeNode.IsExpanded))
			{
				return;
			}

			AdvTreeNode node = sender as AdvTreeNode;
			if (node == null || node.Children.Count == 0)
			{
				return;
			}

			bool newValue = node.IsExpanded;
			bool oldValue = !newValue;

			RaisePropertyChangedEvent(
				ExpandCollapsePatternIdentifiers.ExpandCollapseStateProperty,
				oldValue ? ExpandCollapseState.Expanded : ExpandCollapseState.Collapsed,
				newValue ? ExpandCollapseState.Expanded : ExpandCollapseState.Collapsed);
		}
		
		private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			AdvTreeNode oldNode = e.OldValue as AdvTreeNode;
			if (oldNode != null)
			{
				oldNode.PropertyChanged -= OnPropertyChanged;
			}

			AdvTreeNode newNode = e.NewValue as AdvTreeNode;
			if (newNode != null)
			{
				newNode.PropertyChanged += OnPropertyChanged;
			}
		}
	}
}
