﻿using System.Windows;
using System.Windows.Media;

namespace MSSQLServerAuditor.Controls.TreeView
{
	public class LinesRenderer : FrameworkElement
	{
		static LinesRenderer()
		{
			Pen = new Pen(Brushes.LightGray, 1);
			Pen.Freeze();
		}

		private static readonly Pen Pen;

		AdvTreeNodeView NodeView => TemplatedParent as AdvTreeNodeView;

		protected override void OnRender(DrawingContext dc)
		{
			double indent = NodeView.CalculateIndent();
			Point p = new Point(indent + 4.5, 0);

			if (!NodeView.Node.IsRoot || NodeView.ParentTreeView.ShowRootExpander) {
				dc.DrawLine(Pen, new Point(p.X, ActualHeight / 2), new Point(p.X + 10, ActualHeight / 2));
			}

			if (NodeView.Node.IsRoot) return;

			dc.DrawLine(
				Pen,
				p,
				NodeView.Node.IsLast ? new Point(p.X, ActualHeight / 2) : new Point(p.X, ActualHeight)
			);

			AdvTreeNode current = NodeView.Node;
			while (true)
			{
				p.X -= 19;
				current = current.Parent;
				if (p.X < 0)
				{
					break;
				}

				if (!current.IsLast)
				{
					dc.DrawLine(Pen, p, new Point(p.X, ActualHeight));
				}
			}
		}
	}
}
